package info.guardianproject.keanu.core

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.RemoteException
import android.text.TextUtils
import android.view.WindowManager
import androidx.lifecycle.LifecycleObserver
import androidx.multidex.MultiDexApplication
import androidx.preference.PreferenceManager
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.google.GoogleEmojiProvider
import info.guardianproject.keanu.core.cacheword.ICacheWordSubscriber
import info.guardianproject.keanu.core.cacheword.PRNGFixes
import info.guardianproject.keanu.core.service.Broadcaster
import info.guardianproject.keanu.core.service.RemoteImService
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanu.core.ui.room.KeanuRoomDisplayNameFallbackProvider
import info.guardianproject.keanu.core.util.Debug
import info.guardianproject.keanu.core.util.Languages
import info.guardianproject.keanu.core.verification.VerificationManager
import info.guardianproject.keanuapp.BuildConfig
import info.guardianproject.keanuapp.R
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.cleaninsights.sdk.CleanInsights
import org.matrix.android.sdk.api.Matrix
import org.matrix.android.sdk.api.MatrixConfiguration
import org.matrix.android.sdk.api.session.Session
import timber.log.Timber


open class ImApp : MultiDexApplication(), ICacheWordSubscriber, Session.Listener,
    LifecycleObserver {

    companion object {

        var sImApp: ImApp? = null

        const val EVENT_CONNECTION_DISCONNECTED = 203

        const val backgroundSyncLongPollingTimeout: Long = 120
        const val backgroundSyncRepeatDelay: Long = 60

        @JvmStatic
        fun resetLanguage(activity: Activity?, language: String?) {
            if (!TextUtils.equals(language, Preferences.getLanguage())) {
                // Set the preference after setting the locale in case something goes
                // wrong. If setting the locale causes an Exception, it should not be set in
                // the preferences, otherwise this will be stuck in a crash loop.
                Languages.setLanguage(activity, language, true)
                Preferences.setLanguage(language)
                Languages.forceChangeLanguage(activity)
            }
        }
    }

    var router: Router = Router()

    private var mBroadcaster: Broadcaster? = null

    lateinit var cleanInsights: CleanInsights
        private set

    private val mPrefs: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(this)

    val preferenceProvider: PreferenceProvider
        get() = PreferenceProvider(this)


    fun isRoomInForeground(roomId: String): Boolean {
        return roomId == currentForegroundRoom
    }

    open var currentForegroundRoom = ""
    var appInBackground = false

    lateinit var matrix: Matrix
        private set

    var matrixSession: Session? = null
        set(value) {
            field = value

            if (value != null) {
                startSession()
//                startService()
            }
        }


    private fun initChannel() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return

        val nm = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        var nc = NotificationChannel(
            KeanuConstants.NOTIFICATION_CHANNEL_ID_SERVICE,
            getString(R.string.app_name),
            NotificationManager.IMPORTANCE_MIN
        )
        nc.setShowBadge(false)

        nm.createNotificationChannel(nc)

        nc = NotificationChannel(
            KeanuConstants.NOTIFICATION_CHANNEL_ID_MESSAGE,
            getString(R.string.notifications),
            NotificationManager.IMPORTANCE_HIGH
        )
        nc.lightColor = -0x670000
        nc.enableLights(true)
        nc.enableVibration(preferenceProvider.isNotificationVibrate())
        nc.setShowBadge(true)
        nc.setSound(null, null)

        nm.createNotificationChannel(nc)
    }

    override fun onCreate() {
        super.onCreate()

        Preferences.setup(this)

        initChannel()
        setUpActivityListener()

        initMeasurement()

        Languages.setup(MainActivity::class.java, R.string.use_system_default)
        Languages.setLanguage(this, Preferences.getLanguage(), false)

        sImApp = this

        Debug.onAppStart()
        PRNGFixes.apply() //Google's fix for SecureRandom bug: http://android-developers.blogspot.com/2013/08/some-securerandom-thoughts.html
        EmojiManager.install(GoogleEmojiProvider())

        mBroadcaster = Broadcaster()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        val config = MatrixConfiguration(roomDisplayNameFallbackProvider = KeanuRoomDisplayNameFallbackProvider(this))
        matrix = Matrix(this,config)

        // This can actually crash with a AEADBadTagException.
        try {
            matrixSession = matrix.authenticationService().getLastAuthenticatedSession()

        } catch (ignored: Throwable) {
            Timber.d("error starting matrix session")
        }
    }

    fun reinitMatrix() {
        val config = MatrixConfiguration(
            roomDisplayNameFallbackProvider = KeanuRoomDisplayNameFallbackProvider(this)
        )

        matrix = Matrix(this,config)

        // This can actually crash with a AEADBadTagException.
        try {
            matrixSession = matrix.authenticationService().getLastAuthenticatedSession()
            startSession()
        } catch (ignored: Throwable) {
            Timber.d("error re-starting matrix session")
        }
    }

    private fun startSession() {
        if (matrixSession == null) return

        matrixSession?.open()

        matrixSession?.addListener(this)
        matrixSession?.syncService()?.requireBackgroundSync()

        matrixSession?.syncService()?.startSync(true)
        matrixSession?.syncService()?.startAutomaticBackgroundSync(backgroundSyncLongPollingTimeout, backgroundSyncRepeatDelay)

        VerificationManager.instance.register(this)

        matrixSession?.syncService()?.getSyncStateLive()?.let {
            Timber.d("Sync State: %s", it.value?.toString())
        }
    }

    open fun startService() {

        if (preferenceProvider.isNotificationEnable()) {
            val intent = router.remoteService(this)
            intent.putExtra("notify", true)
            startService(intent)
            /**
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent)
            } else {

            }*/
        }
    }

    private fun initMeasurement() {

        try {
            val inputStream = assets.open("cleanInsights.json")

            cleanInsights = CleanInsights(
                inputStream.reader().readText(),
                filesDir
            )
        } catch (ex: Exception) {
            // Couldn't find or load config.
            Timber.d("couldn't find cleanInsights config file")
        }
    }

    suspend fun signOut() {
        matrixSession?.syncService()?.stopSync()
        matrixSession?.syncService()?.stopAnyBackgroundSync()
        matrixSession?.removeListener(this)

        MainScope().launch {
            VerificationManager.instance.deregister()
        }

        matrixSession?.signOutService()?.signOut(true)

        matrixSession = null
    }

    fun setAppTheme(activity: Activity?) {
        if (mPrefs.getBoolean("themeDark", false)) {
            setTheme(R.style.AppThemeDark)
            activity?.setTheme(R.style.AppThemeDark)
        } else {
            setTheme(R.style.AppTheme)
            activity?.setTheme(R.style.AppTheme)
        }

        resources.updateConfiguration(resources.configuration, resources.displayMetrics)
    }

    fun startImService() {
        Timber.d("Start ImService")
        startService(router.remoteService(this, true))
    }

    fun forceStopImService() {
        Timber.d("stop ImService")

        try {
            RemoteImService.instance?.stopSelf()
        } catch (ignored: RemoteException) {
        }

        stopService(router.remoteService(this, checkAutoLogin = true))
    }

    fun registerForBroadcastEvent(what: Int, target: Handler?) {
        mBroadcaster?.request(what, target, what)
    }

    fun unregisterForBroadcastEvent(what: Int, target: Handler?) {
        mBroadcaster?.cancelRequest(what, target, what)
    }

    fun cancelNotification(id: String) {
        RemoteImService.instance?.cancelNotification(id)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        Languages.setLanguage(this, Preferences.getLanguage(), true)
    }

    override fun onCacheWordUninitialized() {
        // unused
    }

    override fun onCacheWordLocked() {
        // unused
    }

    override fun onCacheWordOpened() {
        // TODO?
    }

    private fun setUpActivityListener() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, p1: Bundle?) {

                val preferenceProvider = PreferenceProvider(applicationContext)
                val enableScreenshot = preferenceProvider.getScreenshotInfo()
                if (enableScreenshot) {
                    activity.window.setFlags(
                        WindowManager.LayoutParams.FLAG_SECURE,
                        WindowManager.LayoutParams.FLAG_SECURE
                    )
                }

            }

            override fun onActivityStarted(p0: Activity) {

            }

            override fun onActivityResumed(p0: Activity) {
                appInBackground = false
            }

            override fun onActivityPaused(p0: Activity) {

            }

            override fun onActivityStopped(p0: Activity) {
                appInBackground = true
            }

            override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {

            }

            override fun onActivityDestroyed(p0: Activity) {

            }

        })
    }
}