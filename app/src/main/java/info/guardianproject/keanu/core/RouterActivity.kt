@file:Suppress("DEPRECATION")

package info.guardianproject.keanu.core

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Message
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import info.guardianproject.keanu.core.ui.chatlist.ChooseRoomActivity
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager
import info.guardianproject.keanu.core.util.extensions.copy
import info.guardianproject.keanu.core.util.extensions.uris
import info.guardianproject.keanuapp.ui.legacy.SimpleAlertHandler
import info.guardianproject.panic.Panic
import info.guardianproject.panic.PanicResponder
import timber.log.Timber

class RouterActivity : AppCompatActivity() {

    companion object {
        const val ACTION_LOCK_APP = "actionLockApp"
    }

    private val mApp: ImApp?
        get() = application as? ImApp

    private val mHandler by lazy {
        MyHandler(this)
    }

    private val mDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (ACTION_LOCK_APP == intent.action) {
            shutdownAndLock()
            return
        } else if (Panic.isTriggerIntent(intent)) {
            if (PanicResponder.receivedTriggerFromConnectedApp(this)) {
                when {
                    Preferences.uninstallApp() -> {
                        // Lock and delete first for rapid response, then uninstall.
                        shutdownAndLock()

                        PanicResponder.deleteAllAppData(this)

                        val uninstall = Intent(Intent.ACTION_DELETE)
                        uninstall.data = Uri.parse("package:$packageName")
                        startActivity(uninstall)
                    }
                    Preferences.clearAppData() -> {
                        // Lock first for rapid response, then delete.
                        shutdownAndLock()
                        PanicResponder.deleteAllAppData(this)
                    }
                    Preferences.lockApp() -> shutdownAndLock()
                }

                // TODO Add other responses here, paying attention to if/else order.
            } else if (PanicResponder.shouldUseDefaultResponseToTrigger(this)) {
                if (Preferences.lockApp()) {
                    shutdownAndLock()
                }
            }

            // This Intent should not trigger any more processing.
            finish()
            return
        }

        // If we have an incoming contact, send it to the right place.
        if (intent.scheme == "keanu") {

            mApp?.router?.addFriendClass?.let { intent.setClass(this, it) }
            startActivity(intent)

            finish()
            return
        }
    }

    override fun onPause() {
        mHandler.unregisterForBroadcastEvents()

        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()

        mDialog?.dismiss()
    }

    override fun onResume() {
        super.onResume()

        mHandler.registerForBroadcastEvents()

        if (intent.action == Intent.ACTION_SENDTO || intent.action == Intent.ACTION_VIEW) {
            if (intent.data?.scheme == "https") {
                // Special keanu.im invite link: https://keanu.im/invite/<base64 encoded username?k=otrFingerprint
                try {
                    // Parse each string and if they are for a new user then add the user.
                    val diLink = OnboardingManager.decodeInviteLink(intent.data.toString())

                    if (diLink?.username?.startsWith("@") == true) {
                        startActivity(mApp?.router?.addFriend(this, username = diLink.username))

                        finish()
                        return
                    } else if (diLink?.username?.startsWith("!") == true) {
                        startActivity(mApp?.router?.main(this, joinRoomId = diLink.username))

                        finish()
                        return
                    }
                } catch (e: Exception) {
                    Timber.w(e, "error parsing QR invite link")
                }
            }

            // No idea what to do with this intent. Start up normally, instead.
            intent = null
        }

        if (intent.action == Intent.ACTION_SEND) {
            val next = mApp?.router?.chooseRoom(this)?.copy(intent)

            val resInfoList =
                packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)

            intent.uris.forEach { uri ->
                this.let {
                    resInfoList.forEach {
                        grantUriPermission(
                            it.activityInfo.packageName,
                            uri,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION
                        )
                    }
                }
            }

            mHandleLink.launch(next)

            // Remove it so it doesn't retrigger on device turn.
            intent = null

            // Wait on return from ChooseRoomActivity.
            return
        }

        if (intent == null || intent.action == null || intent.action == Intent.ACTION_MAIN) {
            if (mApp?.matrixSession != null) {
                startActivity(mApp?.router?.main(this))
            } else {
                startActivity(mApp?.router?.onboarding(this))
            }

            finish()
        }
    }

    private val mHandleLink =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                if (it.data?.hasExtra(ChooseRoomActivity.EXTRA_ROOM_ID) == true) {
                    openRoom(it.data!!)
                }
            }

            // If the user didn't select a room to send their attachment into, we just close
            // this activity and the user will be back at the app from where they started the
            // sharing ("SEND") intent.
            finish()
        }

    private fun openRoom(intent: Intent) {
        startActivity(
            mApp?.router?.main(
                this,
                openRoomId = intent.getStringExtra(ChooseRoomActivity.EXTRA_ROOM_ID)
            )?.copy(intent)
        )

        finish()
    }

    private fun shutdownAndLock() {
        mApp?.forceStopImService()

        finish()
    }

    private class MyHandler(activity: Activity?) : SimpleAlertHandler(activity) {
        override fun handleMessage(msg: Message) {
            if (msg.what == ImApp.EVENT_CONNECTION_DISCONNECTED) {
                promptDisconnectedEvent(msg)
            }

            super.handleMessage(msg)
        }
    }
}
