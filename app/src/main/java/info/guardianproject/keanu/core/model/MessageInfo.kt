package info.guardianproject.keanu.core.model

data class MessageInfo(
    val sender: String,
    val message: String,
    val reply: String,
    val isMentioned: Boolean,
    val mentionedName: String?
)