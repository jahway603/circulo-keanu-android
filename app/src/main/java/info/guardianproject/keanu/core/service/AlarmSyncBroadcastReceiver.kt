package info.guardianproject.keanu.core.service


import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import org.matrix.android.sdk.api.session.sync.SyncService
import org.matrix.android.sdk.api.session.sync.job.SyncAndroidService.Companion.EXTRA_SESSION_ID
import timber.log.Timber

public class AlarmSyncBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Timber.d("## Sync: AlarmSyncBroadcastReceiver received intent")

        val sessionId = intent.getStringExtra(EXTRA_SESSION_ID) ?: return
        KeanuSyncService.newPeriodicIntent(
            context = context,
            sessionId = sessionId,
            syncTimeoutSeconds = 10,
            syncDelaySeconds = 30,
            isNetworkBack = false
        )
            .let {
                try {
                    context.startService(it)
                } catch (ex: Throwable) {
                    Timber.i("## Sync: Failed to start service, Alarm scheduled to restart service")
              //      scheduleAlarm(context, sessionId, 30)
                    Timber.e(ex)
                }
            }
    }

    /**
    companion object {
        private const val REQUEST_CODE = 0

        fun scheduleAlarm(context: Context, sessionId: String, delayInSeconds: Int) {
            // Reschedule
            Timber.v("## Sync: Scheduling alarm for background sync in $delayInSeconds seconds")
            val intent = Intent(context, AlarmSyncBroadcastReceiver::class.java).apply {
                putExtra(SyncService.EXTRA_SESSION_ID, sessionId)
                putExtra(SyncService.EXTRA_PERIODIC, true)
            }
            val pIntent = PendingIntent.getBroadcast(
                context,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
            val firstMillis = System.currentTimeMillis() + delayInSeconds * 1000L
            val alarmMgr = context.getSystemService<AlarmManager>()!!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmMgr.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, firstMillis, pIntent)
            } else {
                alarmMgr.set(AlarmManager.RTC_WAKEUP, firstMillis, pIntent)
            }
        }

        fun cancelAlarm(context: Context) {
            Timber.v("## Sync: Cancel alarm for background sync")
            val intent = Intent(context, AlarmSyncBroadcastReceiver::class.java)
            val pIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PendingIntent.getBroadcast(
                    context,
                    REQUEST_CODE,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                )
            } else {
                PendingIntent.getBroadcast(
                    context,
                    REQUEST_CODE,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
            }
            val alarmMgr = context.getSystemService<AlarmManager>()!!
            alarmMgr.cancel(pIntent)

            // Stop current service to restart
            KeanuSyncService.stopIntent(context).let {
                try {
                    context.startService(it)
                } catch (ex: Throwable) {
                    Timber.i("## Sync: Cancel sync")
                }
            }
        }
    }**/
}