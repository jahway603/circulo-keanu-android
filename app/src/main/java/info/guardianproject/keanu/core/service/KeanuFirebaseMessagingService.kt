package info.guardianproject.keanu.core.service

import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import androidx.core.os.ConfigurationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.mention.TextPillsUtils
import info.guardianproject.keanuapp.R
import org.jsoup.Jsoup
import org.matrix.android.sdk.api.session.pushers.HttpPusher
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import timber.log.Timber
import java.util.Date

class KeanuFirebaseMessagingService: FirebaseMessagingService() {

    private val session
        get() = ImApp.sImApp?.matrixSession

    private var mStatusBarNotifier: StatusBarNotifier? = null

    override fun onCreate() {
        super.onCreate()
        mStatusBarNotifier = StatusBarNotifier(this)
        Log.d("tuancoltech", "onCreate KeanuFirebaseMessagingService mStatusBarNotifier: " + mStatusBarNotifier)
    }


    override fun onNewToken(token: String) {
        Log.i("tuancoltech", "onNewToken: " + token)
        val session = session ?: return
        val deviceId = session.sessionParams.deviceId ?: return
        val locale = ConfigurationCompat.getLocales(resources.configuration)[0] ?: return

        val pm = packageManager

        val appInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            pm.getApplicationInfo(packageName, PackageManager.ApplicationInfoFlags.of(0))
        }
        else {
            @Suppress("DEPRECATION")
            pm.getApplicationInfo(packageName, 0)
        }

        val pusher = HttpPusher(
            pushkey = token,
            appId = packageName,
            profileTag = "mobile_${session.myUserId.hashCode()}",
            lang = locale.language,
            appDisplayName = pm.getApplicationLabel(appInfo).toString(),
            deviceDisplayName = deviceId,
            url = "https://${getString(R.string.default_server)}/_matrix/push/v1/notify",
            enabled = true,
            deviceId = deviceId,
            append = false,
            withEventIdOnly = true)

        session.pushersService().enqueueAddHttpPusher(pusher)

        Log.d("tuancoltech", "onNewToken: pusher= " + pusher)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        Log.i("tuancoltech", "onMessageReceived message: " + message + ". data: " + message.data)
        val body = message.data["body"]
        val title = message.data["title"]
        val roomId = message.data["roomId"]
        val eventId = message.data["eventId"] ?: return
//        Timber.d("tuancoltech onMessageReceived: message=%s\nbody: %s\ntitle: %s\nroomId: %s\neventId: %s",
//            message.notification?.body, body, title, roomId, eventId)

        val room = roomId?.let { session?.roomService()?.getRoom(it) }
        val te = room?.timelineService()?.getTimelineEvent(eventId) ?: return

        val preferenceProvider = ImApp.sImApp?.preferenceProvider

        Timber.i("tuancoltech room: " + room + "\nte: " + room?.timelineService()?.getTimelineEvent(eventId)
                + "\nnoti enabled: " + preferenceProvider?.isNotificationEnable()
                + "\nnotiState: " + preferenceProvider?.getNotificationState()
                + "\nte.root.originServerTs: " + te.root.originServerTs
                + "\nDate().time: " + Date().time + "\nNoti enabled: " + preferenceProvider?.isNotificationEnable())

        if (preferenceProvider?.isNotificationEnable() == true) {
            val hasMention = isMyselfMentioned(te)
            if (preferenceProvider.getNotificationState() == RoomNotificationState.ALL_MESSAGES ||
                preferenceProvider.getNotificationState() == RoomNotificationState.MENTIONS_ONLY && hasMention
            ) {
                if (true/*te.root.originServerTs!! > Date().time*/) {
                    Timber.v("tuancoltech notifyChat with event: " + te.toString() + "\nEvent ID: " + te.eventId
                            + "\nmStatusBarNotifier: " + mStatusBarNotifier)
                    mStatusBarNotifier?.notifyChat(te)
                }
            }

        }

        // TODO: Is this already enough? Isn't session already started and started syncing?
        //   Wait for ops to set up FCM / Sygnal push service.
        super.onMessageReceived(message)
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
        Log.w("tuancoltech", "onDeletedMessages")
    }

    private fun isMyselfMentioned(tEvent: TimelineEvent): Boolean {

        try {
            val messageContent = tEvent.getLastMessageContent()
            Log.v("tuancoltech", "isMyselfMentioned messageContent: " + messageContent)
            if (messageContent is MessageTextContent && messageContent.formattedBody != null
                && messageContent.formattedBody?.contains("mx-reply") == true
            ) {
                // have reply content
                messageContent.formattedBody?.let {

                    val document = Jsoup.parse(it)
                    val mentionPart = document.select(TextPillsUtils.MENTION_TAG).last()

                    val userIdLink = mentionPart?.attributes()?.get("href") ?: ""
                    val idStartIndex = userIdLink.lastIndexOf("@")
                    val mentionedUserId =
                        if (idStartIndex >= 0 && idStartIndex < userIdLink.length) {
                            userIdLink.substring(idStartIndex)
                        } else ""

                    return mentionedUserId == session?.myUserId
                }
            }
        } catch (exception: Exception) {
            Timber.tag("RemoteIMService").e(exception.toString())
        }

        return false
    }

}