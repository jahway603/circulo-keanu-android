package info.guardianproject.keanu.core.service


import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.content.getSystemService
import androidx.lifecycle.LiveData
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import kotlinx.coroutines.flow.SharedFlow
import org.matrix.android.sdk.api.session.sync.SyncRequestState
import org.matrix.android.sdk.api.session.sync.SyncService
import org.matrix.android.sdk.api.session.sync.SyncState
import org.matrix.android.sdk.api.session.sync.job.SyncAndroidService.Companion.ACTION_STOP
import org.matrix.android.sdk.api.session.sync.job.SyncAndroidService.Companion.EXTRA_DELAY_SECONDS
import org.matrix.android.sdk.api.session.sync.job.SyncAndroidService.Companion.EXTRA_NETWORK_BACK_RESTART
import org.matrix.android.sdk.api.session.sync.job.SyncAndroidService.Companion.EXTRA_PERIODIC
import org.matrix.android.sdk.api.session.sync.job.SyncAndroidService.Companion.EXTRA_SESSION_ID
import org.matrix.android.sdk.api.session.sync.job.SyncAndroidService.Companion.EXTRA_TIMEOUT_SECONDS
import org.matrix.android.sdk.api.session.sync.model.SyncResponse
import timber.log.Timber

class KeanuSyncService : SyncService {

    companion object {

        fun newOneShotIntent(context: Context,
                             sessionId: String): Intent {
            return Intent(context, KeanuSyncService::class.java).also {
                it.putExtra(EXTRA_SESSION_ID, sessionId)
                it.putExtra(EXTRA_TIMEOUT_SECONDS, 0)
                it.putExtra(EXTRA_PERIODIC, false)
            }
        }

        fun newPeriodicIntent(context: Context,
                              sessionId: String,
                              syncTimeoutSeconds: Int,
                              syncDelaySeconds: Int,
                              isNetworkBack: Boolean): Intent {
            return Intent(context, KeanuSyncService::class.java).also {
                it.putExtra(EXTRA_SESSION_ID, sessionId)
                it.putExtra(EXTRA_TIMEOUT_SECONDS, syncTimeoutSeconds)
                it.putExtra(EXTRA_PERIODIC, true)
                it.putExtra(EXTRA_DELAY_SECONDS, syncDelaySeconds)
                it.putExtra(EXTRA_NETWORK_BACK_RESTART, isNetworkBack)
            }
        }

        fun stopIntent(context: Context): Intent {
            return Intent(context, KeanuSyncService::class.java).also {
                it.action = ACTION_STOP
            }
        }
    }

/**
    override fun getDefaultSyncDelaySeconds() = 30

    override fun getDefaultSyncTimeoutSeconds() = 10



    override fun onStart(isInitialSync: Boolean) {

     //   mStatusBarNotifier = StatusBarNotifier(this)
     //   startForeground(notifyId, mForegroundNotification)
    }**/


    private val notifyId = 777

  //  private var mStatusBarNotifier: StatusBarNotifier? = null

/**
    private val mForegroundNotification by lazy {

        var flags = PendingIntent.FLAG_UPDATE_CURRENT

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            flags = PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        }

        NotificationCompat.Builder(this, KeanuConstants.NOTIFICATION_CHANNEL_ID_SERVICE)
            .setContentTitle(getString(R.string.app_name))
            .setSmallIcon(R.drawable.notify_app)
            .setOngoing(true)
            .setAutoCancel(false)
            .setWhen(System.currentTimeMillis())
            .setContentText(getString(R.string.app_unlocked))
            .setContentIntent(PendingIntent.getActivity(this, 0,
                mStatusBarNotifier?.getDefaultIntent(),  flags))
            .build()


    }**/

/**
    override fun onRescheduleAsked(sessionId: String,
                                   syncTimeoutSeconds: Int,
                                   syncDelaySeconds: Int) {
        rescheduleSyncService(
            sessionId = sessionId,
            syncTimeoutSeconds = syncTimeoutSeconds,
            syncDelaySeconds = syncDelaySeconds,
            isPeriodic = true,
            isNetworkBack = false
        )
    }


    override fun onNetworkError(sessionId: String,
                                syncTimeoutSeconds: Int,
                                syncDelaySeconds: Int,
                                isPeriodic: Boolean) {
        Timber.d("## Sync: A network error occurred during sync")
        val rescheduleSyncWorkRequest: WorkRequest =
            OneTimeWorkRequestBuilder<RestartWhenNetworkOn>()
                .setInputData(RestartWhenNetworkOn.createInputData(sessionId, syncTimeoutSeconds, syncDelaySeconds, isPeriodic))
                .setConstraints(Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
                )
                .build()

        Timber.d("## Sync: Schedule a work to restart service when network will be on")
        WorkManager
            .getInstance(applicationContext)
            .enqueue(rescheduleSyncWorkRequest)
    }

    override fun onDestroy() {
        removeForegroundNotification()
        super.onDestroy()
    }**/

    private fun removeForegroundNotification() {

    }

    // I do not move or rename this class, since I'm not sure about the side effect regarding the WorkManager
    class RestartWhenNetworkOn(appContext: Context, workerParams: WorkerParameters) :
        Worker(appContext, workerParams) {
        override fun doWork(): Result {
            Timber.d("## Sync: RestartWhenNetworkOn.doWork()")
            val sessionId = inputData.getString(KEY_SESSION_ID) ?: return Result.failure()
            val syncTimeoutSeconds = inputData.getInt(KEY_SYNC_TIMEOUT_SECONDS, 10)
            val syncDelaySeconds = inputData.getInt(KEY_SYNC_DELAY_SECONDS, 10)
            val isPeriodic = inputData.getBoolean(KEY_IS_PERIODIC, false)
            applicationContext.rescheduleSyncService(
                sessionId = sessionId,
                syncTimeoutSeconds = syncTimeoutSeconds,
                syncDelaySeconds = syncDelaySeconds,
                isPeriodic = isPeriodic,
                isNetworkBack = true
            )
            // Indicate whether the work finished successfully with the Result
            return Result.success()
        }

        companion object {
            fun createInputData(sessionId: String,
                                syncTimeoutSeconds: Int,
                                syncDelaySeconds: Int,
                                isPeriodic: Boolean
            ): Data {
                return Data.Builder()
                    .putString(KEY_SESSION_ID, sessionId)
                    .putInt(KEY_SYNC_TIMEOUT_SECONDS, syncTimeoutSeconds)
                    .putInt(KEY_SYNC_DELAY_SECONDS, syncDelaySeconds)
                    .putBoolean(KEY_IS_PERIODIC, isPeriodic)
                    .build()
            }

            private const val KEY_SESSION_ID = "sessionId"
            private const val KEY_SYNC_TIMEOUT_SECONDS = "timeout"
            private const val KEY_SYNC_DELAY_SECONDS = "delay"
            private const val KEY_IS_PERIODIC = "isPeriodic"
        }
    }

    override fun getSyncRequestStateFlow(): SharedFlow<SyncRequestState> {
        TODO("Not yet implemented")
    }

    override fun getSyncState(): SyncState {
        TODO("Not yet implemented")
    }

    override fun getSyncStateLive(): LiveData<SyncState> {
        TODO("Not yet implemented")
    }

    override fun hasAlreadySynced(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isSyncThreadAlive(): Boolean {
        TODO("Not yet implemented")
    }

    override fun requireBackgroundSync() {
        TODO("Not yet implemented")
    }

    override fun startAutomaticBackgroundSync(timeOutInSeconds: Long, repeatDelayInSeconds: Long) {
        TODO("Not yet implemented")
    }

    override fun startSync(fromForeground: Boolean) {
        TODO("Not yet implemented")
    }

    override fun stopAnyBackgroundSync() {
        TODO("Not yet implemented")
    }

    override fun stopSync() {
        TODO("Not yet implemented")
    }

    override fun syncFlow(): SharedFlow<SyncResponse> {
        TODO("Not yet implemented")
    }
}

private fun Context.rescheduleSyncService(sessionId: String,
                                          syncTimeoutSeconds: Int,
                                          syncDelaySeconds: Int,
                                          isPeriodic: Boolean,
                                          isNetworkBack: Boolean) {
    Timber.d("## Sync: rescheduleSyncService")
    val intent = if (isPeriodic) {
        KeanuSyncService.newPeriodicIntent(
            context = this,
            sessionId = sessionId,
            syncTimeoutSeconds = syncTimeoutSeconds,
            syncDelaySeconds = syncDelaySeconds,
            isNetworkBack = isNetworkBack
        )
    } else {
        KeanuSyncService.newOneShotIntent(
            context = this,
            sessionId = sessionId
        )
    }

    if (isNetworkBack || syncDelaySeconds == 0) {
        // Do not wait, do the sync now (more reactivity if network back is due to user action)
        startService(intent)
    } else {
        val pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)
        val firstMillis = System.currentTimeMillis() + syncDelaySeconds * 1000L
        val alarmMgr = getSystemService<AlarmManager>()!!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmMgr.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, firstMillis, pendingIntent)
        } else {
            alarmMgr.set(AlarmManager.RTC_WAKEUP, firstMillis, pendingIntent)
        }
    }
}