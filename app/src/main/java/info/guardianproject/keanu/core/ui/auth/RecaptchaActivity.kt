package info.guardianproject.keanu.core.ui.auth

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapter
import info.guardianproject.keanu.core.util.extensions.betterMessage
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityTermsRecaptchaBinding
import info.guardianproject.keanuapp.ui.BaseActivity

@SuppressLint("LogNotTimber")
class RecaptchaActivity: BaseActivity() {

    companion object {
        const val EXTRA_PUBLIC_KEY = "extra_public_key"
        const val EXTRA_HOME_SERVER = "extra_home_server"
        const val EXTRA_RESPONSE = "extra_response"
    }

    private lateinit var mBinding: ActivityTermsRecaptchaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityTermsRecaptchaBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        @SuppressLint("SetJavaScriptEnabled")
        mBinding.webView.settings.javaScriptEnabled = true

        mBinding.webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val url = request?.url?.toString()

                if (url?.startsWith("js:") == true) {
                    val json = url.substring(3)
                    var response: Response? = null

                    try {
                        val moshi = Moshi.Builder()
                            .build();
                        response = moshi.adapter(Response::class.java).fromJson(json)
                    }
                    catch (t: Throwable) {
                        Log.e(this@RecaptchaActivity::class.java.canonicalName, t.betterMessage)
                    }

                    val r = response?.response

                    if (r != null) {
                        val intent = Intent()
                        intent.putExtra(EXTRA_RESPONSE, r)
                        setResult(Activity.RESULT_OK, intent)
                    }
                    else {
                        setResult(Activity.RESULT_CANCELED)
                    }

                    finish()
                }

                return true
            }
        }

        val server = intent.getStringExtra(EXTRA_HOME_SERVER)
        val publicKey = intent.getStringExtra(EXTRA_PUBLIC_KEY)
        val page = String.format(assets.open("reCaptchaPage.html").bufferedReader().use { it.readText() }, publicKey)

        mBinding.webView.loadDataWithBaseURL(server, page,"text/html", "utf-8", null)

        supportActionBar?.title = getString(R.string.Solve_Recaptcha)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)

                finish()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    @JsonClass(generateAdapter = true)
    data class Response(val response: String? = null)
}