package info.guardianproject.keanu.core.ui.friends

import android.app.SearchManager
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.AdapterView
import android.widget.ListView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.SearchView
import androidx.collection.LongSparseArray
import androidx.core.widget.ListViewCompat
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ContactsPickerActivityBinding
import info.guardianproject.keanuapp.databinding.PickedContactItemBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import java.util.*

/** Activity used to pick a contact.  */
class FriendsPickerActivity : BaseActivity() {

    companion object {
        const val EXTRA_EXCLUDED_CONTACTS = "excludes"
        const val EXTRA_SHOW_GROUPS = "show_groups"
        const val EXTRA_SHOW_ADD_OPTIONS = "show_add_options"
        const val EXTRA_RESULT_USERNAME = "result"
        const val EXTRA_RESULT_USERNAMES = "results"
    }

    private var mAdapter: FriendsListAdapter? = null

    private val mHandler = Handler(Looper.getMainLooper())

    private var mExcludedContacts: ArrayList<String>? = null

    var data: Uri? = null

    private var mShowGroups = false

    private var mSearchString: String? = null

    private var mMenuStartGroupChat: MenuItem? = null

    private val mSelection = LongSparseArray<SelectedFriend>()

    private var mAwaitingUpdate = false

    private lateinit var mBinding: ContactsPickerActivityBinding

    private inner class SelectedFriend(
        var userId: String,
    )

    public override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        mApp.setAppTheme(this)

        mBinding = ContactsPickerActivityBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.flSelectedContacts.addOnLayoutChangeListener { _: View?, _: Int, _: Int, _: Int, bottom: Int, _: Int, _: Int, _: Int, oldBottom: Int ->
            // When the tag view grows we don't want the list to jump around, so
            // compensate for this by trying to scroll the list.
            val diff = bottom - oldBottom
            ListViewCompat.scrollListBy(mBinding.contactsList, diff)
        }

        mExcludedContacts = intent.getStringArrayListExtra(EXTRA_EXCLUDED_CONTACTS)
        mShowGroups = intent.getBooleanExtra(EXTRA_SHOW_GROUPS, false)
        val showAddOptions = intent.getBooleanExtra(EXTRA_SHOW_ADD_OPTIONS, true)

        mBinding.btnCreateGroup.setOnClickListener { isGroupMode = true }
        mBinding.btnCreateGroup.visibility = if (isGroupOnlyMode) View.GONE else View.VISIBLE

        mBinding.btnAddFriend.setOnClickListener {
            val i = mApp.router.addFriend(this@FriendsPickerActivity, null, false)
            mAddFriend.launch(i)
        }
        mBinding.btnAddFriend.visibility = if (showAddOptions) View.VISIBLE else View.GONE

        // Make sure the tag view can not be more than a third of the screen
        mBinding.root.addOnLayoutChangeListener { _: View?, _: Int, top: Int, _: Int, bottom: Int, _: Int, oldTop: Int, _: Int, oldBottom: Int ->
            if (bottom - top != oldBottom - oldTop) {
                val lp = mBinding.flSelectedContacts.layoutParams
                lp.height = (bottom - top) / 3
                mBinding.flSelectedContacts.layoutParams = lp
            }
        }

        isGroupMode = isGroupOnlyMode
        mBinding.contactsList.setOnItemLongClickListener { _: AdapterView<*>?, _: View?, i: Int, _: Long ->
            multiStart(i)
            true
        }

        // Uncomment this to set as list view header instead.
        //((ViewGroup)mSelectedContacts.getParent()).removeView(mSelectedContacts);
        //mSelectedContacts.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
        //mListView.addHeaderView(mSelectedContacts);
        mBinding.contactsList.setOnItemClickListener { _: AdapterView<*>?, _: View?, position: Int, id: Long ->
            if (mBinding.contactsList.choiceMode == ListView.CHOICE_MODE_MULTIPLE) {
                if (isSelected(id)) {
                    unselect(id)
                } else {
                    select(position)
                }
            } else {
                val data = Intent()
                data.putExtra(EXTRA_RESULT_USERNAME, mAdapter?.getItem(position)?.userId)
                setResult(RESULT_OK, data)
                finish()
            }
        }

        doFilterAsync("")
    }

    private fun multiStart(i: Int) {
        isGroupMode = true

        if (i > -1) select(i)
    }

    private val isGroupOnlyMode: Boolean
        get() = intent.hasExtra(EXTRA_EXCLUDED_CONTACTS) || mShowGroups

    private var isGroupMode: Boolean
        get() = mBinding.contactsList.choiceMode != ListView.CHOICE_MODE_SINGLE
        private set(groupMode) {
            setTitle(if (groupMode) R.string.add_people else R.string.choose_friend)
            //mLayoutContactSelect.setVisibility(groupMode ? View.GONE : View.VISIBLE);
            //mLayoutGroupSelect.setVisibility(groupMode ? View.VISIBLE : View.GONE);
            val newChoiceMode =
                if (groupMode) ListView.CHOICE_MODE_MULTIPLE else ListView.CHOICE_MODE_SINGLE
            if (mBinding.contactsList.choiceMode != newChoiceMode) {
                mBinding.contactsList.choiceMode = newChoiceMode
            }
            updateStartGroupChatMenu()
        }

    private fun multiFinish() {
        if (mSelection.size() > 0) {
            val users = ArrayList<String>()

            for (i in 0 until mSelection.size()) {
                val contact = mSelection.valueAt(i)
                users.add(contact.userId)
            }
            val data = Intent()
            data.putStringArrayListExtra(EXTRA_RESULT_USERNAMES, users)
            setResult(RESULT_OK, data)

            finish()
        }
    }

    private val mAddFriend = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            val userId = result.data?.extras?.getString(EXTRA_RESULT_USERNAME) ?: return@registerForActivityResult

            val intent = Intent()
            intent.putExtra(EXTRA_RESULT_USERNAME, userId)
            setResult(RESULT_OK, intent)

            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.contact_list_menu, menu)
        mMenuStartGroupChat = menu.findItem(R.id.action_start_chat)

        updateStartGroupChatMenu()

        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager

        val searchView = menu.findItem(R.id.menu_search).actionView as? SearchView

        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
            searchView.setIconifiedByDefault(false)
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String): Boolean {
                    mSearchString = newText
                    doFilterAsync(mSearchString)
                    return true
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    mSearchString = query
                    doFilterAsync(mSearchString)
                    return true
                }
            })
        }
        return true
    }

    private fun updateStartGroupChatMenu() {
        mMenuStartGroupChat?.isVisible = isGroupMode
        mMenuStartGroupChat?.isEnabled = mSelection.size() > 0
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (isGroupMode && !isGroupOnlyMode) {
                    isGroupMode = false
                } else {
                    finish()
                }
                return true
            }

            R.id.action_start_chat -> {
                multiFinish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (isGroupMode && !isGroupOnlyMode) {
            isGroupMode = false
            return
        }

        super.onBackPressed()
    }

    fun doFilterAsync(query: String?) {
        doFilter(query)
    }

    @Synchronized
    fun doFilter(filterString: String?) {
        mSearchString = filterString

        if (mAdapter == null) {
            mAdapter =
                FriendsListAdapter(
                    this,
                    filterString
                )
            mBinding.contactsList.adapter = mAdapter
        }
        else {
            if (!mAwaitingUpdate) {
                mAwaitingUpdate = true
                mHandler.postDelayed({ mAwaitingUpdate = false }, 1000)
            }
            mAdapter?.setFilter(filterString)
            mBinding.contactsList.adapter = mAdapter
        }
    }

    private fun createTagView(index: Int, friend: SelectedFriend) {
        val user = mAdapter?.getItem(index)
        val itemId = mAdapter?.getItemId(index)

        val binding = PickedContactItemBinding.inflate(LayoutInflater.from(mBinding.flSelectedContacts.context),
            mBinding.flSelectedContacts, false)

        binding.root.tag = friend

        // TODO - Feel a little awkward to create a ContactListItem here just to use the binding code.
        // I guess we should move that somewhere else.
        val cli = FriendListItem(this, null)
        val cvh = FriendViewHolder(binding.root)
        cli.bind(cvh, user?.userId, user?.displayName, user?.avatarUrl)

        binding.btnClose.setOnClickListener(object : View.OnClickListener {
            private var itemId: Long = 0

            fun init(itemId: Long?): View.OnClickListener {
                this.itemId = itemId ?: 0

                return this
            }

            override fun onClick(v: View) {
                unselect(this.itemId)
            }
        }.init(itemId))

        mBinding.flSelectedContacts.addView(binding.root)
    }

    private fun removeTagView(friend: SelectedFriend?) {
        val view = mBinding.flSelectedContacts.findViewWithTag<View>(friend)
        if (view != null) {
            mBinding.flSelectedContacts.removeView(view)
        }
    }

    private fun select(index: Int) {
        val id = mAdapter?.getItemId(index) ?: return

        if (!isSelected(id)) {
            val user = mAdapter?.getItem(index) ?: return
            val contact = SelectedFriend(user.userId)
            mSelection.put(id, contact)
            createTagView(index, contact)

            updateStartGroupChatMenu()
        }
    }

    private fun isSelected(id: Long): Boolean {
        return mSelection.indexOfKey(id) >= 0
    }

    private fun unselect(id: Long) {
        if (isSelected(id)) {
            removeTagView(mSelection[id])
            mSelection.remove(id)

            if (mSelection.isEmpty) {
                isGroupMode = false
            }
            else {
                updateStartGroupChatMenu()
            }
        }
    }

    interface ContactListListener {
        fun openChat(c: Cursor?)
        fun showProfile(c: Cursor?)
    }
}