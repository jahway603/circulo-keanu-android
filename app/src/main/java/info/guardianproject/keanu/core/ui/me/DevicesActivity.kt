package info.guardianproject.keanuapp.ui.contacts

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ui.PasswordDialogFragment
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.PrettyTime
import info.guardianproject.keanu.core.util.extensions.betterMessage
import info.guardianproject.keanu.core.verification.VerificationManager
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.DeviceHeaderViewBinding
import info.guardianproject.keanuapp.databinding.DeviceKeyBackupViewBinding
import info.guardianproject.keanuapp.databinding.DeviceViewBinding
import info.guardianproject.keanuapp.databinding.RecyclerViewBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.auth.UIABaseAuth
import org.matrix.android.sdk.api.auth.UserInteractiveAuthInterceptor
import org.matrix.android.sdk.api.auth.UserPasswordAuth
import org.matrix.android.sdk.api.auth.registration.RegistrationFlowResponse
import org.matrix.android.sdk.api.extensions.getFingerprintHumanReadable
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.crypto.CryptoService
import org.matrix.android.sdk.api.session.crypto.keysbackup.KeysBackupService
import org.matrix.android.sdk.api.session.crypto.keysbackup.KeysBackupState
import org.matrix.android.sdk.api.session.crypto.model.CryptoDeviceInfo
import org.matrix.android.sdk.api.session.user.model.User
import java.lang.ref.WeakReference
import java.util.*
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume


class DevicesActivity : BaseActivity() {

    companion object {
        const val EXTRA_USER_ID = "userId"

        internal var green = 0
        internal var red = 0
        internal var deleteIcon: Drawable? = null
    }

    internal enum class Type {
        HEADER, BACKUP, DEVICE
    }

    private lateinit var mBinding: RecyclerViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = RecyclerViewBinding.inflate(layoutInflater)

        setContentView(mBinding.root)

        val userId = intent.getStringExtra(EXTRA_USER_ID) ?: mSession?.myUserId ?: ""
        val user = mSession?.userService()?.getUser(userId)

        val isMyUser = userId == mSession?.myUserId

        supportActionBar?.title = if (isMyUser) {
            getString(R.string.My_Sessions)
        } else {
            getString(R.string.Sessions_of_, user?.displayName ?: userId)
        }

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        green = ContextCompat.getColor(this, R.color.holo_green_dark)
        red = ContextCompat.getColor(this, R.color.holo_red_dark)
        deleteIcon = ContextCompat.getDrawable(this, R.drawable.ic_delete_white_48dp)

        val adapter = DevicesAdapter(this, mSession, userId, user, isMyUser)

        mBinding.root.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mBinding.root.adapter = adapter

        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(adapter))
        itemTouchHelper.attachToRecyclerView(mBinding.root)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }
}

private class DevicesAdapter(val activity: AppCompatActivity, private val mSession: Session?,
                             private val mUserId: String, private val mUser: User?, private val mIsMyUser: Boolean)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Observer<List<CryptoDeviceInfo>>, View.OnClickListener
{
    private val mCryptoService: CryptoService?
        get() = mSession?.cryptoService()

    private var mDevices: MutableList<CryptoDeviceInfo> = ArrayList()

    private val mKeyBackup: KeysBackupService?
        get() = mCryptoService?.keysBackupService()

    private val mName: String
        get() = mUser?.displayName ?: mUserId

    private var mDefaultTextColor: Int = 0
    private var mDefaultBtTextColor: Int = 0

    private var mBackupEnabled: Boolean? = null

    private val mHeaderSize: Int
        get() = if (mIsMyUser) 2 else 1

    private val mLastSeenMyDevicesTs = HashMap<String, Long?>()


    init {
        mCryptoService?.getCryptoDeviceInfo(mUserId)?.let {
            onChanged(it)
        }

        mCryptoService?.getLiveCryptoDeviceInfo(mUserId)?.observe(activity, this)
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) return DevicesActivity.Type.HEADER.ordinal

        if (position == 1 && mIsMyUser) return DevicesActivity.Type.BACKUP.ordinal

        return DevicesActivity.Type.DEVICE.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            DevicesActivity.Type.HEADER.ordinal -> HeaderViewHolder(DeviceHeaderViewBinding.inflate(inflater, parent, false))

            DevicesActivity.Type.BACKUP.ordinal -> {
                val holder = BackupViewHolder(DeviceKeyBackupViewBinding.inflate(inflater, parent, false))

                mDefaultTextColor = holder.tvStatus.currentTextColor
                mDefaultBtTextColor = holder.btEnable.currentTextColor

                holder.btEnable.setOnClickListener(this)
                holder.btRestoreBackup.setOnClickListener(this)

                return holder
            }

            else -> DeviceViewHolder(DeviceViewBinding.inflate(inflater, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            val keys = countKeys()

            var avatarUrl = mUser?.avatarUrl

            if (avatarUrl != null) {
                avatarUrl = mSession?.contentUrlResolver()?.resolveThumbnail(avatarUrl, 512, 512, ThumbnailMethod.SCALE)
                if (avatarUrl != null) GlideUtils.loadImageFromUri(activity, Uri.parse(avatarUrl), holder.imgAvatar, false)
            }

            mUser?.avatarUrl

            holder.imgVerifiedIcon.setColorFilter(if (keys.second > 0 && keys.first < 1) DevicesActivity.green else DevicesActivity.red)

            if (keys.second > 0 || keys.first > 0) {
                holder.tvSubtitle.text = activity.getString(R.string.__Untrusted_New_Devices_for__, keys.first, mName)

                holder.tvDescription.visibility = View.VISIBLE

                holder.tvDescription.text = if (mIsMyUser)
                    activity.getString(R.string.Make_sure_the_keys_match_the_latest_keys_on_your_devices_)
                else activity.getString(R.string.Make_sure_the_keys_match_your_friend_s_latest_keys_on_his_or_her_devices_)
            }
            else {
                holder.tvSubtitle.text = activity.getString(R.string.No_Devices_for__, mName)

                holder.tvDescription.visibility = View.GONE
            }

            return
        }

        if (holder is BackupViewHolder) {
            when (mKeyBackup?.getState()) {
                KeysBackupState.WrongBackUpVersion -> {
                    holder.tvStatus.text = activity.getString(R.string.Newer_version_on_server_)
                    holder.tvStatus.setTextColor(DevicesActivity.red)
                    mBackupEnabled = false
                }

                KeysBackupState.Disabled -> {
                    holder.tvStatus.text = activity.getString(R.string.disabled)
                    holder.tvStatus.setTextColor(DevicesActivity.red)
                    mBackupEnabled = false
                }

                KeysBackupState.NotTrusted -> {
                    holder.tvStatus.text = activity.getString(R.string.Disabled_because_backup_on_server_is_untrusted_)
                    holder.tvStatus.setTextColor(DevicesActivity.red)
                    mBackupEnabled = false
                }

                KeysBackupState.Enabling,
                KeysBackupState.ReadyToBackUp,
                KeysBackupState.WillBackUp,
                KeysBackupState.BackingUp -> {
                    holder.tvStatus.text = activity.getString(R.string.enabled)
                    holder.tvStatus.setTextColor(DevicesActivity.green)
                    mBackupEnabled = true
                }

                else -> {
                    holder.tvStatus.text = activity.getString(R.string.unknown)
                    holder.tvStatus.setTextColor(mDefaultTextColor)
                    mBackupEnabled = null
                }
            }

            when (mBackupEnabled) {
                true -> {
                    holder.btEnable.text = activity.getString(R.string.Delete_Key_Backup)
                    holder.btEnable.setTextColor(DevicesActivity.red)
                    holder.btEnable.isEnabled = true
                }
                false -> {
                    holder.btEnable.text = activity.getString(R.string.Enable_Key_Backup)
                    holder.btEnable.setTextColor(mDefaultBtTextColor)
                    holder.btEnable.isEnabled = true
                }
                else -> {
                    holder.btEnable.text = activity.getString(R.string.Enable_Key_Backup)
                    holder.btEnable.setTextColor(mDefaultBtTextColor)
                    holder.btEnable.isEnabled = false
                }
            }

            return
        }

        if (holder is DeviceViewHolder) {
            val device = mDevices[position - mHeaderSize] // First is header, second is key backup.

            holder.imgVerifiedIcon.setColorFilter(if (device.isVerified) DevicesActivity.green else DevicesActivity.red)
            holder.tvDeviceName.text = getDeviceName(device)
            holder.tvDeviceId.text = device.deviceId

            holder.tvDeviceFingerprint.text = if (mIsMyUser) {
                activity.getString(R.string.Last_seen_, mLastSeenMyDevicesTs[device.deviceId].let {
                    PrettyTime.format(if (it == null) null else Date(it), activity)
                })
            }
            else {
                device.getFingerprintHumanReadable()
            }

            holder.tvDeviceStatus.text = getStatus(device)
            holder.swVerified.isChecked = device.isVerified

            holder.swVerified.setOnCheckedChangeListener { switch, isChecked ->

                // Ignore non-interactive calls.
                if (!switch.isPressed) return@setOnCheckedChangeListener

                if (isChecked) {
                    // Need to reset prophylactically, otherwise there will be situations where that
                    // switch is on, but the actual values is still off.
                    // Instead, if verification was successful, there's going to be a LiveData update.
                    switch.isChecked = false

                    VerificationManager.instance.manualVerify(device)
                }
                else {
                    VerificationManager.instance.verify(device, false)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mDevices.size + mHeaderSize
    }

    override fun onChanged(value: List<CryptoDeviceInfo>) {
        mDevices = value.sortedBy {
            if (it.deviceId == (mCryptoService?.getMyDevice()?.deviceId ?: false)) return@sortedBy 0

            if (it.isUnknown) return@sortedBy 1

            if (it.isVerified) return@sortedBy 2

            if (it.isBlocked) return@sortedBy 4

            return@sortedBy 3
        }.toMutableList()

        if (mIsMyUser) {
            mLastSeenMyDevicesTs.clear()

            mCryptoService?.getMyDevicesInfo()?.forEach { deviceInfo ->
                deviceInfo.deviceId?.let { mLastSeenMyDevicesTs[it] = deviceInfo.lastSeenTs }
            }
        }

        notifyDataSetChanged()
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.btEnable) {
            if (mBackupEnabled == true) {
                val version = mKeyBackup?.currentBackupVersion ?: return

                AlertDialog.Builder(activity)
                        .setTitle(R.string.Delete_Key_Backup)
                        .setMessage(R.string.Really_delete_the_key_backup_on_the_server_)
                        .setNegativeButton(android.R.string.cancel, null)
                        .setNeutralButton(R.string.Delete_Key_Backup) { _, _ ->
                            mKeyBackup?.deleteBackup(version, object : MatrixCallback<Unit> {
                                override fun onFailure(failure: Throwable) {
                                    showFailure(failure)
                                }
                            })
                        }
                        .show()
            }
            else if (mBackupEnabled == false) {
                PasswordDialogFragment(
                        PasswordDialogFragment.Style.CONFIRM,
                        activity.getString(R.string.Recovery_Password),
                        activity.getString(R.string.Set_a_password_for_the_backup_which_you_can_use_to_restore_later_)
                ) { pdf ->
                    val password = pdf.password ?: return@PasswordDialogFragment
                    if (mSession == null) return@PasswordDialogFragment

                    VerificationManager.instance.addNewBackup(WeakReference(activity), password) { failure ->
                        if (failure != null) showFailure(failure)
                    }
                }.show(activity.supportFragmentManager, "PASSWORD")
            }
        }
        else if (v?.id == R.id.btRestoreBackup) {
            val pdf = PasswordDialogFragment(title = activity.getString(R.string.Backup_Password), okLabel = activity.getString(R.string.Restore)) { pdf ->
                val password = pdf.password ?: return@PasswordDialogFragment
                if (mSession == null) return@PasswordDialogFragment

                VerificationManager.instance.restoreBackup(password) { failure, result ->
                    notifyItemChanged(1)

                    if (failure != null) {
                        showFailure(failure)
                        return@restoreBackup
                    }

                    AlertDialog.Builder(activity)
                            .setTitle(R.string.Restore_Successful)
                            .setMessage(activity.getString(R.string._keys_were_found_where_imported_,
                                    result?.totalNumberOfKeys ?: 0,
                                    result?.successfullyNumberOfImportedKeys ?: 0))
                            .setNeutralButton(android.R.string.ok, null)
                            .show()
                }
            }

            pdf.show(activity.supportFragmentManager, "PASSWORD")
        }
    }

    /**
     * Checks, if a device is allowed to be deleted.
     *
     * The rules are as follows:
     *
     * - Position must be a device, actually.
     * - Device must be on of the users own devices.
     * - This device we're running on, cannot be deleted.
     */
    fun isDeletionAllowed(position: Int): Boolean {
        if (getItemViewType(position) != DevicesActivity.Type.DEVICE.ordinal) return false

        val device = mDevices[position - mHeaderSize]

        if (device.userId != mSession?.myUserId) return false

        if (device.deviceId == mSession.sessionParams.deviceId) return false

        return true
    }

    fun deleteItem(position: Int) {
        if (getItemViewType(position) != DevicesActivity.Type.DEVICE.ordinal) return

        val device = mDevices[position - mHeaderSize]

        PasswordDialogFragment(
                PasswordDialogFragment.Style.ENTRY,
                activity.getString(R.string.Delete),
                activity.getString(R.string.To_delete_the_device_please_provide_your_password_, device.displayName()),
                okLabel = activity.getString(R.string.Delete)
        ) { pdf ->
            val password = pdf.password

            if (password == null) {
                notifyItemChanged(position)

                return@PasswordDialogFragment
            }

            mCryptoService?.deleteDevice(device.deviceId,
                    object : UserInteractiveAuthInterceptor {
                        override fun performStage(flowResponse: RegistrationFlowResponse, errCode: String?, promise: Continuation<UIABaseAuth>) {
                            // TODO: This assumes password authentication.
                            //  Expand, if that's not enough for the servers we want to support!
                            promise.resume(UserPasswordAuth(flowResponse.session, user = device.userId, password = password))
                        }
                    },
                    object: MatrixCallback<Unit> {
                        override fun onSuccess(data: Unit) {
                            mDevices.removeAt(position - mHeaderSize)

                            notifyItemRemoved(position)
                        }

                        override fun onFailure(failure: Throwable) {
                            notifyItemChanged(position)

                            AlertDialog.Builder(activity)
                                    .setTitle(R.string.error)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setMessage(failure.betterMessage)
                                    .setNeutralButton(android.R.string.ok, null)
                                    .show()
                        }
                    })
        }.show(activity.supportFragmentManager, "PASSWORD")
    }

    private fun getStatus(device: CryptoDeviceInfo): String {
        if (device.isVerified) {
            return "verified"
        }

        if (device.isUnknown) {
            return "new"
        }

        if (device.isBlocked) {
            return "blocked"
        }

        return "unverified"
    }

    private fun getDeviceName(device: CryptoDeviceInfo): String {
        val name = device.displayName() ?: activity.getString(R.string._unnamed_)

        if (device.deviceId == mCryptoService?.getMyDevice()?.deviceId) {
            return activity.getString(R.string._THIS_DEVICE, name)
        }

        return name
    }

    private fun countKeys(): Pair<Int, Int> {
        var unverifiedNew = 0
        var verified = 0

        for (device in mDevices) {
            if (device.isVerified) {
                verified += 1
            } else if (device.isUnknown || !device.isBlocked) {
                unverifiedNew += 1
            }
        }

        return Pair(unverifiedNew, verified)
    }

    @SuppressLint("LogNotTimber")
    private fun showFailure(failure: Throwable) {
        Log.e(this::class.java.canonicalName, failure.stackTraceToString())

        if (!activity.isDestroyed)
            AlertDialog.Builder(activity)
                    .setTitle(R.string.error)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(failure.betterMessage)
                    .setNeutralButton(android.R.string.ok, null)
                    .show()
    }
}

class HeaderViewHolder(private val mBinding: DeviceHeaderViewBinding) : RecyclerView.ViewHolder(mBinding.root) {

    val imgAvatar get() = mBinding.imgAvatar
    val imgVerifiedIcon get() = mBinding.verifiedIcon
    val tvSubtitle get() = mBinding.tvSubtitle
    val tvDescription get() = mBinding.tvDescription
}

class BackupViewHolder(private val mBinding: DeviceKeyBackupViewBinding) : RecyclerView.ViewHolder(mBinding.root) {

    val tvStatus get() = mBinding.tvStatus
    val btEnable get() = mBinding.btEnable
    val btRestoreBackup get() = mBinding.btRestoreBackup
}

class DeviceViewHolder(private val mBinding: DeviceViewBinding) : RecyclerView.ViewHolder(mBinding.root) {

    val imgVerifiedIcon get() = mBinding.verifiedIcon
    val tvDeviceName get() = mBinding.tvDeviceName
    val tvDeviceId get() = mBinding.tvDeviceId
    val tvDeviceFingerprint get() = mBinding.tvDeviceFingerprint
    val tvDeviceStatus get() = mBinding.tvDeviceStatus
    val swVerified get() = mBinding.swVerified
}

private class SwipeToDeleteCallback(private val mAdapter: DevicesAdapter)
    : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.START)
{

    private val background = ColorDrawable(DevicesActivity.red)

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun getSwipeDirs(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        if (!mAdapter.isDeletionAllowed(viewHolder.bindingAdapterPosition)) return 0

        return super.getSwipeDirs(recyclerView, viewHolder)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        mAdapter.deleteItem(viewHolder.bindingAdapterPosition)
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                             dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean)
    {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

        val itemView = viewHolder.itemView
        val backgroundCornerOffset = 20

        val icon = DevicesActivity.deleteIcon
        val iconHeight = icon?.intrinsicHeight ?: 0
        val iconWidth = icon?.intrinsicWidth ?: 0

        val iconMargin = (itemView.height - iconHeight) / 2
        val iconTop = itemView.top + (itemView.height - iconHeight) / 2
        val iconBottom = iconTop + iconHeight

        when {
            dX > 0 -> { // Swiping to the right
                val iconLeft = itemView.left + iconMargin + iconWidth
                val iconRight = itemView.left + iconMargin
                icon?.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                background.setBounds(itemView.left, itemView.top,
                        itemView.left + dX.toInt() + backgroundCornerOffset,
                        itemView.bottom)
            }
            dX < 0 -> { // Swiping to the left
                val iconLeft = itemView.right - iconMargin - iconWidth
                val iconRight = itemView.right - iconMargin
                icon?.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                background.setBounds(itemView.right + dX.toInt() - backgroundCornerOffset,
                        itemView.top, itemView.right, itemView.bottom)
            }
            else -> { // view is unSwiped
                background.setBounds(0, 0, 0, 0)
            }
        }

        background.draw(c)
        icon?.draw(c)
    }
}