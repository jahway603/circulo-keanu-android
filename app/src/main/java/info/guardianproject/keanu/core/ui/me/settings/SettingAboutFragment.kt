package info.guardianproject.keanu.core.ui.me.settings

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentSettingAboutBinding
import timber.log.Timber

class SettingAboutFragment: Fragment() {

    private lateinit var mBinding: FragmentSettingAboutBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mBinding = FragmentSettingAboutBinding.inflate(inflater, container, false)

        (activity as SettingsActivity).setBackButtonText(getString(R.string.profile_about))

        val appVersion = getAppVersion()
        if (!appVersion.isNullOrEmpty()) {
            mBinding.tvVersion.text = appVersion
        }
        else {
            mBinding.tvVersion.visibility = View.GONE
        }

        return mBinding.root
    }

    private fun getAppVersion(): String {
        var version = ""
        activity?.let {
            try {
                version = it.packageManager?.getPackageInfo(it.packageName, 0)?.versionName ?: ""
            } catch (e: PackageManager.NameNotFoundException) {
                Timber.e(e)
            }
        }
        return version
    }
}