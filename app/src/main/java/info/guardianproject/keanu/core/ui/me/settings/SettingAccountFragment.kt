package info.guardianproject.keanu.core.ui.me.settings

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.PasswordDialogFragment
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentSettingAccountBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.Session

class SettingAccountFragment : Fragment() {

    private lateinit var mBinding: FragmentSettingAccountBinding
    private val mApp
        get() = activity?.application as? ImApp

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    private val mSession: Session?
        get() = mApp?.matrixSession

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentSettingAccountBinding.inflate(inflater, container, false)

        mBinding.layoutChangePassword.setOnClickListener {
            showChangePassword()
        }

        mBinding.layoutAccountLogout.setOnClickListener {
            AlertDialog.Builder(activity)
                .setTitle(R.string.menu_sign_out)
                .setMessage(R.string.are_you_sure)
                .setNeutralButton(android.R.string.cancel, null)
                .setNegativeButton(R.string.menu_sign_out) { _, _ ->
                    lifecycleScope.launch {
                        mApp?.signOut()

                        val settingActivity = activity as SettingsActivity
                        settingActivity.startActivity(mApp?.router?.router(settingActivity))
                        settingActivity.finish()
                    }
                }
                .show()
        }


        return mBinding.root
    }

    private fun showChangePassword() {

        val preferenceProvider =
            PreferenceProvider(requireActivity())
        PasswordDialogFragment(
            PasswordDialogFragment.Style.NEW_PASSWORD,
            title = getString(R.string.profile_setting_account_change_password),
            okLabel = getString(R.string.ok),
            cancelLabel = getString(R.string.cancel)
        ) { pdf ->
            val oldPassword = pdf.oldPassword ?: return@PasswordDialogFragment
            val newPassword = pdf.password ?: return@PasswordDialogFragment

            mCoroutineScope.launch {
                mSession?.accountService()?.changePassword(oldPassword, newPassword)
                preferenceProvider.removePassword()
            }
        }.show(requireActivity().supportFragmentManager, "PASSWORD")
    }

}