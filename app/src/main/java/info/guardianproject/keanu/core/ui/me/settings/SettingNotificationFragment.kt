package info.guardianproject.keanu.core.ui.me.settings

import android.app.Activity
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanu.core.util.extensions.getParcelableExtraCompat
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentSettingNotificationsBinding
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState

class SettingNotificationFragment : Fragment() {

    private lateinit var mBinding: FragmentSettingNotificationsBinding
    private lateinit var preferenceProvider: PreferenceProvider
    private val mApp
        get() = activity?.application as? ImApp
    private var mSelectingUrgentRingtone = false

    private val mLaunchRingtonePickerIntent =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val uri = it.data?.getParcelableExtraCompat(RingtoneManager.EXTRA_RINGTONE_PICKED_URI, Uri::class.java)
                uri?.let {
                    val ringtoneName = getRingtoneNameFromUri(uri)

                    if (mSelectingUrgentRingtone) {
                        mBinding.tvStateUrgentRingtone.text = ringtoneName
                        preferenceProvider.setMentionNotificationSound(uri.toString())
                    } else {
                        mBinding.tvStateRingtone.text = ringtoneName
                        preferenceProvider.setNotificationSound(uri.toString())
                    }
                }
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentSettingNotificationsBinding.inflate(inflater, container, false)

        preferenceProvider = PreferenceProvider(requireActivity().applicationContext)
        (activity as SettingsActivity).setBackButtonText(getString(R.string.notifications))
        mBinding.sbNotification.isChecked = preferenceProvider.isNotificationEnable()
        mBinding.sbNotification.setOnCheckedChangeListener { _, isChecked ->
            preferenceProvider.setNotificationEnable(isChecked)
            Log.v("tuancoltech", "Calling setNotificationEnable: " + isChecked)
            if (isChecked) {
                mApp?.startImService()
            } else {
                mApp?.forceStopImService()

            }

        }
        mBinding.rgNotificationType.check(getCheckedNotificationType())
        mBinding.rgNotificationType.setOnCheckedChangeListener { _, checkedId ->
            val notificationState = when (checkedId) {
                R.id.rb_when_to_notify_anytime -> {
                    RoomNotificationState.ALL_MESSAGES
                }
                R.id.rb_noti_only_mentioned -> {
                    RoomNotificationState.MENTIONS_ONLY
                }
                else -> {
                    RoomNotificationState.MUTE
                }
            }
            preferenceProvider.setNotificationState(notificationState)
            if (preferenceProvider.isNotificationEnable())
                resetNotificationService()
        }
        mBinding.sbCustomVibrate.isChecked = preferenceProvider.isNotificationVibrate()
        mBinding.sbCustomSound.isChecked = preferenceProvider.isNotificationSound()
        mBinding.sbUrgentSound.isChecked = preferenceProvider.isMentionNotificationSound()
        mBinding.sbUrgentVibrate.isChecked = preferenceProvider.isMentionNotificationVibrate()
        mBinding.tvStateUrgentRingtone.text =
            getRingtoneNameFromUri(preferenceProvider.getMentionNotificationRingtone())
        mBinding.tvStateRingtone.text =
            getRingtoneNameFromUri(preferenceProvider.getNotificationRingtone())
        mBinding.sbCustomVibrate.setOnCheckedChangeListener { _, isChecked ->
            preferenceProvider.setNotificationVibrate(isChecked)
        }
        mBinding.sbCustomSound.setOnCheckedChangeListener { _, isChecked ->
            preferenceProvider.setNotificationSound(isChecked)
        }

        mBinding.sbUrgentSound.setOnCheckedChangeListener { _, isChecked ->
            preferenceProvider.setMentionNotificationSound(isChecked)
        }
        mBinding.sbUrgentVibrate.setOnCheckedChangeListener { _, isChecked ->
            preferenceProvider.setMentionNotificationVibrate(isChecked)
        }
        mBinding.layoutRingtone.setOnClickListener {
            mSelectingUrgentRingtone = false
            pickRingtone()
        }

        mBinding.layoutUrgentRingtone.setOnClickListener {
            mSelectingUrgentRingtone = true
            pickRingtone()
        }


        return mBinding.root
    }

    private fun resetNotificationService() {
        mApp?.forceStopImService()
        mApp?.startImService()
    }

    private fun getCheckedNotificationType(): Int {
        return when (preferenceProvider.getNotificationState()) {
            RoomNotificationState.ALL_MESSAGES -> R.id.rb_when_to_notify_anytime
            RoomNotificationState.MENTIONS_ONLY -> R.id.rb_noti_only_mentioned
            else -> 0
        }

    }

    private fun getRingtoneNameFromUri(ringtoneUri: Uri): String {
        val ringtone = RingtoneManager.getRingtone(context, ringtoneUri)
        return ringtone?.getTitle(context) ?: ""
    }

    private fun pickRingtone() {
        val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
        intent.putExtra(
            RingtoneManager.EXTRA_RINGTONE_TITLE,
            getString(R.string.notification_ringtone_title)
        )
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM)
        mLaunchRingtonePickerIntent.launch(intent)
    }


}