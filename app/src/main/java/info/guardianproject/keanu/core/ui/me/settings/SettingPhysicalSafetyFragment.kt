package info.guardianproject.keanu.core.ui.me.settings

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import `in`.myinnos.library.AppIconNameChanger
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentSettingPhysicalSafetyBinding
import info.guardianproject.keanu.core.ui.me.settings.camouflage.AppIconChooserActivity

class SettingPhysicalSafetyFragment : Fragment() {

    private lateinit var mBinding: FragmentSettingPhysicalSafetyBinding
    companion object {
        val disguisePackage = listOf(
            "info.guardianproject.keanu.core.RouterActivity",
            "org.article19.circulo.next.main.MainActivitySettings",
            "org.article19.circulo.next.main.MainActivityCamera",
            "org.article19.circulo.next.main.Firefox",
            "org.article19.circulo.next.main.Messenger",
            "org.article19.circulo.next.main.Navigator",
            "org.article19.circulo.next.main.Skype",
            "org.article19.circulo.next.main.Telegram",
            "org.article19.circulo.next.main.WhatsApp",
            "org.article19.circulo.next.main.Youtube"
        )
    }
    private lateinit var preferenceProvider: PreferenceProvider

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentSettingPhysicalSafetyBinding.inflate(inflater, container, false)
        preferenceProvider = PreferenceProvider(requireActivity())

        (activity as SettingsActivity).setBackButtonText(getString(R.string.profile_physical_safety))
        mBinding.layoutChangeIcon.setOnClickListener {
            startActivity(Intent(context, AppIconChooserActivity::class.java))
        }
        val enableDisguise = preferenceProvider.isEnableDisguise()
        mBinding.appIconSwitch.isChecked = enableDisguise
        mBinding.layoutChangeIcon.isEnabled = enableDisguise
        mBinding.appIconSwitch.isChecked = preferenceProvider.isEnableDisguise()
        enableLayoutChangeIcon(enableDisguise)

        mBinding.appIconSwitch.setOnCheckedChangeListener { _, isChecked ->
            preferenceProvider.setEnableDisguise(isChecked)
            mBinding.layoutChangeIcon.isEnabled = isChecked
            enableLayoutChangeIcon(isChecked)

            if (!isChecked) {
                val activePackage = disguisePackage[0]
                val disablePackages: MutableList<String> = ArrayList()

                for (i in 1 until disguisePackage.size) {
                    disablePackages.add(disguisePackage[i])
                }

                setAppIconName(activePackage, disablePackages)

                preferenceProvider.disguiseId = 0
            }
        }
        return mBinding.root
    }

    private fun enableLayoutChangeIcon(enable: Boolean) {
        mBinding.layoutChangeIcon.isEnabled = enable
        if (!enable) {
            mBinding.layoutChangeIcon.alpha = 0.5f
        } else {
            mBinding.layoutChangeIcon.alpha = 1f
        }
    }

    private fun setAppIconName(activePackage: String, disablePackages: MutableList<String>) {
        AppIconNameChanger.Builder(activity)
            .activeName(activePackage) // String
            .disableNames(disablePackages) // List<String>
            .packageName(context?.applicationContext?.packageName)
            .build()
            .setNow()
    }


}