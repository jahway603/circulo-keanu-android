package info.guardianproject.keanu.core.ui.mention.member

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanuapp.databinding.AdapterSuggestedMentionBinding
import io.getstream.avatarview.coil.loadImage
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary

class MentionUserAdapter(val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class MentionViewModel(val binding: AdapterSuggestedMentionBinding) :
        RecyclerView.ViewHolder(binding.root)

    private var mUserClickListener: UserPresenter.UserClickListener? = null

    private var mUserList: List<RoomMemberSummary> = mutableListOf()

    open fun setData(data: List<RoomMemberSummary>) {
        mUserList = data
    }

    fun setUserClickListener(userClickListener: UserPresenter.UserClickListener) {
        mUserClickListener = userClickListener
    }

    override fun getItemCount(): Int {
        return mUserList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MentionViewModel {
        val binding = AdapterSuggestedMentionBinding
            .inflate(LayoutInflater.from(context), parent, false)
        return MentionViewModel(binding)
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MentionViewModel) {
            with(holder) {
                with(mUserList[position]) {
                    binding.userName.text = userId
                    binding.displayName.text = displayName
                    if (avatarUrl.isNullOrBlank()) {
                        binding.userImageView.avatarInitials =
                            displayName?.get(0).toString().uppercase()

                    } else {
                        val userImageUrl = GlideUtils.resolve(Uri.parse(avatarUrl), true)
                        binding.userImageView.loadImage(userImageUrl)
                        binding.userImageView.indicatorDrawable
                    }
                    binding.root.setOnClickListener {
                        mUserClickListener?.onUserClicked(
                            this
                        )
                    }
                }
            }
        }
    }
}