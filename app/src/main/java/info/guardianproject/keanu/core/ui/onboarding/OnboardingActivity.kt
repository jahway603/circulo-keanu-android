package info.guardianproject.keanu.core.ui.onboarding

import android.content.DialogInterface
import android.os.*
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import info.guardianproject.keanu.core.ImApp.Companion.resetLanguage
import info.guardianproject.keanu.core.ui.auth.AuthenticationActivity
import info.guardianproject.keanu.core.util.Languages
import info.guardianproject.keanu.core.util.extensions.getParcelableExtraCompat
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityOnboardingBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import org.matrix.android.sdk.internal.legacy.riot.HomeServerConnectionConfig
import java.io.File
import java.util.*

class OnboardingActivity : BaseActivity() {

    companion object {
        const val EXTRA_SHOW_SPLASH = "showSplash"
    }

    private var mShowSplash = true

    private lateinit var mBinding: ActivityOnboardingBinding

    // Supports upgrade from v2 to v3.
    private val mUpgradeCredentialsFile by lazy {
        File(filesDir, "migrate.props")
    }

    // Supports upgrade from v2 to v3.
    private var mIsAutoLogin = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mShowSplash = intent.getBooleanExtra(EXTRA_SHOW_SPLASH, true)

        mBinding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        if (mShowSplash) {
            supportActionBar?.hide()
        }
        else {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)
        }
        supportActionBar?.title = ""

        setAnimLeft()

        mBinding.flipViewMain.imageLogo.setOnClickListener {
            setAnimLeft()
            showOnboarding()
        }

        mBinding.flipViewMain.nextButton.setOnClickListener {
            setAnimLeft()
            showOnboarding()
        }

        mBinding.flipViewRegister.btnShowRegister.setOnClickListener {
            setAnimLeft()

            mRequestSignInUp.launch(mApp.router.authentication(this, isSignUp = true))
        }

        mBinding.flipViewRegister.btnShowLogin.setOnClickListener {
            setAnimLeft()

            mRequestSignInUp.launch(mApp.router.authentication(this))
        }

        // Set up language chooser button.
        mBinding.flipViewMain.languageButton.setOnClickListener {
            val languages = Languages.get(this@OnboardingActivity)
            val languagesAdapter = ArrayAdapter(this@OnboardingActivity,
                    android.R.layout.simple_list_item_single_choice, languages.allNames)

            AlertDialog.Builder(this@OnboardingActivity)
                    .setIcon(R.drawable.ic_settings_language)
                    .setTitle(R.string.KEY_PREF_LANGUAGE_TITLE)
                    .setAdapter(languagesAdapter) { dialog: DialogInterface, position: Int ->
                        val languageCodes = languages.supportedLocales
                        resetLanguage(this@OnboardingActivity, languageCodes[position])
                        dialog.dismiss()
                    }
                    .show()
        }

        if (!mShowSplash) {
            setAnimLeft()
            showOnboarding()
        }


        // Handle upgrade to Keanu v3 using Matrix SDK2 instead of old homegrown base:
        // Check, if a proper Matrix SDK1 `HomeServerConnectionConfig` is available.
        // If so, let the `LegacySessionImporter` use that to upgrade the old session
        // to Matrix SDK2.
        var credentialsList = listOf<HomeServerConnectionConfig>()

        // This can crash, if JSON is broken.
        try {
            credentialsList = LegacyLoginStorage(this).credentialsList
        }
        catch (e: Throwable) {
            // Ignore.
        }

        if (credentialsList.isNotEmpty()) {
            setAnimLeft()
            showWaiting()

            Handler(Looper.getMainLooper()).postDelayed({
                if (mApp.matrix.legacySessionImporter().process()) {

                    // Should we have both and successfully restored the old session,
                    // ignore the credentials file and delete it, to keep the user safe.
                    try {
                        mUpgradeCredentialsFile.delete()
                    }
                    catch (ignored: Throwable) {}

                    mApp.reinitMatrix()
                    showMainScreen(false)
                }
                else {
                    setAnimRight()
                    showSplashScreen()
                }
            }, 500)
        }
        // If not, check, if a credentials file is available. Use that to log the user in.
        // Forward to AuthenticationActivity. It should automatically log in and return.
        else if (mUpgradeCredentialsFile.exists()) {
            val credentials = Properties()
            credentials.load(mUpgradeCredentialsFile.inputStream())
            val userId = credentials.getProperty("u")
            val password = credentials.getProperty("p")

            if (userId.isNotEmpty() && password.isNotEmpty()) {
                mIsAutoLogin = true

                mRequestSignInUp.launch(mApp.router.authentication(this, userId = userId, password = password))
            }
        }
    }

    private fun setAnimLeft() {
        mBinding.root.inAnimation = AnimationUtils.loadAnimation(this, R.anim.push_left_in)
        mBinding.root.outAnimation = AnimationUtils.loadAnimation(this, R.anim.push_left_out)
    }

    private fun setAnimRight() {
        mBinding.root.inAnimation = AnimationUtils.loadAnimation(this, R.anim.push_right_in)
        mBinding.root.outAnimation = AnimationUtils.loadAnimation(this, R.anim.push_right_out)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_onboarding, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            showPrevious()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        showPrevious()
    }

    // Back button should bring us to the previous screen, unless we're on the first screen.
    private fun showPrevious() {
        setAnimRight()

        supportActionBar?.title = ""

        when (mBinding.root.currentView.id) {
            R.id.flipViewMain -> finish()

            R.id.flipViewRegister -> {
                if (mShowSplash) {
                    showSplashScreen()
                } else {
                    finish()
                }
            }
        }
    }

    private fun showSplashScreen() {
        setAnimRight()

        supportActionBar?.hide()
        supportActionBar?.title = ""

        mBinding.root.displayedChild = 0
    }

    private fun showOnboarding() {
        mBinding.root.displayedChild = 1
    }

    private fun showWaiting() {
        mBinding.root.displayedChild = 2
    }

    private fun showMainScreen(isNewAccount: Boolean) {
        startActivity(mApp.router.main(this, isFirstTime = isNewAccount))

        finish()
    }

    private var mRequestSignInUp = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode != RESULT_OK) return@registerForActivityResult

        result.data?.getParcelableExtraCompat(AuthenticationActivity.EXTRA_ONBOARDING_ACCOUNT,
            clazz = OnboardingAccount::class.java)?.let {
            showMainScreen(it.isNew)

            // Handle upgrade to Keanu v3 using Matrix SDK2 instead of old homegrown base:
            // Delete credentials file from upgrade after successful login.
            if (mIsAutoLogin) {
                try {
                    mUpgradeCredentialsFile.delete()
                }
                catch (ignored: Throwable) {}
            }
        }
    }
}