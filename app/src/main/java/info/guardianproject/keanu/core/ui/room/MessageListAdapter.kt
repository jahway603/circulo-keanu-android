package info.guardianproject.keanu.core.ui.room

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.service.RemoteImService
import info.guardianproject.keanu.core.ui.quickresponse.QuickResponseLongClickListener
import info.guardianproject.keanu.core.ui.widgets.MessageViewHolder
import info.guardianproject.keanu.core.ui.widgets.MessageViewHolder.OnImageClickedListener
import info.guardianproject.keanu.core.ui.widgets.StatusItemHolder
import info.guardianproject.keanu.core.ui.widgets.VerificationRequestItemHolder
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.extensions.betterMessage
import info.guardianproject.keanu.core.util.extensions.displayNameWorkaround
import info.guardianproject.keanu.core.util.extensions.getRelatedEventId
import info.guardianproject.keanu.core.util.extensions.isReaction
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.MessageViewLeftBinding
import info.guardianproject.keanuapp.databinding.MessageViewRightBinding
import info.guardianproject.keanuapp.databinding.StatusItemBinding
import info.guardianproject.keanuapp.databinding.VerificationRequestItemBinding
import info.guardianproject.keanuapp.ui.conversation.QuickReaction
import info.guardianproject.keanuapp.ui.widgets.MediaInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.session.content.ContentUploadStateTracker
import org.matrix.android.sdk.api.session.crypto.attachments.toElementToDecrypt
import org.matrix.android.sdk.api.session.crypto.model.EncryptedFileInfo
import org.matrix.android.sdk.api.session.crypto.model.MXEventDecryptionResult
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.LocalEcho
import org.matrix.android.sdk.api.session.events.model.isAttachmentMessage
import org.matrix.android.sdk.api.session.file.FileService
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.session.room.model.message.*
import org.matrix.android.sdk.api.session.room.read.ReadService
import org.matrix.android.sdk.api.session.room.timeline.Timeline
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.TimelineSettings
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import timber.log.Timber

open class MessageListAdapter(
    private val mActivity: AppCompatActivity,
    private val mView: RoomView,
    roomId: String,
    quickResponseLongClick: QuickResponseLongClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), OnImageClickedListener,
    ContentUploadStateTracker.UpdateListener {

    var lastSelectedView: View? = null
        private set

    private val mApp
        get() = mActivity.application as? ImApp

    private val mSession
        get() = mApp?.matrixSession

    private val mRoom = mSession?.roomService()?.getRoom(roomId)

    private val mTimeline = mRoom?.timelineService()?.createTimeline(null, TimelineSettings(50, true))

    private var mTimelineIds = mutableListOf<String>()

    private var mIsBackPage = false

    private val mHandler = Handler(Looper.getMainLooper())

    private var mShowProgressOnPosition: Int? = null
    private var mProgressCurrent: Long = 0
    private var mProgressTotal: Long = 0

    private val mDownloadedDecryptedFiles = HashMap<Int, MediaInfo>()

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler((mActivity as? RoomActivity)?.root))
    }


    private val tListener = object : Timeline.Listener {

        override fun onTimelineUpdated(snapshot: List<TimelineEvent>) {
            mHandler.post {
                val oldIds = mTimelineIds

                mTimelineIds = snapshot
                    .filter(RemoteImService.filterEvents)
                    .map { it.eventId }
                    .reversed() // `snapshot` is newest first, but `RecyclerView` is newest at bottom. -> Reverse!
                    .toMutableList()

                while (mTimelineIds.size < oldIds.size)
                    mTimelineIds.add("-1")

                // First, remove all old events, which aren't available in the new list anymore.
                for (i in oldIds.indices) {
                    if (mTimelineIds.indexOf(oldIds[i]) < 0) {
                        notifyItemRemoved(i)
                    }
                }

                // Then, add and update the others, if needed.
                for (i in mTimelineIds.indices) {
                    val oldI = oldIds.indexOf(mTimelineIds[i])

                    if (oldI < 0) {
                        val te = getTimelineEvent(i)

                        // When new reactions come in, their related event needs to get updated.
                        // Reactions on their own are never rendered.
                        if (te?.isReaction() == true) {
                            val relatedEventId = te.getRelatedEventId()

                            if (relatedEventId != null) {
                                val relatedI = oldIds.indexOf(relatedEventId)

                                if (relatedI > -1) notifyItemChanged(relatedI)
                            }
                        } else {
                            notifyItemInserted(i)
                        }

                        mCoroutineScope.launch {
                            te?.eventId?.let { mRoom?.readService()?.setReadReceipt(it, ReadService.THREAD_ID_MAIN) }
                        }

                    } else if (oldI != i) {
                        notifyItemMoved(oldI, i)
                    }
                }

                //If at bottom, then jump to new message.
                if (mIsBackPage && mTimelineIds.size > 20) {
                    mView.jumpToPosition(20)
                } else if (mView.isAtBottom() == false) {
                    jumpToFirstUnread()
                }

                mIsBackPage = false
            }


        }

        override fun onTimelineFailure(throwable: Throwable) {
            Timber.d(throwable)

            // This is what Element does here, too, so might be a good idea for us, too.
            mTimeline?.restartWithEventId(null)
        }

        override fun onNewTimelineEvents(eventIds: List<String>) {
            // No reason to notify here, since onTimelineUpdated is always called right after this.

            mHandler.post {

                for (eventId in eventIds) {
                    trackProgressIfOngoingUpload(eventId)
                }

            }

        }

        private fun jumpToFirstUnread() {

            var firstUnread = 0
            var firstUnreadId = ""

            for (i in mTimelineIds.indices) {
                if (mRoom?.readService()?.isEventRead(mTimelineIds[i]) == true) {
                    break
                } else {
                    firstUnread = i
                    firstUnreadId = mTimelineIds[i]
                }
            }

            // #isEventRead seems to be continuously off by one.
            firstUnread = (firstUnread - 1).coerceAtLeast(0)

            // List is displayed inverted! (Position 0 at bottom!)
            mView.jumpToPosition(mTimelineIds.size - firstUnread)

            if (mTimelineIds.isEmpty()) return

            mCoroutineScope.launch {
                mRoom?.readService()?.setReadMarker(firstUnreadId)
            }

        }

        private fun trackProgressIfOngoingUpload(eventId: String) {

            val te = mRoom?.timelineService()?.getTimelineEvent(eventId)

            if ((te?.eventId?.let { LocalEcho.isLocalEchoId(it) } == true
                        && te.root.senderId == mSession?.myUserId)
                && te.root.isAttachmentMessage()
            ) {
                mSession?.contentUploadProgressTracker()?.track(te.eventId, this@MessageListAdapter)
            }
        }
    }

    private var quickResponseLongClick: QuickResponseLongClickListener? = null
    private var roomUsers: Map<String?, RoomMemberSummary>? = null


    init {
        setHasStableIds(true)
        this.quickResponseLongClick = quickResponseLongClick
        this.roomUsers = getRoomUsersMap()

        mTimeline?.getSnapshot()?.let { tListener.onTimelineUpdated(it) }

        startListening()

    }

    private fun getRoomUsersMap(): Map<String?, RoomMemberSummary>? {
        val builder = RoomMemberQueryParams.Builder()
        builder.memberships = arrayListOf(Membership.JOIN)
        builder.excludeSelf = true
        val query = builder.build()
        return mRoom?.membershipService()?.getRoomMembers(query)
            ?.associateBy({ it.displayNameWorkaround }, { it })

    }

    fun startListening() {
        mTimeline?.addListener(tListener)
        mTimeline?.start()
    }

    fun stopListening() {
        mTimeline?.removeListener(tListener)
    }

    fun dispose() {
        mTimeline?.dispose()
    }

    fun pageBack() {
        if (mTimeline?.hasMoreToLoad(Timeline.Direction.BACKWARDS) == true) {
            mIsBackPage = true
            mTimeline.paginate(Timeline.Direction.BACKWARDS, 20)
        }
    }

    override fun getItemId(position: Int): Long {
        return getTimelineEvent(position)?.localId ?: 0
    }

    override fun getItemCount(): Int {
        return mTimelineIds.size
    }

    enum class ViewType {
        INBOUND_MESSAGE, OUTBOUND_MESSAGE, STATUS, VERIFICATION_REQUEST
    }

    override fun getItemViewType(position: Int): Int {
        val event = getTimelineEvent(position)

        return when (event?.root?.type) {
            EventType.MESSAGE, EventType.ENCRYPTED, EventType.REDACTION -> {
                when {
                    event.getLastMessageContent()?.msgType == MessageType.MSGTYPE_VERIFICATION_REQUEST ->
                        ViewType.VERIFICATION_REQUEST.ordinal

                    event.root.senderId != mSession?.myUserId -> ViewType.INBOUND_MESSAGE.ordinal

                    else -> ViewType.OUTBOUND_MESSAGE.ordinal
                }
            }

            else -> ViewType.STATUS.ordinal
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val vh = when (ViewType.values()[viewType]) {

            ViewType.INBOUND_MESSAGE -> {
                MessageViewHolder(MessageViewLeftBinding.inflate(inflater, parent, false))
            }

            ViewType.OUTBOUND_MESSAGE -> {
                MessageViewHolder(MessageViewRightBinding.inflate(inflater, parent, false))
            }

            ViewType.VERIFICATION_REQUEST -> {
                VerificationRequestItemHolder(
                    VerificationRequestItemBinding.inflate(
                        inflater,
                        parent,
                        false
                    )
                )
            }

            else -> {
                StatusItemHolder(StatusItemBinding.inflate(inflater, parent, false))
            }
        }

        if (vh is MessageViewHolder) {
            vh.onImageClickedListener = this
        }

        return vh
    }

    private val mThumbnails = HashMap<Int, MediaInfo>()

    override fun onBindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        val te = getTimelineEvent(position)
        val mc = te?.getLastMessageContent()
        var attachment: Uri? = null
        var thumbnail: Uri? = null

        if (mc is MessageWithAttachmentContent) {
            // Trigger download and decryption, if not available, yet.
            if (!mDownloadedDecryptedFiles.containsKey(position) && mc.getFileUrl()
                    ?.isNotEmpty() == true
            ) {
                val fileState = mSession?.fileService()?.fileState(mc)

                var fileName: String = mc.getFileName()
                var url: String? = null
                var mimeType: String? = null
                var info: EncryptedFileInfo? = null

                if (fileState != FileService.FileState.Downloading) {
                    mCoroutineScope.launch {
                        mimeType = mc.mimeType
                        url = mc.getFileUrl()
                        info = mc.encryptedFileInfo

                        if (url?.startsWith("content") == true) {
                            mDownloadedDecryptedFiles[position] =
                                MediaInfo(Uri.parse(url), mimeType)

                            (mActivity as? RoomActivity)?.historyView?.post {
                                notifyItemChanged(position)
                            }
                        } else if (url?.startsWith("mxc") == true) {
                            val fileMedia = mSession?.fileService()?.downloadFile(
                                fileName, mimeType, url, info?.toElementToDecrypt()
                            )

                            mDownloadedDecryptedFiles[position] =
                                MediaInfo(Uri.fromFile(fileMedia), mimeType)

                            (mActivity as? RoomActivity)?.historyView?.post {
                                notifyItemChanged(position, fileMedia)
                            }
                        }
                    }
                }

                when (mc) {
                    is MessageImageContent -> {
                        url = mc.info?.thumbnailUrl ?: mc.info?.thumbnailFile?.url
                        mimeType = mc.info?.thumbnailInfo?.mimeType
                        info = mc.info?.thumbnailFile
                        fileName = "thumb" + mc.getFileName()
                    }

                    is MessageStickerContent -> {
                        url = mc.info?.thumbnailUrl ?: mc.info?.thumbnailFile?.url
                        mimeType = mc.info?.thumbnailInfo?.mimeType
                        info = mc.info?.thumbnailFile
                        fileName = "thumb" + mc.getFileName()
                    }

                    is MessageVideoContent -> {
                        url = mc.videoInfo?.thumbnailUrl ?: mc.videoInfo?.thumbnailFile?.url
                        mimeType = mc.videoInfo?.thumbnailInfo?.mimeType
                        info = mc.videoInfo?.thumbnailFile
                        fileName = "thumb" + mc.getFileName()
                    }

                    is MessageFileContent -> {
                        if (mc.info?.mimeType?.contains("video", true) == true
                        ) {
                            url = mc.url
                            fileName = "thumb" + mc.getFileName()
                        } else if (mc.info?.mimeType?.contains("octet-stream", true) == true) {
                            url = mc.url
                        }
                    }
                }

                if (url?.isNotEmpty() == true) {
                    val fileThumbState = mSession?.fileService()
                        ?.fileState(url, mc.getFileName(), mimeType, info?.toElementToDecrypt())

                    if (fileThumbState != FileService.FileState.Downloading) {
                        mCoroutineScope.launch {
                            if (url?.startsWith("content") == true) {
                                mThumbnails[position] = MediaInfo(Uri.parse(url), mimeType)

                                (mActivity as? RoomActivity)?.historyView?.post {
                                    notifyItemChanged(position)
                                }
                            } else if (url?.startsWith("mxc") == true) {
                                val fileMedia = mSession?.fileService()?.downloadFile(
                                    fileName,
                                    mimeType,
                                    url,
                                    info?.toElementToDecrypt()
                                )

                                mThumbnails[position] = MediaInfo(Uri.fromFile(fileMedia), mimeType)

                                (mActivity as? RoomActivity)?.historyView?.post {
                                    notifyItemChanged(position, fileMedia)
                                }
                            }
                        }
                    }
                }
            } else {
                attachment = mDownloadedDecryptedFiles[position]?.uri
                thumbnail = mThumbnails[position]?.uri
            }
        }

        val reactions = HashMap<String, QuickReaction>()

        for (reaction in te?.annotations?.reactionsSummary ?: emptyList()) {
            val qr = reactions[reaction.key] ?: QuickReaction(reaction.key, ArrayList())
            if (reaction.addedByMe) qr.sentByMe = true

            for (eventId in reaction.sourceEvents) {
                mRoom?.timelineService()?.getTimelineEvent(eventId)?.senderInfo?.userId?.let {
                    qr.senders.add(it)
                }
            }

            reactions[reaction.key] = qr
        }

        val event = te?.root

        if (event?.isEncrypted() == true) {
            val timelineId = mTimeline?.timelineID
            val messageMap = event.getClearContent()

            if (timelineId != null && (messageMap?.get("body") as? String).isNullOrEmpty()) {
                mSession?.cryptoService()?.decryptEventAsync(
                    event,
                    timelineId,
                    object : MatrixCallback<MXEventDecryptionResult> {

                        override fun onSuccess(data: MXEventDecryptionResult) {
                            (mActivity as? RoomActivity)?.historyView?.post {
                                notifyItemChanged(position)
                            }
                        }

                        override fun onFailure(failure: Throwable) {
                            Timber.d(failure)
                        }
                    })
            }
        }

        when (viewHolder) {
            is MessageViewHolder -> {
                val isIncoming = event?.senderId != mSession?.myUserId

                val contextMenuView = when {

                    viewHolder.container != null -> viewHolder.container

                    else -> viewHolder.itemView
                }
                contextMenuView?.setOnLongClickListener {
                    if (te != null) {
                        quickResponseLongClick?.onQuickResponseClick(
                            timeline = te,
                            attachment,
                            thumbnail
                        )
                    }
                    true
                }

                if (te != null) {
                    viewHolder.bind(
                        te, null, attachment, thumbnail, reactions.values.toList(),
                        mRoom?.roomCryptoService()?.isEncrypted() == true,
                        users = roomUsers!!
                    )

                    if (isIncoming) {
                        viewHolder.avatar?.setOnClickListener {
                            mActivity.startActivity(
                                mApp?.router?.friend(
                                    mActivity,
                                    te.senderInfo.userId
                                )
                            )
                        }
                    }

                    viewHolder.mediaThumbnail?.setOnLongClickListener {
                        quickResponseLongClick?.onQuickResponseClick(
                            timeline = te,
                            attachment,
                            thumbnail
                        )
                        true
                    }
                }
            }

            is VerificationRequestItemHolder -> {
                if (te != null) {
                    viewHolder.bind(te)
                }
            }

            is StatusItemHolder -> {
                if (te != null) {
                    viewHolder.bind(te)
                }
            }
        }
    }

    fun forwardMediaFile() {
        //need to use
    }

    override fun onImageClicked(image: Uri) {
        if (mActivity is RoomActivity) {
            val uris = ArrayList<Uri>()
            val mimeTypes = ArrayList<String>()

            mDownloadedDecryptedFiles.filter { it.value.isImage }.keys.sortedDescending()
                .forEach { i ->
                    mDownloadedDecryptedFiles[i]?.let { it ->
                        uris.add(it.uri)
                        mimeTypes.add(it.mimeType)
                    }
                }

            mActivity.requestImageView.launch(
                mApp?.router?.imageView(
                    mActivity, uris, mimeTypes,
                    0.coerceAtLeast(uris.indexOf(image)), showResend = true
                )
            )
        }
    }

    /**
    override fun onQuickReactionClicked(quickReaction: QuickReaction, eventId: String) {
    mView.sendQuickReaction(quickReaction.reaction, eventId)
    }**/

    override fun onUpdate(state: ContentUploadStateTracker.State) {
        when (state) {
            ContentUploadStateTracker.State.Idle -> {
                unsetProgress()
            }

            ContentUploadStateTracker.State.EncryptingThumbnail -> {
                setProgress()
            }

            ContentUploadStateTracker.State.CompressingImage -> {
                setProgress()
            }

            is ContentUploadStateTracker.State.CompressingVideo -> {
                setProgress((state.percent * 10).toLong(), 1000)
            }

            is ContentUploadStateTracker.State.UploadingThumbnail -> {
                setProgress(state.current, state.total)
            }

            is ContentUploadStateTracker.State.Encrypting -> {
                setProgress(state.current, state.total)
            }

            is ContentUploadStateTracker.State.Uploading -> {
                setProgress(state.current, state.total)
            }

            ContentUploadStateTracker.State.Success -> {
                unsetProgress()
            }

            is ContentUploadStateTracker.State.Failure -> {
                (mActivity as? RoomActivity)?.let {
                    Snackbar.make(
                        it.root,
                        mActivity.getString(R.string.error_prefix) + state.throwable.betterMessage,
                        Snackbar.LENGTH_LONG
                    )
                        .show()
                }

                unsetProgress()
            }
        }
    }

    fun notifyItemChanged(eventId: String?) {
        if (eventId == null) return

        val idx = mTimelineIds.indexOf(eventId)
        if (idx < 0) return

        notifyItemChanged(idx)
    }

    private fun setProgress(current: Long = 0, total: Long = 0) {
        val pos = mShowProgressOnPosition ?: return

        mProgressCurrent = current
        mProgressTotal = total

        notifyItemChanged(pos)
    }

    private fun unsetProgress() {
        val pos = mShowProgressOnPosition ?: return

        mShowProgressOnPosition = null

        getTimelineEvent(pos)?.eventId?.let {
            mSession?.contentUploadProgressTracker()?.untrack(it, this)
        }

        notifyItemChanged(pos)
    }

    /**
     * [TimelineEvent]s are stored upside-down: newest first, but displayed newest at bottom.
     *
     * This will give back the correct [TimelineEvent] for a given [RecyclerView] position.
     *
     * @return
     *      the correct [TimelineEvent] for the given position.
     */
    fun getTimelineEvent(position: Int): TimelineEvent? {
        // Don't use `mTimeline?.getTimelineEventWithId(eventId)`, it won't show unsent events!
        return mRoom?.timelineService()?.getTimelineEvent(mTimelineIds[position])
    }
}

