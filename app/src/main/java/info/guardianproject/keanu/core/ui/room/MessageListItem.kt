package info.guardianproject.keanu.core.ui.room

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.AttributeSet
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.content.FileProvider
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.widgets.MessageViewHolder
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.R
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.lang.ref.WeakReference

class MessageListItem(context: Context, attrs: AttributeSet?) : RelativeLayout(context, attrs) {

    val attachment: Uri?
        get() = mHolder?.get()?.attachment

    val mimeType: String?
        get() = mHolder?.get()?.mimeType

    val eventId: String?
        get() = mHolder?.get()?.eventId

    val message: String?
        get() = mHolder?.get()?.message

    private val mApp
        get() = context.applicationContext as? ImApp

    private var mHolder: WeakReference<MessageViewHolder>? = null

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        mHolder?.get()?.audioWife?.pause()
    }

    fun bind(holder: MessageViewHolder) {
        mHolder = WeakReference(holder)
    }

    fun nearbyMediaFile() {
        val mimeType = mimeType ?: return
        val attachment = attachment ?: return

        val sharePath = Uri.parse("vfs:/" + attachment.path)

       // context.startActivity(mApp?.router?.nearbyShare(context, sharePath, mimeType))
    }

    fun forwardMediaFile() {
        val currentMimeType = mimeType
        val currentAttachment = attachment

        if (currentMimeType != null && currentAttachment != null) {
            val exportPath = SecureMediaStore.exportPath(context, currentMimeType, currentAttachment, false)

            SecureMediaStore.exportContent(mimeType, currentAttachment, exportPath)

            val uriShare = FileProvider.getUriForFile(
                context,
                context.packageName + ".provider",
                exportPath
            )

            val intent = mApp?.router?.router(context)
            intent?.action = Intent.ACTION_SEND

            if (mimeType != null && attachment != null) {
                intent?.setDataAndTypeAndNormalize(uriShare, mimeType)
            } else {
                intent?.putExtra(Intent.EXTRA_TEXT, message)
                intent?.setTypeAndNormalize("text/plain")
            }

            context.startActivity(intent)
        }
    }

    fun exportMediaFile() {
        val currentMimeType = mimeType
        val currentAttachment = attachment

        if (currentMimeType != null && currentAttachment != null) {
            val exportPath = SecureMediaStore.exportPath(context, currentMimeType, currentAttachment, true)
            exportMediaFile(currentMimeType, currentAttachment, exportPath)
            Timber.v("ExportPath messageList==$exportPath")
        }
        else {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_TEXT, message)
            shareIntent.type = "text/plain"

            context.startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.export_media)))
        }
    }

    fun downloadMediaFile() {
        val currentMimeType = mimeType
        val currentAttachment = attachment

        if (currentMimeType != null && currentAttachment != null) {
            val exportPath =
                SecureMediaStore.exportPath(context, currentMimeType, currentAttachment,true)
            try {
                SecureMediaStore.exportContent(
                    mimeType,
                    currentAttachment,
                    exportPath
                )
                Toast.makeText(
                    context,
                    "$exportPath",
                    Toast.LENGTH_LONG
                ).show()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    }

    private fun exportMediaFile(mimeType: String, mediaUri: Uri, exportPath: File) {
        try {
            SecureMediaStore.exportContent(mimeType, mediaUri, exportPath)

            val uriShare = FileProvider.getUriForFile(context, context.packageName + ".provider", exportPath)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_STREAM, uriShare)
            shareIntent.type = mimeType

            context.startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.export_media)))
        }
        catch (e: Exception) {
            Toast.makeText(context, "Export Failed " + e.message, Toast.LENGTH_LONG).show()
            Timber.e(e)
        }
    }
}