package info.guardianproject.keanu.core.ui.widgets

import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanuapp.R

/**
 * Created by n8fr8 on 8/10/15.
 */
open class MediaViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var mediaThumbnail: ImageView? = view.findViewById(R.id.media_thumbnail)

    var videoIcon: ImageView? = view.findViewById(R.id.video_icon)
    var gallerySender :TextView? = view.findViewById(R.id.gallerySenderTextView)
    var galleryTime :TextView? = view.findViewById(R.id.galleryTimeTextView)
    var videoDuration :TextView? = view.findViewById(R.id.videoDurationTextView)

    var mediaPlay: ImageView? = view.findViewById(R.id.media_thumbnail_play)

    var progressBar: ProgressBar? = view.findViewById(R.id.progress)

    var container: ViewGroup? = view.findViewById(R.id.message_container)

    var attachment: Uri? = null
        protected set

    var mimeType: String = ""
        protected set
}