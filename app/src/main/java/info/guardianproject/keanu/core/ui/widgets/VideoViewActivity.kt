package info.guardianproject.keanu.core.ui.widgets

import android.Manifest
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.TransferListener
import info.guardianproject.keanu.core.util.ActivityCompatUtils
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityVideoViewerBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.viewmodel.gallery.MediaProcessViewModel
import java.io.*
import java.util.*


class VideoViewActivity : BaseActivity() {

    private var mShowResend = false

    private var mMediaUri: Uri? = null

    private var mMimeType: String? = null

    private lateinit var mExoPlayer: ExoPlayer

    private lateinit var mBinding: ActivityVideoViewerBinding
    private val REQUEST_CODE_READ_WRITE_EXTERNAL_STORAGE = 104

    private lateinit var mMediaProcessViewModel: MediaProcessViewModel

    private var mOpenDocumentTreeResult = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            val baseDocumentTreeUri = result.data?.data
            val takeFlags =
                Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION

            // take persistable Uri Permission for future use
            if (baseDocumentTreeUri != null) {
                contentResolver.takePersistableUriPermission(baseDocumentTreeUri, takeFlags)
                mMediaProcessViewModel.saveVideoFile(mMediaUri, baseDocumentTreeUri, mMimeType)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mMediaProcessViewModel = ViewModelProvider(this)[MediaProcessViewModel::class.java]

        mBinding = ActivityVideoViewerBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mMediaUri = intent.data
        mMimeType = intent.type
        mShowResend = intent.getBooleanExtra("showResend", false)

        supportActionBar?.show()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        title = mMediaUri?.lastPathSegment

        mExoPlayer = ExoPlayer.Builder(this).build()
        mBinding.root.player = mExoPlayer

        observeLiveData()
    }

    override fun onResume() {
        super.onResume()

        val item = mMediaUri?.let { MediaItem.fromUri(it) }
        if (item != null) mExoPlayer.setMediaItem(item)

        mExoPlayer.prepare()
    }

    override fun onPause() {
        super.onPause()

        mExoPlayer.stop()
        mExoPlayer.release()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_message_context, menu)

        menu.findItem(R.id.menu_message_copy).isVisible = false
        menu.findItem(R.id.menu_message_resend).isVisible = mShowResend
        menu.findItem(R.id.menu_message_delete).isVisible = false

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }

            R.id.menu_message_forward -> {
                forwardMediaFile()
                return true
            }

            R.id.menu_message_share -> {
                exportMediaFile()
                return true
            }

            R.id.menu_message_resend -> {
                resendMediaFile()
                return true
            }

            R.id.menu_downLoad -> {
                if (ActivityCompatUtils.hasPermission(this, WRITE_EXTERNAL_STORAGE)
                    && ActivityCompatUtils.hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    mOpenDocumentTreeResult.launch(Intent(Intent.ACTION_OPEN_DOCUMENT_TREE))
                } else {
                    ActivityCompatUtils.requestReadWriteExternalStorage(this@VideoViewActivity,
                        REQUEST_CODE_READ_WRITE_EXTERNAL_STORAGE)
                }
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun checkPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) return true
        if (!ActivityCompatUtils.hasPermission(this, WRITE_EXTERNAL_STORAGE)) {
            ActivityCompatUtils.requestWriteExternalStorage(this, 1)
            return false
        }
        return true
    }

    private fun exportMediaFile() {
        val mimeType = mMimeType ?: return
        val mediaUri = mMediaUri ?: return

        if (checkPermissions()) {
            val exportPath = SecureMediaStore.exportPath(this, mimeType, mediaUri, true)
            val uri = FileProvider.getUriForFile(this, "${this.packageName}.provider", exportPath)
            startActivity(mApp.router.router(this, uri, mMimeType))
        }
    }

    private fun forwardMediaFile() {
        startActivity(mApp.router.router(this, mMediaUri, mMimeType))
    }

    private fun resendMediaFile() {
        val intentResult = Intent()
        intentResult.putExtra("resendImageUri", mMediaUri)
        intentResult.putExtra("resendImageMimeType", mMimeType)
        setResult(RESULT_OK, intentResult)
        finish()
    }

    class InputStreamDataSource(private val context: Context, private val dataSpec: DataSpec) :
        DataSource {
        private var inputStream: InputStream? = null
        private var bytesRemaining: Long = 0
        private var opened = false
        override fun addTransferListener(transferListener: TransferListener) {}

        @Throws(IOException::class)
        override fun open(dataSpec: DataSpec): Long {
            try {
                inputStream = convertUriToInputStream(context, dataSpec.uri)
                if (inputStream != null) {
                    val skipped = inputStream!!.skip(dataSpec.position)
                    if (skipped < dataSpec.position) throw EOFException()
                    if (dataSpec.length != C.LENGTH_UNSET.toLong()) {
                        bytesRemaining = dataSpec.length
                    } else {
                        bytesRemaining = inputStream!!.available().toLong()
                        if (bytesRemaining == Int.MAX_VALUE.toLong()) bytesRemaining =
                            C.LENGTH_UNSET.toLong()
                    }
                } else {
                    opened = false
                    return -1
                }
            } catch (e: IOException) {
                throw IOException(e)
            }
            opened = true
            return bytesRemaining
        }

        @Throws(IOException::class)
        override fun read(buffer: ByteArray, offset: Int, readLength: Int): Int {
            if (readLength == 0) {
                return 0
            } else if (bytesRemaining == 0L) {
                return C.RESULT_END_OF_INPUT
            }
            val bytesRead = try {
                val bytesToRead = if (bytesRemaining == C.LENGTH_UNSET.toLong()) {
                    readLength
                } else {
                    bytesRemaining.coerceAtMost(readLength.toLong()).toInt()
                }

                inputStream!!.read(buffer, offset, bytesToRead)
            } catch (e: IOException) {
                throw IOException(e)
            }
            if (bytesRead == -1) {
                if (bytesRemaining != C.LENGTH_UNSET.toLong()) {
                    throw IOException(EOFException())
                }
                return C.RESULT_END_OF_INPUT
            }
            if (bytesRemaining != C.LENGTH_UNSET.toLong()) {
                bytesRemaining -= bytesRead.toLong()
            }
            return bytesRead
        }

        override fun getUri(): Uri {
            return dataSpec.uri
        }

        override fun getResponseHeaders(): MutableMap<String, MutableList<String>> {
            return mutableMapOf()
        }

        @Throws(IOException::class)
        override fun close() {
            try {
                inputStream?.close()
            } finally {
                inputStream = null
                opened = false
            }
        }

        private fun convertUriToInputStream(context: Context, mediaUri: Uri): InputStream? {
            //Your implementation of obtaining InputStream from mediaUri
            return if (mediaUri.scheme == null || mediaUri.scheme == "vfs") {
                try {
                    FileInputStream(mediaUri.path)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    null
                }
            } else {
                try {
                    context.contentResolver.openInputStream(mediaUri)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    null
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_READ_WRITE_EXTERNAL_STORAGE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mOpenDocumentTreeResult.launch(Intent(Intent.ACTION_OPEN_DOCUMENT_TREE))
            } else {
                // Permission Denied
                Toast.makeText(this@VideoViewActivity, getString(R.string.permission_denied), Toast.LENGTH_SHORT)
                    .show()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun observeLiveData() {
        mMediaProcessViewModel.observableSaveMediaLiveData.observe(this) {
            if (!it.isNullOrBlank()) {
                showToast(getString(R.string.msg_download_video, it))
            } else {
                showToast(getString(R.string.msg_download_video_error))
            }
        }
    }
}