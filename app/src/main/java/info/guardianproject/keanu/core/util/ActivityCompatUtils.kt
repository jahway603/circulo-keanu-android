package info.guardianproject.keanu.core.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

object ActivityCompatUtils {
    fun requestReadWriteExternalStorage(activity: Activity, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //Must request granular media permissions from Android 13.
            //Reference: https://developer.android.com/about/versions/13/behavior-changes-13#granular-media-permissions
            ActivityCompat.requestPermissions(activity,
                arrayOf(Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO),
                requestCode
            )
        } else {
            ActivityCompat.requestPermissions(activity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                requestCode
            )
        }
    }

    fun requestWriteExternalStorage(activity: Activity, requestCode: Int) {
        ActivityCompat.requestPermissions(activity,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            requestCode
        )
    }

    fun requestReadExternalStorage(activity: Activity, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //Must request granular media permissions from Android 13.
            //Reference: https://developer.android.com/about/versions/13/behavior-changes-13#granular-media-permissions
            ActivityCompat.requestPermissions(activity,
                arrayOf(Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO),
                requestCode
            )
        } else {
            ActivityCompat.requestPermissions(activity,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                requestCode
            )
        }
    }

    fun hasPermission(context: Context, permission: String): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            when (permission) {
                Manifest.permission.WRITE_EXTERNAL_STORAGE -> true
                Manifest.permission.READ_EXTERNAL_STORAGE -> {
                    (hasPermission(context, Manifest.permission.READ_MEDIA_IMAGES)
                            && hasPermission(context, Manifest.permission.READ_MEDIA_VIDEO))
                }

                else -> ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
            }
        } else {
            ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
        }
    }

    fun shouldShowRequestPermissionRationale(activity: Activity, permission: String): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            when (permission) {
                Manifest.permission.READ_EXTERNAL_STORAGE -> {
                    (shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_MEDIA_IMAGES)
                            && shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_MEDIA_VIDEO))
                }

                else -> ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)
            }
        } else {
            ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)
        }
    }
}