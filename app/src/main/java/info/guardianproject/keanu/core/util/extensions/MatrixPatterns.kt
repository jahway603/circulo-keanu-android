package info.guardianproject.keanu.core.util.extensions

import org.matrix.android.sdk.api.MatrixPatterns

fun MatrixPatterns.extractUserName(matrixId: String?): String? {
    if (matrixId.isNullOrEmpty()) return null

    return "@([A-Z0-9\\x21-\\x39\\x3B-\\x7F]+):".toRegex(RegexOption.IGNORE_CASE).find(matrixId)?.groupValues?.getOrNull(1)
}