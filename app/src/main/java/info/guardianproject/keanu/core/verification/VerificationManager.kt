package info.guardianproject.keanu.core.verification

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.service.StatusBarNotifier
import info.guardianproject.keanu.core.ui.PasswordDialogFragment
import info.guardianproject.keanuapp.R
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.auth.UIABaseAuth
import org.matrix.android.sdk.api.auth.UserInteractiveAuthInterceptor
import org.matrix.android.sdk.api.auth.UserPasswordAuth
import org.matrix.android.sdk.api.auth.data.LoginFlowTypes
import org.matrix.android.sdk.api.auth.registration.RegistrationFlowResponse
import org.matrix.android.sdk.api.auth.registration.nextUncompletedStage
import org.matrix.android.sdk.api.session.crypto.crosssigning.DeviceTrustLevel
import org.matrix.android.sdk.api.session.crypto.keysbackup.*
import org.matrix.android.sdk.api.session.crypto.model.CryptoDeviceInfo
import org.matrix.android.sdk.api.session.crypto.model.ImportRoomKeysResult
import org.matrix.android.sdk.api.session.crypto.verification.PendingVerificationRequest
import org.matrix.android.sdk.api.session.crypto.verification.VerificationMethod
import org.matrix.android.sdk.api.session.crypto.verification.VerificationService
import org.matrix.android.sdk.api.session.crypto.verification.VerificationTransaction
import java.lang.ref.WeakReference
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume

class NoBackupFoundException : Exception("No backup could be found to restore from!")

@SuppressLint("LogNotTimber")
class VerificationManager : VerificationService.Listener, Observer<List<CryptoDeviceInfo>> {

    companion object {
        const val EXTRA_USER_ID = "user_id"
        const val EXTRA_DEVICE_ID = "device_id"

        val instance = VerificationManager()
    }

    var transaction: VerificationTransaction? = null
    var request: PendingVerificationRequest? = null
    var state: VerificationActivity.State = VerificationActivity.State.WaitingOnAccept

    private var mApp: WeakReference<ImApp>? = null

    private val mContext
        get() = mApp?.get()?.applicationContext

    private val mCanScan
        get() = mContext?.packageManager?.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY) ?: false

    private val mSession
        get() = mApp?.get()?.matrixSession

    private val mVerificationService
        get() = mSession?.cryptoService()?.verificationService()


    private var mCurrentSessionTs = System.currentTimeMillis()

    private var mLiveCryptoDeviceInfo: LiveData<List<CryptoDeviceInfo>>? = null


    /**
     * Register this class to listen for incoming verification requests.
     *
     * @param app: A reference to `ImApp` needed to add this class as listener and keep a weak reference to a context.
     */
    fun register(app: ImApp) {
        mApp = WeakReference(app)

        mSession?.cryptoService()?.verificationService()?.addListener(this)

        mLiveCryptoDeviceInfo = mSession?.cryptoService()?.getLiveCryptoDeviceInfo(mSession?.myUserId ?: "")
        mLiveCryptoDeviceInfo?.observeForever(this)
    }

    fun deregister() {
        mVerificationService?.removeListener(this)
        mLiveCryptoDeviceInfo?.removeObserver(this)
        mLiveCryptoDeviceInfo = null


    }

    override fun verificationRequestCreated(pr: PendingVerificationRequest) {
        request = pr

        // Don't show, if it's a request as a room event and the room is currently open.
        if (pr.roomId == null || pr.roomId != mApp?.get()?.currentForegroundRoom) {
            mContext?.let { presentActivity(it) }
        }
    }

    override fun transactionCreated(tx: VerificationTransaction) {
        transaction = tx

        mContext?.let { presentActivity(it) }
    }

    override fun onChanged(value: List<CryptoDeviceInfo>) {
        val context = mContext ?: return

        for (device in value) {
            // Only notify...
            // - if it's not *this* device.
            // - if the device is not verified, yet.
            // - if the device is not blocked.
            // - if this device is not brand new. (So we would be the actual new device!)

            // Update to real value, if we find our device, so the later check works better!
            if (device.deviceId == mSession?.sessionParams?.deviceId) {
                device.firstTimeSeenLocalTs?.let { mCurrentSessionTs = it }
                return
            }

            if (!device.isVerified && !device.isBlocked
                && (device.firstTimeSeenLocalTs ?: 0) > mCurrentSessionTs + 60_000
            ) {
                StatusBarNotifier(context).notifyNewDevice(device)
            }
        }
    }


    /**
     * Set another device's verified state just like that.
     *
     * The user should have acknowledged that they compared fingerprints before calling this to verify a device!
     * Better actually: Let the user very through interactive means! (See `#interactiveVerify`!)
     *
     * @param device
     *      The `MXDeviceInfo` of the device to verify.
     *
     * @param locallyVerified
     *      The verification state to set.
     */
    fun verify(device: CryptoDeviceInfo, locallyVerified: Boolean) {
        mSession?.cryptoService()?.setDeviceVerification(
                DeviceTrustLevel(false, locallyVerified),
                device.userId, device.deviceId)
    }

    /**
     * Trigger interactive verification of another device.
     *
     * Keanu currently supports verification through Emoji compare (method "SAS"), through scanning a QR code,
     * through displaying a QR code and through "reciprocate". (The network acknowledgement of the other device,
     * that a QR code was scanned and it is legit.)
     *
     * @param activity
     *      An activity.
     *
     * @param device
     *      The `MXDeviceInfo` of the device to verify.
     */
    fun interactiveVerify(activity: AppCompatActivity, device: CryptoDeviceInfo, completion: (() -> Unit)? = null) {

        val startVerification = {
            val methods = arrayListOf(VerificationMethod.QR_CODE_SHOW, VerificationMethod.SAS)
            if (mCanScan) methods.add(VerificationMethod.QR_CODE_SCAN)

            request = mVerificationService?.requestKeyVerification(
                    methods, device.userId, arrayListOf(device.deviceId))

            transaction = null

            presentActivity(activity)
        }

        if (mSession?.cryptoService()?.crossSigningService()?.canCrossSign() == true) {
            completion?.invoke()
            startVerification()
            return
        }

        PasswordDialogFragment(
                title = activity.getString(R.string.Enable_Cross_Signing),
                message = activity.getString(R.string.Cross_signing_on_this_device_is_not_enabled_yet_)) { pdf ->

            completion?.invoke()

            val password = pdf.password ?: return@PasswordDialogFragment

            enableCrossSigning(password) {
                if (it != null) {
                    request = null
                    transaction = null
                    state = VerificationActivity.State.Error(it)

                    presentActivity(activity)

                    return@enableCrossSigning
                }

                startVerification()
            }

        }.show(activity.supportFragmentManager, "PASSWORD")
    }

    fun interactiveVerifyFromEvent(otherUserId: String, roomId: String) {
        val methods = arrayListOf(VerificationMethod.QR_CODE_SHOW, VerificationMethod.SAS)
        if (mCanScan) methods.add(VerificationMethod.QR_CODE_SCAN)

        val request = getRequestByUserIdAndRoom(otherUserId, roomId)

        val tid = request?.transactionId ?: request?.requestInfo?.transactionId ?: request?.readyInfo?.transactionId

        if (tid != null && mVerificationService?.readyPendingVerificationInDMs(methods, otherUserId, roomId, tid) == true) {
            this.request = request

            // WTF? I don't know why, but only this combination of #readyPendingVerificationInDMs
            // and #readyPendingVerification with a delay seems to work to get this pending request
            // accepted.
            Handler(Looper.getMainLooper()).postDelayed({
                mVerificationService?.readyPendingVerification(methods, otherUserId, tid)

            }, 500)
        }
        else {
            this.request = null
            transaction = null
            state = VerificationActivity.State.Error()
        }

        mContext?.let { presentActivity(it) }
    }

    fun interactiveVerifyOwn(device: CryptoDeviceInfo) {
        mContext?.let {
            val intent = mApp?.get()?.router?.newDevice(it, device.userId, device.deviceId)

            intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent?.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent?.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            it.startActivity(intent)
        }
    }

    fun manualVerify(device: CryptoDeviceInfo) {
        mContext?.let {
            val intent = mApp?.get()?.router?.manualCompare(it, device.userId, device.deviceId)

            intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent?.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent?.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            it.startActivity(intent)
        }
    }

    /**
     * Enables cross-signing on this device, in case it isn't, already.
     *
     * @param password
     *      The user's account password.
     *
     * @param completion
     *      Callback when everything finished. In case of an error happens, contains that. Optional.
     */
    fun enableCrossSigning(password: String, completion: ((failure: Throwable?) -> Unit)? = null) {
        val csService = mSession?.cryptoService()?.crossSigningService()

        if (csService?.canCrossSign() == true) {
            completion?.invoke(null)
            return
        }

        csService?.initializeCrossSigning(
                object : UserInteractiveAuthInterceptor {
                    override fun performStage(flowResponse: RegistrationFlowResponse, errCode: String?, promise: Continuation<UIABaseAuth>) {
                        if (flowResponse.nextUncompletedStage() == LoginFlowTypes.PASSWORD && errCode == null) {
                            promise.resume(UserPasswordAuth(
                                    session = flowResponse.session,
                                    user = mSession?.myUserId,
                                    password = password
                            ))
                        }
                        else {
                            promise.resumeWith(Result.failure(Exception("Login flows other then of type 'PASSWORD' are not supported, yet!")))
                        }
                    }
                },
                object : MatrixCallback<Unit> {

                    override fun onSuccess(data: Unit) {
                        completion?.invoke(null)
                    }

                    override fun onFailure(failure: Throwable) {
                        completion?.invoke(failure)
                    }
                })
    }

    /**
     * Check, if a backup is already ongoing.
     */
    fun checkBackup(completion: (isBackingUp: Boolean, failure: Throwable?) -> Unit) {
        val backup = mSession?.cryptoService()?.keysBackupService()

        val check = {
            when(backup?.getState()) {
                KeysBackupState.Enabling,
                KeysBackupState.ReadyToBackUp,
                KeysBackupState.WillBackUp,
                KeysBackupState.BackingUp -> completion(true, null)

                else -> completion(false, null)
            }
        }

        backup?.keysBackupVersion?.let {
            backup.getKeysBackupTrust(it, object: MatrixCallback<KeysBackupVersionTrust> {
                override fun onSuccess(data: KeysBackupVersionTrust) {
                    check()
                }

                override fun onFailure(failure: Throwable) {
                    check()
                }
            })
        }
                ?: check()
    }

    /**
     * Adds a new room key backup to the server, if there's not already one enabled.
     *
     * Tries to store the recovery key to the user's keychain and displays an alert showing the
     * recovery key to the user, if a `vc` is given..
     *
     * @param activity
     *      Weak reference to an activity to show an `AlertDialog` on.
     *
     * @param password
     *      The user's account password for convenience or a different, special key backup password.
     *
     * @param completion
     *      Callback when everything finished. In case of an error happens, contains that. Optional.
     */
    fun addNewBackup(activity: WeakReference<Activity>, password: String, completion: ((failure: Throwable?) -> Unit)? = null)
    {
        checkBackup { isBackingUp, failure ->
            if (failure != null) {
                completion?.invoke(failure)
                return@checkBackup
            }

            // Already backing up, leave alone.
            if (isBackingUp) {
                completion?.invoke(null)
                return@checkBackup
            }

            val backup = mSession?.cryptoService()?.keysBackupService()

            backup?.prepareKeysBackupVersion(password, null, object: MatrixCallback<MegolmBackupCreationInfo> {

                override fun onSuccess(data: MegolmBackupCreationInfo) {
                    val recoveryKey = data.recoveryKey

                    backup.createKeysBackupVersion(data, object: MatrixCallback<KeysVersion> {

                        @SuppressLint("StringFormatInvalid")
                        override fun onSuccess(data: KeysVersion) {
                            val a = activity.get() ?: return
                            if (a.isFinishing) return

                            AlertDialog.Builder(a)
                                    .setTitle(R.string.Recovery_Key)
                                    .setMessage(a.getString(R.string.Please_note_down_your_recovery_key_, recoveryKey))
                                    .setNeutralButton(android.R.string.ok, null)
                                    .show()

                            completion?.invoke(null)
                        }

                        override fun onFailure(failure: Throwable) {
                            completion?.invoke(failure)
                        }
                    })
                }

                override fun onFailure(failure: Throwable) {
                    completion?.invoke(failure)
                }
            })
        }
    }

    /**
     * Restore the latest room key backup from the server, if there's one available.
     *
     * If the provided password fails, this method will try to find a stored key backup recovery key
     * in the keychain and try to use that.
     *
     * @param password
     *      The key backup password or the recovery key. Might be the same as the account password.
     *
     * @param completion
     *      Callback when everything finished. In case of an error happens, contains that, else
     *      the `ImportRoomKeysResult`. Optional.
     */
    fun restoreBackup(password: String, completion: ((failure: Throwable?, result: ImportRoomKeysResult?) -> Unit)? = null) {
        val backup = mSession?.cryptoService()?.keysBackupService()

        backup?.getCurrentVersion(object: MatrixCallback<KeysBackupLastVersionResult?> {

            override fun onSuccess(data: KeysBackupLastVersionResult?) {
                if (data == null) {
                    completion?.invoke(NoBackupFoundException(), null)
                    return
                }

                data.toKeysVersionResult()?.let {
                    backup.restoreKeyBackupWithPassword(it, password, null, null, null, object: RestoreBackupCallback(completion) {

                        override fun onFailure(failure: Throwable) {
                            backup.restoreKeysWithRecoveryKey(
                                data.toKeysVersionResult()!!, password, null, null,
                                null, object: RestoreBackupCallback(completion) {})
                        }
                    })
                }
            }

            override fun onFailure(failure: Throwable) {
                completion?.invoke(failure, null)
            }

        })
    }

    /**
     * Restores the latest room key backup from the server, if there's one available and the
     * password/recovery key is correct, or adds a new backup, if no backup found.
     *
     * @param activity
     *      Weak reference to an Activity to show an `AlertDialog` on.
     *
     * @param password
     *      The key backup password or the recovery key. Might be the same as the account password.
     * @param completion
     *      Callback when everything finished. In case of an error happens, contains that. Optional.
     */
    fun restoreOrAddNewBackup(activity: WeakReference<Activity>, password: String, completion: ((failure: Throwable?) -> Unit)? = null) {
        restoreBackup(password) { failure, _ ->
            if (failure is NoBackupFoundException) {
                addNewBackup(activity, password) {
                    completion?.invoke(it)
                }
            }
            else {
                completion?.invoke(failure)
            }
        }
    }

    fun getRequestByUserIdAndRoom(otherUserId: String, roomId: String): PendingVerificationRequest? {
        return mVerificationService?.getExistingVerificationRequests(otherUserId)
            ?.filter { it.roomId == roomId && it.transactionId?.isNotEmpty() == true }
            ?.maxByOrNull { it.ageLocalTs }
    }

    private abstract class RestoreBackupCallback(private val completion: ((failure: Throwable?, result: ImportRoomKeysResult?) -> Unit)? = null) : MatrixCallback<ImportRoomKeysResult> {

        override fun onSuccess(data: ImportRoomKeysResult) {
            completion?.invoke(null, data)
        }

        override fun onFailure(failure: Throwable) {
            completion?.invoke(failure, null)
        }
    }

    private fun presentActivity(context: Context) {
        val intent = mApp?.get()?.router?.verification(context)
        intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent?.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        intent?.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        context.startActivity(intent)
    }
}