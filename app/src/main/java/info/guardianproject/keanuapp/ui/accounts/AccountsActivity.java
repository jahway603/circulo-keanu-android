package info.guardianproject.keanuapp.ui.accounts;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.BaseActivity;

public class AccountsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.awesome_activity_accounts);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //not set color
        final PreferenceProvider preferenceProvider = new PreferenceProvider(getApplicationContext());
        int themeColorHeader = preferenceProvider.getHeaderColor();
        int themeColorText = preferenceProvider.getTextColor();
        int themeColorBg = preferenceProvider.getBackgroundColor();


        if (themeColorBg != -1)
            findViewById(R.id.main_content).setBackgroundColor(themeColorBg);

        if (themeColorHeader != -1) {
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().setNavigationBarColor(themeColorHeader);
                getWindow().setStatusBarColor(themeColorHeader);
            }

            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(themeColorHeader));

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_accounts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

}
