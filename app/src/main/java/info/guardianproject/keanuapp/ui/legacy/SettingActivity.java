/*
 * Copyright (C) 2007-2008 Esmertec AG. Copyright (C) 2007-2008 The Android Open
 * Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package info.guardianproject.keanuapp.ui.legacy;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.provider.Settings;

import info.guardianproject.keanu.core.Preferences;
import info.guardianproject.keanu.core.util.Languages;

import java.util.ArrayList;

import info.guardianproject.keanuapp.R;
import info.guardianproject.keanu.core.ImApp;
import info.guardianproject.panic.Panic;
import info.guardianproject.panic.PanicResponder;

public class SettingActivity extends PreferenceActivity {
    private static final int CHOOSE_RINGTONE = 5;

    private PackageManager pm;
    private String currentLanguage;
    ListPreference mPanicTriggerApp;
    Preference mPanicConfig;
    ListPreference mLanguage;

    CheckBoxPreference mEnableNotification;
    CheckBoxPreference mNotificationVibrate;
    CheckBoxPreference mNotificationSound;
    CheckBoxPreference mAllowScreenshot;

    Preference mNotificationRingtone, mNotificationBattery;

    private void setInitialValues() {

        mEnableNotification.setChecked(Preferences.isNotificationEnabled());
        mNotificationVibrate.setChecked(Preferences.getNotificationVibrate());
        mNotificationSound.setChecked(Preferences.getNotificationSound());

        ArrayList<CharSequence> entries = new ArrayList<CharSequence>();
        ArrayList<CharSequence> entryValues = new ArrayList<CharSequence>();
        entries.add(0, getString(R.string.panic_app_none));
        entries.add(1, getString(R.string.panic_app_default));
        entryValues.add(0, Panic.PACKAGE_NAME_NONE);
        entryValues.add(1, Panic.PACKAGE_NAME_DEFAULT);

        for (ResolveInfo resolveInfo : PanicResponder.resolveTriggerApps(pm)) {
            if (resolveInfo.activityInfo == null)
                continue;
            entries.add(resolveInfo.activityInfo.loadLabel(pm));
            entryValues.add(resolveInfo.activityInfo.packageName);
        }
        mPanicTriggerApp.setEntries(entries.toArray(new CharSequence[entries.size()]));
        mPanicTriggerApp.setEntryValues(entryValues.toArray(new CharSequence[entryValues.size()]));
        PanicResponder.configTriggerAppListPreference(mPanicTriggerApp,
                R.string.panic_trigger_app_summary, R.string.panic_app_none_summary);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        pm = getPackageManager();


        mAllowScreenshot = (CheckBoxPreference)findPreference("prefBlockScreenshots");

        mPanicTriggerApp = (ListPreference) findPreference("pref_panic_trigger_app");
        mPanicConfig = findPreference("pref_panic_config");
        mLanguage = (ListPreference) findPreference("pref_language");

        mEnableNotification = (CheckBoxPreference) findPreference("pref_enable_notification");
        mNotificationVibrate = (CheckBoxPreference) findPreference("pref_notification_vibrate");
        mNotificationSound = (CheckBoxPreference) findPreference("pref_notification_sound");

        mNotificationRingtone = findPreference("pref_notification_ringtone");
        mNotificationBattery = findPreference("pref_disable_battery_opt");

        Languages languages = Languages.get(this);
        currentLanguage = getResources().getConfiguration().locale.getLanguage();
        mLanguage.setDefaultValue(currentLanguage);
        mLanguage.setEntries(languages.getAllNames());
        mLanguage.setEntryValues(languages.getSupportedLocales());
        mLanguage.setOnPreferenceChangeListener((preference, newValue) -> {
            String language = (String) newValue;
            ImApp.resetLanguage(SettingActivity.this, language);
            setResult(RESULT_OK);
            return true;
        });

        mAllowScreenshot.setOnPreferenceChangeListener((preference, o) -> {
            setResult(RESULT_OK);
            return true;
        });

        mPanicTriggerApp.setOnPreferenceChangeListener((preference, newValue) -> {
            String packageName = (String) newValue;
            PanicResponder.setTriggerPackageName(SettingActivity.this, packageName);
            PanicResponder.configTriggerAppListPreference(mPanicTriggerApp,
                    R.string.panic_trigger_app_summary, R.string.panic_app_none_summary);
            return true;
        });

        mPanicConfig.setOnPreferenceClickListener(preference -> {
            startActivity(((ImApp) getApplication()).getRouter().panicSetup(SettingActivity.this));

            return true;
        });

        findPreference("pref_color_reset").setOnPreferenceClickListener(preference -> {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(SettingActivity.this);

            SharedPreferences.Editor pEdit = settings.edit();
            pEdit.remove(Preferences.THEME_COLOR_BG);
            pEdit.remove("themeColorText");
            pEdit.remove(Preferences.THEME_COLOR_HEADER);
            pEdit.apply();
            setResult(RESULT_OK);
            finish();
            return true;
        });

        mNotificationRingtone.setOnPreferenceClickListener(arg0 -> {

            Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, getString(R.string.notification_ringtone_title));
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Preferences.getNotificationRingtoneUri());
            startActivityForResult(intent, CHOOSE_RINGTONE);
            return true;
        });

        mNotificationBattery.setOnPreferenceClickListener(preference -> {
            disableBatteryOptimization();
            return true;
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == CHOOSE_RINGTONE) {
            Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
            Preferences.setNotificationRingtone(uri);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setInitialValues();
    }

    private void disableBatteryOptimization ()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + packageName));
            startActivity(intent);

        }
    }
}
