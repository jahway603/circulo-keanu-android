package info.guardianproject.keanuapp.ui.lock

import android.content.Intent
import android.content.SharedPreferences
import android.os.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.beautycoder.pflockscreen.PFFLockScreenConfiguration
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment.OnPFLockScreenCodeCreateListener
import com.beautycoder.pflockscreen.security.PFSecurityManager
import com.beautycoder.pflockscreen.viewmodels.PFPinCodeViewModel
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanuapp.R

const val ACTION_CHANGE_PASSPHRASE = "cp"


class AppLockScreenActivity : AppCompatActivity() {

    private val mPrefs: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(this)

    private val mCodeCreateListener: OnPFLockScreenCodeCreateListener =
        object : OnPFLockScreenCodeCreateListener {
            override fun onCodeCreated(encodedCode: String) {
                savePinCode(encodedCode)

                setResult(RESULT_OK,Intent())
                finish()
            }

            override fun onNewCodeValidationFailed() {
                Toast.makeText(
                    this@AppLockScreenActivity,
                    R.string.lock_screen_passphrases_not_matching,
                    Toast.LENGTH_SHORT
                ).show()

                vibrateOnError()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_app_lock_screen)

        if (intent?.action != null) {
            if (intent.action.equals(ACTION_CHANGE_PASSPHRASE, true)) {
                resetPinCode()
            }
        }
        else {
            showLockScreenView()
        }
    }

    private fun showLockScreenView() {
        PFPinCodeViewModel().isPinCodeEncryptionKeyExist.observe(this) { pinCodeResult ->
            if (pinCodeResult == null) {
                return@observe
            }

            if (pinCodeResult.error != null) {
                Toast.makeText(this, getString(R.string.pincodeNotFound), Toast.LENGTH_SHORT)
                    .show()

                return@observe
            }

            showCreateLockScreenView(pinCodeResult.result)
        }
    }

    private fun showCreateLockScreenView(pinExists: Boolean) {
        val configuration = PFFLockScreenConfiguration.Builder(this)
            .setTitle(if (pinExists) getString(R.string.title_activity_lock_screen) else getString(R.string.lock_screen_passphrase_not_set_enter))
            .setCodeLength(6)
            .setNewCodeValidation(true)
            .setNewCodeValidationTitle(getString(R.string.lock_screen_passphrase_not_set_confirm))
            .setUseFingerprint(true)
            .setErrorVibration(true)
            .setClearCodeOnError(true)
            .setErrorAnimation(true)
            .setMode(PFFLockScreenConfiguration.MODE_CREATE)
            .build()

        val lockFragment = PFLockScreenFragment()
        lockFragment.setConfiguration(configuration)
        lockFragment.setCodeCreateListener(mCodeCreateListener)

        supportFragmentManager.beginTransaction()
            .replace(R.id.lockContainerLayout, lockFragment).commit()
    }

    private fun savePinCode(encryptedCode: String) {
        mPrefs.edit().putString(KeanuConstants.PREFERENCE_KEY_ENCODED_PASS, encryptedCode).apply()
    }

    private fun deletePinCode() {
        PFSecurityManager.getInstance().pinCodeHelper.delete { result ->
            if (result?.error != null) {
                Toast.makeText(this, getString(R.string.general_error, result.error.code), Toast.LENGTH_LONG).show()

                vibrateOnError()
            }
            else {
                mPrefs.edit().remove(KeanuConstants.PREFERENCE_KEY_ENCODED_PASS).apply()
            }
        }
    }

    private fun resetPinCode() {
        deletePinCode()

        showCreateLockScreenView(false)
    }

    private fun vibrateOnError() {
        val vibrator = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            (getSystemService(VIBRATOR_MANAGER_SERVICE) as? VibratorManager)?.defaultVibrator
        }
        else {
            @Suppress("DEPRECATION")
            getSystemService(VIBRATOR_SERVICE) as? Vibrator
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator?.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
        }
        else {
            @Suppress("DEPRECATION")
            vibrator?.vibrate(500)
        }
    }
}

