package info.guardianproject.keanuapp.ui.qr

import android.hardware.Camera

@Suppress("deprecation")
interface PreviewConsumer {
	fun start(camera: Camera)
	fun stop()
}

