@file:Suppress("DEPRECATION")

package info.guardianproject.keanuapp.ui.qr

import android.Manifest
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.hardware.Camera
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.Result
import com.google.zxing.ResultMetadataType
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager.decodeInviteLink
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.viewmodel.qr.QrViewModel
import timber.log.Timber
import java.io.ByteArrayOutputStream

class QrScanActivity : BaseActivity(), QrCodeDecoder.ResultCallback {
    private val mDataResult = Intent()
    private val mResultStrings = ArrayList<String>()
    private var mCameraView: CameraView? = null
    private var mLayoutMain: View? = null
    private var mCamera: Camera? = null
    private var mFinishOnFirst = false
    private var mGetRaw = false
    private var mIvQrCode: ImageView? = null

    private lateinit var mQrCodeViewModel: QrViewModel

    override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        mQrCodeViewModel = ViewModelProvider(this)[QrViewModel::class.java]
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        supportActionBar?.hide()
        val permissionCheck = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        )
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.CAMERA
                )
            ) {
                finish()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.CAMERA),
                    MY_PERMISSIONS_REQUEST_CAMERA
                )
            }
        } else {
            init()
        }
        observeLiveData()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                init()
            } else {
                finish()
            }
            return
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun init() {
        setContentView(R.layout.awesome_activity_scan)
        mLayoutMain = findViewById(R.id.layout_main)
        val cameraBorder = findViewById<View>(R.id.camera_box) as LinearLayout
        mCameraView = CameraView(this)
        cameraBorder.addView(mCameraView)
        val intent = intent
        var qrData: String? = null
        if (intent != null) {
            qrData = intent.getStringExtra(Intent.EXTRA_TEXT)
            mFinishOnFirst = intent.getBooleanExtra(EXTRA_FINISH_ON_FIRST, false)
            mGetRaw = intent.getBooleanExtra(EXTRA_GET_RAW, false)
        }
        qrData ?: return
        mIvQrCode = findViewById(R.id.qr_box_image)
        mQrCodeViewModel.getQrBitmap(100, qrData)
    }

    override fun onResume() {
        super.onResume()
        openCamera()
    }

    override fun onPause() {
        super.onPause()
        releaseCamera()
    }

    @Synchronized
    private fun openCamera() {
        Timber.d("Opening camera")
        if (mCamera == null) {
            try {
                mCamera = Camera.open()
                mCameraView?.start(mCamera, QrCodeDecoder(this), 0, true)
            } catch (e: Exception) {
                Timber.e(e, "Error opening camera")
            }
        }
    }

    private fun releaseCamera() {
        Timber.d("Releasing camera")
        try {
            if (mCamera != null) mCameraView?.stop()
            mCamera?.release()
            mCamera = null
        } catch (e: Exception) {
            Timber.e(e, "Error releasing camera")
        }
    }

    override fun handleResult(result: Result?) {
        result ?: return
        runOnUiThread {
            var resultString = result.text
            getRawBytes(result)?.let {
                if (mGetRaw) resultString = String(it, Charsets.ISO_8859_1)
            }

            if (!mResultStrings.contains(resultString)) {
                mResultStrings.add(resultString)
                mDataResult.putStringArrayListExtra("result", mResultStrings)
                val diLink = decodeInviteLink(resultString)
                var message: String? = null
                if (diLink != null) {
                    message = getString(R.string.add_contact_success, diLink.username)
                }
                if (message != null) {
                    mLayoutMain?.let {
                        Snackbar.make(it, message, Snackbar.LENGTH_LONG).show()
                    }
                }
                setResult(RESULT_OK, mDataResult)
                if (mFinishOnFirst) {
                    finish()
                }
            }
        }
    }

    // Copied from https://github.com/markusfisch/BinaryEye/blob/
    // 9d57889b810dcaa1a91d7278fc45c262afba1284/app/src/main/kotlin/de/markusfisch/android/binaryeye/activity/CameraActivity.kt#L434
    private fun getRawBytes(result: Result?): ByteArray? {
        val metadata = result?.resultMetadata ?: return null
        val segments = metadata[ResultMetadataType.BYTE_SEGMENTS] as? List<ByteArray>?
            ?: return null
        val stream = ByteArrayOutputStream()
        for (s in segments) {
            stream.write(s, 0, s.size)
        }
        val bytes = stream.toByteArray()

        // Byte segments can never be shorter than the text.
        // Zxing cuts off content prefixes like "WIFI:"
        return if (bytes.size >= result.text.length) bytes else null
    }

    private fun observeLiveData() {
        mQrCodeViewModel.observableGetQrBitmapLiveData.observe(this) {
            mIvQrCode?.setImageBitmap(it)
        }
    }

    companion object {
        const val EXTRA_FINISH_ON_FIRST = "extra_finish_on_first"
        const val EXTRA_GET_RAW = "extra_get_raw"
        private const val MY_PERMISSIONS_REQUEST_CAMERA = 1
    }
}