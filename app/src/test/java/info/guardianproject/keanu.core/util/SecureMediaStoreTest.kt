package info.guardianproject.keanu.core.util

import android.net.Uri
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.Assert
import org.junit.Test

class SecureMediaStoreTest {
    @Test
    fun `test valid vfs uri`() {

        mockkStatic(Uri::class)
        val mockedUri = mockk<Uri>()
        every { Uri.parse(any()) } returns mockedUri
        every { mockedUri.scheme } returns "vfs"
        val validUri = Uri.parse("vfs://authority/path/query/fragment")

        Assert.assertEquals(SecureMediaStore.isVfsUri(validUri), true)
    }

    @Test
    fun `test invalid vfs uri`() {

        mockkStatic(Uri::class)
        val mockedUri = mockk<Uri>()
        every { Uri.parse(any()) } returns mockedUri

        //Uri with content scheme
        every { mockedUri.scheme } returns "content"
        var invalidUri = Uri.parse("content://authority/path/query/fragment")

        Assert.assertEquals(SecureMediaStore.isVfsUri(invalidUri), false)

        //Empty scheme
        every { mockedUri.scheme } returns ""
        invalidUri = Uri.parse("content://authority/path/query/fragment")

        Assert.assertEquals(SecureMediaStore.isVfsUri(invalidUri), false)

        //Null scheme
        every { mockedUri.scheme } returns null
        invalidUri = Uri.parse("https://authority/path/query/fragment")

        Assert.assertEquals(SecureMediaStore.isVfsUri(invalidUri), false)
    }

    @Test
    fun `test valid content uri`() {

        mockkStatic(Uri::class)
        val mockedUri = mockk<Uri>()
        every { Uri.parse(any()) } returns mockedUri
        every { mockedUri.scheme } returns "content"
        val validUri = Uri.parse("content://authority/path/query/fragment")

        Assert.assertEquals(SecureMediaStore.isContentUri(validUri), true)
    }

    @Test
    fun `test invalid content uri`() {

        mockkStatic(Uri::class)
        val mockedUri = mockk<Uri>()
        every { Uri.parse(any()) } returns mockedUri

        //Uri with content scheme
        every { mockedUri.scheme } returns "https"
        var invalidUri = Uri.parse("https://authority/path/query/fragment")

        Assert.assertEquals(SecureMediaStore.isContentUri(invalidUri), false)

        //Empty scheme
        every { mockedUri.scheme } returns ""
        invalidUri = Uri.parse("vfs://authority/path/query/fragment")

        Assert.assertEquals(SecureMediaStore.isContentUri(invalidUri), false)

        //Null scheme
        every { mockedUri.scheme } returns null
        invalidUri = Uri.parse("file://authority/path/query/fragment")

        Assert.assertEquals(SecureMediaStore.isContentUri(invalidUri), false)
    }

    @Test
    fun `test valid content uri with String input param`() {

        val validUri1 = "content://authority/path/query/fragment"
        val validUri2 = "content://authority"
        val validUri3 = "content://"

        Assert.assertEquals(SecureMediaStore.isContentUri(validUri1), true)
        Assert.assertEquals(SecureMediaStore.isContentUri(validUri2), true)
        Assert.assertEquals(SecureMediaStore.isContentUri(validUri3), true)

        val invalidUri1 = "http://www.test.com"
        val invalidUri2 = "vfs://test/"
        val invalidUri3 = ""
        Assert.assertEquals(SecureMediaStore.isContentUri(invalidUri1), false)
        Assert.assertEquals(SecureMediaStore.isContentUri(invalidUri2), false)
        Assert.assertEquals(SecureMediaStore.isContentUri(invalidUri3), false)

    }

    @Test
    fun `test valid vfs uri with String input param`() {

        val validUri1 = "vfs://authority/path/query/fragment"
        val validUri2 = "vfs://authority"
        val validUri3 = "vfs://"

        Assert.assertEquals(SecureMediaStore.isVfsUri(validUri1), true)
        Assert.assertEquals(SecureMediaStore.isVfsUri(validUri2), true)
        Assert.assertEquals(SecureMediaStore.isVfsUri(validUri3), true)

        val invalidUri1 = "http://www.test.com"
        val invalidUri2 = "content://test/"
        val invalidUri3 = ""
        Assert.assertEquals(SecureMediaStore.isVfsUri(invalidUri1), false)
        Assert.assertEquals(SecureMediaStore.isVfsUri(invalidUri2), false)
        Assert.assertEquals(SecureMediaStore.isVfsUri(invalidUri3), false)

        var nullString: String? = null
        Assert.assertEquals(nullString?.endsWith("png") ?: false, false)
        nullString = ".png"
        Assert.assertEquals(nullString?.endsWith("png"), true)

    }
}