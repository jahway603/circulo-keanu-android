package org.article19.circulo.next

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import androidx.fragment.app.FragmentActivity
import info.guardianproject.keanu.core.Preferences
import org.article19.circulo.next.common.ui.LoadingDialog
import org.article19.circulo.next.databinding.LayoutNoInternetBinding
import timber.log.Timber


open class BaseActivity: FragmentActivity() {

    private val mConnectivityManager by lazy { getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager }
    private val mNetworkCallback by lazy { getNetworkCallback() }
    private var mIsNetworkCallbackRegistered = false
    private val mNetworkRequest by lazy { getNetworkRequest() }
    private var mIsNetworkConnected = false
    private val mLoadingDialog by lazy { LoadingDialog() }
    private var mIsLoadingDialogShowing = false

    private lateinit var mLayoutNoInternet: View

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {

        if (Preferences.doBlockScreenshots()) window.setFlags(
            WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE
        )

        /**
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            this.window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            this.window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }**/

        return super.onCreateView(name, context, attrs)
    }

    override fun setContentView(layoutResID: Int) {
        val inflater = this.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val screenView: View = inflater.inflate(layoutResID, null)
        super.setContentView(addNoInternetStatusView(screenView))
    }

    override fun setContentView(view: View?) {
        super.setContentView(view?.let { addNoInternetStatusView(it) })
        observeLiveData()
    }

    private fun addNoInternetStatusView(screenView: View): LinearLayout {
        val screenRootView = LinearLayout(this)
        screenRootView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        screenRootView.orientation = LinearLayout.VERTICAL


        val inflater = this.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mLayoutNoInternet = LayoutNoInternetBinding.inflate(inflater).root
        mLayoutNoInternet.visibility = View.GONE

        screenRootView.addView(mLayoutNoInternet)
        screenRootView.addView(screenView)
        return screenRootView
    }

    private fun isNetworkConnected(): Boolean {
        return try {
            val pingCmd = "ping -c 1 google.com"
            Runtime.getRuntime().exec(pingCmd).waitFor() == 0
        } catch (exception: Exception) {
            Timber.e(exception)
            mIsNetworkConnected
        }

    }

    private fun getNetworkRequest(): NetworkRequest {
        val networkRequestBuilder = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            networkRequestBuilder.addCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)
        }
        return networkRequestBuilder.build()
    }

    private fun getNetworkCallback(): ConnectivityManager.NetworkCallback {
        return object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                mIsNetworkConnected = true
                updateInternetStatus(true)
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                mIsNetworkConnected = false
                updateInternetStatus(false)
            }

            override fun onUnavailable() {
                super.onUnavailable()
                updateInternetStatus(false)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mConnectivityManager?.registerNetworkCallback(mNetworkRequest, mNetworkCallback)
        mIsNetworkCallbackRegistered = true
        updateInternetStatus(isNetworkConnected())
    }

    override fun onStop() {
        super.onStop()

        if (mIsNetworkCallbackRegistered) {
            mConnectivityManager?.unregisterNetworkCallback(mNetworkCallback)
            mIsNetworkCallbackRegistered = false
        }
    }

    private fun updateInternetStatus(hasInternet: Boolean) {
        runOnUiThread {
            if (hasInternet) {
                mLayoutNoInternet.visibility = View.GONE
            } else {
                mLayoutNoInternet.visibility = View.VISIBLE
            }
        }
    }

    fun showLoading() {
        if (!mLoadingDialog.isAdded && !mIsLoadingDialogShowing) {
            mIsLoadingDialogShowing = true
            supportFragmentManager.beginTransaction().add(mLoadingDialog, LoadingDialog::class.java.name)
                .commitAllowingStateLoss()
        }
    }

    fun hideLoading() {
        if (mIsLoadingDialogShowing) {
            mLoadingDialog.dismissAllowingStateLoss()
            mIsLoadingDialogShowing = false
        }
    }

    open fun observeLiveData() {}
}
