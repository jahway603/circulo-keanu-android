package org.article19.circulo.next

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import dagger.hilt.android.HiltAndroidApp
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.service.RemoteImService
import org.article19.circulo.next.notify.StatusBarNotifier
import org.article19.circulo.next.settings.NotificationType
import org.jsoup.Jsoup
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.session.room.timeline.isReply
import timber.log.Timber

@HiltAndroidApp
class CirculoApp : ImApp() {

    private var mStatusBarNotifier: StatusBarNotifier? = null
    val cleanInsightsManager = CleanInsightsManager()

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        router = CirculoRouter()
        mAppInstance = this
        mStatusBarNotifier = StatusBarNotifier(this)

        initChannel()

        setupAcra()

        cleanInsightsManager.init(this)

        ProcessLifecycleOwner.get().lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onResume(owner: LifecycleOwner) {
                super.onResume(owner)
                appInBackground = false
            }

            override fun onStop(owner: LifecycleOwner) {
                super.onStop(owner)
                appInBackground = true
            }
        })
    }

    override var currentForegroundRoom: String
        get() {
            val roomId = SharedTimeline.instance.room?.roomId ?: ""

            super.currentForegroundRoom = roomId
            Timber.d("tuancoltech set currentForegroundRoom 1: " + roomId)

            return roomId
        }
        set(_) {
            super.currentForegroundRoom = SharedTimeline.instance.room?.roomId ?: ""
            Timber.d("tuancoltech set currentForegroundRoom 2: " + super.currentForegroundRoom)
        }

    override fun startService() {
        val intent = router.remoteService(this)
        intent.putExtra("notify",false)
        startService(intent)
        /**
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        }
        else {

        }**/
    }

    private fun setupAcra() {

        /** //disable for now
        initAcra {
            //core configuration:
            buildConfigClass = BuildConfig::class.java
            reportFormat = StringFormat.JSON
            //each plugin you chose above can be configured in a block like this:
            dialog {
                //required
                text = getString(R.string.crash_dialog_text)

            }
        }**/

    }

    companion object {
        const val NOTIFICATION_CHANNEL_ID_ALERT = "org.article19.circulo.next.alert.now"
        const val NOTIFICATION_CHANNEL_ID_ALERT_NO_VIBRATE = "org.article19.circulo.next.alert.now.no_vibrate"

        const val NOTIFICATION_CHANNEL_ID_LOCATION = "org.article19.circulo.next.alert.location"

        private lateinit var mAppInstance: CirculoApp

        fun getInstance(): CirculoApp {
            return mAppInstance
        }
    }


    private fun initChannel() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return

        val nm = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        nm.deleteNotificationChannel(NOTIFICATION_CHANNEL_ID_ALERT)
        nm.deleteNotificationChannel(NOTIFICATION_CHANNEL_ID_ALERT_NO_VIBRATE)

        val channelId = NOTIFICATION_CHANNEL_ID_ALERT
        val nc = NotificationChannel(channelId,
            getString(info.guardianproject.keanuapp.R.string.notifications), NotificationManager.IMPORTANCE_HIGH)
        nc.lightColor = -0x670000
        nc.enableLights(true)
        nc.enableVibration(true)
        val alertVibratePattern = longArrayOf(50, 0, 50, 0, 50, 0, 500, 0, 500, 0, 50, 0, 50, 0, 50)
        nc.vibrationPattern = alertVibratePattern
        nc.setShowBadge(true)
        nc.setSound(Uri.parse("android.resource://$packageName/raw/alert2"),Notification.AUDIO_ATTRIBUTES_DEFAULT)
        nm.createNotificationChannel(nc)

        val channelId2 = NOTIFICATION_CHANNEL_ID_ALERT_NO_VIBRATE
        val nc2 = NotificationChannel(channelId2,
            getString(info.guardianproject.keanuapp.R.string.notifications), NotificationManager.IMPORTANCE_HIGH)
        nc2.lightColor = -0x670000
        nc2.enableLights(true)
        nc2.enableVibration(false)
        nc2.setShowBadge(true)
        nc2.setSound(null,null)
        nm.createNotificationChannel(nc2)
    }


    fun stopApp() {
        stopService(Intent(this, RemoteImService::class.java))
    }

    fun isNetworkAvailable(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager ?: return false

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val cap = cm.getNetworkCapabilities(cm.activeNetwork) ?: return false

            when {
                cap.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    return true
                }
                cap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    return true
                }
                cap.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    return true
                }
            }

            return false
        }
        else {
            @Suppress("DEPRECATION")
            return cm.activeNetworkInfo?.isConnected ?: false
        }
    }

    fun cancelNotifications() {
        mStatusBarNotifier?.stopVibration()
    }

    fun getStatusBarNotifier(): StatusBarNotifier? {
        return mStatusBarNotifier
    }
}

