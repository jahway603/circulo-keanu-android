package org.article19.circulo.next

import android.app.Activity
import android.content.Context
import android.os.Build
import org.article19.circulo.next.common.providers.PreferenceProvider
import org.article19.circulo.next.settings.feedback.FeedbackConsentSheet
import org.cleaninsights.sdk.*
import timber.log.Timber
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

open class CleanInsightsManager {

    companion object {
        private const val CI_CAMPAIGN = "circulo-usage"
    }

    private var mCleanInsights: CleanInsights? = null

    fun init(context : Context) {
        if (mCleanInsights == null) {
            try {
                mCleanInsights = CleanInsights(
                    context.assets.open("cleaninsights.json").reader().readText(),
                    context.filesDir
                )
            } catch (e: IOException) {
                Timber.e(e)
            }
        }

//        mCleanInsights?.testServer {
//            Timber.d("test server: " + it.toString())
//        }
    }

    val hasConsent: Boolean
    get() {
        return mCleanInsights?.isCampaignCurrentlyGranted(CI_CAMPAIGN) ?: false
    }

    val remainingConsentDays: Long
    get() {
        val end = mCleanInsights?.consent(CI_CAMPAIGN)?.endDate ?: return 0

        return TimeUnit.DAYS.convert(end.time - Date().time, TimeUnit.MILLISECONDS)
    }

    val isConfigured: Boolean
    get() {
        return mCleanInsights?.state(CI_CAMPAIGN) != Consent.State.Unconfigured
    }

    private val groupCode: String
    get() = PreferenceProvider.ciGroupCode ?: "action"

    fun stopConsent() {
        mCleanInsights?.deny(CI_CAMPAIGN)
    }

    fun getConsent(context: Activity, completed: (hasConsented: Boolean) -> Unit) {
        FeedbackConsentSheet().show(context) {
            displayPositiveButton(false)
            displayNegativeButton(false)
            displayHandle(true)
            onClose {
                if (isConsentGiven) {
                    mCleanInsights?.grant(CI_CAMPAIGN)
                }
                else {
                    mCleanInsights?.deny(CI_CAMPAIGN)
                }

                PreferenceProvider.ciGroupCode = groupCode?.ifBlank { null }

                completed(isConsentGiven)
            }
        }
    }

    @Suppress("unused")
    fun measureView(view: String) {
        measureView(listOf(view))
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun measureView(views: List<String>) {
        mCleanInsights?.measureVisit(views, CI_CAMPAIGN)
    }

    fun measureEvent(action: String, name: String? = null, value: Double? = null) {
        mCleanInsights?.measureEvent(groupCode, action, CI_CAMPAIGN, name, value)
    }
}
