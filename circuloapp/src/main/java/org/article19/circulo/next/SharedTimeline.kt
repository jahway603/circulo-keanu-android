package org.article19.circulo.next

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import info.guardianproject.keanu.core.model.MessageInfo
import info.guardianproject.keanu.core.service.RemoteImService
import info.guardianproject.keanu.core.util.extensions.getReplyToEventId
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.main.now.Status
import org.article19.circulo.next.main.now.Thread
import org.article19.circulo.next.mention.TextPillsUtils
import org.article19.circulo.next.notify.getLastMessageBody
import org.jsoup.Jsoup
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.session.crypto.model.MXEventDecryptionResult
import org.matrix.android.sdk.api.session.crypto.model.OlmDecryptionResult
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.getRootThreadEventId
import org.matrix.android.sdk.api.session.events.model.isThread
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.message.MessageContent
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.timeline.Timeline
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.TimelineSettings
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.session.room.timeline.isEdition
import org.matrix.android.sdk.api.session.room.timeline.isReply
import org.matrix.android.sdk.api.session.room.timeline.isRootThread
import org.matrix.android.sdk.api.session.user.model.User
import org.matrix.android.sdk.api.util.Optional
import java.util.Calendar
import java.util.Date
import java.util.concurrent.ConcurrentHashMap

class SharedTimeline private constructor() : Timeline.Listener, LifecycleOwner {

    companion object {

        private var _instance: SharedTimeline? = null
        val instance: SharedTimeline
            get() {
                if (_instance == null) _instance = SharedTimeline()

                _instance?.reinit()

                return _instance!!
            }

        private const val HISTORY_CHUNK = 250

        private val ROOM_SUMMARY_QUERY = RoomSummaryQueryParams.Builder().apply {
            memberships = listOf(Membership.JOIN, Membership.INVITE)
        }.build()

        private val ROOM_MEMBER_QUERY = RoomMemberQueryParams.Builder().apply {
            memberships = listOf(Membership.JOIN, Membership.INVITE)
        }.build()

        private val ALLOWED_ROOM_TYPES = listOf(
            EventType.MESSAGE,
            EventType.STICKER,
            EventType.REACTION,
            EventType.TYPING,
            EventType.RECEIPT)

        /**
         * Filter to remove non-reply events. Will drop all events, which are
         *
         * - a redaction (The closest thing to a deletion, we can have in Matrix.)
         * - not of any type listed in `ALLOWED_ROOM_TYPES`.
         * - an edition of another event (Matrix SDK will show the edition on the original event, typically, anyway.)
         * - neither a reply nor a thread reply.
         *
         * @return true, if ok, false if it should be discarded.
         */
        private val FILTER_EVENT_REPLY = { te: TimelineEvent ->
            // This is ordered in perceived weight of executed code!
            ALLOWED_ROOM_TYPES.contains(te.root.getClearType())
                    && !te.root.isRedacted()
                    && !te.isEdition()
                    && (te.isReply() || te.root.isThread())
        }


        fun getStatus(message: String?): Pair<Status, Boolean> {
            val isUrgent = message?.contains(Status.URGENT_STRING) ?: false

            val status = when {
                message?.contains(Status.SAFE.value) == true -> Status.SAFE
                message?.contains(Status.NOT_SAFE.value) == true -> Status.NOT_SAFE
                message?.contains(Status.UNCERTAIN.value) == true -> Status.UNCERTAIN
                else -> Status.UNDEFINED
            }

            return Pair(status, isUrgent)
        }

        fun getFormattedMessage(content: MessageContent?): MessageInfo? {
            val mtc = content as? MessageTextContent ?: return null
            val formattedBody = mtc.formattedBody ?: return null

            if (!formattedBody.contains("mx-reply")) return null

            val document = Jsoup.parse(formattedBody)
            val replyPart = document.getElementsByTag("mx-reply")
            val messagePart = replyPart.select("a").last()?.text()

            val mentionedName = document.select(TextPillsUtils.MENTION_TAG).last()?.text()
            val isMentioned = !mentionedName.isNullOrEmpty()

            if (messagePart != null) {
                val sender = messagePart.split(":")[0].replace("@", "")

                replyPart.select("a").remove()
                val message = replyPart.text()
                replyPart.remove()
                val reply = document.text()

                return MessageInfo(sender, message, reply, isMentioned, if (isMentioned) mentionedName else null)
            }

            return MessageInfo("", document.text(), "", isMentioned, if (isMentioned) mentionedName else null)
        }

        fun isOnline(roomMembers: List<RoomMemberSummary>, userId: String? = null): Boolean {
            val memberSummary = if (userId != null) {
                roomMembers.firstOrNull {
                    it.userId == userId
                }
            }
            else {
                roomMembers.firstOrNull()
            }

            return memberSummary?.userPresence?.isCurrentlyActive == true
        }
    }

    val isInsideCircle: Boolean
        get() = mRoomSummaries?.isNotEmpty() == true

    val containsStatusByMe: Boolean
        get() {
            val userId = myUserId ?: return false

            return mThreads.firstOrNull { it.userId == userId } != null
        }

    val getMyStatusThread: String?
        get() {
            val userId = myUserId ?: return null
            val myThread = mThreads.firstOrNull { it.userId == userId }
            myThread?.eventId;
            return null
        }

    private val _threads = MutableLiveData<List<Thread>>()
    val threads: LiveData<List<Thread>>
        get() = _threads

    private val _notifyableEvent = MutableLiveData<TimelineEvent?>()
    val notifyableEvent: LiveData<TimelineEvent?>
        get() = _notifyableEvent

    var room: Room? = null
        private set

    val threadEvents = ConcurrentHashMap<String, List<TimelineEvent>>()

    override val lifecycle: Lifecycle
        get() = mLifecycleRegistry

    private var mLifecycleRegistry = LifecycleRegistry(this)

    private val mSession
        get() = CirculoApp.getInstance().matrixSession

    private val myUserId
        get() = mSession?.myUserId

    private var mRoomSummaries: List<RoomSummary>? = null

    private var mLastReadTs = Date().time

    private var mTimeline: Timeline? = null

    private var mThreads = arrayListOf<Thread>()

    private var mLastGoodEventId: String? = null

    private var mRoomSummariesLive: LiveData<List<RoomSummary>>? = null
    private var mReadMarkerLive: LiveData<Optional<String>>? = null
    private var mRoomMembersLive: LiveData<List<RoomMemberSummary>>? = null
    //private var mUsersLive: LiveData<List<User>>? = null
    private var mThreadEventsLive = hashMapOf<String, MutableLiveData<List<TimelineEvent>>>()


    init {
        mLifecycleRegistry.currentState = Lifecycle.State.CREATED
    }


    fun observeThreadEvents(owner: LifecycleOwner, observer: Observer<List<TimelineEvent>>, threadId: String) {
        if (!mThreadEventsLive.containsKey(threadId)) {
            mThreadEventsLive[threadId] = MutableLiveData()
            mThreadEventsLive[threadId]?.postValue(threadEvents[threadId])
        }

        mThreadEventsLive[threadId]?.observe(owner, observer)
    }

    fun unobserveThreadEvents(owner: LifecycleOwner, threadId: String) {
        mThreadEventsLive[threadId]?.removeObservers(owner)

        if (mThreadEventsLive[threadId]?.hasObservers() == false) {
            mThreadEventsLive.remove(threadId)
        }
    }

    override fun onTimelineFailure(throwable: Throwable) {
        mTimeline?.restartWithEventId(mLastGoodEventId)
    }

    override fun onTimelineUpdated(snapshot: List<TimelineEvent>) {
        CoroutineScope(Dispatchers.IO).launch {
            // Load history of up to 1 year from now.
            // This method will be called multiple times with a longer and longer `snapshot` argument.
            if (mTimeline?.hasMoreToLoad(Timeline.Direction.BACKWARDS) == true) {
                val ts = snapshot.lastOrNull()?.root?.originServerTs

                // Don't go back further than 1 year from now.
                if (ts == null || ts > Calendar.getInstance()
                        .apply { add(Calendar.YEAR, -1) }.timeInMillis
                ) {
                    mTimeline?.paginate(Timeline.Direction.BACKWARDS, HISTORY_CHUNK)
                }
            }

            val threadRootEvents = arrayListOf<TimelineEvent>()

            // First, collect all threads to remove,...
            val removeThreads = ArrayList<Thread>()

            for (event in snapshot) {
                mLastGoodEventId = event.eventId

                // Remove redacted threads again.
                if (event.root.isRedacted()) {
                    val thread = mThreads.firstOrNull { it.eventId == event.eventId }

                    if (thread != null) removeThreads.add(thread)

                    continue
                }

                // Only accept the following as thread root events:
                // - Events which are thread roots in the sense of the new Thread API.
                // - Events, which are of type "message" and are not replies or thread replies.
                if (!event.isRootThread() &&
                    !(event.root.getClearType() == EventType.MESSAGE && !event.isReply() && !event.root.isThread())
                ) {
                    continue
                }

                threadRootEvents.add(event)
            }

            // ... then remove them all at once to reduce UI updates and therefore flickering.
            if (removeThreads.isNotEmpty()) {
                CoroutineScope(Dispatchers.Main).launch {
                    for (thread in removeThreads) {
                        threadEvents.remove(thread.eventId)
                        mThreadEventsLive[thread.eventId]?.postValue(emptyList())

                        mThreads.remove(thread)
                    }

                    _threads.postValue(mThreads)
                }
            }

            val replyEvents = snapshot
                .filter(FILTER_EVENT_REPLY)
                .reversed()

            val newThreads = arrayListOf<Thread>()

            for (event in threadRootEvents) {
                val messageContent = event.getLastMessageContent()

                var (status, isUrgent) = getStatus(
                    when (messageContent) {
                        is MessageTextContent -> messageContent.body
                        is MessageWithAttachmentContent -> messageContent.getFileName()
                        else -> null
                    }
                )

                var isUnread = false
                var lastCommentTime: Long? = null

                val threadEvents = arrayListOf(event)

                // Find replies to that thread, update the status from them and add them to the
                // thread history.
                for (replyEvent in replyEvents) {
                    if (replyEvent.root.getRootThreadEventId() != event.eventId
                        && replyEvent.getReplyToEventId() != event.eventId
                    ) {
                        continue
                    }

                    threadEvents.add(replyEvent)

                    val mc = replyEvent.getLastMessageContent()
                    val message = replyEvent.getLastMessageBody()

                    var statusText = message

                    getFormattedMessage(mc)?.let {
                        if (it.sender.isNotEmpty()) statusText = it.reply
                    }

                    val (newStatus, newIsUrgent) = getStatus(statusText)
                    if (newStatus != Status.UNDEFINED) {
                        status = newStatus
                        isUrgent = newIsUrgent
                    }

                    replyEvent.root.originServerTs?.also {
                        isUnread = it > mLastReadTs
                        lastCommentTime = it
                    }
                }

                val timelineId = mTimeline?.timelineID

                // Retry decryption, if it failed.
                if (!timelineId.isNullOrEmpty()) {
                    for (@Suppress("NAME_SHADOWING") event in threadEvents) {
                        if (event.isEncrypted() && (event.root.getClearContent()
                                ?.get("body") as? String).isNullOrEmpty()
                        ) {
                            mSession?.cryptoService()?.decryptEventAsync(
                                event.root,
                                timelineId,
                                object : MatrixCallback<MXEventDecryptionResult> {

                                    override fun onSuccess(data: MXEventDecryptionResult) {
                                        event.root.mxDecryptionResult = OlmDecryptionResult(
                                            payload = data.clearEvent,
                                            senderKey = data.senderCurve25519Key,
                                            keysClaimed = data.claimedEd25519Key?.let { mapOf("ed25519" to it) },
                                            forwardingCurve25519KeyChain = data.forwardingCurve25519KeyChain,
                                            isSafe = data.isSafe
                                        )
                                    }
                                })
                        }
                    }
                }

                val old = this@SharedTimeline.threadEvents[event.eventId]

                if (!old.isNullOrEmpty()) {
                    for (@Suppress("NAME_SHADOWING") event in old) {
                        // Only check event IDs, since, when a new message arrives, the next older
                        // one seems to get updated, too, and then the equality check will fail
                        // and the event will be doubled.
                        if (threadEvents.firstOrNull { it.eventId == event.eventId } == null) {
                            threadEvents.add(event)
                        }
                    }
                }

                // Thread root is first element, replies are sorted by timestamp.
                threadEvents.sortWith(
                    nullsLast(
                        compareBy(
                            { !it.isRootThread() && (it.isReply() || it.root.isThread()) },
                            { it.root.originServerTs })
                    )
                )

                CoroutineScope(Dispatchers.Main).launch {
                    // Cache messages' event IDs and corresponding replies to memory for usage in activities.
                    this@SharedTimeline.threadEvents[event.eventId] = threadEvents
                    mThreadEventsLive[event.eventId]?.postValue(threadEvents)
                }

                val senderId = event.senderInfo.userId

                val isOnline = room?.membershipService()?.getRoomMembers(ROOM_MEMBER_QUERY)?.let {
                    isOnline(it, senderId)
                } ?: true

                val tUser = mSession?.userService()?.getUser(senderId)
                val name = tUser?.displayName
                val avatarUrl = tUser?.avatarUrl


                var newThread = Thread(
                    event, name, status, isUnread,
                    isUrgent, lastCommentTime, isOnline, event.getLastMessageBody()
                )
                newThread.avatarUrl = avatarUrl
                newThreads.add(newThread)
            }

            CoroutineScope(Dispatchers.Main).launch {
                mThreads = newThreads

                _threads.postValue(mThreads)
            }
        }
    }

    private fun reinit() {
        if (mRoomSummariesLive != null) return

        mLifecycleRegistry.currentState = Lifecycle.State.STARTED

        mRoomSummariesLive = mSession?.roomService()?.getRoomSummariesLive(ROOM_SUMMARY_QUERY)
        mRoomSummariesLive?.observe(this@SharedTimeline) {
            CoroutineScope(Dispatchers.IO).launch {
                mRoomSummaries = it

                for (summary in it) {
                    val te = summary.latestPreviewableEvent

                    if (te != null
                        && summary.hasNewMessages
                        && RemoteImService.filterEvents(te)
                        && te.senderInfo.userId != myUserId)
                    {
                        CoroutineScope(Dispatchers.Main).launch {
                            _notifyableEvent.postValue(te)
                        }
                    }
                }

                CoroutineScope(Dispatchers.Main).launch {
                    reload()
                }
            }
        }
    }

    private fun reload() {
        val room = mRoomSummaries?.firstOrNull { it.membership == Membership.JOIN }?.roomId?.let {
            mSession?.roomService()?.getRoom(it)
        }

        if (room?.roomId == this.room?.roomId) return

        mTimeline?.dispose()
        mTimeline = null
        mLastGoodEventId = null
        mThreads = arrayListOf()
        threadEvents.keys().asSequence().forEach { threadEvents.remove(it) }
        mThreadEventsLive.forEach { it.value.postValue(emptyList()) }

        this.room = room

        mLifecycleRegistry.currentState = Lifecycle.State.RESUMED

        if (room == null) {
            mReadMarkerLive?.removeObservers(this)
            mReadMarkerLive = null

            mRoomMembersLive?.removeObservers(this)
            mRoomMembersLive = null

            //mUsersLive?.removeObservers(this)
            //mUsersLive = null

            return
        }

        if (mReadMarkerLive == null) {
            mReadMarkerLive = room.readService().getReadMarkerLive()

            mReadMarkerLive?.observe(this) { readMarker ->
                CoroutineScope(Dispatchers.IO).launch {
                    if (readMarker.getOrNull() != null) {
                        room.timelineService()
                            .getTimelineEvent(readMarker.get())?.root?.originServerTs?.let {
                            mLastReadTs = it
                        }
                    }
                }
            }
        }

        if (mRoomMembersLive == null) {
            mRoomMembersLive = room.membershipService().getRoomMembersLive(ROOM_MEMBER_QUERY)

            mRoomMembersLive?.observe(this) {
                CoroutineScope(Dispatchers.Main).launch {
                    var update = false

                    for (summary in it) {
                        val userId = summary.userId

                        val userThreads = mThreads.filter { it.userId == userId }
                        if (userThreads.isEmpty()) continue

                        val isOnline = isOnline(it, userId)

                        for (thread in userThreads) {
                            if (thread.isOnline != isOnline) {
                                thread.isOnline = isOnline
                                update = true
                            }

                            if (thread.userId?.equals(summary.userId) == true) {
                                thread.avatarUrl = summary.avatarUrl
                                update = true
                            }
                        }

                        if (update) _threads.postValue(mThreads)

                    }

                    CoroutineScope(Dispatchers.Main).launch {
                        val iterator = mThreads.iterator()

                        while (iterator.hasNext()) {
                            val thread = iterator.next()

                            if (it.firstOrNull { it.userId == thread.userId } == null) {
                                iterator.remove()
                                update = true
                            }
                        }

                        if (update) _threads.postValue(mThreads)
                    }
                }
            }
        }

        /**
        if (mUsersLive == null) {
            mUsersLive = mSession?.userService()?.getUsersLive()

            mUsersLive?.observe(this) {
                CoroutineScope(Dispatchers.IO).launch {
                    var update = false

                    for (user in it) {
                        val userId = user.userId

                        val userThreads = mThreads.filter { it.userId == userId }
                        if (userThreads.isEmpty()) continue

                        for (thread in userThreads) {
                            if (thread.avatarUrl != user.avatarUrl && !user.avatarUrl.isNullOrBlank()) {
                                thread.avatarUrl = user.avatarUrl
                                update = true
                            }
                        }
                    }

                    if (update) {
                        CoroutineScope(Dispatchers.Main).launch {
                            _threads.postValue(mThreads)
                        }
                    }
                }
            }
        }**/

        // First attempt at thread API support.
        // The Android SDK version is convoluted, almost undocumented and many methods aren't
        // actually used in Element Android.
        // So this is the best idea I currently have:
        //
        // Fetch the thread summaries, then load the corresponding TimelineEvents of all
        // events mentioned in that summary and throw that into `#onTimelineUpdated`
        // to give us a head start.
        CoroutineScope(Dispatchers.IO).launch {
            val summaries = room.threadsService().getAllThreadSummaries()

            val snapshot = arrayListOf<TimelineEvent>()

            for (summary in summaries) {
                room.timelineService().getTimelineEvent(summary.rootEventId)?.let {
                    snapshot.add(it)
                }

                summary.latestEvent?.eventId?.let { eventId ->
                    room.timelineService().getTimelineEvent(eventId)?.let {
                        snapshot.add(it)
                    }
                }

                // Completely unclear, where the events actually arrive. Code too confusing because
                // of indirections. No example in Element Android, but this looks like it would
                // give use the full thread history. Somehwere.
                // room.threadsService().fetchThreadTimeline(summary.rootEventId, "", HISTORY_CHUNK)
            }

            if (snapshot.isNotEmpty()) {
                this@SharedTimeline.onTimelineUpdated(snapshot)
            }

            mTimeline = room.timelineService().createTimeline(null, TimelineSettings(HISTORY_CHUNK))
            mTimeline?.addListener(this@SharedTimeline)
            mTimeline?.start()
        }
    }
}