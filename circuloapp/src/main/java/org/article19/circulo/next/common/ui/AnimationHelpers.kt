package org.article19.circulo.next.common.ui

import android.animation.Animator
import android.annotation.SuppressLint
import android.os.Build
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.ScaleAnimation
import android.view.animation.TranslateAnimation


@SuppressLint("NewApi")
object AnimationHelpers {
    fun translateY(view: View, fromY: Float, toY: Float, duration: Long) {
        if (Build.VERSION.SDK_INT >= 12) {
            if (duration == 0L) view.translationY = toY else view.animate().translationY(toY)
                .setDuration(duration).start()
        } else {
            val translate = TranslateAnimation(0f, 0f, fromY, toY)
            translate.duration = duration
            translate.isFillEnabled = true
            translate.fillBefore = true
            translate.fillAfter = true
            addAnimation(view, translate)
        }
    }

    fun scale(view: View, fromScale: Float, toScale: Float, duration: Long, whenDone: Runnable?) {
        if (Build.VERSION.SDK_INT >= 12) {
            if (duration == 0L) {
                view.scaleX = toScale
                view.scaleY = toScale
                whenDone?.run()
            } else {
                val animation = view.animate().scaleX(toScale).scaleY(toScale).setDuration(duration)
                if (whenDone != null) {
                    animation.setListener(object : Animator.AnimatorListener {
                        override fun onAnimationCancel(animation: Animator) {
                            whenDone.run()
                        }

                        override fun onAnimationEnd(animation: Animator) {
                            whenDone.run()
                        }

                        override fun onAnimationRepeat(animation: Animator) {}
                        override fun onAnimationStart(animation: Animator) {}
                    })
                }
                animation.start()
            }
        } else {
            val scale = ScaleAnimation(
                fromScale,
                toScale,
                fromScale,
                toScale,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            scale.duration = duration
            scale.isFillEnabled = true
            scale.fillBefore = true
            scale.fillAfter = true
            if (whenDone != null) {
                scale.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationEnd(animation: Animation) {
                        whenDone.run()
                    }

                    override fun onAnimationRepeat(animation: Animation) {}
                    override fun onAnimationStart(animation: Animation) {}
                })
            }
            addAnimation(view, scale)
        }
    }

    @JvmOverloads
    fun addAnimation(view: View, animation: Animation, first: Boolean = false) {
        var previousAnimation = view.animation
        if (previousAnimation == null || previousAnimation.javaClass == animation.javaClass) {
            if (animation.startTime == Animation.START_ON_FIRST_FRAME.toLong()) view.startAnimation(
                animation
            ) else view.animation =
                animation
            return
        }
        if (previousAnimation !is AnimationSet) {
            val newSet = AnimationSet(false)
            newSet.addAnimation(previousAnimation)
            previousAnimation = newSet
        }

        // Remove old of same type
        //
        val set = previousAnimation
        for (i in set.animations.indices) {
            val anim = set.animations[i]
            if (anim.javaClass == animation.javaClass) {
                set.animations.removeAt(i)
                break
            }
        }

        // Add this (first if it is a scale animation ,else at end)
        if (animation is ScaleAnimation || first) {
            set.animations.add(0, animation)
        } else {
            set.animations.add(animation)
        }
        animation.startNow()
        view.animation = set
    }
}

