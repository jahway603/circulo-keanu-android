package org.article19.circulo.next.common.utils

import android.util.TypedValue
import androidx.annotation.AttrRes
import org.article19.circulo.next.CirculoApp


object ResourceUtils {

    /**
     * Obtain a resource ID from provided attribute
     */
    fun getResIdFromAttribute(@AttrRes attribute: Int): Int {
        val outValue = TypedValue()
        CirculoApp.getInstance().applicationContext.theme.resolveAttribute(attribute, outValue, true)
        return outValue.resourceId
    }
}