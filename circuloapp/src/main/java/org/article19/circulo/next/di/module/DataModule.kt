package org.article19.circulo.next.di.module

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import org.article19.circulo.next.repository.push.PushRepository
import org.article19.circulo.next.repository.push.PushRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataModule {

    @Binds
    @Singleton
    abstract fun providePushRepository(repo: PushRepositoryImpl): PushRepository

}