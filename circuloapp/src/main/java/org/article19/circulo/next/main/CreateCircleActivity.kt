package org.article19.circulo.next.main

import android.app.Activity
import android.os.Bundle
import android.view.View
import info.guardianproject.keanu.core.ImApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.databinding.ActivityCreateCircleBinding
import org.matrix.android.sdk.api.session.room.model.GuestAccess
import org.matrix.android.sdk.api.session.room.model.PowerLevelsContent
import org.matrix.android.sdk.api.session.room.model.RoomDirectoryVisibility
import org.matrix.android.sdk.api.session.room.model.RoomHistoryVisibility
import org.matrix.android.sdk.api.session.room.model.RoomJoinRules
import org.matrix.android.sdk.api.session.room.model.create.CreateRoomParams
import org.matrix.android.sdk.api.session.room.powerlevels.Role

class CreateCircleActivity : BaseActivity() {

    private val mApp: ImApp?
        get() = application as? ImApp


    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    private lateinit var mViewBinding: ActivityCreateCircleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = ActivityCreateCircleBinding.inflate(layoutInflater)
        setContentView(mViewBinding.root)

        mViewBinding.toolbar.tvBackText.visibility = View.GONE
        mViewBinding.toolbar.tvAction.visibility = View.VISIBLE
        mViewBinding.toolbar.tvAction.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }

        val editTextRoomName = mViewBinding.editTextRoomName

        mViewBinding.btnNext.setOnClickListener {

            val params = CreateRoomParams()
            params.historyVisibility = RoomHistoryVisibility.INVITED
            params.visibility = RoomDirectoryVisibility.PRIVATE
            params.enableEncryption()
            params.name = editTextRoomName.text.toString()
            params.powerLevelContentOverride = PowerLevelsContent(Role.Admin.value)

            mCoroutineScope.launch {
                val roomId = mApp?.matrixSession?.roomService()?.createRoom(params)
                if (roomId != null) {
                    ImApp.sImApp?.matrixSession?.roomService()?.getRoom(roomId)?.stateService()
                        ?.updateJoinRule(RoomJoinRules.PUBLIC, GuestAccess.Forbidden)
                }
                setResult(Activity.RESULT_OK)
                finish()
            }


        }
    }
}