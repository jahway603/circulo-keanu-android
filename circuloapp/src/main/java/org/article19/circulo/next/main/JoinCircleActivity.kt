package org.article19.circulo.next.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.maxkeppeler.sheets.core.SheetStyle
import com.maxkeppeler.sheets.info.InfoSheet
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.extensions.betterMessage
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.Config
import org.article19.circulo.next.R
import org.article19.circulo.next.common.utils.ToastUtils
import org.article19.circulo.next.databinding.ActivityJoinCircleBinding
import org.matrix.android.sdk.api.failure.Failure
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary

class JoinCircleActivity : BaseActivity() {

    private val TAG = "JoinCircleActivity"

    private lateinit var mViewBinding: ActivityJoinCircleBinding

    private val mApp: ImApp?
        get() = application as? ImApp

    private var mCurrentRoom: RoomSummary? = null

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO) + mCoroutineExceptionHandler
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = ActivityJoinCircleBinding.inflate(layoutInflater)
        setContentView(mViewBinding.root)

        mViewBinding.toolbar.tvBackText.visibility = View.GONE
        mViewBinding.toolbar.tvAction.visibility = View.VISIBLE
        mViewBinding.toolbar.tvAction.setOnClickListener {
            finish()
        }

        val editTextCircleLink = mViewBinding.editTextCircleLink

        if (intent?.dataString?.isNotEmpty() == true)
            editTextCircleLink.setText(intent?.dataString)
        if (intent?.data != null)
            editTextCircleLink.setText(intent?.data?.toString())

        mViewBinding.btnNext.setOnClickListener {
            Config.ENABLE_EMPTY_STATE = false

            joinRoom()

        }

        queryCircles()
    }

    fun queryCircles () {
        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN)
        val queryParams = builder.build()

        val listRooms = mApp?.matrixSession?.roomService()?.getRoomSummaries(queryParams)
        if (listRooms?.isEmpty() == false)
            mCurrentRoom = listRooms.get(0)
    }

    private val mCoroutineExceptionHandler = CoroutineExceptionHandler { coroutineContext, exception ->
        Log.e(TAG, exception.toString())
        if (exception is Failure.ServerError) {
            val httpCode = exception.httpCode
            val errorMessage = exception.betterMessage

            if (httpCode == 404 && "No known servers" == errorMessage) {
                ToastUtils.showToast(getString(R.string.error_message_circle_not_found), this)
            }
        }
    }

    private fun joinRoom () {
        var roomId = mViewBinding.editTextCircleLink.text.toString()

        if (roomId.isNotEmpty()) {
            mCoroutineScope.launch (mCoroutineExceptionHandler) {

                roomId = roomId.split("#")[1]

                if (mCurrentRoom == null) {
                    roomId = mApp?.matrixSession?.roomService()?.joinRoom(roomId).toString()
                    if (isTaskRoot) {
                        finish()
                        startActivity((application as? ImApp)?.router?.main(this@JoinCircleActivity, preselectTab = MainActivity.TAB_CIRCLE))
                    } else {
                        setResult(RESULT_OK, Intent())
                        finish()
                    }
                }
                else
                {
                    showLeaveCircleAlert(roomId)
                }


            }
        }

    }

    private fun showLeaveCircleAlert(newRoomId: String) {

        //val newRoom = mApp?.matrixSession?.getRoom(newRoomId)
        //val newRoomDisplayName = newRoom?.roomSummary()?.displayName

        val leaveCircleString =
            getString(R.string.leave_circle_title, mCurrentRoom?.displayName)
        val leaveCircleMessage = getString(R.string.leave_circle_message)
        val notNowString = getString(R.string.not_now)
        val yesString = getString(R.string.Yes)
        let {
            InfoSheet().show(it) {
                title(leaveCircleString)
                content(leaveCircleMessage)
                style(SheetStyle.DIALOG)
                onNegative(notNowString) {
                    dismiss()
                }
                onPositive(yesString) {

                    mCoroutineScope.launch {

                        //leave current room
                        mApp?.matrixSession?.roomService()?.leaveRoom(mCurrentRoom?.roomId!!,"")

                        //join new room
                        mApp?.matrixSession?.roomService()?.joinRoom(newRoomId).toString()
                    }

                    finish()
                }

            }
        }
    }

}