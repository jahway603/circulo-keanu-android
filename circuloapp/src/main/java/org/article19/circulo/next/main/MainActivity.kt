package org.article19.circulo.next.main

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.beautycoder.pflockscreen.PFFLockScreenConfiguration
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.beautycoder.pflockscreen.viewmodels.PFPinCodeViewModel
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.MainActivity.Companion.EXTRA_PRESELECT_TAB
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.common.providers.PreferenceProvider
import org.article19.circulo.next.databinding.ActivityMainBinding
import org.article19.circulo.next.main.listeners.OnCircleLoadedListener
import org.article19.circulo.next.main.now.Thread
import org.article19.circulo.next.main.now.ThreadsAdapter
import org.article19.circulo.next.main.updatestatus.StatusDetailActivity
import org.article19.circulo.next.notify.viewmodel.PushViewModel
import org.article19.circulo.next.notify.viewmodel.PushViewModelImpl
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.sync.SyncState
import org.matrix.android.sdk.api.session.user.model.User
import timber.log.Timber


@AndroidEntryPoint
class MainActivity : BaseActivity(), Observer<Any> {

    private lateinit var fragmentNow: NowFragment
    private lateinit var fragmentCircle: Fragment
    private lateinit var fragmentOurCircle: Fragment
    private lateinit var fragmentProfile: ProfileFragment
    private lateinit var fragmentManager: FragmentManager
    private lateinit var currentFragment: Fragment
    private lateinit var mBinding: ActivityMainBinding

    private var inACircle : Boolean = false

    private val mApp: CirculoApp?
        get() = application as? CirculoApp

    private var mRoomSummary: RoomSummary? = null
    private var mRoom : Room? = null

    private val mSession: Session?
        get() = mApp?.matrixSession

    private var mCircleLoadedListeners = mutableSetOf<OnCircleLoadedListener>()

    private val mUserInfoLiveData = MutableLiveData<User?>()
    val observableUserInfo = mUserInfoLiveData as LiveData<User?>

    private lateinit var mPushViewModel: PushViewModel

    val joinOrCreateCircleResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            val queryParams = RoomSummaryQueryParams.Builder().apply {
                memberships = listOf(Membership.JOIN)
            }.build()
            inACircle = mApp?.matrixSession?.roomService()?.getRoomSummaries(queryParams)?.isEmpty() == false

            currentFragment = if (inACircle) {
                fragmentManager.beginTransaction().hide(currentFragment).show(fragmentOurCircle).commit()
                fragmentOurCircle
            } else {
                fragmentManager.beginTransaction().hide(currentFragment).show(fragmentCircle).commit()
                fragmentCircle
            }
        }
    }

    val viewCircleInfoResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        val isLeavingCircle = it.data?.getStringExtra(BUNDLE_KEY_LEAVE_CIRCLE)?.equals(BUNDLE_VALUE_LEAVE_CIRCLE) == true
        if ((it.resultCode == Activity.RESULT_OK) && isLeavingCircle) {
            fragmentManager.beginTransaction().hide(currentFragment).show(fragmentCircle).commit()
            currentFragment = fragmentCircle
        }
    }

    companion object {
        const val KEY_LAST_FRAGMENT_INDEX = "key_last_fragment_index"
        const val TAB_NOW = 0
        const val TAB_CIRCLE = 1
        const val TAB_PROFILE = 2
        const val BUNDLE_KEY_LEAVE_CIRCLE = "bundle_key_leave_circle"
        const val BUNDLE_VALUE_LEAVE_CIRCLE = "bundle_value_leave_circle"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPushViewModel = ViewModelProvider(this)[PushViewModelImpl::class.java]
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.bottomBar.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.now -> {
                    fragmentManager.beginTransaction().hide(currentFragment).show(fragmentNow).commit()
                    currentFragment = fragmentNow
                    true
                }

                R.id.circle -> {
                    currentFragment = if (inACircle) {
                        fragmentManager.beginTransaction().hide(currentFragment).show(fragmentOurCircle).commit()
                        fragmentOurCircle
                    } else {
                        fragmentManager.beginTransaction().hide(currentFragment).show(fragmentCircle).commit()
                        fragmentCircle
                    }

                    true
                }

                R.id.profile -> {
                    fragmentManager.beginTransaction().hide(currentFragment).show(fragmentProfile).commit()
                    currentFragment = fragmentProfile

                    true
                }

                else -> false
            }
        }

        queryCircles()

        fragmentManager = supportFragmentManager
        initFragments(savedInstanceState)

        checkAndShowLockScreenFragment()

      //  disableBatteryOptimization()

        requestNotificationPermission()
        startObservingUserInfo()

        stopVibration()
        initFCMData()

    }

    private fun initFCMData() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener {
            if (!it.isSuccessful) {
                return@addOnCompleteListener
            }
            onNewToken(it.result)
        }
    }

    private fun onNewToken(token: String) {
        mPushViewModel.registerPushToken(token)
    }

    private fun initFragments(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            fragmentNow = NowFragment()
            fragmentCircle = CircleFragment()
            fragmentOurCircle = OurCircleFragment()
            fragmentProfile = ProfileFragment()

            currentFragment = fragmentNow

            fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentCircle, CircleFragment.TAG).hide(fragmentCircle).commit()
            fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentOurCircle, OurCircleFragment.TAG).hide(fragmentOurCircle).commit()
            fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentProfile, ProfileFragment.TAG).hide(fragmentProfile).commit()

            fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentNow, NowFragment.TAG).commit()

        } else {
            //Restore the fragment's instance
            fragmentNow = fragmentManager.getFragment(savedInstanceState, NowFragment.TAG) as NowFragment
            fragmentCircle = fragmentManager.getFragment(savedInstanceState, CircleFragment.TAG) as CircleFragment
            fragmentOurCircle = fragmentManager.getFragment(savedInstanceState, OurCircleFragment.TAG) as OurCircleFragment
            fragmentProfile = fragmentManager.getFragment(savedInstanceState, ProfileFragment.TAG) as ProfileFragment

            val lastFragmentIndex = savedInstanceState.getInt(KEY_LAST_FRAGMENT_INDEX)
            currentFragment = getFragment(lastFragmentIndex)
        }

        mCircleLoadedListeners.add(fragmentOurCircle as OurCircleFragment)
        mCircleLoadedListeners.add(fragmentProfile)

        val preselectedTabPos = intent.getIntExtra(EXTRA_PRESELECT_TAB, -1)
        if (preselectedTabPos >= 0) {
            mBinding.bottomBar.selectedItemId = getTabIdFromPosition(preselectedTabPos)
        }
    }

    private fun getFragment(index: Int): Fragment {
        return when (index) {
            TAB_NOW -> fragmentNow
            TAB_CIRCLE -> fragmentOurCircle
            TAB_PROFILE -> fragmentProfile
            else -> {fragmentNow}
        }
    }

    private fun getTabIdFromPosition(pos: Int): Int {
        return when (pos) {
            TAB_NOW -> R.id.now
            TAB_CIRCLE -> R.id.circle
            TAB_PROFILE -> R.id.profile
            else -> R.id.now
        }
    }


    fun replaceCurrentFragment(newFragment: Fragment) {
        fragmentManager.beginTransaction().hide(currentFragment).add(R.id.layout_main_container, newFragment, "").commit()
        currentFragment = newFragment
        fragmentManager.beginTransaction().show(currentFragment).commit()
    }

    fun showProfileFragment(reloadUserInfo: Boolean) {
        fragmentManager.beginTransaction().hide(currentFragment).show(fragmentProfile).commit()
        currentFragment = fragmentProfile

        if (reloadUserInfo) {
            //this is often called after a reload fragment
            fragmentProfile.reloadUserInfo()
        }
    }

    fun removeLockScreenFragment() {
        mBinding.bottomBar.visibility = View.VISIBLE
        fragmentManager.beginTransaction().remove(currentFragment).show(fragmentNow).commit()
        currentFragment = fragmentNow
        mBinding.bottomBar.selectedItemId = R.id.now
    }

    private fun queryCircles() {
        val syncService = mApp?.matrixSession?.syncService()

        syncService?.startSync(true)
        syncService?.startAutomaticBackgroundSync(5L,5L)

        val queryParams = RoomSummaryQueryParams.Builder().apply {
            memberships = listOf(Membership.JOIN)
        }.build()

        mApp?.matrixSession?.roomService()?.getRoomSummariesLive(queryParams)?.observe(this, this)

        syncService?.getSyncStateLive()?.observe(this, this)
    }


    override fun onChanged(value: Any) {
        val syncState = value as? SyncState

        if (syncState != null) {
            Timber.d("SyncState: $syncState")

            return
        }

        val roomSummary = (value as? List<*>)?.firstOrNull() as? RoomSummary

        val newInACircle: Boolean
        if (roomSummary != null) {
            newInACircle = true

            //Circulo is always meant to be in just ONE room, so we can just get that room
            mRoomSummary = roomSummary
            mRoom = mSession?.roomService()?.getRoom(roomSummary.roomId)

            for (circleLoadedListener in mCircleLoadedListeners) {
                circleLoadedListener.onCircleLoaded(mRoom, mRoomSummary)
            }
        }
        else {
            newInACircle = false
        }

        inACircle = newInACircle

        //Circle was just deleted. Reset Now fragment to default empty view
        if (inACircle && !newInACircle) {
            fragmentNow.showDefaultEmptyView()
        }

        inACircle = newInACircle

        if (getCurrentSelectedPos() == 1) {
            currentFragment = if (inACircle) {
                fragmentManager.beginTransaction().hide(currentFragment).show(fragmentOurCircle).commit()
                fragmentOurCircle
            } else {
                fragmentManager.beginTransaction().hide(currentFragment).show(fragmentCircle).commit()
                fragmentCircle
            }
        }
    }

    fun hideNavigationView() {
        mBinding.bottomBar.visibility = View.GONE
    }

    fun showNavigationView() {
        mBinding.bottomBar.visibility = View.VISIBLE
    }

    /**
     * check if the log is enables or not
     */
    private fun checkAndShowLockScreenFragment() {
        PFPinCodeViewModel().isPinCodeEncryptionKeyExist.observe(
            this,
            Observer { result ->
                if (result == null) {
                    return@Observer
                }
                if (result.error != null) {
                    Toast.makeText(
                        this@MainActivity,
                        info.guardianproject.keanuapp.R.string.lock_screen_passphrases_not_matching,
                        Toast.LENGTH_SHORT
                    ).show()
                    return@Observer
                }
                showLockScreenFragment(result.result)
            }
        )
    }

    /**
     * show or hide the lock screen
     */
    fun showLockScreenFragment(isPinExist: Boolean) {

        if (!isPinExist) {
            return
        }

        val builder = PFFLockScreenConfiguration.Builder(this)
            .setTitle(getString(info.guardianproject.keanuapp.R.string.lock_screen_passphrase_not_set_enter))
            .setCodeLength(6)
            .setNewCodeValidation(true)
            .setNewCodeValidationTitle(getString(info.guardianproject.keanuapp.R.string.lock_screen_confirm_passphrase))
            .setErrorVibration(true)
            .setClearCodeOnError(true)
            .setErrorAnimation(true)
            .setTitle(getString(info.guardianproject.keanuapp.R.string.title_activity_lock_screen))

        val fragment = PFLockScreenFragment()
        builder.setMode(PFFLockScreenConfiguration.MODE_AUTH)
        fragment.setEncodedPinCode(PreferenceProvider.appPreferences.getString(KeanuConstants.PREFERENCE_KEY_ENCODED_PASS, ""))
        fragment.setLoginListener(mLoginListener)
        fragment.setConfiguration(builder.build())

        mBinding.bottomBar.visibility = View.GONE
        replaceCurrentFragment(fragment)
    }

    fun showAlertFragment () {
        mBinding.bottomBar.visibility = View.GONE
        val fragment = SendAlertFragment()
        replaceCurrentFragment(fragment)
    }

    fun removeAlertFragment() {
        mBinding.bottomBar.visibility = View.VISIBLE
        fragmentManager.beginTransaction().remove(currentFragment).show(fragmentNow).commit()
        currentFragment = fragmentNow
        mBinding.bottomBar.selectedItemId = R.id.now
    }

    private val mLoginListener: PFLockScreenFragment.OnPFLockScreenLoginListener = object :
        PFLockScreenFragment.OnPFLockScreenLoginListener {
        override fun onCodeInputSuccessful() {
            removeLockScreenFragment()
        }

        override fun onFingerprintSuccessful() {
            removeLockScreenFragment()
        }

        override fun onPinLoginFailed() {
            Toast.makeText(this@MainActivity, info.guardianproject.keanuapp.R.string.lock_screen_passphrases_not_matching, Toast.LENGTH_SHORT).show()
        }

        override fun onFingerprintLoginFailed() {

            Toast.makeText(this@MainActivity, info.guardianproject.keanuapp.R.string.lock_screen_passphrases_not_matching, Toast.LENGTH_SHORT).show()
        }
    }

    fun stopApp () {
        mApp?.stopApp()
        finish()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        //Save the fragment's instance
        fragmentManager.putFragment(outState, NowFragment.TAG, fragmentNow)
        fragmentManager.putFragment(outState, CircleFragment.TAG, fragmentCircle)
        fragmentManager.putFragment(outState, OurCircleFragment.TAG, fragmentOurCircle)
        fragmentManager.putFragment(outState, ProfileFragment.TAG, fragmentProfile)

        outState.putInt(KEY_LAST_FRAGMENT_INDEX, getCurrentSelectedPos())
    }

    fun getCurrentSelectedPos(): Int {
        return when (mBinding.bottomBar.selectedItemId) {
            R.id.now -> TAB_NOW
            R.id.circle -> TAB_CIRCLE
            R.id.profile ->  {
                if (currentFragment !is ProfileFragment) {
                    -1
                } else {
                    TAB_PROFILE
                }
            }
            else -> {
                TAB_NOW
            }
        }
    }

    fun setCurrentFragment(fragment: Fragment) {
        currentFragment = fragment
    }

    /**
    private fun disableBatteryOptimization() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val intent = Intent()
            val packageName = packageName
             val pm = getSystemService(POWER_SERVICE) as PowerManager
              if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                  intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                  intent.data = Uri.parse("package:$packageName")
                  startActivity(intent)
              }
        }
    }**/

    private fun requestNotificationPermission () {
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.POST_NOTIFICATIONS
            ) -> {
                // You can use the API that requires the permission.
            }
            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
                }
            }
        }
    }

    private fun stopVibration() {
        mApp?.cancelNotifications()
    }

    override fun onNewIntent(intent: Intent?) {
        Log.d("tuancoltech", "onNewIntent: " + intent?.getBundleExtra("body") + "\nbundle: " + intent?.extras)
        super.onNewIntent(intent)

        val detailIntent = Intent(this, StatusDetailActivity::class.java)

        val personStatus = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent?.extras?.getParcelable(ThreadsAdapter.BUNDLE_EXTRA_THREAD, Thread::class.java)
        } else {
            @Suppress("DEPRECATION")
            intent?.extras?.getParcelable(ThreadsAdapter.BUNDLE_EXTRA_THREAD)
        }

        personStatus?.let {
            detailIntent.putExtra(StatusDetailActivity.EXTRA_THREAD_ID, it.eventId)
            startActivity(detailIntent)
        }
    }

    private fun startObservingUserInfo() {
        mApp?.matrixSession?.myUserId?.let {
            mApp?.matrixSession?.userService()?.getUserLive(it)?.observe(this) { currentUser ->
                mUserInfoLiveData.value = currentUser.getOrNull()
            }
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { }
}
