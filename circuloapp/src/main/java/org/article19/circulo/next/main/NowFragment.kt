package org.article19.circulo.next.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.util.WrapContentLinearLayoutManager
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.databinding.FragmentNowBinding
import org.article19.circulo.next.main.now.ThreadsAdapter
import org.article19.circulo.next.main.updatestatus.UpdateLocationStatusService
import org.article19.circulo.next.main.updatestatus.UpdateStatusActivity
import java.util.Timer


class NowFragment: Fragment() {

    private lateinit var mBinding: FragmentNowBinding

    companion object {
        const val TAG = "NowFragment"
    }

    private val mApp: CirculoApp?
        get() = activity?.application as? CirculoApp

    private val mSharedTimeline: SharedTimeline
        get() = SharedTimeline.instance

    private var mThreadsAdapter : ThreadsAdapter? = null

    private var mTimerPress = Timer()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        mBinding = FragmentNowBinding.inflate(inflater, container, false)

        mBinding.toolbar.ivNewStatus.setOnClickListener {

            newStatus()
        }

        mBinding.toolbar.ivNewLocation.setOnClickListener {
            askSendLocationStatus()
        }

        val llm = WrapContentLinearLayoutManager(context)
        mBinding.recyclerViewPeople.layoutManager = llm

        subscribeUserInfo()
        update(false)

        mThreadsAdapter = ThreadsAdapter(requireActivity(), emptyList())
        mBinding.recyclerViewPeople.adapter = mThreadsAdapter

        mBinding.recyclerViewPeople.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if (!mSharedTimeline.containsStatusByMe) {
                    if (dy > 0) {
                        mBinding.btnSendAlert.visibility = View.GONE
                    } else {
                        mBinding.btnSendAlert.visibility = View.VISIBLE
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        mBinding.btnSendAlert.setOnLongClickListener {

            sendUrgentStatus()

            false
        }

        return mBinding.root
    }

    private fun newStatus () {
        startActivity(Intent(context, UpdateStatusActivity::class.java))

        var isNetworkOnline = 0.0
        if (mApp?.isNetworkAvailable() == true)
            isNetworkOnline = 1.0

        mApp?.cleanInsightsManager?.measureEvent("status-created",
            "online", isNetworkOnline)
    }

    private fun sendUrgentStatus () {

        if(ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(requireActivity(),arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION),requestCodeLocationUrgent)

        }
        else {
            (requireActivity() as MainActivity).showAlertFragment()
        }

    }

    private fun askSendLocationStatus () {

        val items = arrayOf(getString(R.string.share_location_15_min),
            getString(R.string.share_location_1_hr),
            getString(R.string.share_location_4_hrs),
            getString(R.string.share_location_8_hrs),
            getString(R.string.share_location_until_i_arrive));

        val builder = AlertDialog.Builder(requireActivity())
        builder
            .setItems(items) { dialog, which ->

                sendLocationStatus(which)

            }
            .setCancelable(true)

        val alert = builder.create()
        alert.show()


    }

    private var requestCodeLocation = 9999
    private var requestCodeLocationUrgent = 9998

    private fun sendLocationStatus (timeIntervalSelect : Int) {
        if(ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(requireActivity(),arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION),requestCodeLocation)

        }
        else
        {
            var timeIntervalMin = 15;

            when (timeIntervalSelect)
            {
                0 -> timeIntervalMin = 15
                1 -> timeIntervalMin = 60
                2 -> timeIntervalMin = 60*4
                3 -> timeIntervalMin = 60*8
                4 -> timeIntervalMin = -1
            }
            val intent = Intent(context, UpdateLocationStatusService::class.java)

            intent.putExtra(UpdateLocationStatusService.EXTRA_TIME_TOTAL_MIN,timeIntervalMin)
            intent.putExtra(UpdateLocationStatusService.EXTRA_SEND_LOCATION,true)

            ContextCompat.startForegroundService(requireContext(),intent)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            //if location granted, then add location
            requestCodeLocation -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    askSendLocationStatus()
                } else {

                }
                return
            }
            requestCodeLocationUrgent -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendUrgentStatus()
                } else {

                }
                return
            }
        }
    }


    override fun onResume() {
        super.onResume()

        update(mSharedTimeline.threads.value?.isNotEmpty() == true)

        mSharedTimeline.threads.observe(this) { threads ->
            mThreadsAdapter?.submitList(threads.sortedByDescending { it.lastCommentTime })

            update(threads.isNotEmpty())
        }
    }

    override fun onPause() {
        super.onPause()

        mSharedTimeline.threads.removeObservers(this)
    }

    fun showDefaultEmptyView() {
        mBinding.layoutNoCircleState.visibility = View.VISIBLE
        mBinding.tvPeopleTitle.visibility = View.GONE
        mBinding.recyclerViewPeople.visibility = View.GONE
        mBinding.btnSendAlert.visibility = View.GONE
        mBinding.toolbar.ivNewStatus.visibility = View.GONE
        mBinding.toolbar.ivNewLocation.visibility =  View.GONE
    }

    private fun subscribeUserInfo() {
        (activity as? MainActivity)?.observableUserInfo?.observe(viewLifecycleOwner) { user ->
            user?.let {
                mBinding.tvNowGreetings.text = getString(R.string.hello_, it.displayName)
                mBinding.tvNowGreetings.visibility = View.VISIBLE
            }
        }
    }

    private fun update(hasPeople: Boolean) {
        if (mSharedTimeline.isInsideCircle) {
            mBinding.layoutNoCircleState.visibility = View.GONE

            mBinding.toolbar.ivNewStatus.visibility = if (mSharedTimeline.containsStatusByMe) View.GONE else View.VISIBLE
            mBinding.toolbar.ivNewLocation.visibility = if (mSharedTimeline.containsStatusByMe) View.GONE else View.VISIBLE
            mBinding.btnSendAlert.visibility = if (mSharedTimeline.containsStatusByMe) View.GONE else View.VISIBLE

            if (hasPeople) {
                mBinding.recyclerViewPeople.visibility = View.VISIBLE
                mBinding.layoutNoStatuses.visibility = View.GONE
                mBinding.tvPeopleTitle.visibility = View.VISIBLE
                mBinding.recyclerViewPeople.smoothScrollToPosition(0)

            }
            else {
                mBinding.recyclerViewPeople.visibility = View.GONE
                mBinding.layoutNoStatuses.visibility = View.VISIBLE
                mBinding.tvPeopleTitle.visibility = View.GONE
            }
        }
        else {
            showDefaultEmptyView()
        }
    }
}

