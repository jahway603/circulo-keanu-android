package org.article19.circulo.next.main

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import kotlinx.coroutines.withContext
import org.article19.circulo.next.R
import org.article19.circulo.next.common.providers.PreferenceProvider
import org.article19.circulo.next.databinding.FragmentProfileBinding
import org.article19.circulo.next.main.listeners.OnCircleLoadedListener
import org.article19.circulo.next.main.listeners.OnNotificationChanged
import org.article19.circulo.next.profile.EditProfileFragment
import org.article19.circulo.next.settings.SettingAboutFragment
import org.article19.circulo.next.settings.SettingAccountFragment
import org.article19.circulo.next.settings.SettingFeedbackFragment
import org.article19.circulo.next.settings.SettingNotificationsFragment
import org.article19.circulo.next.settings.SettingPhysicalSafetyFragment
import org.article19.circulo.next.settings.SettingPreferencesFragment
import org.article19.circulo.next.settings.SettingSecurityPrivacyFragment
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import timber.log.Timber

class ProfileFragment: Fragment(), OnCircleLoadedListener, OnNotificationChanged {

    companion object {
        const val BUNDLE_EXTRA_DISPLAY_NAME = "bundle_extra_display_name"
        const val BUNDLE_EXTRA_AVATAR_URL = "bundle_extra_avatar_url"
        const val REQUEST_KEY_PROFILE_CHANGE = "request_key_profile_change"
        const val BUNDLE_KEY_IS_AVATAR_CHANGE = "bundle_key_is_avatar_change"
        const val BUNDLE_EXTRA_NOTIFICATION_ENABLED = "bundle_extra_notification_enabled"
        const val TAG = "ProfileFragment"
    }

    private val mApp: ImApp?
        get() = activity?.application as? ImApp


    private val mSession: Session?
        get() = mApp?.matrixSession

    private var mRoomSummary: RoomSummary? = null
    private var mRoom : Room? = null

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO) + mCoroutineExceptionHandler
    }

    private val mCoroutineExceptionHandler = CoroutineExceptionHandler { _, exception ->
        Timber.e("mCoroutineExceptionHandler: $exception")
    }

    private var mAvatarUrl: String? = null
    private var mNotificationsEnabled = true

    private lateinit var mBinding: FragmentProfileBinding
    private val AVATAR_SIZE = 512

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        mBinding = FragmentProfileBinding.inflate(inflater, container, false)

        val username = mApp?.matrixSession?.myUserId
        setDisplayName(username.toString().split(":")[0])

        mBinding.layoutItemAccount.setOnClickListener {
            mApp?.router?.devices(requireActivity())?.let { it1 -> startActivity(it1) }
        }

        mBinding.layoutItemSecurityPrivacy.setOnClickListener {
            mApp?.router?.settings(requireActivity())?.let { it1 -> startActivity(it1) }
        }

        mBinding.tvNotificationState.setText(if (mNotificationsEnabled) R.string.on else R.string.off)

        subscribeUserInfo()

        mBinding.btnEditProfile.setOnClickListener {
            val args = Bundle()
            args.putString(BUNDLE_EXTRA_DISPLAY_NAME, mBinding.tvUsername.text.toString())
            args.putString(BUNDLE_EXTRA_AVATAR_URL, mAvatarUrl)
            val editProfileFragment = EditProfileFragment()
            editProfileFragment.arguments = args
            (activity as MainActivity).replaceCurrentFragment(editProfileFragment)
        }

        mBinding.layoutItemNotifications.setOnClickListener {
            val notificationFragment = SettingNotificationsFragment(this)
            val bundleData = Bundle()
            bundleData.putBoolean(BUNDLE_EXTRA_NOTIFICATION_ENABLED, mNotificationsEnabled)
            notificationFragment.arguments = bundleData
            (activity as MainActivity).replaceCurrentFragment(notificationFragment)
        }

        mBinding.layoutItemPreferences.setOnClickListener {
            val preferencesFragment = SettingPreferencesFragment()
            (activity as MainActivity).replaceCurrentFragment(preferencesFragment)
        }

        mBinding.layoutItemAccount.setOnClickListener {
            val accountFragment = SettingAccountFragment()
            (activity as MainActivity).replaceCurrentFragment(accountFragment)
        }

        mBinding.layoutItemSecurityPrivacy.setOnClickListener {
            val securityPrivacyFragment = SettingSecurityPrivacyFragment()
            (activity as MainActivity).replaceCurrentFragment(securityPrivacyFragment)
        }

        mBinding.layoutItemPhysicalSafety.setOnClickListener {
            val physicalSafetyFragment = SettingPhysicalSafetyFragment()
            (activity as MainActivity).replaceCurrentFragment(physicalSafetyFragment)
        }

        mBinding.layoutItemAbout.setOnClickListener {
            val aboutFragment = SettingAboutFragment()
            (activity as MainActivity).replaceCurrentFragment(aboutFragment)
        }

        mBinding.layoutItemFeedback.setOnClickListener {
            val feedbackFragment = SettingFeedbackFragment()
            (activity as MainActivity).replaceCurrentFragment(feedbackFragment)
        }

        mBinding.layoutItemStopApp.setOnClickListener {
            (activity as MainActivity).stopApp()
        }

        //Listen to possible avatar changes when coming back here from EditProfileFragment
        setFragmentResultListener(REQUEST_KEY_PROFILE_CHANGE) { _, bundle ->
            val shouldRefreshAvatar = bundle.getBoolean(BUNDLE_KEY_IS_AVATAR_CHANGE)
            if (shouldRefreshAvatar) {
                reloadUserInfo()
            }
        }

        return mBinding.root
    }

    private fun loadRoomState () {

        val data = mRoom?.roomPushRuleService()?.getLiveRoomNotificationState()
        data?.observe(requireActivity()) {
            if (isAdded) {
                //headerViewHolder?.checkNotifications?.isChecked = data.value == RoomNotificationState.ALL_MESSAGES
                if (data.value == RoomNotificationState.ALL_MESSAGES) {
                    mBinding.tvNotificationState.text = getString(R.string.on)
                    mNotificationsEnabled = true
                }
                else {
                    mBinding.tvNotificationState.text = getString(R.string.off)
                    mNotificationsEnabled = false
                }

                ImApp.sImApp?.preferenceProvider?.setNotificationEnable(mNotificationsEnabled)
            }
        }
    }

    private fun setDisplayName (display : String)
    {
        activity?.runOnUiThread {
            mBinding.tvUsername.text = display
        }
    }

    fun reloadUserInfo() {
        mCoroutineScope.launch (mCoroutineExceptionHandler) {
            val user = mApp?.matrixSession?.myUserId?.let { mApp?.matrixSession?.userService()?.getUser(it) }
            if (user?.displayName?.isNotEmpty() == true) {
                setDisplayName(user.displayName!!)
                mAvatarUrl = user.avatarUrl

                mSession?.contentUrlResolver()?.resolveThumbnail(mAvatarUrl, AVATAR_SIZE, AVATAR_SIZE, ContentUrlResolver.ThumbnailMethod.SCALE)
                    ?.let {
                        withContext(Dispatchers.Main) {
                            GlideUtils.loadImageFromUri(activity, Uri.parse(it), mBinding.ivAvatar, true)
                        }
                    }

            }
        }
    }

    private fun subscribeUserInfo () {
        (activity as? MainActivity)?.observableUserInfo?.observe(ProfileFragment@this) {
            val user = it ?: return@observe
            setDisplayName(user.displayName ?: user.userId)
            mCoroutineScope.launch (mCoroutineExceptionHandler) {
                mAvatarUrl = user.avatarUrl
                mSession?.contentUrlResolver()?.resolveThumbnail(mAvatarUrl, AVATAR_SIZE, AVATAR_SIZE, ContentUrlResolver.ThumbnailMethod.SCALE)
                    ?.let {
                        withContext(Dispatchers.Main) {
                            GlideUtils.loadImageFromUri(activity, Uri.parse(it), mBinding.ivAvatar, true)
                        }
                    }
            }
        }
    }

    override fun onCircleLoaded(circle: Room?, circleSummary: RoomSummary?) {
        mRoom = circle
        mRoomSummary = circleSummary
        loadRoomState()

        mRoom?.roomPushRuleService()?.getLiveRoomNotificationState()?.observe(requireActivity()) {

            mNotificationsEnabled = it?.equals(RoomNotificationState.MUTE) == false

            mBinding.tvNotificationState.setText(if (mNotificationsEnabled) R.string.on else R.string.off)
        }
    }

    override fun onChanged(enable: Boolean) {

        Log.d("tuancoltech", "ProfileFragment onChanged enable: " + enable + ". mNotificationsEnabled: " + mNotificationsEnabled)

        if (enable != mNotificationsEnabled) {
            mNotificationsEnabled = enable
            mCoroutineScope.launch(mCoroutineExceptionHandler) {
            mRoom?.roomPushRuleService()?.setRoomNotificationState(if (mNotificationsEnabled) RoomNotificationState.ALL_MESSAGES else RoomNotificationState.MUTE)
                PreferenceProvider.enableNotification = mNotificationsEnabled
            }

            mBinding.tvNotificationState.setText(if (mNotificationsEnabled) R.string.on else R.string.off)
        }
    }
}
