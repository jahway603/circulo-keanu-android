package org.article19.circulo.next.main


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import info.guardianproject.keanu.core.KeanuConstants.LOG_TAG
import org.article19.circulo.next.databinding.FragmentSendAlertBinding
import org.article19.circulo.next.main.updatestatus.UpdateStatusActivity
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Timer
import java.util.TimerTask


class SendAlertFragment : Fragment(), OnTouchListener {

    private var recorder: MediaRecorder? = null
    private var fileAudio: String? = null

    private var recordTime = 1000L;
    private var requestCodeAudio = 9998

    private lateinit var mBinding: FragmentSendAlertBinding

    private  val timer = Timer();

    companion object {
        const val TAG = "Alert"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        mBinding = FragmentSendAlertBinding.inflate(inflater, container, false)
        mBinding.alertButton?.setOnTouchListener(this)
        fileAudio = File.createTempFile("alert","m4a").absolutePath

        return mBinding.root
    }

    override fun onResume() {
        super.onResume()

        if(ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(requireActivity(),arrayOf(Manifest.permission.RECORD_AUDIO),requestCodeAudio)
        }
        else {
            if (recorder == null)
                startRecording()
        }


    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            requestCodeAudio -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                    startRecording()
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the feature requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun startRecording() {
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(fileAudio)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

            try {
                prepare()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }

            start()
        }

        timer?.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                recordTime+=1000
                updateTimer()
            }
        }, 0, 1000)

    }
    private fun updateTimer() {
        activity?.runOnUiThread {
            mBinding.textAudioTimer.text = "${SimpleDateFormat("mm:ss").format(recordTime)}"
        }
    }


    private fun stopRecording() {
        timer.cancel()
        recorder?.apply {
            stop()
            release()
        }
        recorder = null
    }

    override fun onTouch(view: View, event: MotionEvent): Boolean {


        val actualY = event.rawY-200

        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
             //   Toast.makeText(this,"touch down",Toast.LENGTH_SHORT).show()
            }

            MotionEvent.ACTION_MOVE -> {
                mBinding.alertButton.y = actualY
            }

            MotionEvent.ACTION_UP -> {

                stopRecording()
                closeAlert()

                if (event.rawY > mBinding.alertDelete.y)
                {
                    fileAudio?.let { File(it).delete() }

                }
                else
                {
                    sendAlert()

                }
            }

            else -> return false
        }

        return true
    }

    private fun closeAlert () {

        (requireActivity() as MainActivity).removeAlertFragment()
    }

    private fun sendAlert () {


        val intentStatus = Intent(requireContext(), UpdateStatusActivity::class.java)
        intentStatus.putExtra(UpdateStatusActivity.STATUS_SEND_URGENT,true)
        intentStatus.putExtra(UpdateStatusActivity.EXTRA_SEND_AUDIO,fileAudio)
        startActivity(intentStatus)

    }




}