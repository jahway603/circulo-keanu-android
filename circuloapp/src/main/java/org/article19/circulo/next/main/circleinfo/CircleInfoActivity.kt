package org.article19.circulo.next.main.circleinfo

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.maxkeppeler.sheets.core.SheetStyle
import com.maxkeppeler.sheets.info.InfoSheet
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.extensions.getParcelableExtraCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.ActivityCircleInfoBinding
import org.article19.circulo.next.databinding.DialogJoinStatusBinding
import org.article19.circulo.next.main.MainActivity.Companion.BUNDLE_KEY_LEAVE_CIRCLE
import org.article19.circulo.next.main.MainActivity.Companion.BUNDLE_VALUE_LEAVE_CIRCLE
import org.article19.circulo.next.main.OurCircleFragment.Companion.BUNDLE_EXTRA_CIRCLE_SUMMARY
import org.article19.circulo.next.main.circleinfo.viewmodel.CircleInfoViewModel
import org.article19.circulo.next.onboarding.CirculoOnboardProvider
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.getRoom
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomJoinRules
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.session.room.powerlevels.Role
import timber.log.Timber

class CircleInfoActivity : BaseActivity() {

    private lateinit var mBinding: ActivityCircleInfoBinding

    private val mApp: ImApp?
        get() = application as? ImApp

    val session: Session?
        get() = mApp?.matrixSession

    private var mRoom: Room? = null
    private var mCircleSummary: CircleSummary? = null

    private var memberList = mutableListOf<CircleMember>()
    private lateinit var memberAdapter: CircleMemberAdapter

    val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    private lateinit var mCircleInfoViewModel: CircleInfoViewModel
    private var mRoomMemberSummaryLiveData: LiveData<List<RoomMemberSummary>>? = null

    private val mEditCircleDetailsResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {

                //Update Circle details successfully. Should fetch latest Circle details now.
                val roomId = mRoom?.roomId ?: return@registerForActivityResult
                session?.roomService()?.getRoomSummary(roomId)?.let { summary ->
                    val newName =
                        if (summary.displayName == mBinding.tvCircleName.text.toString()) {
                            it.data?.getStringExtra(BUNDLE_KEY_CIRCLE_NAME) ?: summary.displayName
                        } else {
                            summary.displayName
                        }

                    val newDescription =
                        if (summary.topic == mBinding.tvCircleDescription.text.toString()) {
                            it.data?.getStringExtra(BUNDLE_KEY_CIRCLE_TOPIC) ?: summary.topic
                        } else {
                            summary.topic
                        }
                    mBinding.tvCircleName.text = newName
                    mBinding.tvCircleDescription.text = newDescription
                }
            }
        }

    companion object {
        const val BUNDLE_KEY_CIRCLE_NAME = "bundle_key_circle_name"
        const val BUNDLE_KEY_CIRCLE_TOPIC = "bundle_key_circle_topic"
        const val BUNDLE_KEY_ROOM_ID = "bundle_key_room_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mCircleInfoViewModel = ViewModelProvider(this)[CircleInfoViewModel::class.java]
        mBinding = ActivityCircleInfoBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mCircleSummary =
            intent.getParcelableExtraCompat(BUNDLE_EXTRA_CIRCLE_SUMMARY, CircleSummary::class.java)

        if (mCircleSummary != null) {

            val roomId = mCircleSummary?.roomId ?: ""
            if (roomId.isNotEmpty()) {
                mRoom = session?.roomService()?.getRoom(roomId)
            }

            mBinding.toolbar.tvTitle.text = getString(R.string.circle_info)
            mBinding.toolbar.tvBackText.setOnClickListener {
                finish()
            }

            mBinding.tvShareLink.setOnClickListener {
                openInviteView()
            }

            mBinding.tvShowQr.setOnClickListener {
                openQrView()

            }

            mBinding.tvLeaveCircle.setOnClickListener {
                showLeaveCircleAlert()
            }

            mBinding.tvDeleteCircle.setOnClickListener {
                showDeleteCirclePrompt()
            }

            mBinding.tvCircleName.text = mCircleSummary?.name

            if (mCircleSummary?.topic?.isNotEmpty() == true) {
                mBinding.tvCircleDescription.visibility = View.VISIBLE
                mBinding.tvCircleDescription.text = mCircleSummary?.topic
            }

            if (!mCircleSummary?.creatorName.isNullOrEmpty()) {
                mBinding.tvCircleCreator.text = String.format(
                    getString(R.string.circle_created_by),
                    mCircleSummary?.creatorName
                )
                mBinding.tvCircleCreator.visibility = View.VISIBLE
            }

            val isPublicCircle = mCircleSummary?.isPublic ?: false

            initCircleTypeLayout(isPublicCircle)
            mBinding.layoutCircleType.setOnClickListener {
                showCircleJoinRuleDialog()
            }

            memberList = mCircleSummary?.circleMembers?.toMutableList() ?: mutableListOf()
            memberAdapter = CircleMemberAdapter(memberList)
            memberAdapter.setOnMemberClickListener(object : CircleMemberAdapter.MemberClickListener {
                override fun onMemberClicked(member: CircleMember) {
                    val myUserId = ImApp.sImApp?.matrixSession?.myUserId
                    if (member.userId == myUserId) return
                    if (!shouldShowAdminControls()) return

                    var roleActionText: String? = null
                    if (member.role == Role.Moderator.value) {
                        roleActionText = getString(R.string.reset_member_role)
                    }
                    MemberActionSheet(name = member.name,
                        lastActiveTime = member.lastActiveAgo,
                        roleActionText = roleActionText).show(this@CircleInfoActivity) {
                        displayPositiveButton(false)
                        displayNegativeButton(false)
                        displayHandle(true)
                        setMemberActionListener(object: MemberActionSheet.MemberActionListener {
                            override fun onKickOut() {
                                showLoading()
                                mCircleInfoViewModel.removeMember(member.userId, mRoom?.roomId)
                                dismiss()
                            }

                            override fun onGrantModeratorPermission() {
                                showLoading()
                                var newRole: Role = Role.Moderator
                                if (member.role == Role.Moderator.value) {
                                    newRole = Role.Default
                                }
                                mCircleInfoViewModel.setMemberRole(member.userId, mRoom?.roomId, memberRole = newRole)
                                dismiss()
                            }

                        })
                    }
                }

            })

            val recyclerViewMemberList = mBinding.recyclerViewMembers
            val layoutMgr = LinearLayoutManager(this)
            recyclerViewMemberList.layoutManager = layoutMgr
            recyclerViewMemberList.adapter = memberAdapter

            val showAdminControl = shouldShowAdminControls()
            if (showAdminControl) {
                mBinding.groupAdminOnly.visibility = View.VISIBLE
                mBinding.tvEditCircleDetails.visibility = View.VISIBLE
                mBinding.viewSeparator0.visibility = View.VISIBLE
                mBinding.tvEditCircleDetails.setOnClickListener {
                    val editIntent = Intent(this, EditCircleDetailsActivity::class.java).apply {
                        putExtra(BUNDLE_KEY_CIRCLE_NAME, mBinding.tvCircleName.text.toString())
                        putExtra(
                            BUNDLE_KEY_CIRCLE_TOPIC,
                            mBinding.tvCircleDescription.text.toString()
                        )
                        putExtra(BUNDLE_KEY_ROOM_ID, mRoom?.roomId ?: "")
                    }
                    mEditCircleDetailsResult.launch(editIntent)
                }
            } else {
                mBinding.groupAdminOnly.visibility = View.GONE
                mBinding.tvEditCircleDetails.visibility = View.GONE
                mBinding.viewSeparator0.visibility = View.GONE
            }
            enableLayoutCircleType(showAdminControl)
        } else {
            finish()
        }

        observeLiveData()
    }

    private fun showCircleJoinRuleDialog() {
        lateinit var joinRuleDialog: AlertDialog
        val dialogBuilder = AlertDialog.Builder(this)
        val binding = DialogJoinStatusBinding.inflate(LayoutInflater.from(this))
        dialogBuilder.setView(binding.root)
        dialogBuilder.setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
            dialog.dismiss()
        }

        binding.tvOpen.setOnClickListener {
            val roomId = mRoom?.roomId ?: return@setOnClickListener
            showLoading()
            mCircleInfoViewModel.updateJoiningRule(RoomJoinRules.PUBLIC, roomId = roomId)
            joinRuleDialog.dismiss()
        }

        binding.tvClosed.setOnClickListener {
            val roomId = mRoom?.roomId ?: return@setOnClickListener
            showLoading()
            mCircleInfoViewModel.updateJoiningRule(RoomJoinRules.RESTRICTED, roomId = roomId)
            joinRuleDialog.dismiss()
        }

        joinRuleDialog = dialogBuilder.create()
        joinRuleDialog.show()
    }

    private fun showDeleteCirclePrompt() {

        InfoSheet().show(this) {
            title(R.string.are_you_sure)
            content(R.string.delete_circle_warning)
            style(SheetStyle.BOTTOM_SHEET)
            displayCloseButton()
            onNegative(R.string.cancel) { }
            onPositive(R.string.yes) {
                showLoading()
                mCircleInfoViewModel.deleteCircleMessagesAndReplies(mRoom?.roomId, memberList)
            }
            onClose { }

        }

    }

    private fun shouldShowAdminControls(): Boolean {
        for (roomMember in memberList) {
            if (session?.myUserId == roomMember.userId && roomMember.role == Role.Admin.value) {
                return true
            }
        }
        return false
    }

    private fun initCircleTypeLayout(isPublic: Boolean) {
        mBinding.tvShareLink.visibility = if (isPublic) View.VISIBLE else View.GONE
        mBinding.tvShowQr.visibility = if (isPublic) View.VISIBLE else View.GONE
        mBinding.viewSeparatorShareLink.visibility = if (isPublic) View.VISIBLE else View.GONE
        mBinding.viewSeparatorShareLink.visibility = if (isPublic) View.VISIBLE else View.GONE

        mBinding.tvCircleType.text =
            if (isPublic) getString(R.string.open) else getString(R.string.closed)
        mBinding.tvCircleTypeDescription.text =
            if (isPublic) getString(R.string.anyone_with_a_link_can_join)
            else getString(R.string.only_people_added_in_your_circle)
    }

    private fun showLeaveCircleAlert() {
        val leaveCircleString =
            getString(R.string.leave_circle_direct_title, mCircleSummary?.name)
        val leaveCircleMessage = ""
        val notNowString = getString(R.string.not_now)
        val yesString = getString(R.string.Yes)
        let {
            InfoSheet().show(it) {
                title(leaveCircleString)
                content(leaveCircleMessage)
                style(SheetStyle.DIALOG)
                onNegative(notNowString) {
                    dismiss()
                }
                onPositive(yesString) {
                    mCircleInfoViewModel.leaveCircle(mRoom?.roomId)

                }

            }
        }
    }

    private fun openInviteView() {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, CirculoOnboardProvider.generateInviteLink(mRoom?.roomId ?: ""))
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)

    }

    private fun openQrView() {
        val mimeType = "text/plain"
        val i = Intent(this, ShowQrActivity::class.java)
        i.putExtra(Intent.EXTRA_TEXT, CirculoOnboardProvider.generateInviteLink(mRoom?.roomId ?: ""))
        i.setTypeAndNormalize(mimeType)
        startActivity(i)

    }

    override fun observeLiveData() {
        mCircleInfoViewModel.observableUpdateJoinRuleLiveData.observe(this) {
            mCircleInfoViewModel.getJoinRule(mRoom?.roomId)
        }
        mCircleInfoViewModel.observableErrorLiveData.observe(this) {
            Timber.e(it)
            hideLoading()
        }

        mCircleInfoViewModel.observableGetJoinRuleLiveData.observe(this) {
            hideLoading()
            initCircleTypeLayout(isPublic = it == RoomJoinRules.PUBLIC)
        }

        mCircleInfoViewModel.observableRemoveMemberLiveData.observe(this) {
            //Fetch room members again
            observeMembersChange()
        }

        mCircleInfoViewModel.observableGetCircleMembersLiveData.observe(this) {
            hideLoading()
            memberAdapter.updateMemberList(it)
            mRoomMemberSummaryLiveData?.removeObservers(this@CircleInfoActivity)
        }

        mCircleInfoViewModel.observableGrantAdminLiveData.observe(this) {
            //Fetch room members info again
            observeMembersChange()
        }

        mCircleInfoViewModel.observableLeaveCircleLiveData.observe(this) {
            hideLoading()
            val data = Intent()
            data.putExtra(BUNDLE_KEY_LEAVE_CIRCLE, BUNDLE_VALUE_LEAVE_CIRCLE)
            setResult(RESULT_OK, data)
            finish()
        }

        mCircleInfoViewModel.observableDeleteCircleLiveData.observe(this) {
            hideLoading()
        }

    }

    private fun observeMembersChange() {
        val roomId = mRoom?.roomId ?: return
        val builder = RoomMemberQueryParams.Builder()
        builder.memberships = arrayListOf(Membership.JOIN, Membership.INVITE)
        val query = builder.build()
        mRoomMemberSummaryLiveData = ImApp.sImApp?.matrixSession?.getRoom(roomId)?.membershipService()?.getRoomMembersLive(query)
        mRoomMemberSummaryLiveData?.observe(this) { roomMembers ->
            mCircleInfoViewModel.getCircleMembers(roomId, roomMembers)
        }
    }

    private fun enableLayoutCircleType(enable: Boolean) {
        mBinding.layoutCircleType.isEnabled = enable
        if (!enable) {
            mBinding.layoutCircleType.alpha = 0.5f
        } else {
            mBinding.layoutCircleType.alpha = 1f
        }
    }
}
