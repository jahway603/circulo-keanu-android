package org.article19.circulo.next.main.circleinfo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CircleMember(
    val name: String?,
    val userId: String,
    val avatarUrl: String?,
    val role: Int,
    val isActive: Boolean?,
    val lastActiveAgo: Long?,
    val isOnline: Boolean = false
) : Parcelable {
    override fun toString(): String {
        return "CircleMember(name=$name, userId='$userId', avatarUrl=$avatarUrl, role=$role, isActive=$isActive, lastActiveAgo=$lastActiveAgo, isOnline=$isOnline)"
    }
}
