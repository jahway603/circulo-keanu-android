package org.article19.circulo.next.main.circleinfo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CircleSummary(val roomId: String,
                         val name: String,
                         val circleMembers: List<CircleMember>,
                         val isPublic: Boolean,
                         val creatorName: String,
                         val topic: String): Parcelable {

}
