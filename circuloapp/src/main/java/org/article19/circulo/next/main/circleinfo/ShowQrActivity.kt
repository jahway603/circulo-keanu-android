package org.article19.circulo.next.main.circleinfo

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import info.guardianproject.keanuapp.viewmodel.qr.QrViewModel
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.ActivityShowQrBinding

class ShowQrActivity : BaseActivity() {

    private lateinit var mViewBinding: ActivityShowQrBinding
    private lateinit var mQrCodeViewModel: QrViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mQrCodeViewModel = ViewModelProvider(this)[QrViewModel::class.java]
        mViewBinding = ActivityShowQrBinding.inflate(layoutInflater)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR

        setContentView(mViewBinding.root)

        mViewBinding.toolbar.tvBackText.setOnClickListener {
            finish()
        }
        mViewBinding.toolbar.tvTitle.text = getString(R.string.qr_to_join_circle)
        showQrCode()
    }

    private fun showQrCode() {
        showLoading()
        val qrData = intent.getStringExtra(Intent.EXTRA_TEXT) ?: ""
        mQrCodeViewModel.getQrBitmap(240, qrData)
    }

    override fun observeLiveData() {
        mQrCodeViewModel.observableGetQrBitmapLiveData.observe(this) {
            hideLoading()
            mViewBinding.ivQr.setImageBitmap(it)
        }

        mQrCodeViewModel.observableErrorLiveData.observe(this) {
            hideLoading()
        }
    }

}