package org.article19.circulo.next.main.circleinfo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.squareup.moshi.Moshi
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanuapp.viewmodel.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.main.circleinfo.CircleMember
import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.toContent
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.getRoom
import org.matrix.android.sdk.api.session.room.model.GuestAccess
import org.matrix.android.sdk.api.session.room.model.PowerLevelsContent
import org.matrix.android.sdk.api.session.room.model.RoomJoinRules
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.session.room.powerlevels.PowerLevelsHelper
import org.matrix.android.sdk.api.session.room.powerlevels.Role

class CircleInfoViewModel: BaseViewModel() {

    private val mEditResultLiveData = MutableLiveData<Boolean>()
    val observableEditDetailsLiveData = mEditResultLiveData as LiveData<Boolean>

    private val mUpdateJoinRuleLiveData = MutableLiveData<Boolean>()
    val observableUpdateJoinRuleLiveData = mUpdateJoinRuleLiveData as LiveData<Boolean>

    private val mGetJoinRuleLiveData = MutableLiveData<RoomJoinRules?>()
    val observableGetJoinRuleLiveData = mGetJoinRuleLiveData as LiveData<RoomJoinRules?>

    private val mRemoveMemberLiveData = MutableLiveData<Boolean>()
    val observableRemoveMemberLiveData = mRemoveMemberLiveData as LiveData<Boolean>

    private val mGetCircleMembersLiveData = MutableLiveData<List<CircleMember>>()
    val observableGetCircleMembersLiveData = mGetCircleMembersLiveData as LiveData<List<CircleMember>>

    private val mGrantAdminLiveData = MutableLiveData<Boolean>()
    val observableGrantAdminLiveData = mGrantAdminLiveData as LiveData<Boolean>

    private val mLeaveCircleLiveData = MutableLiveData<Boolean>()
    val observableLeaveCircleLiveData = mLeaveCircleLiveData as LiveData<Boolean>

    private val mDeleteCircleLiveData = MutableLiveData<Boolean>()
    val observableDeleteCircleLiveData = mDeleteCircleLiveData as LiveData<Boolean>

    fun updateCircleDetails(roomId: String, newCircleName: String, newCircleDescription: String) {
        viewModelScope.launch (Dispatchers.IO + mExceptionHandler) {
            val app = ImApp.sImApp ?: return@launch
            val stateService = app.matrixSession?.getRoom(roomId)?.stateService()
            stateService?.updateName(newCircleName)
            stateService?.updateTopic(newCircleDescription)

            mEditResultLiveData.postValue(true)
        }
    }

    fun updateJoiningRule(rule: RoomJoinRules, roomId: String) {
        viewModelScope.launch (Dispatchers.IO + mExceptionHandler) {
            ImApp.sImApp?.matrixSession?.roomService()?.getRoom(roomId)?.stateService()
                ?.updateJoinRule(rule, GuestAccess.Forbidden)
            mUpdateJoinRuleLiveData.postValue(true)
        }
    }

    fun getJoinRule(roomId: String?) {
        roomId ?: return
        viewModelScope.launch (Dispatchers.IO + mExceptionHandler) {
            val roomSummary = ImApp.sImApp?.matrixSession?.roomService()?.getRoomSummary(roomId)
            mGetJoinRuleLiveData.postValue(roomSummary?.joinRules)
        }
    }

    fun removeMember(userId: String?, roomId: String?) {
        userId ?: return
        roomId ?: return

        viewModelScope.launch (Dispatchers.IO + mExceptionHandler) {
            val room = ImApp.sImApp?.matrixSession?.getRoom(roomId)
            room?.membershipService()?.remove(userId, null)
            //Room members might not be reflected immediately after above API call, hence delay a
            //bit here for server to update
            delay(200L)
            mRemoveMemberLiveData.postValue(true)
        }
    }

    fun getCircleMembers(roomId: String?, roomMembers: List<RoomMemberSummary>) {
        roomId ?: return
        viewModelScope.launch (Dispatchers.IO + mExceptionHandler) {
            var powerLevelHelper: PowerLevelsHelper?
            val circleMembers = mutableListOf<CircleMember>()

            val room = ImApp.sImApp?.matrixSession?.getRoom(roomId)
            val powerEvent = room?.stateService()?.getStateEvent(
                EventType.STATE_ROOM_POWER_LEVELS,
                QueryStringValue.IsEmpty
            ) ?: return@launch

            val mapLevels = powerEvent.getClearContent()

            val moshi = Moshi.Builder().build()
            moshi.adapter(PowerLevelsContent::class.java).fromJsonValue(mapLevels).let { powerLevelsContent ->
                if (powerLevelsContent == null) {
                    return@let
                }
                powerLevelHelper = PowerLevelsHelper(powerLevelsContent)

                for (roomMember in roomMembers) {
                    val memberRole = powerLevelHelper?.getUserRole(roomMember.userId) ?: Role.Default
                    val canGrantAdmin = memberRole == Role.Admin
                    val circleMember = CircleMember(name = roomMember.displayName,
                        userId = roomMember.userId,
                        avatarUrl = roomMember.avatarUrl,
                        role = memberRole.value,
                        isActive = roomMember.userPresence?.isCurrentlyActive,
                        lastActiveAgo = roomMember.userPresence?.lastActiveAgo,
                        isOnline = roomMember.userPresence?.isCurrentlyActive == true)

                    if (canGrantAdmin) {
                        circleMembers.add(0, circleMember)
                    } else {
                        circleMembers.add(circleMember)
                    }
                }

                mGetCircleMembersLiveData.postValue(circleMembers)
            }
        }
    }

    fun setMemberRole(newAdminUserId: String, roomId: String?, memberRole: Role) {
        roomId ?: return
        viewModelScope.launch (Dispatchers.IO + mExceptionHandler) {
            val room = ImApp.sImApp?.matrixSession?.getRoom(roomId) ?: return@launch
            val mapLevels = room.stateService().getStateEvent(
                EventType.STATE_ROOM_POWER_LEVELS,
                QueryStringValue.IsEmpty
            )?.getClearContent()
                ?.toModel<PowerLevelsContent>()
                ?.setUserPowerLevel(newAdminUserId, memberRole.value)
                ?.toContent()
                ?: return@launch

            room.stateService()
                .sendStateEvent(EventType.STATE_ROOM_POWER_LEVELS, stateKey = "", mapLevels)

            //Room power level might not be reflected immediately after above API call, hence delay a
            //bit here for server to update
            delay(200L)
            mGrantAdminLiveData.postValue(true)
        }
    }

    fun deleteCircleMessagesAndReplies(roomId: String?, members: List<CircleMember>) {
        roomId ?: return
        viewModelScope.launch (Dispatchers.IO + mExceptionHandler) {
            val sharedTimeline = SharedTimeline.instance

            // 1. Redact all known events in the room.
            for ((_, events) in sharedTimeline.threadEvents) {
                // The thread root event is the first, so reverse list to redact root event last.
                for (event in events.reversed()) {
                    sharedTimeline.room?.sendService()?.redactEvent(event.root, "resolved")
                }
            }

            // 2. Kick out all the existing members of the room except me
            for (member in members) {
                val isAdmin = member.role == Role.Admin.value
                if (!isAdmin) {
                    sharedTimeline.room?.membershipService()?.remove(member.userId, null)
                }
            }

            // 3. Delete any existing aliases: For now there's no alias associated with a Circle,
            // so ignoring this.

            // 4. Set room to invite-only mode.
            sharedTimeline.room?.stateService()?.updateJoinRule(RoomJoinRules.RESTRICTED, null)

            // 5. Leave the room
            leaveCircle(roomId)

            mDeleteCircleLiveData.postValue(true)
        }
    }

    fun leaveCircle(roomId: String?) {
        roomId ?: return
        viewModelScope.launch (Dispatchers.IO + mExceptionHandler) {
            ImApp.sImApp?.matrixSession?.roomService()?.leaveRoom(roomId, "")
            mLeaveCircleLiveData.postValue(true)
        }
    }
}