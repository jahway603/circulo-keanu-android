package org.article19.circulo.next.main.now

enum class Status (val value: String) {

    UNDEFINED(""),
    SAFE("#safe"),
    UNCERTAIN("#uncertain"),
    NOT_SAFE("#notsafe");

    companion object {
        const val URGENT_STRING = "#urgent"
        const val RESOLVED_STRING = "#resolved"
        const val GEO_STRING = "geo:"
    }

}