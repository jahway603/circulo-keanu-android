package org.article19.circulo.next.main.now

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import org.article19.circulo.next.SharedTimeline
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent

@Parcelize
data class Thread(
    val roomId: String,
    val eventId: String,
    val userId: String,
    val name: String,
    val status: Status? = Status.UNCERTAIN,
    val commentCount: Int? = 0,
    val isUnread: Boolean? = false,
    val isUrgent: Boolean? = false,
    val lastCommentTime: Long? = 0L,
    var isOnline: Boolean? = true,
    var avatarUrl: String? = null,
    var msgBody: String? = null,
    var latLon: String? = null,
    var address: String? = null
) : Parcelable {

    constructor(
        event: TimelineEvent,
        name: String?,
        status: Status? = Status.UNCERTAIN,
        isUnread: Boolean? = false,
        isUrgent: Boolean? = false,
        lastCommentTime: Long?,
        isOnline: Boolean? = true,
        msgBody: String? = null
    ) : this(
        event.roomId,
        event.eventId,
        event.senderInfo.userId,
        name ?: event.senderInfo.disambiguatedDisplayName,
        status,
        0,
        isUnread,
        isUrgent,
        lastCommentTime ?: event.root.originServerTs,
        isOnline,
        null,
        msgBody
    )


    override fun toString(): String {
        return "People(roomId='$roomId', eventId='$eventId', userId='$userId', name='$name', status=$status, commentCount=$commentCount, isUnread=$isUnread, isUrgent=$isUrgent, lastCommentTime=$lastCommentTime, isOnline=$isOnline, avatarUrl=$avatarUrl)"
    }

    val events
        get() = SharedTimeline.instance.threadEvents[eventId] ?: emptyList()
}
