package org.article19.circulo.next.main.now

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.PrettyTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.R
import org.article19.circulo.next.common.ui.RoundRectCornerImageView
import org.article19.circulo.next.databinding.AlertPeopleRowItemBinding
import org.article19.circulo.next.databinding.PeopleRowItemBinding
import org.article19.circulo.next.main.MainActivity
import org.article19.circulo.next.main.updatestatus.StatusDetailActivity
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import java.util.Date

class ThreadsAdapter(private var activity: Activity, threads: List<Thread>) :
    ListAdapter<Thread, ThreadsAdapter.ViewHolder>(DIFF_CALLBACK) {

    companion object {
        const val BUNDLE_EXTRA_THREAD = "bundle_extra_thread"

        private const val AVATAR_SIZE = 59

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Thread>() {

            override fun areItemsTheSame(oldItem: Thread, newItem: Thread): Boolean {
                return oldItem.eventId == newItem.eventId
            }

            override fun areContentsTheSame(oldItem: Thread, newItem: Thread): Boolean {
                return oldItem == newItem
            }
        }
    }


    init {
        setHasStableIds(true)

        submitList(threads)
    }

    private val mSession: Session?
        get() = (activity.application as? ImApp)?.matrixSession

    private lateinit var mContext: Context
    private lateinit var mSafeString: String
    private lateinit var mUnsafeString: String
    private lateinit var mUncertainString: String

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    override fun getItemViewType(position: Int): Int {

        val currentPersonStatus = currentList[position]
        if (currentPersonStatus.isUrgent == true)
            return 1;
        else
            return 0;

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        if (viewType == 1) {
            val binding = AlertPeopleRowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            mContext = parent.context

            mSafeString = mContext.getString(R.string.safe)
            mUnsafeString = mContext.getString(R.string.not_safe)
            mUncertainString = mContext.getString(R.string.uncertain)

            return AlertPeopleViewHolder(binding)
        }
        else {
            val binding = PeopleRowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            mContext = parent.context

            mSafeString = mContext.getString(R.string.safe)
            mUnsafeString = mContext.getString(R.string.not_safe)
            mUncertainString = mContext.getString(R.string.uncertain)

            return PeopleViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentPersonStatus = currentList[position]

        if (holder is AlertPeopleViewHolder) {

            holder.tvName.text = currentPersonStatus.name
            holder.tvCommentCount.text = mContext.resources.getQuantityString(
                R.plurals.comment_counts,
                currentPersonStatus.commentCount ?: 0,
                currentPersonStatus.commentCount
            )

            holder.ivAvatar.visibility = View.VISIBLE
            holder.tvUnread.visibility = View.GONE
            holder.ivUrgent.visibility = View.GONE

            /**
            holder.tvLastCommentTime.text = context.resources.getString(
            R.string.last_comment_time,
            currentPersonStatus.lastCommentTime
            )**/

            //sometimes the server time is ahead a few seconds, so take of 5 seconds so it doesn't appear in the future
            var lastCommentTime =
                currentPersonStatus.lastCommentTime?.let { it }
            holder.tvLastCommentTime.text = PrettyTime.format(lastCommentTime?.let { Date(it) })

            holder.rootLayout.setOnClickListener {
                val i = Intent(mContext, StatusDetailActivity::class.java)
                i.putExtra(StatusDetailActivity.EXTRA_THREAD_ID, currentPersonStatus.eventId)

                mContext.startActivity(i)
            }
        }
        else if (holder is PeopleViewHolder) {
            holder.tvName.text = currentPersonStatus.name
            holder.tvCommentCount.text = mContext.resources.getQuantityString(
                R.plurals.comment_counts,
                currentPersonStatus.commentCount ?: 0,
                currentPersonStatus.commentCount
            )

            val mainActivity = activity as? MainActivity ?: return

            loadUserAvatar(currentPersonStatus.avatarUrl, holder.ivAvatar)
            if (currentPersonStatus.isOnline == true) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.ivAvatar.foreground = null
                }
                //    holder.ivNoInternet.visibility = View.GONE
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.ivAvatar.foreground =
                        ContextCompat.getDrawable(mainActivity, R.color.grey_30)
                }
                //   holder.ivNoInternet.visibility = View.VISIBLE
            }

            if (currentPersonStatus.isUnread == true) {
                holder.tvUnread.visibility = View.VISIBLE
            } else {
                holder.tvUnread.visibility = View.GONE
            }

            if (currentPersonStatus.isUrgent == true) {
                holder.ivUrgent.visibility = View.VISIBLE
            } else {
                holder.ivUrgent.visibility = View.GONE
            }

            /**
            holder.tvLastCommentTime.text = context.resources.getString(
            R.string.last_comment_time,
            currentPersonStatus.lastCommentTime
            )**/

            //sometimes the server time is ahead a few seconds, so take of 5 seconds so it doesn't appear in the future
            var lastCommentTime =
                currentPersonStatus.lastCommentTime?.let { it }
            holder.tvLastCommentTime.text = PrettyTime.format(lastCommentTime?.let { Date(it) })

            when (currentPersonStatus.status) {
                Status.SAFE -> {
                    holder.tvStatus.text = mSafeString
                    holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        AppCompatResources.getDrawable(mContext, R.drawable.ic_status_safe),
                        null,
                        null,
                        null
                    )
                }

                Status.NOT_SAFE -> {
                    holder.tvStatus.text = mUnsafeString
                    holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        AppCompatResources.getDrawable(mContext, R.drawable.ic_status_not_safe),
                        null,
                        null,
                        null
                    )
                }

                Status.UNCERTAIN -> {
                    holder.tvStatus.text = mUncertainString
                    holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        AppCompatResources.getDrawable(mContext, R.drawable.ic_status_uncertain),
                        null,
                        null,
                        null
                    )
                }

                else -> {

                    var statusText : String? = null

                    if (currentPersonStatus.msgBody?.contains("geo:")==true)
                    {
                        var geoString = currentPersonStatus.msgBody!!.substring(currentPersonStatus.msgBody!!.indexOf("geo:")+4).split(" ").get(0)
                        var latLon = geoString.split(",")

                        if (currentPersonStatus.address == null)
                        {
                            statusText = geoString

                            CoroutineScope(Dispatchers.Main).launch {
                                // Fetches data from backend
                                currentPersonStatus.latLon = geoString
                                currentPersonStatus.address =  getAddress(latLon[0].toDouble(),latLon[1].toDouble())
                                statusText = currentPersonStatus.address
                                holder.tvStatus.text = statusText

                            }
                        }
                        else
                        {
                            statusText = currentPersonStatus.address
                        }


                    }
                    else
                    {
                        statusText = ""
                    }

                    holder.tvStatus.text = statusText
                    holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        null,
                        null
                    )
                }
            }

            holder.rootLayout.setOnClickListener {
                val i = Intent(mContext, StatusDetailActivity::class.java)
                i.putExtra(StatusDetailActivity.EXTRA_THREAD_ID, currentPersonStatus.eventId)

                mContext.startActivity(i)
            }
        }
    }

    private fun getAddress(lat: Double, lng: Double): String? {
        val geocoder = mContext?.let { Geocoder(it) }
        val list = geocoder?.getFromLocation(lat, lng, 1)
        list.let {
            if (list?.size!! > 0)
                return list[0]?.getAddressLine(0)
        }

        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun getPhotoThumbFromUrl (url: String?): String? {
        return mSession?.contentUrlResolver()?.resolveThumbnail(
            url,
            AVATAR_SIZE,
            AVATAR_SIZE,
            ContentUrlResolver.ThumbnailMethod.SCALE
        )
    }

    private fun loadUserAvatar(avatarUrl: String?, ivAvatar: RoundRectCornerImageView) {
        mCoroutineScope.launch {
            getPhotoThumbFromUrl(avatarUrl)?.let {avatarUrl ->
                withContext(Dispatchers.Main) {
                    if (!activity.isDestroyed) {
                        GlideUtils.loadImageFromUri(activity, Uri.parse(avatarUrl), ivAvatar, false)
                    }
                }
            }
        }
    }

    open class PeopleViewHolder(binding: PeopleRowItemBinding) : ViewHolder(binding.root) {
        val rootLayout = binding.cardViewRoot
        val tvName = binding.tvName
        val tvStatus = binding.tvStatus
        val tvCommentCount = binding.tvCommentCount
        val tvUnread = binding.tvUnread
        val ivUrgent = binding.ivUrgent
        val tvLastCommentTime = binding.tvLastCommentTime
        val ivAvatar = binding.ivAvatar
        val ivNoInternet = binding.ivNoInternet
    }

    class AlertPeopleViewHolder(binding: AlertPeopleRowItemBinding) : ViewHolder(binding.root)  {
        val rootLayout = binding.cardViewRoot
        val tvName = binding.tvName
        val tvStatus = binding.tvStatus
        val tvCommentCount = binding.tvCommentCount
        val tvUnread = binding.tvUnread
        val ivUrgent = binding.ivUrgent
        val tvLastCommentTime = binding.tvLastCommentTime
        val ivAvatar = binding.ivAvatar
        val ivNoInternet = binding.ivNoInternet

    }

    open class ViewHolder (root: LinearLayout) : RecyclerView.ViewHolder(root)  {

    }
}