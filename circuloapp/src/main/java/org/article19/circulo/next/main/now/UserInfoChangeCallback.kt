package org.article19.circulo.next.main.now

import org.matrix.android.sdk.api.session.user.model.User

interface UserInfoChangeCallback {
    fun onUserInfoChanged (user: User?)
}