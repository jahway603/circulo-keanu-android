package org.article19.circulo.next.main.updatestatus

import android.Manifest
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import com.google.android.material.snackbar.Snackbar
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import info.guardianproject.keanu.core.ui.room.RoomActivity
import info.guardianproject.keanu.core.util.ActivityCompatUtils
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.common.utils.dp2Px
import org.article19.circulo.next.databinding.PopupAddMediaBinding

abstract class CommonSelectMediaActivity: BaseActivity() {
    private val mApp: CirculoApp?
        get() = application as? CirculoApp

    private val mTakePictureResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult
            it.data?.data?.let { uri -> handleTakePicture(uri) }
        }

    private val mStartPhotoTakerIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) startPhotoTaker()
        }

    private val mStartImagePickerIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            var permissionsGranted = true
            it.entries.forEach { entry ->
                if (!entry.value) permissionsGranted = false
            }
            if (permissionsGranted) {
                startImagePicker()
            }
        }

    private fun startPhotoTaker() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                Snackbar.make(getRootView(), info.guardianproject.keanuapp.R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(info.guardianproject.keanuapp.R.string.ok) {
                        startPhotoTaker()
                    }.show()
            } else {
                mStartPhotoTakerIfPermitted.launch(Manifest.permission.CAMERA)
            }
        } else {
            mTakePictureResult.launch(mApp?.router?.camera(this, oneAndDone = true))
        }
    }

    private fun startImagePicker() {
        var reqPerm = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            reqPerm = arrayOf(Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO)
        }

        val hasReadPermission = ActivityCompatUtils.hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        if (!hasReadPermission) {
            if (ActivityCompatUtils.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                mStartImagePickerIfPermitted.launch(reqPerm)
            }
            else {
                Snackbar.make(getRootView(), R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok) {
                    }
                    .show()
            }
        } else {
            @Suppress("DEPRECATION")
            Matisse.from(this)
                .choose(mutableSetOf<MimeType?>().apply {
                    addAll(MimeType.ofImage())
                    addAll(MimeType.ofVideo())
                })
                .countable(true)
                .maxSelectable(100)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(GlideEngine())
                .showPreview(false)
                .forResult(RoomActivity.REQUEST_ADD_MEDIA)
        }
    }

    fun showMediaChooserPopUp(anchorView: View, bottomBar: ViewGroup) {
        val binding = PopupAddMediaBinding.inflate(layoutInflater)
        val popupWindow = PopupWindow(binding.root,
            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        popupWindow.elevation = 10f
        popupWindow.isOutsideTouchable = true
        popupWindow.isFocusable = true
        popupWindow.showAtLocation(anchorView, Gravity.BOTTOM or Gravity.END, 20,
            (bottomBar.height/2).dp2Px())

        binding.tvCamera.setOnClickListener {
            startPhotoTaker()
            popupWindow.dismiss()
        }

        binding.tvGallery.setOnClickListener {
            popupWindow.dismiss()
            startImagePicker()
        }
    }

    abstract fun getRootView(): View
    abstract fun handleTakePicture(photoUri: Uri)
}