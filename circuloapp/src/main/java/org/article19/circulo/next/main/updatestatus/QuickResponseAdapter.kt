package org.article19.circulo.next.main.updatestatus

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.article19.circulo.next.databinding.RowQuickResponseStatusBinding

class QuickResponseAdapter(var responseList: List<String>) :
    RecyclerView.Adapter<QuickResponseAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowQuickResponseStatusBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentResponse = responseList[position]
        holder.tvResponse.text = currentResponse


        holder.tvResponse.setOnClickListener {
            if (context is UpdateStatusActivity) {
                (context as UpdateStatusActivity).updateQuickResponseList(currentResponse)
            }
        }
    }

    override fun getItemCount(): Int {
        return responseList.size
    }

    class ViewHolder(binding: RowQuickResponseStatusBinding) : RecyclerView.ViewHolder(binding.root) {
        val tvResponse = binding.tvResponse
    }

    fun replaceData(newResponseList: List<String>) {
        responseList = newResponseList
        notifyDataSetChanged()
    }
}