package org.article19.circulo.next.main.updatestatus

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.AttributeSet
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.content.FileProvider
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.R
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.lang.ref.WeakReference

class ResponseListItem(context: Context, attrs: AttributeSet?) : RelativeLayout(context, attrs) {

    private val attachment: Uri?
        get() = mHolder?.get()?.attachment

    private val mimeType: String?
        get() = mHolder?.get()?.mimeType

    val message: String?
        get() = mHolder?.get()?.message

    private var mHolder: WeakReference<ResponseViewHolder>? = null

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        mHolder?.get()?.audioWife?.pause()
    }

    fun bind(holder: ResponseViewHolder) {
        mHolder = WeakReference(holder)
    }

    fun exportMediaFile() {
        val currentMimeType = mimeType
        val currentAttachment = attachment

        if (currentMimeType != null && currentAttachment != null) {
            val exportPath = SecureMediaStore.exportPath(context, currentMimeType, currentAttachment,true)
            exportMediaFile(mimeType!!, attachment!!, exportPath)
            Timber.v("ExportPath messageList==$exportPath")
        } else {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_TEXT, message)
            shareIntent.type = "text/plain"

            context.startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.export_media)))
        }
    }

    fun downloadMediaFile() {
        val exportPath =
            SecureMediaStore.exportPath(context, mimeType ?: return, attachment ?: return,true)
        try {
            SecureMediaStore.exportContent(
                mimeType,
                attachment ?: return,
                exportPath
            )
            Toast.makeText(
                context,
                "$exportPath",
                Toast.LENGTH_LONG
            ).show()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun exportMediaFile(mimeType: String, mediaUri: Uri, exportPath: File) {
        try {
            SecureMediaStore.exportContent(mimeType, mediaUri, exportPath)

            val uriShare = FileProvider.getUriForFile(context, context.packageName + ".provider", exportPath)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_STREAM, uriShare)
            shareIntent.type = mimeType

            context.startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.export_media)))
        }
        catch (e: Exception) {
            Toast.makeText(context, "Export Failed " + e.message, Toast.LENGTH_LONG).show()
            Timber.e(e)
        }
    }
}
