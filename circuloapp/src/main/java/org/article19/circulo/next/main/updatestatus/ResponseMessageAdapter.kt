package org.article19.circulo.next.main.updatestatus

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanu.core.ui.quickresponse.QuickResponseLongClickListener
import info.guardianproject.keanu.core.ui.widgets.StatusItemHolder
import info.guardianproject.keanu.core.ui.widgets.VerificationRequestItemHolder
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.extensions.betterMessage
import info.guardianproject.keanu.core.util.extensions.getRelatedEventId
import info.guardianproject.keanu.core.util.extensions.isReaction
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.StatusItemBinding
import info.guardianproject.keanuapp.databinding.VerificationRequestItemBinding
import info.guardianproject.keanuapp.ui.conversation.QuickReaction
import info.guardianproject.keanuapp.ui.widgets.MediaInfo
import info.guardianproject.keanuapp.ui.widgets.ShareRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.databinding.StatusReplyBaseBinding
import org.article19.circulo.next.main.now.Thread
import org.article19.circulo.next.matrix.isLocal
import org.matrix.android.sdk.api.session.content.ContentUploadStateTracker
import org.matrix.android.sdk.api.session.crypto.attachments.toElementToDecrypt
import org.matrix.android.sdk.api.session.crypto.model.EncryptedFileInfo
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.LocalEcho
import org.matrix.android.sdk.api.session.events.model.isAttachmentMessage
import org.matrix.android.sdk.api.session.file.FileService
import org.matrix.android.sdk.api.session.room.model.message.MessageAudioContent
import org.matrix.android.sdk.api.session.room.model.message.MessageFileContent
import org.matrix.android.sdk.api.session.room.model.message.MessageImageContent
import org.matrix.android.sdk.api.session.room.model.message.MessageStickerContent
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import org.matrix.android.sdk.api.session.room.model.message.MessageVideoContent
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.model.message.getFileUrl
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent

class ResponseMessageAdapter(
    private val mActivity: StatusDetailActivity,
    private val mView: RecyclerView,
    private val mReadReceiptsUpdateCallback: StatusDetailActivity.ReadReceiptUpdateCallback
) : ListAdapter<TimelineEvent, RecyclerView.ViewHolder>(DIFF_CALLBACK),
    ContentUploadStateTracker.UpdateListener, ResponseViewHolder.OnImageClickedListener,
    Observer<List<TimelineEvent>> {

    enum class ViewType {
        INBOUND_MESSAGE, OUTBOUND_MESSAGE, STATUS, VERIFICATION_REQUEST
    }

    companion object {
        private const val BUNDLE_RESEND_IMAGE_URI = "resendImageUri"
        private const val BUNDLE_RESEND_IMAGE_MIME_TYPE = "resendImageMimeType"

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TimelineEvent>() {

            override fun areItemsTheSame(oldItem: TimelineEvent, newItem: TimelineEvent): Boolean {
                return oldItem.eventId == newItem.eventId
            }

            override fun areContentsTheSame(oldItem: TimelineEvent, newItem: TimelineEvent): Boolean {
                return oldItem == newItem
            }
        }
    }

    init {
        setHasStableIds(true)
    }

    var thread: Thread? = null
        set(value) {
            val oldId = field?.eventId
            val newId = value?.eventId

            if (oldId != null && oldId != newId) {
                SharedTimeline.instance.unobserveThreadEvents(mActivity, oldId)
            }

            field = value

            if (newId != null && oldId != newId) {
                SharedTimeline.instance.observeThreadEvents(mActivity, this, newId)
            }
        }

    private val mApp
        get() = mActivity.application as? CirculoApp

    private val mSession
        get() = mApp?.matrixSession

    private var mIsBackPage = false

    private var mShowProgressOnPosition: Int? = null
    private var mProgressCurrent: Long = 0
    private var mProgressTotal: Long = 0

    private val mDownloadedDecryptedFiles = HashMap<Int, MediaInfo>()

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mView))
    }

    private val mAllReadReceipts = mutableListOf<ReadReceiptData>()
    private val mSeenUsers = mutableSetOf<String>()

    private val mRequestImageViewForResult =
        mActivity.registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            if (it.data?.hasExtra(BUNDLE_RESEND_IMAGE_URI) == true) {
                val request = ShareRequest()
                request.deleteFile = false
                request.resizeImage = false
                request.importContent = false
                request.media = Uri.parse(it.data?.getStringExtra(BUNDLE_RESEND_IMAGE_URI))
                request.mimeType = it.data?.getStringExtra(BUNDLE_RESEND_IMAGE_MIME_TYPE)

                mActivity.sendShareRequest(request)
            }
        }

    private val mThumbnails = HashMap<Int, MediaInfo>()

    private var mQuickResponseLongClick: QuickResponseLongClickListener? = null


    override fun onChanged(value: List<TimelineEvent>) {
        submitList(value.filter { !it.isLocal() })

        for (event in value) {
            trackProgressIfOngoingUpload(event)

            fetchReadReceiptFromEvent(event)

            // When new reactions come in, their related event needs to get updated.
            // Reactions on their own are never rendered.
            if (event.isReaction()) {
                event.getRelatedEventId()?.let { eventId ->
                    val i = value.indexOfFirst { it.eventId == eventId }

                    if (i > -1) notifyItemChanged(i)
                }
            }
        }

        // Attach timeline view for the second last item.
        notifyItemChanged(value.count() - 1)

        mReadReceiptsUpdateCallback.onReadReceiptsUpdated(mAllReadReceipts)

        //If at bottom, then jump to new message.
        if (mIsBackPage && itemCount > 20) {
            jumpToPosition(20)
        } else if (!mView.canScrollVertically(1)) {
            jumpToFirstUnread()
        }

        mIsBackPage = false
    }

    fun redactAll() {
        for (event in currentList) {
            SharedTimeline.instance.room?.sendService()?.redactEvent(event.root, "resolved")
        }
    }

    override fun getItemId(position: Int): Long {
        return getItem(position)?.localId ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        val event = getItem(position)

        return when (event?.root?.type) {
            EventType.MESSAGE, EventType.ENCRYPTED -> {
                when {
                    event.getLastMessageContent()?.msgType == MessageType.MSGTYPE_VERIFICATION_REQUEST ->
                        ViewType.VERIFICATION_REQUEST.ordinal

                    event.root.senderId != mSession?.myUserId -> ViewType.INBOUND_MESSAGE.ordinal

                    else -> ViewType.OUTBOUND_MESSAGE.ordinal
                }
            }

            else -> ViewType.STATUS.ordinal
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val vh = when (ViewType.entries[viewType]) {

            ViewType.INBOUND_MESSAGE -> {
                ResponseViewHolder(StatusReplyBaseBinding.inflate(inflater, parent, false))
            }

            ViewType.OUTBOUND_MESSAGE -> {
                ResponseViewHolder(StatusReplyBaseBinding.inflate(inflater, parent, false))
                //MessageViewHolder(MessageViewRightBinding.inflate(inflater, parent, false))
            }

            ViewType.VERIFICATION_REQUEST -> {
                VerificationRequestItemHolder(
                    VerificationRequestItemBinding.inflate(
                        inflater,
                        parent,
                        false
                    )
                )
            }

            else -> {
                StatusItemHolder(StatusItemBinding.inflate(inflater, parent, false))
            }
        }

        if (vh is ResponseViewHolder) {
            vh.onImageClickedListener = this
        }

        return vh
    }

    override fun onBindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {

        val tEventId = getItem(position).eventId
        val te = SharedTimeline.instance.room?.timelineService()?.getTimelineEvent(tEventId)
        val mc = te?.getLastMessageContent()
        var attachment: Uri? = null
        var thumbnail: Uri? = null

        if (mc is MessageWithAttachmentContent) {
            // Trigger download and decryption, if not available, yet.
            if (!mDownloadedDecryptedFiles.containsKey(position) && mc.getFileUrl()
                    ?.isNotEmpty() == true
            ) {
                val fileState = mSession?.fileService()?.fileState(mc)

                var fileName: String = mc.getFileName()
                var url: String? = null
                var mimeType: String? = null
                var info: EncryptedFileInfo? = null

                if (fileState != FileService.FileState.Downloading) {
                    mCoroutineScope.launch {
                        mimeType = mc.mimeType
                        url = mc.getFileUrl()
                        info = mc.encryptedFileInfo

                        if (url?.startsWith("content") == true) {
                            mDownloadedDecryptedFiles[position] =
                                MediaInfo(Uri.parse(url), mimeType)

                            mView.post {
                                notifyItemChanged(position)
                            }
                        } else if (url?.startsWith("mxc") == true) {
                            val fileMedia = mSession?.fileService()?.downloadFile(
                                fileName, mimeType, url, info?.toElementToDecrypt()
                            )

                            mDownloadedDecryptedFiles[position] =
                                MediaInfo(Uri.fromFile(fileMedia), mimeType)

                            mView.post {
                                notifyItemChanged(position, fileMedia)
                            }
                        }
                    }
                }

                when (mc) {
                    is MessageImageContent -> {
                        url = mc.info?.thumbnailUrl ?: mc.info?.thumbnailFile?.url
                        mimeType = mc.info?.thumbnailInfo?.mimeType
                        info = mc.info?.thumbnailFile
                        fileName = "thumb" + mc.getFileName()
                    }

                    is MessageStickerContent -> {
                        url = mc.info?.thumbnailUrl ?: mc.info?.thumbnailFile?.url
                        mimeType = mc.info?.thumbnailInfo?.mimeType
                        info = mc.info?.thumbnailFile
                        fileName = "thumb" + mc.getFileName()
                    }

                    is MessageVideoContent -> {
                        url = mc.videoInfo?.thumbnailUrl ?: mc.videoInfo?.thumbnailFile?.url
                        mimeType = mc.videoInfo?.thumbnailInfo?.mimeType
                        info = mc.videoInfo?.thumbnailFile
                        fileName = "thumb" + mc.getFileName()
                    }
                    is MessageAudioContent -> {

                        url = mc.getFileUrl()
                        mimeType = mc.audioInfo?.mimeType
                        info = mc.encryptedFileInfo
                        fileName = "thumb" + mc.getFileName()

                    }
                    is MessageFileContent -> {

                        if (mc.info?.mimeType?.contains("video", true) == true
                        ) {
                            url = mc.url
                            fileName = "thumb" + mc.getFileName()
                        } else if (mc.info?.mimeType?.contains("octet-stream", true) == true) {
                            url = mc.url
                        }

                    }
                }

                if (url?.isNotEmpty() == true) {
                    val fileThumbState = mSession?.fileService()
                        ?.fileState(url, mc.getFileName(), mimeType, info?.toElementToDecrypt())

                    if (fileThumbState != FileService.FileState.Downloading) {
                        mCoroutineScope.launch {
                            if (url?.startsWith("content") == true) {
                                mThumbnails[position] = MediaInfo(Uri.parse(url), mimeType)

                                mView.post {
                                    notifyItemChanged(position)
                                }
                            } else if (url?.startsWith("mxc") == true) {
                                val fileMedia = mSession?.fileService()?.downloadFile(
                                    fileName,
                                    mimeType,
                                    url,
                                    info?.toElementToDecrypt()
                                )

                                mThumbnails[position] = MediaInfo(Uri.fromFile(fileMedia), mimeType)

                                mView.post {
                                    notifyItemChanged(position, fileMedia)
                                }
                            }
                        }
                    }
                }
            } else {
                attachment = mDownloadedDecryptedFiles[position]?.uri
                thumbnail = mThumbnails[position]?.uri
            }
        }

        val reactions = HashMap<String, QuickReaction>()

        for (reaction in te?.annotations?.reactionsSummary ?: emptyList()) {
            val qr = reactions[reaction.key] ?: QuickReaction(reaction.key, ArrayList())
            if (reaction.addedByMe) qr.sentByMe = true

            for (eventId in reaction.sourceEvents) {
                SharedTimeline.instance.room?.timelineService()?.getTimelineEvent(eventId)?.senderInfo?.userId?.let {
                    qr.senders.add(it)
                }
            }

            reactions[reaction.key] = qr
        }

        val event = te?.root

        when (viewHolder) {
            is ResponseViewHolder -> {
                val isIncoming = event?.senderId != mSession?.myUserId

                val contextMenuView = when {
                    viewHolder.container != null -> viewHolder.container

                    else -> viewHolder.itemView
                }

                if (isIncoming) {
                    contextMenuView?.setOnClickListener {
                        mActivity.startActivity(
                            mApp?.router?.friend(mActivity, te?.senderInfo?.userId, showSendPrivateMsg = false)
                        )
                    }
                }

                contextMenuView?.setOnLongClickListener {
                    if (te != null) {
                        mQuickResponseLongClick?.onQuickResponseClick(
                            timeline = te,
                            attachment,
                            thumbnail
                        )
                    }
                    true
                }

                if (te != null) {

                    viewHolder.bind(
                        te, null, attachment, thumbnail, reactions.values.toList()
                    )
                }

                //As per design: Hide timeline vertical view if this is the last message item
                if (position == itemCount - 1) {
                    viewHolder.timelineView.visibility = View.GONE
                } else {
                    viewHolder.timelineView.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onImageClicked(image: Uri) {
        val uris = ArrayList<Uri>()
        val mimeTypes = ArrayList<String>()

        mDownloadedDecryptedFiles.filter { it.value.isImage }.keys.sortedDescending()
            .forEach { i ->
                mDownloadedDecryptedFiles[i]?.let {
                    uris.add(it.uri)
                    mimeTypes.add(it.mimeType)
                }
            }

        mRequestImageViewForResult.launch(
            mApp?.router?.imageView(mActivity, uris, mimeTypes, 0.coerceAtLeast(uris.indexOf(image)),
                showResend = false, showForward = false)
        )

    }

    override fun onUpdate(state: ContentUploadStateTracker.State) {
        when (state) {
            ContentUploadStateTracker.State.Idle -> {
                unsetProgress()
            }

            ContentUploadStateTracker.State.EncryptingThumbnail -> {
                setProgress()
            }

            ContentUploadStateTracker.State.CompressingImage -> {
                setProgress()
            }

            is ContentUploadStateTracker.State.CompressingVideo -> {
                setProgress((state.percent * 10).toLong(), 1000)
            }

            is ContentUploadStateTracker.State.UploadingThumbnail -> {
                setProgress(state.current, state.total)
            }

            is ContentUploadStateTracker.State.Encrypting -> {
                setProgress(state.current, state.total)
            }

            is ContentUploadStateTracker.State.Uploading -> {
                setProgress(state.current, state.total)
            }

            ContentUploadStateTracker.State.Success -> {
                unsetProgress()
            }

            is ContentUploadStateTracker.State.Failure -> {
                Snackbar.make(mView,
                    mActivity.getString(R.string.error_prefix) + state.throwable.betterMessage,
                    Snackbar.LENGTH_LONG).show()

                unsetProgress()
            }
        }
    }

    private fun setProgress(current: Long = 0, total: Long = 0) {
        val pos = mShowProgressOnPosition ?: return

        mProgressCurrent = current
        mProgressTotal = total

        notifyItemChanged(pos)
    }

    private fun unsetProgress() {
        val pos = mShowProgressOnPosition ?: return

        mShowProgressOnPosition = null

        getItem(pos)?.eventId?.let {
            mSession?.contentUploadProgressTracker()?.untrack(it, this)
        }

        notifyItemChanged(pos)
    }

    private fun trackProgressIfOngoingUpload(te: TimelineEvent) {
        if (LocalEcho.isLocalEchoId(te.eventId)
            && te.root.senderId == mSession?.myUserId
            && te.root.isAttachmentMessage())
        {
            mSession?.contentUploadProgressTracker()?.track(te.eventId, this@ResponseMessageAdapter)
        }
    }

    private fun jumpToPosition(position: Int) {
        val count = itemCount

        if (count > 0) {

            if (position > 0) {
                mView.layoutManager?.scrollToPosition(position - 1)
            }
            else {
                mView.layoutManager?.scrollToPosition(0)
            }
        }
    }

    private fun jumpToFirstUnread() {
        var firstUnread = 0

        for (event in currentList) {
            if (SharedTimeline.instance.room?.readService()?.isEventRead(event.eventId) == false) {
                firstUnread = currentList.indexOf(event)

                break
            }
        }

        // #isEventRead seems to be continuously off by one.
        firstUnread = (firstUnread - 1).coerceAtLeast(0)

        // List is displayed inverted! (Position 0 at bottom!)
        jumpToPosition(itemCount - firstUnread)
    }

    /**
     * Fetch all read receipts from a timeline event into [mAllReadReceipts]
     * without duplicated user receipt data
     */
    private fun fetchReadReceiptFromEvent(te: TimelineEvent?) {
        val readReceipts = te?.readReceipts ?: listOf()
        val readReceiptsData = readReceipts
            .filter {
                !mSeenUsers.contains(it.roomMember.userId)
            }
            .map {
                val currentUserId = it.roomMember.userId
                mSeenUsers.add(currentUserId)
                ReadReceiptData(currentUserId, it.roomMember.avatarUrl, it.roomMember.displayName, it.originServerTs)
            }
            .toMutableList()

        mAllReadReceipts.addAll(readReceiptsData)
    }
}
