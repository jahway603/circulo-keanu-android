package org.article19.circulo.next.main.updatestatus

import agency.tango.android.avatarview.views.AvatarView
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.net.Uri
import android.provider.Browser
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager
import info.guardianproject.keanu.core.ui.widgets.MediaViewHolder
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.LinkifyHelper
import info.guardianproject.keanu.core.util.extensions.getLastMessageBody
import info.guardianproject.keanuapp.ui.conversation.QuickReaction
import info.guardianproject.keanuapp.ui.widgets.AudioWife
import info.guardianproject.keanuapp.ui.widgets.QuickReactionsRecyclerViewAdapter
import info.guardianproject.keanuapp.ui.widgets.QuickReactionsRecyclerViewAdapter.QuickReactionsRecyclerViewAdapterListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.common.utils.TimeTextUtils
import org.article19.circulo.next.databinding.StatusReplyBaseBinding
import org.article19.circulo.next.main.now.Status
import org.article19.circulo.next.mention.RoundedBackgroundSpan
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.room.model.message.MessageAudioContent
import org.matrix.android.sdk.api.session.room.model.message.MessageImageContent
import org.matrix.android.sdk.api.session.room.model.message.MessageStickerContent
import org.matrix.android.sdk.api.session.room.model.message.MessageVideoContent
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.send.SendState
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.session.room.timeline.hasBeenEdited
import timber.log.Timber
import java.io.FileInputStream
import java.net.URLConnection
import java.net.URLDecoder
import java.util.Date
import java.util.Locale
import java.util.regex.Pattern


/**
 * Created by n8fr8 on 12/11/15.
 */
class ResponseViewHolder(view: View) : MediaViewHolder(view),
    QuickReactionsRecyclerViewAdapterListener, View.OnClickListener {

    companion object {
        /**
         * Returns a list with all links contained in the input
         */
        private fun extractUrls(text: String): List<String> {
            val containedUrls: MutableList<String> = ArrayList()
            val urlRegex =
                "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?+-=\\\\.&]*)"
            val pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE)
            val urlMatcher = pattern.matcher(text)

            while (urlMatcher.find()) {
                containedUrls.add(text.substring(urlMatcher.start(0), urlMatcher.end(0)))
            }

            return containedUrls
        }

        private fun isIntentAvailable(context: Context?, intent: Intent): Boolean {
            return context?.packageManager?.queryIntentActivities(
                intent,
                PackageManager.MATCH_DEFAULT_ONLY
            )?.isNotEmpty() == true
        }
    }

    interface OnImageClickedListener {
        fun onImageClicked(image: Uri)
    }

    interface OnQuickReactionClickedListener {
        fun onQuickReactionClicked(quickReaction: QuickReaction, eventId: String)
    }

    var eventId: String? = null
        private set

    var message: String? = null
        private set

    var onImageClickedListener: OnImageClickedListener? = null

    private var onQuickReactionClickedListener: OnQuickReactionClickedListener? = null

    var audioWife: AudioWife? = null

    var avatar: AvatarView? = null

    private var mTextViewForMessages: TextView? = null

    private var mTextViewForTimestamp: TextView? = null

    private var mTextViewMapMessage: TextView? = null
    private var mTextViewMapTime: TextView? = null

    private var mTextViewForStatus: TextView? = null

    private var mMapLocation: View? = null

    private var mMediaContainer: ViewGroup? = null

    private var mAudioContainer: ViewGroup? = null

    private var mQuickReactionContainer: RecyclerView? = null

    lateinit var timelineView: View

    // reply View
    /**
    private var replyContainer: ViewGroup? = null
    private var messageSenderTextView: TextView? = null
    private var messageContentTextView: TextView? = null
    private var messageContentImageView: ImageView? = null
    **/

    private val mSession: Session?
        get() = ImApp.sImApp?.matrixSession

    private val mContext: Context?
        get() = itemView.context


    constructor(binding: StatusReplyBaseBinding) : this(binding.root) {
        mTextViewForMessages = binding.message
        mTextViewForTimestamp = binding.messageTs
        mTextViewMapTime = binding.tvMapTime
        mTextViewMapMessage = binding.tvViewLocation
        mTextViewForStatus = binding.tvStatus
        mMapLocation = binding.responseLocation
        avatar = binding.avatar
        mMediaContainer = binding.mediaThumbnailContainer
        mAudioContainer = binding.audioContainer
        mQuickReactionContainer = binding.quickReactionContainer
        /**
        replyContainer = binding.replyParentLayout.root
        messageSenderTextView = binding.replyParentLayout.messageSenderTextView
        messageContentTextView = binding.replyParentLayout.messageContentTextView
        messageContentImageView = binding.replyParentLayout.messageImageView
        **/

        // Rebind to enforce correct XML definition.
        mediaThumbnail = binding.mediaThumbnail
        mediaPlay = binding.mediaThumbnailPlay
        progressBar = binding.progress
        container = binding.messageContainer
        timelineView = binding.viewTimeline
    }

    /**
    constructor(binding: MessageViewRightBinding) : this(binding.root) {
        mTextViewForMessages = binding.message
        mTextViewForTimestamp = binding.messageTs
        avatar = binding.avatar
        mMediaContainer = binding.mediaThumbnailContainer
        mAudioContainer = binding.audioContainer
        mQuickReactionContainer = binding.quickReactionContainer

        replyContainer = binding.replyParentLayout.root
        messageSenderTextView = binding.replyParentLayout.messageSenderTextView
        messageContentTextView = binding.replyParentLayout.messageContentTextView
        messageContentImageView = binding.replyParentLayout.messageImageView

        // Rebind to enforce correct XML definition.
        mediaThumbnail = binding.mediaThumbnail
        mediaPlay = binding.mediaThumbnailPlay
        progressBar = binding.progress
        container = binding.messageContainer
    }**/

    init {
        // Disable built-in autoLink so we can add custom ones.
        mTextViewForMessages?.autoLinkMask = 0
    }


    override fun onReactionClicked(reaction: QuickReaction) {
        eventId?.let {
            onQuickReactionClickedListener?.onQuickReactionClicked(reaction, it)
        }
    }

    fun bind(
        te: TimelineEvent,
        progress: Int?,
        attachment: Uri?,
        thumbnail: Uri?,
        reactions: List<QuickReaction>?
    ) {

        val updateAvatar = !(eventId.equals(te.eventId))

        mimeType = ""
        this.attachment = null
        eventId = te.eventId
        message = te.getLastMessageBody()

        itemView.visibility = View.VISIBLE
        var isMapMessage = false

        (itemView as? ResponseListItem)?.bind(this)

        if (attachment == null && thumbnail == null && progress != null) {
            if (progress >= 0) {
                progressBar?.isIndeterminate = false
                progressBar?.progress = progress
            } else {
                progressBar?.isIndeterminate = true
            }

            progressBar?.visibility = View.VISIBLE
            progressBar?.animate()
        } else {
            progressBar?.visibility = View.GONE
        }

        if (updateAvatar) {
            avatar?.let {
                GlideUtils.loadAvatar(it, te.senderInfo)
            }
        }

        val mc = te.getLastMessageContent()

        //avatar?.visibility = if (isOutgoing) View.GONE else View.VISIBLE
       // avatar?.visibility = View.GONE //no avatars

        container?.visibility = View.VISIBLE
        mAudioContainer?.visibility = View.GONE
        mMediaContainer?.visibility = View.GONE
        mediaPlay?.visibility = View.GONE
        mTextViewForMessages?.visibility = View.VISIBLE
        mTextViewForTimestamp?.visibility = View.VISIBLE
        mTextViewForStatus?.visibility = View.GONE
        mMapLocation?.visibility = View.GONE

        mediaThumbnail?.scaleType = ImageView.ScaleType.FIT_CENTER

        setMediaClickListener(null, null)

        mediaThumbnail?.setImageResource(android.R.color.transparent)

        when (mc) {
            is MessageAudioContent -> {
                this.attachment = attachment
                setAudioContent(mc.mimeType, attachment)

            }

            is MessageImageContent -> {
                mTextViewForMessages?.visibility = View.GONE
                mMediaContainer?.visibility = View.VISIBLE

                mimeType = getMimeType(mc.mimeType, attachment?.lastPathSegment)

                // If this is the same attachment, we don't need to reload it.
                if (!this.attachment?.path.equals(attachment?.path)) {
                    this.attachment = attachment

                    (thumbnail ?: attachment)?.let {
                        GlideUtils.loadImageFromUri(mContext, it, mediaThumbnail, false)
                    }
                }
                //replyContainer?.visibility = View.GONE

                setMediaClickListener(mimeType, attachment)
            }

            is MessageStickerContent -> {
                mTextViewForMessages?.visibility = View.GONE
                mMediaContainer?.visibility = View.VISIBLE

                mimeType = getMimeType(mc.mimeType, attachment?.lastPathSegment)

                // If this is the same attachment, we don't need to reload it.
                if (!this.attachment?.path.equals(attachment?.path)) {
                    this.attachment = attachment

                    (thumbnail ?: attachment)?.let {
                        GlideUtils.loadImageFromUri(mContext, it, mediaThumbnail, false)
                    }
                }
              //  replyContainer?.visibility = View.GONE
            }

            is MessageVideoContent -> {
                //    setVideoContent(mc.mimeType, thumbnail)
                mTextViewForMessages?.visibility = View.GONE

                mMediaContainer?.visibility = View.VISIBLE
                mediaPlay?.visibility = View.VISIBLE

                mediaPlay?.setImageResource(R.drawable.media_audio_play)

                mimeType = getMimeType(mc.mimeType, attachment?.lastPathSegment)

                // If this is the same attachment, we don't need to reload it.
                if (!this.attachment?.path.equals(attachment?.path)) {
                    this.attachment = attachment

                    if (thumbnail != null) {
                        GlideUtils.loadImageFromUri(mContext, thumbnail, mediaThumbnail, false)
                    } else {
                        mediaThumbnail?.setImageResource(R.drawable.video256)
                    }
                }
                //replyContainer?.visibility = View.GONE

                setMediaClickListener(mimeType, attachment)
            }

            is MessageWithAttachmentContent -> {
                mMediaContainer?.visibility = View.VISIBLE

                mediaThumbnail?.scaleType = ImageView.ScaleType.FIT_START

                mimeType = getMimeType(mc.mimeType, attachment?.lastPathSegment)
                this.attachment = attachment

                when {
                    mimeType.isEmpty() -> {
                        mediaThumbnail?.setImageResource(R.drawable.file_unknown)
                    }

                    mimeType.contains("html") -> {
                        val containedImageUri = getImageFromContent(attachment)

                        if (containedImageUri?.isNotEmpty() == true) {
                            GlideUtils.loadImageFromUri(
                                mContext,
                                containedImageUri.toUri(),
                                mediaThumbnail,
                                false
                            )

                            mediaPlay?.setImageResource(R.drawable.ic_cloud_download_black_48dp)
                            mediaPlay?.visibility = View.VISIBLE
                        } else {
                            mediaThumbnail?.setImageResource(R.drawable.file_unknown)
                        }
                    }

                    mimeType == "text/csv" && attachment?.lastPathSegment?.contains("proof.csv") == true -> {
                        mTextViewForMessages?.visibility = View.GONE
                        mTextViewForTimestamp?.visibility = View.GONE

                        mediaThumbnail?.setImageResource(R.drawable.proofmodebadge)
                    }

                    mimeType.contains("pdf") -> {
                        mediaThumbnail?.setImageResource(R.drawable.file_pdf)
                    }

                    mimeType.contains("doc") || mimeType.contains("word") -> {
                        mediaThumbnail?.setImageResource(R.drawable.file_doc)
                    }

                    mimeType.contains("zip") -> {
                        mediaThumbnail?.setImageResource(R.drawable.file_zip)
                    }
                    mimeType.contains("video") -> {
                        setVideoContent(mc.mimeType, thumbnail)
                    }
                    mimeType.contains("octet-stream") -> {
                        setAudioContent("audio/mp4", attachment)

                    }
                    else -> {
                        mediaThumbnail?.setImageResource(R.drawable.file_unknown)
                    }
                }
                //replyContainer?.visibility = View.GONE

                mTextViewForMessages?.text = cleanDisplayName(mc.getFileName())
                if (!mimeType.contains("octet-stream")) {
                    setMediaClickListener(mimeType, attachment)
                }
            }

            else -> {
                var asset: String? = null


                if (message?.startsWith("/sticker:") == true) {
                    // Just get up to any whitespace.
                    asset = message?.split(":")?.toTypedArray()?.get(1)?.split(" ")?.toTypedArray()
                        ?.first()?.lowercase(Locale.ROOT)
                } else if (message?.startsWith(":") == true) {
                    val parts =
                        message?.split(":")?.toTypedArray()?.get(1)?.split("-")?.toTypedArray()
                    val folder = parts?.firstOrNull()
                    val name = StringBuffer()

                    for (i in 1 until (parts?.size ?: 0)) {
                        name.append(parts?.get(i))
                        if (i + 1 < (parts?.size ?: 0)) name.append('-')
                    }

                    asset = "stickers/$folder/$name.png"
                }

                if (asset != null && assetExists(asset)) {
                    mTextViewForMessages?.visibility = View.GONE
                    mTextViewForStatus?.visibility = View.GONE

                    mMediaContainer?.visibility = View.VISIBLE

                    mediaThumbnail?.scaleType = ImageView.ScaleType.FIT_START

                    GlideUtils.loadImageFromUri(
                        mContext,
                        "asset://localhost/$asset".toUri(),
                        mediaThumbnail,
                        false
                    )
                } else {
                    if (te.root.type == EventType.REACTION || te.root.type == EventType.REDACTION || message.isNullOrBlank()) {
                        avatar?.visibility = View.GONE
                        mTextViewForMessages?.visibility = View.GONE
                        mTextViewForStatus?.visibility = View.GONE
                        mTextViewForTimestamp?.visibility = View.GONE
                        mMediaContainer?.visibility = View.GONE

                        return
                    } else {


                        var statusText = message ?: ""
                        var mentionedName = ""

                        // have reply content
                        SharedTimeline.getFormattedMessage(mc)?.let {
                            if (it.sender.isNotEmpty()) {
                                statusText = it.reply
                                mentionedName = it.mentionedName ?: ""
                            }
                        }

                        if (statusText.contains(Status.UNCERTAIN.value)) {
                            mTextViewForStatus?.visibility = View.VISIBLE
                            mTextViewForMessages?.visibility = View.GONE
                            mTextViewForStatus?.setText(R.string.status_set_to_uncertain)
                            mTextViewForStatus?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_status_uncertain_round, 0, 0, 0)
                        }
                        else if (statusText.contains(Status.NOT_SAFE.value)) {
                            mTextViewForStatus?.visibility = View.VISIBLE
                            mTextViewForMessages?.visibility = View.GONE
                            mTextViewForStatus?.setText(R.string.status_set_to_not_safe)
                            mTextViewForStatus?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_status_not_safe_round, 0, 0, 0)
                        }
                        else if (statusText.contains(Status.SAFE.value)) {
                            mTextViewForStatus?.visibility = View.VISIBLE
                            mTextViewForMessages?.visibility = View.GONE
                            mTextViewForStatus?.setText(R.string.status_set_to_safe)
                            mTextViewForStatus?.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_status_safe_round, 0, 0, 0)
                        }
                        else if (statusText.contains(Status.GEO_STRING)) {
                            mTextViewForStatus?.visibility = View.VISIBLE
                            mTextViewForMessages?.visibility = View.GONE

                            var geoString = statusText.substring(statusText.indexOf(Status.GEO_STRING)+4).split(" ").get(0)
                            val latLon = geoString.split(",")

                            val address =
                                getAddress(latLon.get(0).toDouble(), latLon.get(1).toDouble())
                            mTextViewMapMessage?.text = address


                            mMapLocation?.visibility = View.VISIBLE
                            isMapMessage = true


                            mMapLocation?.setOnClickListener {

                                val uriGeo = Uri.parse("geo:0,0?q=${latLon.get(0)},${latLon.get(1)}")
                                val mapIntent = Intent(Intent.ACTION_VIEW, uriGeo)
                                mContext?.startActivity(mapIntent)
                            }
                        }
                        else {
                            mTextViewForMessages?.visibility = View.VISIBLE
                            mTextViewForStatus?.visibility = View.GONE

                            if (te.hasBeenEdited()) {
                                statusText += " " + mContext?.getString(R.string.edited)
                            }

                            //replyContainer?.visibility = View.GONE

                            val displayName = mentionedName

                            val startIndex = statusText.indexOf(displayName)
                            val spannable = SpannableString(statusText)

                            if (startIndex >= 0 && startIndex < statusText.length) {
                                val backgroundSpan = RoundedBackgroundSpan(mContext!!, ContextCompat.getColor(mContext!!, R.color.background_mention))
                                spannable.setSpan(backgroundSpan, startIndex, startIndex + displayName.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                            }
                            mTextViewForMessages?.text = spannable

                        }
                    }
                }
            }
        }

        val timestamp = te.root.originServerTs

        if (timestamp != null) {
            updateMessageSendState(te)
            val senderName =
                if (te.senderInfo.displayName.isNullOrBlank()) te.senderInfo.userId else te.senderInfo.displayName

            val timeString = TimeTextUtils.formatMessageInfo(Date(timestamp), senderName)

            if (isMapMessage) {
                mTextViewForTimestamp?.visibility = View.GONE
                mTextViewMapTime?.visibility = View.VISIBLE
                mTextViewMapTime?.text = timeString
            } else {
                mTextViewForTimestamp?.text = timeString
                mTextViewForTimestamp?.visibility = View.VISIBLE
            }

        } else {
            mTextViewForTimestamp?.visibility = View.GONE
        }

        if (Preferences.getDoLinkify()) LinkifyHelper.addLinks(
            mTextViewForMessages,
            URLSpanConverter()
        )

        if (reactions?.isNotEmpty() == true) {
            val adapter = QuickReactionsRecyclerViewAdapter(mContext, reactions)
            mQuickReactionContainer?.adapter = adapter
            adapter.setListener(this)
        } else {
            mQuickReactionContainer?.adapter = null
        }
    }

    /**
     * Render Send/Delivered state of a message if it's sent/delivered by current user
     */
    private fun updateMessageSendState(te: TimelineEvent?) {
        if (te?.senderInfo?.userId == mSession?.myUserId) {
            val sendState = te?.root?.sendState
            var rightDrawable: Int? = null
            if (sendState == SendState.SENT) {
                rightDrawable = R.drawable.ic_message_sent
            } else if (sendState == SendState.SYNCED) {
                rightDrawable = R.drawable.ic_message_delivered
            }

            if (rightDrawable != null) {
                mTextViewForTimestamp?.setCompoundDrawablesWithIntrinsicBounds(0, 0, rightDrawable, 0)
            }
        }
    }

    private fun getAddress(lat: Double, lng: Double): String? {
        val geocoder = mContext?.let { Geocoder(it) }
        val list = geocoder?.getFromLocation(lat, lng, 1)
        list.let {
            if (list?.size!! > 0)
            return list[0]?.getAddressLine(0)
        }

        return null
    }

    override fun onClick(v: View?) {
        when {
            mimeType.startsWith("image") -> {
                if (onImageClickedListener != null) {
                    attachment?.let { onImageClickedListener?.onImageClicked(it) }
                } else {
                    mContext?.let {
                        val attachment = attachment ?: return@let

                        it.startActivity(
                            ImApp.sImApp?.router?.imageView(
                                it,
                                arrayListOf(attachment)
                            )
                        )
                    }
                }
            }

            mimeType.contains("pdf") -> {
                mContext?.let {
                    it.startActivity(
                        ImApp.sImApp?.router?.pdfView(
                            it,
                            attachment,
                            mimeType
                        )
                    )
                }
            }

            mimeType.contains("html") || mimeType.contains("text/plain") -> {
                mContext?.let {
                    it.startActivity(
                        ImApp.sImApp?.router?.webView(
                            it,
                            attachment,
                            mimeType,
                            eventId
                        )
                    )
                }
            }

            mimeType.contains("video") -> {
                mContext?.let {
                    it.startActivity(
                        ImApp.sImApp?.router?.videoView(
                            it,
                            attachment,
                            mimeType
                        )
                    )
                }
            }

            attachment?.scheme == "content" -> {
                var intent = Intent(Intent.ACTION_VIEW)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)

                // Set a general mime type not specific.
                intent.setDataAndType(attachment, mimeType)

                if (isIntentAvailable(mContext, intent)) {
                    mContext?.startActivity(intent)
                } else {
                    intent = Intent(Intent.ACTION_SEND)
                    intent.setDataAndType(attachment, mimeType)

                    if (isIntentAvailable(mContext, intent)) {
                        mContext?.startActivity(intent)
                    } else {
                        Toast.makeText(
                            mContext,
                            R.string.there_is_no_viewer_available_for_this_file_format,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }

            else -> {
                (itemView as? ResponseListItem)?.exportMediaFile()
            }
        }
    }

    /**
     * In case we don't have a good MIME type, guess a better one and return that.
     *
     * @param mimeType
     *      MIME type we already have.
     *
     * @param filename
     *      Filename to guess a MIME type from.
     */
    private fun getMimeType(mimeType: String?, filename: String?): String {
        if (filename?.isNotEmpty() == true && (mimeType.isNullOrEmpty() || mimeType.startsWith("application"))) {
            val guessed = URLConnection.guessContentTypeFromName(filename)

            if (guessed?.isNotEmpty() == true) {
                return if (guessed == "video/3gpp") "audio/3gpp" else guessed
            }
        }

        return if (mimeType == "video/3gpp") "audio/3gpp" else mimeType ?: ""
    }

    private fun getImageFromContent(file: Uri?): String? {
        if (file == null) return null

        val inputStream = try {
            FileInputStream(file.path)
        } catch (e: Exception) {
            Timber.e(e)
            return null
        }

        val urls = extractUrls(inputStream.bufferedReader().use { it.readText() })

        for (url in urls) {
            if ("\\.(jpe?g|png|gif)$".toRegex(RegexOption.IGNORE_CASE)
                    .containsMatchIn(url)
            ) return url
        }

        return null
    }

    private fun cleanDisplayName(name: String?): String? {
        var result = name

        try {
            result = URLDecoder.decode(result, "UTF-8")
            result = result.replace('_', ' ')
            result = result.split("\\.").toTypedArray().firstOrNull()
        } catch (ignored: Exception) {
            return result
        }

        return result
    }

    private fun setMediaClickListener(mimeType: String?, mediaUri: Uri?) {
        mediaPlay?.setOnClickListener(null)
        mediaThumbnail?.setOnClickListener(null)

        if (mimeType == null || mediaUri == null) return

        val viewForClick = if (mediaPlay?.visibility == View.VISIBLE) mediaPlay else mediaThumbnail

        viewForClick?.setOnClickListener(this)
    }

    private fun assetExists(asset: String): Boolean {
        return try {
            val fd = mContext?.assets?.openFd(asset) ?: return false
            fd.length
            fd.close()

            true
        } catch (failure: Throwable) {
            false
        }
    }

    private fun setVideoContent(fileMimeType: String?, thumbnail: Uri?) {
        mTextViewForMessages?.visibility = View.GONE
        mMediaContainer?.visibility = View.VISIBLE
        mediaPlay?.visibility = View.VISIBLE

        mediaPlay?.setImageResource(R.drawable.media_audio_play)

        mimeType = getMimeType(fileMimeType, attachment?.lastPathSegment)

        // If this is the same attachment, we don't need to reload it.
        if (!this.attachment?.path.equals(attachment?.path)) {
            this.attachment = attachment

            if (thumbnail != null) {
                GlideUtils.loadImageFromUri(mContext, thumbnail, mediaThumbnail, false)
            } else {
                mediaThumbnail?.setImageResource(R.drawable.video256)
            }
        }
       // replyContainer?.visibility = View.GONE

        setMediaClickListener(mimeType, attachment)
    }

    private fun setAudioContent(fileMineType: String?, attachment: Uri?) {
        mTextViewForMessages?.visibility = View.GONE
        mMediaContainer?.visibility = View.GONE
        mAudioContainer?.visibility = View.VISIBLE
      //  replyContainer?.visibility = View.GONE

        if (this.attachment != null) {
            mimeType = getMimeType(fileMineType, attachment?.lastPathSegment)
            audioWife?.pause()
            audioWife?.release()

            mAudioContainer?.removeAllViews()

            if (attachment != null) {
                mContext?.let {
                    // When done playing, release the resources.
                    audioWife = AudioWife()
                    audioWife?.init(it, attachment, mimeType)
                        ?.useDefaultUi(mAudioContainer, LayoutInflater.from(it))
                }
            }
        }
    }

    /**
     * This trickery is needed in order to have clickable links that open things
     * in a new `Task` rather than in Keanu's `Task.` Thanks to @commonsware
     * https://stackoverflow.com/a/11417498
     *
     */
    internal inner class NewTaskUrlSpan(private val urlString: String) : ClickableSpan() {

        override fun onClick(widget: View) {
            val diLink = OnboardingManager.decodeInviteLink(urlString)

            val uri = Uri.parse(urlString)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, widget.context.packageName)

            // Not an invite link, so just send it out.
            if (diLink == null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            } else {
                // It is an invite link, so target it back at us!
                intent.setPackage(widget.context.packageName) // The package name of the app to which intent is to be sent.
            }

            widget.context.startActivity(intent)
        }
    }

    internal inner class URLSpanConverter : LinkifyHelper.SpanConverter<URLSpan?, ClickableSpan?> {
        override fun convert(span: URLSpan?): NewTaskUrlSpan {
            return NewTaskUrlSpan(span?.url ?: "")
        }
    }
}