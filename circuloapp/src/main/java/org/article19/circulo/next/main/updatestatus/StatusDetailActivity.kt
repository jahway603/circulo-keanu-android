package org.article19.circulo.next.main.updatestatus

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.location.Geocoder
import android.location.Location
import android.media.AudioManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.maxkeppeler.sheets.core.SheetStyle
import com.maxkeppeler.sheets.info.InfoSheet
import com.otaliastudios.autocomplete.Autocomplete
import com.otaliastudios.autocomplete.AutocompleteCallback
import com.otaliastudios.autocomplete.AutocompletePresenter
import com.otaliastudios.autocomplete.CharPolicy
import com.tougee.recorderview.AudioRecordView
import com.zhihu.matisse.Matisse
import info.guardianproject.keanu.core.ui.room.RoomActivity
import info.guardianproject.keanu.core.util.AttachmentHelper
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.PrettyTime
import info.guardianproject.keanu.core.util.WrapContentLinearLayoutManager
import info.guardianproject.keanu.core.util.extensions.getLastMessageBody
import info.guardianproject.keanuapp.ui.widgets.ShareRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.common.utils.ResourceUtils
import org.article19.circulo.next.common.utils.TimeTextUtils
import org.article19.circulo.next.databinding.ActivityStatusDetailBinding
import org.article19.circulo.next.databinding.DialogMyConditionBinding
import org.article19.circulo.next.main.now.Status
import org.article19.circulo.next.main.now.Thread
import org.article19.circulo.next.mention.PillImageSpan
import org.article19.circulo.next.mention.TextPillsUtils
import org.article19.circulo.next.mention.member.UserPresenter
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import org.matrix.android.sdk.api.session.room.model.relation.RelationDefaultContent
import org.matrix.android.sdk.api.session.room.model.relation.ReplyToContent
import org.matrix.android.sdk.api.session.room.read.ReadService
import org.matrix.android.sdk.api.util.MatrixItem
import org.matrix.android.sdk.api.util.toMatrixItem
import timber.log.Timber
import java.io.File
import java.util.*
import kotlin.math.max


class StatusDetailActivity : CommonSelectMediaActivity(), AudioRecordView.Callback,
    Observer<List<Thread>> {

    companion object {
        const val EXTRA_THREAD_ID = "extra_thread_id"
    }

    private lateinit var mBinding: ActivityStatusDetailBinding

    private var rvAdapter: ResponseMessageAdapter? = null
    private lateinit var mSeenByAdapter: SeenByAdapter

    private val mApp: CirculoApp?
        get() = application as? CirculoApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private lateinit var mThreadId: String

    private var mThread: Thread? = null

    private val mRoom
        get() = SharedTimeline.instance.room

    private var mLocation = ""
    private var mAddress : String? = null

    private lateinit var mAudioFilePath : File
    private var isAudioRecording = false

    private lateinit var mMediaRecorder : MediaRecorder

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mApp?.cancelNotifications()

        mBinding = ActivityStatusDetailBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.toolbar.ivBack.setOnClickListener {
            finish()
        }

        mBinding.etUpdateStatus.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                sendTextMessage()
                return@setOnEditorActionListener true
            }
            false
        }

        // Init Send button behavior
        mBinding.etUpdateStatus.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ((s?.length ?: 0) > 0) {
                    mBinding.ivSendMessage.setColorFilter(ContextCompat.getColor(applicationContext, R.color.black))
                    mBinding.ivSendMessage.setBackgroundResource(
                        ResourceUtils.getResIdFromAttribute(android.R.attr.selectableItemBackgroundBorderless))
                    mBinding.ivSendMessage.setOnClickListener {
                        sendTextMessage()
                    }
                } else {
                    mBinding.ivSendMessage.setColorFilter(
                        ContextCompat.getColor(applicationContext, info.guardianproject.keanuapp.R.color.holo_grey_dark))
                    mBinding.ivSendMessage.setBackgroundResource(0)
                    mBinding.ivSendMessage.setOnClickListener {}
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        mBinding.layoutLocation.setOnClickListener {
            if (mLocation.isNotEmpty())
            {
                showMap()
            }
        }

        mBinding.ivVoiceMsg.setOnClickListener {
            activateVoiceMessage()
        }

        mBinding.ivAddMedia.setOnClickListener {
            showMediaChooserPopUp(mBinding.ivAddMedia, mBinding.layoutBottomControlBoard)
        }


        val llm = WrapContentLinearLayoutManager(this)
        mBinding.rvReplies.layoutManager = llm

        //Init latest button behavior: Appear when we are not at the latest message position,
        //                             Disappear when we are at the latest message position
        mBinding.btnLatest.setOnClickListener {
            mBinding.scrollViewContainer.post {
                mBinding.scrollViewContainer.fullScroll(ScrollView.FOCUS_DOWN)
            }
        }

        mBinding.scrollViewContainer.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, _, _, _ ->

            // We take the last son in the scrollview
            val diff: Int = v.getChildAt(v.childCount - 1).bottom - (v.height + v.scrollY)

            // if diff is zero, then the bottom has been reached
            if (diff == 0) {
                mBinding.btnLatest.visibility = View.GONE
            } else {
                mBinding.btnLatest.visibility = View.VISIBLE
            }
        })

        mThreadId = intent.getStringExtra(EXTRA_THREAD_ID) ?: return finish()

        setupMentionAutoComplete(mBinding.etUpdateStatus)

        mBinding.ivChooseLocation.setOnClickListener {
            addLocation()
        }

        val readReceiptUpdateCallback = object : ReadReceiptUpdateCallback {
            override fun onReadReceiptsUpdated(readReceipts: MutableList<ReadReceiptData>) {
                mSeenByAdapter.updateList(readReceipts)

                mBinding.layoutSeenBy.root.visibility = if (readReceipts.size == 0) View.GONE else View.VISIBLE
            }
        }

        rvAdapter = ResponseMessageAdapter(
            this@StatusDetailActivity,
            mBinding.rvReplies, readReceiptUpdateCallback
        )
        mBinding.rvReplies.adapter = rvAdapter

        // Set read receipt by current user on the parent timeline event of the selected
        // status.
        mCoroutineScope.launch {
            mThread?.eventId?.let {

                //Fetch read receipts data from the parent timeline event
                val readReceipts = mThread?.events?.firstOrNull()?.readReceipts ?: listOf()
                val readReceiptsData = readReceipts
                    .map {
                        ReadReceiptData(
                            it.roomMember.userId,
                            it.roomMember.avatarUrl,
                            it.roomMember.displayName,
                            it.originServerTs
                        )
                    }
                    .toMutableList()

                //Check if current user already seen the event or not
                var alreadySetReadReceipt = false
                for (readReceipt in readReceiptsData) {
                    if (readReceipt.userId == mSession?.myUserId) {
                        alreadySetReadReceipt = true
                        break
                    }
                }

                //Only set read receipt/read marker if the current user has not ever seen
                //the message yet
                if (!alreadySetReadReceipt) {
                  //  mRoom?.readService()?.setReadReceipt(eventId, ReadService.THREAD_ID_MAIN)
                   // mRoom?.readService()?.setReadMarker(eventId)
                    mRoom?.readService()?.markAsRead()
                }
            }
        }

        mSeenByAdapter = SeenByAdapter(mutableListOf())
        mBinding.layoutSeenBy.recyclerViewSeenBy.adapter = mSeenByAdapter
    }

    override fun onResume() {
        super.onResume()

        SharedTimeline.instance.threads.observe(this, this)
    }

    override fun onPause() {
        super.onPause()

        SharedTimeline.instance.threads.removeObservers(this)
    }

    override fun onChanged(value: List<Thread>) {
        mThread = value.firstOrNull { it.eventId == mThreadId }

        Timber.d("StatusDetailActivity onChanged ${mThread?.eventId}")

        rvAdapter?.thread = mThread

        if (mThread == null) finish()

        val senderIsMe = mThread?.userId == mSession?.myUserId

        if (senderIsMe) {
            mBinding.toolbar.tvResolveStatus.visibility = View.VISIBLE
            mBinding.toolbar.tvResolveStatus.setOnClickListener {
                addStatusReply("#resolved")

                rvAdapter?.redactAll()

                val resolveTime = Date().time - (mThread?.events?.firstOrNull()?.root?.originServerTs ?: 0)
                mApp?.cleanInsightsManager?.measureEvent("status-resolved",
                    "resolve-time", resolveTime.toDouble())

                val intent = Intent(this, UpdateLocationStatusService::class.java)
                stopService(intent)

                finish()
            }
        }

        when (mThread?.status) {
            Status.SAFE -> {
                mBinding.toolbar.root.setBackgroundResource(R.drawable.bg_toolbar_status_detail_safe)
                mBinding.toolbar.tvStatus.text = getString(R.string.safe)
                mBinding.toolbar.tvStatus.visibility = View.VISIBLE
            }

            Status.UNCERTAIN -> {
                mBinding.toolbar.root.setBackgroundResource(R.drawable.bg_toolbar_status_detail_uncertain)
                mBinding.toolbar.tvStatus.text = getString(R.string.uncertain)
                mBinding.toolbar.tvStatus.visibility = View.VISIBLE
            }
            Status.NOT_SAFE -> {
                mBinding.toolbar.root.setBackgroundResource(R.drawable.bg_toolbar_status_detail_not_safe)
                mBinding.toolbar.tvStatus.text = getString(R.string.not_safe)
                mBinding.toolbar.tvStatus.visibility = View.VISIBLE
            }
            else -> {
                //mBinding.toolbar.root.setBackgroundResource(R.drawable.ab_transparent_chatsecure)
                mBinding.toolbar.tvStatus.visibility = View.GONE
            }
        }

       // mBinding.toolbar.tvUrgent.visibility = if (mThread?.isUrgent == true) View.VISIBLE else View.GONE

        mBinding.toolbar.tvAuthorName.text = mThread?.name

        mCoroutineScope.launch {
            val defaultAvatar = ContextCompat.getDrawable(applicationContext, R.drawable.ic_avatar_marta) ?: return@launch
            withContext(Dispatchers.Main) {
                GlideUtils.loadAvatar(this@StatusDetailActivity,
                    mThread?.avatarUrl, defaultAvatar, mBinding.toolbar.ivAvatarAuthor)
            }
        }

        val event = mThread?.events?.firstOrNull()
        val ts = event?.root?.originServerTs ?: 0
        mBinding.tvMessage.text = processStatus(event?.getLastMessageBody())

        mBinding.tvTime.text = PrettyTime.format(Date(ts), this)

        //Sender's name and message time on status's map view
        mBinding.tvMapTime.text = TimeTextUtils.formatMessageInfo(Date(ts), mThread?.name)

        if (senderIsMe) {
            mBinding.ivConditions.setOnClickListener {
                showMyConditionDialog(mThread?.isUrgent ?: false)
            }
        }
        else {
            mBinding.ivConditions.visibility = View.GONE
        }

        updateStatusFromString(ts, mThread?.status, mThread?.isUrgent ?: false)

        mBinding.scrollViewContainer.fullScroll(ScrollView.FOCUS_DOWN)
    }

    private fun sendTextMessage() {
        addStatusReply(mBinding.etUpdateStatus.text)
        mBinding.etUpdateStatus.text.clear()
        mBinding.toolbar.tvStatus.requestFocus()
    }

    private var mLastStatusTs = -1L

    private fun updateStatusFromString(statusTs: Long?, status: Status?, isUrgent: Boolean) : Boolean {

        if (statusTs != null) {
            if (statusTs < mLastStatusTs)
                return false

            mLastStatusTs = statusTs
        }

        when (status) {
            Status.UNDEFINED -> {
           //     mBinding.toolbar.root.setBackgroundResource(R.drawable.bg_status_toolbar)
                mBinding.toolbar.tvStatus.text = null
            //    mBinding.toolbar.tvUrgent.visibility = if (isUrgent) View.VISIBLE else View.GONE

                return true
            }
            Status.SAFE -> {
                mBinding.toolbar.root.setBackgroundResource(R.drawable.bg_toolbar_status_detail_safe)
                mBinding.toolbar.tvStatus.text = getString(R.string.safe)
            //    mBinding.toolbar.tvUrgent.visibility = if (isUrgent) View.VISIBLE else View.GONE

                return true
            }
            Status.UNCERTAIN -> {
                mBinding.toolbar.root.setBackgroundResource(R.drawable.bg_toolbar_status_detail_uncertain)
                mBinding.toolbar.tvStatus.text = getString(R.string.uncertain)
             //   mBinding.toolbar.tvUrgent.visibility = if (isUrgent) View.VISIBLE else View.GONE

                return true
            }
            Status.NOT_SAFE -> {
                mBinding.toolbar.root.setBackgroundResource(R.drawable.bg_toolbar_status_detail_not_safe)
                mBinding.toolbar.tvStatus.text = getString(R.string.not_safe)
             //   mBinding.toolbar.tvUrgent.visibility = if (isUrgent) View.VISIBLE else View.GONE

                return true
            }
            null -> {
                showResolved()

                return false
            }
        }
    }

    private fun showResolved() {
        val joinCircleString = getString(R.string.resolve_title)
        val joinCircleMessageString = getString(R.string.resolve_message)
        val okString = getString(R.string.ok)

        InfoSheet().show(this) {
            title(joinCircleString)
            content(joinCircleMessageString)
            style(SheetStyle.BOTTOM_SHEET)
            displayCloseButton()
            onNegative (""){ }
            onPositive (okString) {
                dismiss()
                requireActivity().finish()
            }
            onClose {
            }
        }
    }
    private fun processStatus (status: String?): String {
        val pStatus = StringBuffer()

        val statusParts = status?.split(" ")

        if (statusParts != null) {
            for (statusPart in statusParts)
            {
                if (statusPart.startsWith("#"))
                {
                    if (statusPart.startsWith("#geo"))
                    {
                        mLocation = statusPart.substring(1)
                        if (mAddress != null)
                            pStatus.append(mAddress)
                        else
                        {
                            CoroutineScope(Dispatchers.Main).launch {
                                var latLon = mLocation.substring(4).split(",")
                                mAddress = getAddress(
                                    latLon.get(0).toDouble(),
                                    latLon.get(1).toDouble()
                                )
                                pStatus.append(mAddress)
                                mBinding.tvMessage.text = pStatus

                            }
                        }
                        mBinding.layoutLocation.visibility = View.GONE
                    }
                    else
                    {
                        //hide it
                    }
                }
                else if (statusPart.startsWith("geo:"))
                {
                    mLocation = statusPart
                    mBinding.layoutLocation.visibility = View.GONE

                    CoroutineScope(Dispatchers.IO).launch {

                        var latLon = mLocation.substring(4).split(",")
                        pStatus.append(
                            getAddress(
                                latLon.get(0).toDouble(),
                                latLon.get(1).toDouble()
                            )
                        )
                    }

                    return pStatus.toString().trim()
                }
                else
                {
                    pStatus.append(statusPart).append(" ")
                }
            }
        }

        return pStatus.toString().trim()
    }

    private fun getAddress(lat: Double, lng: Double): String? {
        val geocoder = Geocoder(this)
        val list = geocoder?.getFromLocation(lat, lng, 1)
        list.let {
            if (list?.size!! > 0)
                return list[0]?.getAddressLine(0)
        }

        return null
    }
    private fun showMyConditionDialog(isUrgent: Boolean) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        val binding = DialogMyConditionBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val cbUrgent = binding.checkBoxHelpNow
        cbUrgent.isChecked = isUrgent
        binding.layoutNotSafe
            .setOnClickListener {
                mBinding.toolbar.tvStatus.visibility = View.VISIBLE
                addStatusReply(Status.NOT_SAFE.value, cbUrgent.isChecked)
                dialog.dismiss()
            }

        binding.layoutUncertain
            .setOnClickListener {
                mBinding.toolbar.tvStatus.visibility = View.VISIBLE
                addStatusReply(Status.UNCERTAIN.value, cbUrgent.isChecked)

                dialog.dismiss()
            }

        binding.layoutSafe
            .setOnClickListener {
                mBinding.toolbar.tvStatus.visibility = View.VISIBLE
                addStatusReply(Status.SAFE.value, cbUrgent.isChecked)
                dialog.dismiss()
            }

        dialog.show()
    }

    private fun showMap() {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(mLocation)))
    }

    private fun addStatusReply (update : CharSequence, isUrgent: Boolean = false)
    {
        val event = mThread?.events?.firstOrNull() ?: return
        var processedText = TextPillsUtils.processSpecialSpansToHtml(update) ?: update

        if (isUrgent) {
            processedText = StringBuilder(processedText).append(" ").append(Status.URGENT_STRING).toString()
        }

        mRoom?.relationService()?.replyInThread(event.eventId,processedText, MessageType.MSGTYPE_TEXT)
    }

    private fun activateVoiceMessage()
    {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.RECORD_AUDIO),requestCodeAudio)
        }
        else {
            if (mBinding.recordView.visibility == View.GONE) {
                mBinding.recordView.visibility = View.VISIBLE
                mBinding.recordView.activity = this
                mBinding.recordView.callback = this

                //Hide Latest button to avoid UI overlapping issue
                mBinding.btnLatest.visibility = View.GONE
            }
            else {
                mBinding.recordView.visibility = View.GONE
            }
        }
    }

    override fun isReady(): Boolean {
        return true
    }

    override fun onRecordCancel() {
        stopAudioRecording(false)
    }

    override fun onRecordEnd() {
        stopAudioRecording(true)
        mBinding.recordView.visibility = View.GONE

        //Show Latest button upon recording end
        mBinding.btnLatest.visibility = View.VISIBLE
    }

    override fun onRecordStart(audio: Boolean) {

        startAudioRecording()
    }

    private val mStartAudioRecordingIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) startAudioRecording()
        }


    private fun startAudioRecording() {
        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
                Snackbar.make(mBinding.root, R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok) {
                        startAudioRecording()
                    }
                    .show()
            }
            else {
                mStartAudioRecordingIfPermitted.launch(Manifest.permission.RECORD_AUDIO)
            }
        }
        else {
            val am = getSystemService(AUDIO_SERVICE) as? AudioManager

            if (am?.mode == AudioManager.MODE_NORMAL) {
                val fileName = UUID.randomUUID().toString().substring(0, 8) + ".m4a"
                mAudioFilePath = File(filesDir, fileName)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    mMediaRecorder = MediaRecorder(this)
                }
                else {
                    @Suppress("DEPRECATION")
                    mMediaRecorder = MediaRecorder()
                }

                mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC)
                mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

                // Maybe we can modify these in the future, or allow people to tweak them.
                mMediaRecorder.setAudioChannels(1)
                mMediaRecorder.setAudioEncodingBitRate(44100 * 16)
                mMediaRecorder.setAudioSamplingRate(44100)
                mMediaRecorder.setOutputFile(mAudioFilePath.absolutePath)

                try {
                    isAudioRecording = true
                    mMediaRecorder.prepare()
                    mMediaRecorder.start()
                }
                catch (e: Exception) {
                    Timber.e(e, "couldn't start audio")
                }
            }
        }
    }

    private fun stopAudioRecording(send: Boolean) {
        if (isAudioRecording) {
            try {
                mMediaRecorder.stop()
                mMediaRecorder.reset()
                mMediaRecorder.release()

                if (send) {
                    mThread?.eventId?.let { eventId -> sendAudioStatus(eventId) }
                }
                else {
                    mAudioFilePath.delete()
                }
            }
            catch (ise: IllegalStateException) {
                Timber.w(ise, "error stopping audio recording")
            }
            catch (re: RuntimeException) {
                // Stop can fail so we should catch this here.

                Timber.w(re, "error stopping audio recording")
            }

            isAudioRecording = false
        }
    }


    private var requestCodeLocation = 9999
    private var requestCodeAudio = 9998

    private fun addLocation() {

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION),requestCodeLocation)
        }
        else {
            UpdateStatusActivity.LocationUtils()
                .getLocation(this)?.observe(this) { location: Location? ->
                    addStatusReply("#geo:${location?.latitude},${location?.longitude}")
                }
        }
    }


    private fun sendMediaAttachment(mediaUri: Uri, replyId: String) {
        sendAttachment(mediaUri, null, replyId)
    }

    private fun sendAudioStatus(replyId: String) {
        if (this::mAudioFilePath.isInitialized) {
            val uriAudio = Uri.fromFile(mAudioFilePath)
            sendAttachment(uriAudio, org.article19.circulo.next.common.utils.MimeTypes.DEFAULT_AUDIO_FILE, replyId)
        }
    }

    private fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null, replyId: String) {
        val room = mRoom ?: return

        try {
            val mimeType = fallbackMimeType
                ?: MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(contentUri.toString()))

            val attachment =
                AttachmentHelper.createFromContentUri(contentResolver, contentUri, mimeType)

            room.sendService().sendMedia(
                attachment,
                true,
                setOf(room.roomId),
                replyId,
                RelationDefaultContent(null,null, ReplyToContent(replyId))
            )

        }
        catch (e: Exception) {
            Timber.e(e, "error sending file")
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RoomActivity.REQUEST_ADD_MEDIA) {
            if (resultCode != RESULT_OK) return

            for (mediaUri in Matisse.obtainResult(data)) {
                mThread?.eventId?.let { eventId -> sendMediaAttachment(mediaUri, eventId) }
            }
        }
        else {
            // Need to keep this for the Matisse library, which didn't get an overhaul in the
            // last 1.5 years. Grml.
            @Suppress("DEPRECATION")
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun setupMentionAutoComplete(editTextSendMessage: EditText) {
        val currentRoom = mRoom ?: return
        val elevation = 6f
        val backgroundDrawable: Drawable = ColorDrawable(Color.WHITE)
        val presenter: AutocompletePresenter<RoomMemberSummary> = UserPresenter(this, currentRoom)
        val callback: AutocompleteCallback<RoomMemberSummary> = object : AutocompleteCallback<RoomMemberSummary> {
            override fun onPopupItemClicked(editable: Editable, item: RoomMemberSummary): Boolean {
                editable.clear()
                insertMatrixItem(mBinding.etUpdateStatus, editable, "@", item.toMatrixItem())
                return true
            }

            override fun onPopupVisibilityChanged(shown: Boolean) {}
        }

        Autocomplete.on<RoomMemberSummary>(editTextSendMessage)
            .with(elevation)
            .with(backgroundDrawable)
            .with(CharPolicy('@'))
            .with(presenter)
            .with(callback)
            .build()
    }

    @Suppress("SameParameterValue")
    private fun insertMatrixItem(editText: EditText, editable: Editable, firstChar: String, matrixItem: MatrixItem) {
        // Detect last firstChar and remove it.
        val startIndex = max(editable.lastIndexOf(firstChar), 0)

        // Detect next word separator.
        var endIndex = editable.indexOf(" ", startIndex)
        if (endIndex < 0) endIndex = editable.length

        // Replace the word by its completion.
        val displayName = matrixItem.displayName ?: ""

        // Adding trailing space " " or ": " if the user started mention someone.
        val displayNameSuffix = if (firstChar == "@" && startIndex == 0) ": " else " "

        editable.replace(startIndex, endIndex, "$displayName$displayNameSuffix")

        // Add the span.
        val span = PillImageSpan(editText.context, displayName, matrixItem)
        span.bind(editText)

        editable.setSpan(span, startIndex, startIndex + displayName.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    fun sendShareRequest(request: ShareRequest) {
        sendAttachment(request.media, request.mimeType)
    }

    private fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null) {
        val room = mRoom ?: return

        val mimeType = fallbackMimeType ?: MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                MimeTypeMap.getFileExtensionFromUrl(contentUri.toString()))

        val attachment = AttachmentHelper.createFromContentUri(contentResolver, contentUri, mimeType)

        room.sendService().sendMedia(attachment, true, setOf(room.roomId), null)
    }

    override fun getRootView(): View {
        return mBinding.root
    }

    override fun handleTakePicture(photoUri: Uri) {
        mThread?.eventId?.let { eventId -> sendMediaAttachment(photoUri, eventId) }
    }

    interface ReadReceiptUpdateCallback {
        fun onReadReceiptsUpdated(readReceipts: MutableList<ReadReceiptData>)
    }
}
