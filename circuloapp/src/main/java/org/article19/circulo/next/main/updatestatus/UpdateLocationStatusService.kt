package org.article19.circulo.next.main.updatestatus

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.app.job.JobInfo.PRIORITY_MIN
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.IBinder
import android.os.Looper
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.ServiceCompat
import androidx.core.app.TaskStackBuilder
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.Observer
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Granularity
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.Priority
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.CancellationTokenSource
import com.google.android.gms.tasks.OnTokenCanceledListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.CirculoApp.Companion.NOTIFICATION_CHANNEL_ID_LOCATION
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.main.MainActivity
import org.article19.circulo.next.main.now.Thread
import org.matrix.android.sdk.api.session.room.getTimelineEvent
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import java.util.Timer
import java.util.TimerTask


class UpdateLocationStatusService : LifecycleService(), Observer<List<Thread>> {

    private lateinit var mLastLocation: Location
    private lateinit var mLocationRequest: LocationRequest

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private var mThreadId: String? = null

    //5 minutes default
    private var sLocationUpdateInterval : Long = 60 * 1000 * 5

    //1 minutes default
    private var sLocationUpdateFastInterval : Long = 60 * 1000 * 1

    companion object {
        const val EXTRA_SEND_URGENT = "urgent"
        const val EXTRA_SEND_LOCATION = "location"
        const val EXTRA_TIME_TOTAL_MIN = "time"
        const val EXTRA_THREAD_ID = "eventid"
        const val EXTRA_TIME_INTERVAL_MIN = "interval"

        const val ACTION_SEND_URGENT = "org.article19.circulo.next.SEND_ALERT"
        const val ACTION_SEND_LOCATION = "org.article19.circulo.next.SEND_LOCATION"

    }

    override fun onCreate() {
        super.onCreate()

        startForeground()

        startTimelineListener()

    }

    private fun startTimelineListener () {
        SharedTimeline.instance.threads.observe(this, this)

        mThreadId = SharedTimeline.instance.getMyStatusThread
    }

    private fun startForeground() {

        val notifyIntent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val notifyPendingIntent = PendingIntent.getActivity(
            this, 0, notifyIntent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )

        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel()
            } else {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                ""
            }

        val notificationBuilder = NotificationCompat.Builder(this, channelId )
        val notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(R.drawable.ic_discuss)
            .setCategory(Notification.CATEGORY_SERVICE)
            .setContentText(getString(R.string.circulo_location_updates))
            .setContentIntent(notifyPendingIntent)
            .build()
        startForeground(101, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String{
        val channelId = NOTIFICATION_CHANNEL_ID_LOCATION
        val channelName = getString(R.string.circulo_location_updates)
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_LOW)
        chan.lightColor = Color.GREEN
        chan.importance = NotificationManager.IMPORTANCE_NONE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    override fun onDestroy() {
        super.onDestroy()
        fusedLocationProviderClient.flushLocations()
        fusedLocationProviderClient.removeLocationUpdates { mLocationCallback }
    }

    @Deprecated("Deprecated in Java")
    override fun onStart(intent: Intent?, startId: Int) {
        super.onStart(intent, startId)

        if (intent?.hasExtra(EXTRA_THREAD_ID)==true) {
            mThreadId = intent?.getStringExtra(EXTRA_THREAD_ID)
        }

        val totalTimeMin = intent?.getIntExtra(EXTRA_TIME_TOTAL_MIN, -1)

        if (intent?.hasExtra(EXTRA_TIME_INTERVAL_MIN) == true) {
            var intervalFast : Long? = 1L
            intervalFast = intent?.getLongExtra(EXTRA_TIME_INTERVAL_MIN,1L)
            if (intervalFast != null) {
                sLocationUpdateFastInterval = intervalFast * 60L * 1000L
                sLocationUpdateInterval = intervalFast * 2 * 60L * 1000L

            }
        }

        if (!this::fusedLocationProviderClient.isInitialized)
            startLocationUpdates()

        if (totalTimeMin != null && totalTimeMin > 0) {

            //run for specified time, then stop
            Timer().schedule(object : TimerTask() {
                override fun run() {

                    fusedLocationProviderClient?.removeLocationUpdates { mLocationCallback }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        stopForeground(Service.STOP_FOREGROUND_REMOVE)
                    }
                    else
                    {
                        stopForeground(true)
                    }

                    stopSelf()
                }
            }, totalTimeMin * 60 * 1000L) //wait this long in MS
        }
    }

    private fun startLocationUpdates() {

        val minMeters = 20F

        mLocationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, sLocationUpdateInterval)
            .setWaitForAccurateLocation(false)
            .setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
            .setMinUpdateIntervalMillis(10000)
            .setMinUpdateDistanceMeters(minMeters)
            .build()

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        fusedLocationProviderClient.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper()!!)


    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            locationResult.lastLocation?.let { locationChanged(it) }

        }
    }

    fun locationChanged(location: Location) {
        mLastLocation = location

        sendStatus()
    }


    private var mLastStatusText = ""
    private var mWaitingForThreadId = false

    private fun sendStatus () {

        val statusText = StringBuffer()

        mLastLocation.let {
            //Log.i(TAG,"Location: ${location.latitude}  ${location.longitude}")
            statusText.append("#geo:${it.latitude},${it.longitude}")

            mLastStatusText = statusText.toString()

            if (mThreadId == null) {
                if (!mWaitingForThreadId) {
                    SharedTimeline.instance.room?.sendService()?.sendTextMessage(mLastStatusText)
                    mWaitingForThreadId = true
                }
                else
                {
                    //keep waiting
                }
            } else {
                SharedTimeline.instance.room?.relationService()
                    ?.replyInThread(mThreadId!!, mLastStatusText, MessageType.MSGTYPE_TEXT)
            }
        }
    }

    override fun onChanged(value: List<Thread>) {

        if (mThreadId == null) {

            for (thread in value) {
                val event = thread.events.firstOrNull() ?: continue

                val eventId = event.eventId
                if (eventId.contains("local")) continue

                val mc = event.getLastMessageContent() as? MessageTextContent ?: continue

                if (mc.body == mLastStatusText) {

                    mThreadId = eventId
                    mWaitingForThreadId = false

                    break
                }
            }
        }
    }


}