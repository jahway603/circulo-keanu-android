package org.article19.circulo.next.main.updatestatus

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.media.AudioManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.stefanosiano.powerful_libraries.imageview.PowerfulImageView
import com.stefanosiano.powerful_libraries.imageview.shape.PivShapeMode
import com.stefanosiano.powerful_libraries.imageview.shape.PivShapeScaleType
import com.tougee.recorderview.AudioRecordView
import com.zhihu.matisse.Matisse
import info.guardianproject.keanu.core.ui.room.RoomActivity.Companion.REQUEST_ADD_MEDIA
import info.guardianproject.keanu.core.util.AttachmentHelper
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanuapp.ui.widgets.AudioWife
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.common.utils.MimeTypes
import org.article19.circulo.next.common.utils.dp2Px
import org.article19.circulo.next.databinding.ActivityUpdateStatusBinding
import org.article19.circulo.next.databinding.DialogMyConditionBinding
import org.article19.circulo.next.main.now.Status
import org.article19.circulo.next.main.now.Thread
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.relation.RelationDefaultContent
import org.matrix.android.sdk.api.session.room.model.relation.ReplyToContent
import org.matrix.android.sdk.api.session.room.read.ReadService
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.util.*


class UpdateStatusActivity : CommonSelectMediaActivity(), AudioRecordView.Callback,
    Observer<List<Thread>>
{
    private lateinit var mViewBinding: ActivityUpdateStatusBinding

    private lateinit var firstQuickResponseList: List<String>
    private lateinit var secondQuickResponseList: List<String>
    private lateinit var quickResponseAdapter: QuickResponseAdapter

    private lateinit var mMediaRecorder : MediaRecorder

    private var condition = Status.UNDEFINED
    private var mIsUrgent = false
    private var location : Location? = null

    private lateinit var mAudioFilePath : File
    private var isAudioRecording = false

    private var mLastStatus : String = ""

    private var mAudioMessagePlayer: AudioWife? = null
    private var mMediaUris = mutableListOf<Uri>()

    private var mAutoUrgent = false
    private var mAutoLocation = false

    companion object {
        const val STATUS_SEND_URGENT = "urgent"
        const val STATUS_SEND_LOCATION = "location"
        const val EXTRA_SEND_AUDIO = "audio_path"

        const val ACTION_SEND_URGENT = "org.article19.circulo.next.SEND_ALERT"
        const val ACTION_SEND_LOCATION = "org.article19.circulo.next.SEND_LOCATION"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (SharedTimeline.instance.containsStatusByMe)
            return

        mAutoUrgent = intent.getBooleanExtra(STATUS_SEND_URGENT, false)
        mAutoLocation = intent.getBooleanExtra(STATUS_SEND_LOCATION, false)

        if (intent.action == (ACTION_SEND_LOCATION))
            mAutoLocation = true

        if (intent.action == (ACTION_SEND_URGENT))
            mAutoUrgent = true



        mViewBinding = ActivityUpdateStatusBinding.inflate(layoutInflater)
        setContentView(mViewBinding.root)

        mViewBinding.toolbar.ivBack.setOnClickListener {
            finish()
        }

        mViewBinding.ivChooseLocation.setOnClickListener {

            addLocation()
        }

        mViewBinding.ivConditions.setOnClickListener {
            showMyConditionDialog()
        }

        mViewBinding.ivVoiceMsg.setOnClickListener {

           activateVoiceMessage()
        }

        mViewBinding.ivAddMedia.setOnClickListener {
            showMediaChooserPopUp(mViewBinding.ivAddMedia, mViewBinding.layoutBottomControlBoard)
        }


        mViewBinding.toolbar.tvSendToCircle.setOnClickListener {
            sendStatus()
        }

        val recyclerViewQuickResponse = mViewBinding.recyclerViewSuggestedKeywords
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewQuickResponse.layoutManager = layoutManager

        firstQuickResponseList = ArrayList(resources.getStringArray(R.array.quick_response_1).toMutableList())
        quickResponseAdapter = QuickResponseAdapter(firstQuickResponseList)
        recyclerViewQuickResponse.adapter = quickResponseAdapter



        SharedTimeline.instance.threads.observe(this, this)

        if (intent.hasExtra(EXTRA_SEND_AUDIO))
        {
            val fileAudioPath = intent.getStringExtra(EXTRA_SEND_AUDIO)
            fileAudioPath.let {
                mAudioFilePath = File(fileAudioPath)
            }
        }

        if (mAutoUrgent)
        {
            //automatically set urgent status and close
            setCondition(Status.NOT_SAFE,true)
            addLocation()
        }
        else if (mAutoLocation)
        {
            setCondition(Status.UNCERTAIN,false)
            addLocation()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        //Stop playing audio record message
        mAudioMessagePlayer?.pause()
        mAudioMessagePlayer?.release()

        SharedTimeline.instance.threads.removeObservers(this)
    }

    override fun onChanged(value: List<Thread>) {
        if (mLastStatus.isEmpty()) return

        for (thread in value) {
            val event = thread.events.firstOrNull() ?: continue

            val eventId = event.eventId
            if (eventId.contains("local")) continue

            val mc = event.getLastMessageContent() as? MessageTextContent ?: continue

            if (mc.body == mLastStatus) {
                sendMediaAttachment(eventId)
                sendAudioStatus(eventId)

                SharedTimeline.instance.threads.removeObservers(this)

                if (mAutoUrgent)
                {
                    val intentService = Intent(this, UpdateLocationStatusService::class.java)
                    intentService.putExtra(UpdateLocationStatusService.EXTRA_THREAD_ID,eventId)
                    intentService.putExtra(UpdateLocationStatusService.EXTRA_TIME_TOTAL_MIN,-1)
                    intentService.putExtra(UpdateLocationStatusService.EXTRA_SEND_LOCATION,true)
                    intentService.putExtra(UpdateLocationStatusService.EXTRA_TIME_INTERVAL_MIN,1L)

                    ContextCompat.startForegroundService(this,intentService)
                }

                finish()

                break
            }
        }
    }

    private fun showMyConditionDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        val dialogViewBinding: DialogMyConditionBinding = DialogMyConditionBinding.inflate(layoutInflater)
        dialog.setContentView(dialogViewBinding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialogViewBinding.layoutNotSafe
            .setOnClickListener {
                dialog.dismiss()
                setCondition (Status.NOT_SAFE, dialogViewBinding.checkBoxHelpNow.isChecked)
            }

        dialogViewBinding.layoutUncertain
            .setOnClickListener {
                dialog.dismiss()
                setCondition (Status.UNCERTAIN, dialogViewBinding.checkBoxHelpNow.isChecked)
            }

        dialogViewBinding.layoutSafe
            .setOnClickListener {
                dialog.dismiss()
                setCondition (Status.SAFE, dialogViewBinding.checkBoxHelpNow.isChecked)
            }

        dialog.show()

    }

    private fun appendText (appendStatus: String) {
        mViewBinding.editTextStatus.text.append("$appendStatus ")

    }

    fun updateQuickResponseList(selectedResponse: String) {
        if (firstQuickResponseList.contains(selectedResponse)) {
            secondQuickResponseList = ArrayList(resources.getStringArray(R.array.quick_response_2).toMutableList())
            quickResponseAdapter.replaceData(secondQuickResponseList)
        }

        appendText("$selectedResponse ")

    }

    private fun activateVoiceMessage() {

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.RECORD_AUDIO),requestCodeAudio)
        }
        else {
            val arView = mViewBinding.recordView
            arView.visibility = View.VISIBLE
            arView.activity = this
            arView.callback = this
        }
    }


    private var requestCodeLocation = 9999
    private var requestCodeAudio = 9998

    private fun addLocation() {

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION),requestCodeLocation)

        }
        else {
            LocationUtils().getLocation(this)?.observe(this) { loc: Location? ->
                location = loc
                mViewBinding.layoutLocation.visibility = View.VISIBLE

                if (mAutoUrgent || mAutoLocation) {
                    //once location sends, then auto send
                    sendStatus()

                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            //if location granted, then add location
            requestCodeLocation -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    addLocation()
                } else {

                }
                return
            }
        }
    }


    private fun setCondition(newCondition : Status, newUrgent: Boolean)
    {
        condition = newCondition
        mIsUrgent = newUrgent

        val tvStatus = mViewBinding.tvStatus

        tvStatus.visibility = View.VISIBLE

        when (condition) {
            Status.NOT_SAFE -> {
                tvStatus.setText(R.string.status_set_to_not_safe)
                tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_status_not_safe_round,
                    0, 0, 0)
            }
            Status.UNCERTAIN -> {
                tvStatus.setText(R.string.status_set_to_uncertain)
                tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_status_uncertain_round,
                    0, 0, 0)
            }
            Status.SAFE -> {
                tvStatus.setText(R.string.status_set_to_safe)
                tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_status_safe_round,
                    0, 0, 0)
            }

            else -> {}
        }
    }

    private fun sendStatus() {

        val statusText = StringBuffer()

        statusText.append(mViewBinding.editTextStatus.text.toString())

        if (statusText.isEmpty())
            statusText.append(" ")

        val app = application as? CirculoApp

        location?.let {
            //Log.i(TAG,"Location: ${location.latitude}  ${location.longitude}")
            statusText.append(" ").append("#geo:${it.latitude},${it.longitude}")

            app?.cleanInsightsManager?.measureEvent("location")
        }

        if (condition != Status.UNDEFINED)
        {
            var statusString = ""

            when (condition) {
                Status.NOT_SAFE -> statusString = (Status.NOT_SAFE.value)
                Status.UNCERTAIN -> statusString = (Status.UNCERTAIN.value)
                Status.SAFE -> statusString = (Status.SAFE.value)
                else -> {}
            }

            statusText.append(" ").append(statusString)

            app?.cleanInsightsManager?.measureEvent("condition", statusString, 0.0)

            if (app?.isNetworkAvailable() == false) {
                app.cleanInsightsManager.measureEvent("offline-send")
            }
        }

        if (mIsUrgent)
            statusText.append(" ").append(Status.URGENT_STRING)

        if (statusText.isNotEmpty()) {
            SharedTimeline.instance.room?.sendService()?.sendTextMessage(statusText)

            mLastStatus = statusText.toString()
        }

        // If no attachments, leave immediately, otherwise wait for callback to
        // send attachments as replies.
        if (!this::mAudioFilePath.isInitialized && mMediaUris.isEmpty()) {
            finish()
        }
    }

    class LocationUtils {

        private var fusedLocationProviderClient: FusedLocationProviderClient ?= null
        private var location : MutableLiveData<Location> = MutableLiveData()

        // using singleton pattern to get the locationProviderClient
        private fun getInstance(appContext: Context): FusedLocationProviderClient{
            if(fusedLocationProviderClient == null)
                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(appContext)
            return fusedLocationProviderClient!!
        }

        fun getLocation(context: Context) : LiveData<Location>? {

            val client = getInstance(context)

            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null
            }
            client.lastLocation
                .addOnSuccessListener { loc: Location? ->
                    loc?.let { location.value = it }
                }

            return location
        }
    }

    override fun isReady(): Boolean {
        return true
    }

    override fun onRecordCancel() {
        stopAudioRecording(false)
    }

    override fun onRecordEnd() {
        stopAudioRecording(true)
        inflateAudioMessageView()
    }

    override fun onRecordStart(audio: Boolean) {

        startAudioRecording()
    }

    private val mStartAudioRecordingIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) startAudioRecording()
        }


    private fun startAudioRecording() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
                Snackbar.make(mViewBinding.updateroot, R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok) {
                        startAudioRecording()
                    }
                    .show()
            }
            else {
                mStartAudioRecordingIfPermitted.launch(Manifest.permission.RECORD_AUDIO)
            }
        }
        else {
            val am = getSystemService(AUDIO_SERVICE) as? AudioManager

            if (am?.mode == AudioManager.MODE_NORMAL) {
                val fileName = UUID.randomUUID().toString().substring(0, 8) + ".m4a"
                mAudioFilePath = File(filesDir, fileName)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    mMediaRecorder = MediaRecorder(this)
                }
                else {
                    @Suppress("DEPRECATION")
                    mMediaRecorder = MediaRecorder()
                }

                mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC)
                mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

                // Maybe we can modify these in the future, or allow people to tweak them.
                mMediaRecorder.setAudioChannels(1)
                mMediaRecorder.setAudioEncodingBitRate(44100 * 16)
                mMediaRecorder.setAudioSamplingRate(44100)
                mMediaRecorder.setOutputFile(mAudioFilePath.absolutePath)

                try {
                    isAudioRecording = true
                    mMediaRecorder.prepare()
                    mMediaRecorder.start()
                } catch (e: Exception) {
                   Timber.e(e, "couldn't start audio")
                }
            }
        }
    }

    private fun stopAudioRecording(send: Boolean) {
        if (isAudioRecording) {
            try {
                mMediaRecorder.stop()
                mMediaRecorder.reset()
                mMediaRecorder.release()

                if (!send) {
                    mAudioFilePath.delete()
                }
            }
            catch (ise: IllegalStateException) {
                Timber.w(ise, "error stopping audio recording")
            }
            catch (re: RuntimeException) {
                // Stop can fail so we should catch this here.

                Timber.w(re, "error stopping audio recording")
            }

            isAudioRecording = false
        }
    }

    /**
     * Display audio recorded message data in voice message view
     */
    private fun inflateAudioMessageView () {
        if (this::mAudioFilePath.isInitialized) {

            try {
                val layoutVoiceMessage = mViewBinding.layoutVoiceMessage
                layoutVoiceMessage.visibility = View.VISIBLE

                val uriAudio = Uri.fromFile(mAudioFilePath)

                var mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(uriAudio.toString())
                )
                if (mimeType == null) {
                    mimeType = MimeTypes.DEFAULT_AUDIO_FILE
                }

                mAudioMessagePlayer = AudioWife()
                mAudioMessagePlayer?.init(this@UpdateStatusActivity, uriAudio, mimeType)
                    ?.useDefaultUi(layoutVoiceMessage, LayoutInflater.from(this@UpdateStatusActivity))
            }
            catch (exception: IOException) {
                Timber.e(exception)
            }
        }
    }

    private fun sendMediaAttachment (replyId: String) {
        for (uri in mMediaUris) {
            sendAttachment(uri, null, replyId)
        }
    }

    private fun sendAudioStatus (replyId: String) {
        if (this::mAudioFilePath.isInitialized) {
            val uriAudio = Uri.fromFile(mAudioFilePath)
            sendAttachment(uriAudio, MimeTypes.DEFAULT_AUDIO_FILE, replyId)
        }
    }

    private fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null, replyId: String) {
        try {
            var mimeType = fallbackMimeType
            if (mimeType == null) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(contentUri.toString())
                )
            }

            val attachment =
                AttachmentHelper.createFromContentUri(contentResolver, contentUri, mimeType)

            //    public abstract fun sendMedia(attachment: org.matrix.android.sdk.api.session.content.ContentAttachmentData,
            //    compressBeforeSending: kotlin.Boolean,
            //    roomIds: kotlin.collections.Set<kotlin.String>,
            //    rootThreadEventId: kotlin.String? /* = compiled code */,
            //    relatesTo: org.matrix.android.sdk.api.session.room.model.relation.RelationDefaultContent? /* = compiled code */,
            //    additionalContent: org.matrix.android.sdk.api.session.events.model.Content? /* = kotlin.collections.Map<kotlin.String, @kotlin.jvm.JvmSuppressWildcards kotlin.Any>? */ /* = compiled code */): org.matrix.android.sdk.api.util.Cancelable

            val room = SharedTimeline.instance.room

            room?.sendService()?.sendMedia(
                attachment,
                true,
                setOf(room.roomId),
                replyId,
                RelationDefaultContent(null,null, ReplyToContent(replyId))
            )

        } catch (e: Exception) {
            Timber.e(e, "error sending file")
        }
    }

    override fun getRootView(): View {
        return mViewBinding.root
    }

    override fun handleTakePicture(photoUri: Uri) {
        addMediaThumbView(photoUri)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_ADD_MEDIA) {
            if (resultCode != RESULT_OK) return

            for (mediaUri in Matisse.obtainResult(data)) {
                addMediaThumbView(mediaUri)
            }
        }
        else {
            // Need to keep this for the Matisse library, which didn't get an overhaul in the
            // last 1.5 years. Grml.
            @Suppress("DEPRECATION")
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun addMediaThumbView(uri: Uri) {
        val thumbLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 200.dp2Px()).apply {
            setMargins(0, 10.dp2Px(), 0, 0)
        }
        val ivThumb = PowerfulImageView(this).apply {
            isFocusableInTouchMode = true
            scaleType = ImageView.ScaleType.CENTER_CROP
            setShapeMode(PivShapeMode.ROUNDED_RECTANGLE)
            setPivShapeRadiusX(resources.getInteger(R.integer.media_thumb_shape_radius) * 1f)
            setPivShapeRadiusY(resources.getInteger(R.integer.media_thumb_shape_radius) * 1f)
            setPivShapeScaleType(PivShapeScaleType.CENTER_CROP)
            layoutParams = thumbLayoutParams
        }

        mViewBinding.mediaThumbnailContainer.addView(ivThumb)
        mViewBinding.mediaThumbnailContainer.visibility = View.VISIBLE

        GlideUtils.loadImageFromUri(this, uri, ivThumb, false)
        mMediaUris.add(uri)

    }
}
