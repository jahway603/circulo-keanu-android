package org.article19.circulo.next.matrix

import org.article19.circulo.next.main.now.Status
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent

fun TimelineEvent.getEventStatus(): Status {
    val messageContent = this.getLastMessageContent()
    var messageText = ""

    if (messageContent is MessageTextContent) {
        messageText = messageContent.body
    } else if (messageContent is MessageWithAttachmentContent) {
        messageText = messageContent.getFileName()
    }

    return when {
        messageText.contains(Status.SAFE.value) -> Status.SAFE
        messageText.contains(Status.NOT_SAFE.value) -> Status.NOT_SAFE
        messageText.contains(Status.UNCERTAIN.value) -> Status.UNCERTAIN
        else -> Status.UNDEFINED
    }
}

fun TimelineEvent.isUrgent(): Boolean {
    this.getLastMessageContent()?.body?.let {
        return it.contains(Status.URGENT_STRING)
    } ?: return false
}

fun TimelineEvent.isLocal(): Boolean {
    return this.eventId.contains("local")
}
