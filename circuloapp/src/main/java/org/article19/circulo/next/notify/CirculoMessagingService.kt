package org.article19.circulo.next.notify

import android.content.pm.PackageManager
import android.os.Build
import androidx.core.os.ConfigurationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.extensions.getBestName
import info.guardianproject.keanu.core.ui.mention.TextPillsUtils
import info.guardianproject.keanuapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.CirculoApp
import org.jsoup.Jsoup
import org.matrix.android.sdk.api.extensions.tryOrNull
import org.matrix.android.sdk.api.session.getRoom
import org.matrix.android.sdk.api.session.getUserOrDefault
import org.matrix.android.sdk.api.session.pushers.HttpPusher
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.session.room.sender.SenderInfo
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.util.toMatrixItem
import timber.log.Timber
import java.util.Date

class CirculoMessagingService: FirebaseMessagingService() {

    private val session
        get() = ImApp.sImApp?.matrixSession

    private val mStatusBarNotifier by lazy { CirculoApp.getInstance().getStatusBarNotifier() }

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO/* + SnackbarExceptionHandler(mBinding.root)*/)
    }

    override fun onNewToken(token: String) {
        val session = session ?: return
        val deviceId = session.sessionParams.deviceId ?: return
        val locale = ConfigurationCompat.getLocales(resources.configuration)[0] ?: return

        val pm = packageManager

        val appInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            pm.getApplicationInfo(packageName, PackageManager.ApplicationInfoFlags.of(0))
        }
        else {
            @Suppress("DEPRECATION")
            pm.getApplicationInfo(packageName, 0)
        }

        val pusher = HttpPusher(
            pushkey = token,
            appId = packageName,
            profileTag = "mobile_${session.myUserId.hashCode()}",
            lang = locale.language,
            appDisplayName = pm.getApplicationLabel(appInfo).toString(),
            deviceDisplayName = deviceId,
            url = "https://${getString(R.string.default_server)}/_matrix/push/v1/notify",
            enabled = true,
            deviceId = deviceId,
            append = false,
            withEventIdOnly = true)

        session.pushersService().enqueueAddHttpPusher(pusher)

    }

    override fun onMessageReceived(message: RemoteMessage) {
        val roomId = message.data["room_id"] ?: return
        val eventId = message.data["event_id"] ?: return
        val room = roomId.let { session?.roomService()?.getRoom(it) }

        val timelineEvent = room?.timelineService()?.getTimelineEvent(eventId)

        if (timelineEvent == null) {
            mCoroutineScope.launch {
                val currentSession = session ?: return@launch
                val event = tryOrNull { currentSession.eventService().getEvent(roomId, eventId) } ?: return@launch

                event.senderId?.let { senderId ->

                    val room = currentSession.getRoom(roomId)
                    //Querying room membership
                    val builder = RoomMemberQueryParams.Builder()
                    builder.memberships = arrayListOf(Membership.JOIN, Membership.INVITE)
                    val query = builder.build()

                    val senderDisplayName = room?.membershipService()?.getRoomMembers(query)?.firstOrNull { it.userId == senderId}?.displayName

                    val user = session.getUserOrDefault(senderId)
                    val newTimelineEvent = TimelineEvent(
                        root = event,
                        localId = -1,
                        eventId = eventId,
                        displayIndex = 0,
                        senderInfo = SenderInfo(
                            userId = user.userId,
                            displayName = senderDisplayName ?: user.toMatrixItem().getBestName(),
                            isUniqueDisplayName = true,
                            avatarUrl = user.avatarUrl
                        )
                    )
                    CoroutineScope(Dispatchers.Main).launch {
                        mStatusBarNotifier?.notifyChat(newTimelineEvent)
                    }
                }
            }
            return
        } else {
            val preferenceProvider = ImApp.sImApp?.preferenceProvider

            if (preferenceProvider?.isNotificationEnable() == true) {
                val hasMention = isMyselfMentioned(timelineEvent)
                if (preferenceProvider.getNotificationState() == RoomNotificationState.ALL_MESSAGES ||
                    preferenceProvider.getNotificationState() == RoomNotificationState.MENTIONS_ONLY && hasMention
                ) {
                    if (timelineEvent.root.originServerTs!! > Date().time) {
                        CoroutineScope(Dispatchers.Main).launch {
                            mStatusBarNotifier?.notifyChat(timelineEvent)
                        }
                    }
                }

            }
        }


        // TODO: Is this already enough? Isn't session already started and started syncing?
        //   Wait for ops to set up FCM / Sygnal push service.
        super.onMessageReceived(message)
    }

    private fun isMyselfMentioned(tEvent: TimelineEvent): Boolean {

        try {
            val messageContent = tEvent.getLastMessageContent()
            if (messageContent is MessageTextContent && messageContent.formattedBody != null
                && messageContent.formattedBody?.contains("mx-reply") == true
            ) {
                // have reply content
                messageContent.formattedBody?.let {

                    val document = Jsoup.parse(it)
                    val mentionPart = document.select(TextPillsUtils.MENTION_TAG).last()

                    val userIdLink = mentionPart?.attributes()?.get("href") ?: ""
                    val idStartIndex = userIdLink.lastIndexOf("@")
                    val mentionedUserId =
                        if (idStartIndex >= 0 && idStartIndex < userIdLink.length) {
                            userIdLink.substring(idStartIndex)
                        } else ""

                    return mentionedUserId == session?.myUserId
                }
            }
        } catch (exception: Exception) {
            Timber.tag("RemoteIMService").e(exception.toString())
        }

        return false
    }
}