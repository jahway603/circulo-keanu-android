package org.article19.circulo.next.notify

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import androidx.core.app.NotificationCompat
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanu.core.util.extensions.displayNameWorkaround
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.CirculoRouter
import org.article19.circulo.next.R
import org.article19.circulo.next.common.providers.PreferenceProvider
import org.article19.circulo.next.main.now.Status
import org.article19.circulo.next.main.now.Thread
import org.article19.circulo.next.main.now.ThreadsAdapter
import org.article19.circulo.next.matrix.getEventStatus
import org.article19.circulo.next.matrix.isUrgent
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.model.message.MessageRelationContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getEditedEventId


class StatusBarNotifier(private val mContext: Context) {

    companion object {
        val ALERT_VIBRATE_PATTERN = longArrayOf(0, 2000, 1000, 2000,1000, 2000,1000)

        private var UNIQUE_INT_PER_CALL = 10000

        private val ALLOWED_NOTIFICATION_TYPES = listOf(
            EventType.MESSAGE,
            EventType.STICKER,
            EventType.REACTION,
            EventType.REDACTION)
    }

    private val mApp
        get() = ImApp.sImApp

    private val mSession
        get() = mApp?.matrixSession

    private val mNotificationManager: NotificationManager? = mContext
            .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager

    private val mNotificationInfos = ArrayList<NotificationInfo>()

    private var mVibrator: Vibrator? = null

    private var mVibeEffect: VibrationEffect? = null


    fun notifyChat(te: TimelineEvent) {
        // Ignore, when type is not whitelisted.
        val clearType = te.root.getClearType()
        if (!ALLOWED_NOTIFICATION_TYPES.contains(clearType)) return

        val roomId = te.roomId

        val statusEventId = te.root.content.toModel<MessageRelationContent>()?.relatesTo?.inReplyTo?.eventId ?: ""

        val currentRoom = mSession?.roomService()?.getRoom(roomId)
        val statusRootEvent = currentRoom?.timelineService()?.getTimelineEvent(statusEventId)

        val statusAuthorName = mApp?.matrixSession?.userService()?.getUser(statusRootEvent?.senderInfo?.userId ?: "")?.displayName

        val thread = Thread(
            roomId = te.roomId,
            eventId = statusEventId,
            userId = te.senderInfo.userId,
            name = statusAuthorName ?: te.senderInfo.disambiguatedDisplayName,
            status = te.getEventStatus(),
            isUrgent = te.isUrgent())

        // This will only be a REDACTION type, if the message was actually deleted.
        // In that case, we remove the old message for that event ID.
        // If it was a change, the type will be of the original event (e.g. MESSAGE)
        // and, since we're using the roomId as notification ID, will automatically be
        // updated later on.
        if (clearType == EventType.REDACTION) {
            val notification = mNotificationInfos.firstOrNull { it.id == roomId } ?: return

            // To be honest: The fallbacks are probably wrong, but better than nothing, maybe?
            notification.removeMessage(te.root.redacts ?: te.getEditedEventId() ?: te.eventId)

            // If that was the last message, cancel the whole notification.
            if (notification.isEmpty()) {
                return cancelNotification(roomId)
            }

            // Update the changed notification.
            return notify(notification)
        }

        // Ignore when already notified.
        if (mNotificationInfos.any { it.containsMessage(te.eventId) }) {
            return
        }

        if (!Preferences.isNotificationEnabled()) {
          //  Timber.d("Notification for room $te.roomId is not enabled.")

            return cancelNotification(roomId)
        }

        /**
        // Ignore when room in foreground.
        if (mApp?.isRoomInForeground(roomId) == true) {
            return cancelNotification(roomId)
        }**/

        // Ignore, when our user already read this.
        if (te.readReceipts.firstOrNull { it.roomMember.userId == mSession?.myUserId } != null) {
            return cancelNotification(roomId)
        }

        val roomSummary = mSession?.roomService()?.getRoomSummary(roomId) ?: let {
            return cancelNotification(roomId)
        }

        val msg = te.getStatusSummary(mContext)

        val isUrgent = te.isUrgent() || te.getEventStatus() == Status.NOT_SAFE

        if (Preferences.hasNotificationFilters()) {

            if (Preferences.getNotificationFilters().contains(msg)) {
                return
            }
        }

        val intent = getDefaultIntent(roomId, thread)
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(KeanuConstants.IMPS_CATEGORY)

        val icon = if (isUrgent) R.drawable.ic_alert else R.drawable.ic_discuss
        val largeIcon = BitmapFactory.decodeResource(mApp?.resources,R.drawable.ic_launcher_circulo_foreground)

        notify(roomId, roomSummary.displayNameWorkaround, te.getEditedEventId() ?: te.eventId,
            msg, intent, icon, largeIcon, msg, isUrgent)
    }

/**
    fun notifyNewDevice(device: CryptoDeviceInfo) {
        // Don't notify multiple times about the same device!
        if (mNotificationInfos.any { it.id == device.deviceId }) return

        val intent = mApp?.router?.newDevice(mContext, device.userId, device.deviceId)

        val title = mContext.getString(R.string.app_name)
        val message = mContext.getString(R.string._logged_into_your_account_Please_confirm_it_was_you_, device.displayName() ?: device.deviceId)

        notify(device.deviceId, title, device.deviceId, message, intent, R.drawable.ic_devices)
    }**/

private fun cancelNotification(id: String) {
        val notification = mNotificationInfos.firstOrNull { it.id == id } ?: return

        mNotificationManager?.cancel(notification.idInt)
        mNotificationInfos.remove(notification)
        mVibrator?.cancel()

    }

    private fun notify(id: String, title: String, messageId: String,
                       message: String, intent: Intent?, icon: Int, largeIcon: Bitmap? = null,
                       tickerText: String? = null, isAlert: Boolean = true)
    {
        intent?.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP

        var info: NotificationInfo? = null

        synchronized(mNotificationInfos) {
            info = mNotificationInfos.firstOrNull { it.id == id }

            if (info != null) {
                // Need to remove, as it gets added later again!
                mNotificationInfos.remove(info)
            }
            else {
                info = NotificationInfo(id, icon = icon, isAlert = isAlert)
            }

            info?.title = title
            info?.intent = intent
            info?.icon = icon
            info?.largeIcon = largeIcon
            info?.tickerText = tickerText
            info?.doVibrateSound = true
            info?.isAlert = isAlert

            info?.putMessage(messageId, message)

            info?.let {
                mNotificationInfos.add(it)
            }
        }

        info?.let { notify(it) }
    }

    private fun notify(notificationInfo: NotificationInfo)
    {
        mNotificationManager?.notify(notificationInfo.idInt, notificationInfo.build())
    }

    @JvmOverloads
    fun getDefaultIntent(roomId: String? = null, thread: Thread? = null): Intent {
        val i = CirculoRouter.instance.main(mContext, openRoomId = roomId)
        i.putExtra(ThreadsAdapter.BUNDLE_EXTRA_THREAD, thread)

        return i
    }

    internal inner class Message(
        var id: String,

        var text: String
    )

    internal inner class NotificationInfo(
        var id: String? = null,

        var title: String? = null,

        var intent: Intent? = null,

        var icon: Int,

        var largeIcon: Bitmap? = null,

        var tickerText: String? = null,

        var doVibrateSound: Boolean = true,

        var isAlert: Boolean = false
    ) {

        val idInt
            get() = id.hashCode()

        private val mMessages = arrayListOf<Message>()

        fun containsMessage (checkMsgId: String) : Boolean {
            for (message in mMessages)
            {
                if (message.id == checkMsgId)
                    return true
            }

            return false
        }

        fun putMessage(id: String, text: String) {
            val m = mMessages.firstOrNull { it.id == id }

            if (m != null) {
                m.text = text
            }
            else {
                mMessages.add(Message(id, text))
            }
        }

        fun removeMessage(id: String) {
            mMessages.removeAll { it.id == id }
        }

        fun isEmpty(): Boolean {
            return mMessages.isEmpty()
        }

        @SuppressLint("UnspecifiedImmutableFlag")
        fun build(): Notification
        {
            val shouldVibrate = if (isAlert) {
                PreferenceProvider.notificationVibrateUrgent
            } else {
                Preferences.getNotificationVibrate()
            }

            val channelId = if (shouldVibrate) {
                CirculoApp.NOTIFICATION_CHANNEL_ID_ALERT
            }
            else {
                CirculoApp.NOTIFICATION_CHANNEL_ID_ALERT_NO_VIBRATE
            }

            val builder = NotificationCompat.Builder(mContext, channelId)

            builder
                .setSmallIcon(icon)
                .setTicker(tickerText)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(title)
                .setContentText(mMessages.lastOrNull()?.text)
                .setAutoCancel(true)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                builder.setContentIntent(PendingIntent.getActivity(mContext, UNIQUE_INT_PER_CALL++,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE))
            }
            else {
                builder.setContentIntent(PendingIntent.getActivity(mContext, UNIQUE_INT_PER_CALL++,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT))
            }

            builder.setStyle(NotificationCompat.BigTextStyle()
                    .bigText(mMessages.lastOrNull()?.text))

                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setVibrate(ALERT_VIBRATE_PATTERN)



                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    builder.color = mContext.getColor(R.color.circulo_alert_notification)
                }


            if (isAlert) {

                var notificationRingtoneUri= "android.resource://" + mContext.packageName.toString() + "/raw/alert1"

                if (isAlert) {
                    notificationRingtoneUri = "android.resource://" + mContext.packageName.toString() + "/raw/alert2"
                }
                else {
                    if (Preferences.getNotificationSound()) {
                        notificationRingtoneUri =
                            Preferences.getNotificationRingtoneUri().toString()
                    }
                }

                try {
                    // Play sound
                    mContext.startService(mApp?.router?.soundService(mContext, notificationRingtoneUri))
                }
                catch (e: IllegalStateException) {
                    // Error: can't start services in the background.
                    e.printStackTrace()
                }

                    // Play vibration
                    if (mVibrator == null) {
                        mVibrator = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                            (mContext.getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as? VibratorManager)?.defaultVibrator
                        }
                        else {
                            @Suppress("DEPRECATION")
                            mContext.getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            mVibeEffect = VibrationEffect.createWaveform(ALERT_VIBRATE_PATTERN, 0)
                        }
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        mVibrator?.vibrate(mVibeEffect)
                    }
                    else {
                        @Suppress("DEPRECATION")
                        mVibrator?.vibrate(ALERT_VIBRATE_PATTERN, 0)
                    }

                // wait 1 second and then cancel the vibrator
                // wait 1 second and then cancel the vibrator

            }

            if (mMessages.isNotEmpty()) {
                // Sets the big view "big text" style and supplies the
                // text (the user's reminder message) that will be displayed
                // in the detail area of the expanded notification
                // These calls are ignored by the support library for
                // pre-4.1 devices.
                builder.setStyle(NotificationCompat.BigTextStyle()
                        .bigText(mMessages.joinToString(separator = "\n") { it.text }))
            }

            if (largeIcon != null) builder.setLargeIcon(largeIcon)

            return builder.build()
        }
    }

    fun stopVibration () {
        mVibrator?.cancel()
    }
}
