package org.article19.circulo.next.notify

import android.content.Context
import info.guardianproject.keanuapp.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.main.now.Status
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.model.message.MessageAudioContent
import org.matrix.android.sdk.api.session.room.model.message.MessageImageContent
import org.matrix.android.sdk.api.session.room.model.message.MessageStickerContent
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.message.MessageVideoContent
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.relation.ReactionContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.session.room.timeline.isReply

fun TimelineEvent.getStatusSummary(context: Context): String {
    val name = senderInfo.displayName ?: senderInfo.disambiguatedDisplayName
    val mc = getLastMessageContent()

    when {
        mc is MessageImageContent -> return context.getString(R.string._sent_an_image, name)

        mc is MessageAudioContent -> return context.getString(R.string._sent_audio, name)

        mc is MessageVideoContent -> return context.getString(R.string._sent_a_video, name)

        mc is MessageStickerContent -> return context.getString(R.string._sent_a_sticker_, name, mc.body)

        mc is MessageWithAttachmentContent -> return context.getString(R.string._sent_a_file, name)

        mc?.body?.isNotEmpty() == true -> {

            var msgBody = mc.body

            SharedTimeline.getFormattedMessage(mc)?.let {
                msgBody = it.reply
            }

            var statusUpdate = context.getString(org.article19.circulo.next.R.string.status_notification_new)

            if (isReply())
            {
                statusUpdate = context.getString(org.article19.circulo.next.R.string.status_notification_updated)

                if (msgBody.contains(Status.RESOLVED_STRING))
                {
                    statusUpdate = context.getString(org.article19.circulo.next.R.string.status_notification_resolved)
                }
                else if (msgBody.contains(Status.GEO_STRING))
                {
                    statusUpdate = context.getString(org.article19.circulo.next.R.string.status_notification_geo)
                }
            }

            if (isUrgent()) {
                val urgent = context.getString(org.article19.circulo.next.R.string.content_description_urgent).uppercase()
                return "$urgent: $name $statusUpdate"
            }
            else
                return "$name $statusUpdate"

        }

        mc == null -> {
            val reaction = root.getClearContent().toModel<ReactionContent>()

            if (reaction != null) {
                return context.getString(
                    R.string._reacted_with_, name,
                    reaction.relatesTo?.key ?: context.getString(R.string.unknown))
            }
        }
    }

    return context.getString(R.string._did_something, name)
}


fun TimelineEvent.getLastMessageBody(): String {

    val mc = getLastMessageContent()
    val message = mc?.body

    var statusText = message ?: ""

    // have reply content
    SharedTimeline.getFormattedMessage(mc)?.let {
        if (it.sender.isNotEmpty()) statusText = it.reply
    }

    return statusText
}

fun TimelineEvent.isUrgent(): Boolean {
    val messageContent = this.getLastMessageContent()
    if (messageContent is MessageTextContent) {
        return messageContent.body.contains(Status.URGENT_STRING)
    }

    return false
}