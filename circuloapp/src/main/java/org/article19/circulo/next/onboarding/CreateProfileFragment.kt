package org.article19.circulo.next.onboarding

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import info.guardianproject.keanu.core.ui.auth.AuthenticationActivity
import info.guardianproject.keanu.core.ui.auth.RecaptchaActivity
import info.guardianproject.keanu.core.ui.auth.TermsActivityArgument
import info.guardianproject.keanu.core.ui.auth.toLocalizedLoginTerms
import info.guardianproject.keanu.core.ui.onboarding.OnboardingAccount
import info.guardianproject.keanu.core.util.extensions.betterMessage
import info.guardianproject.keanu.core.verification.VerificationManager
import kotlinx.coroutines.launch
import org.article19.circulo.next.Config
import org.article19.circulo.next.R
import org.article19.circulo.next.main.MainActivity
import org.article19.circulo.next.profile.BaseProfileFragment
import org.matrix.android.sdk.api.auth.AuthenticationService
import org.matrix.android.sdk.api.auth.data.HomeServerConnectionConfig
import org.matrix.android.sdk.api.auth.data.LoginFlowResult
import org.matrix.android.sdk.api.auth.registration.RegistrationResult
import org.matrix.android.sdk.api.auth.registration.RegistrationWizard
import org.matrix.android.sdk.api.auth.registration.Stage
import java.util.Locale
import java.util.UUID

class CreateProfileFragment: BaseProfileFragment() {


    private val mAuthService: AuthenticationService?
        get() = app?.matrix?.authenticationService()

    private val mRegistrationWizard: RegistrationWizard?
        get() = app?.matrix?.authenticationService()?.getRegistrationWizard()

    private lateinit var mDisplayName : String

    private var mOnboardingAccount: OnboardingAccount? = null

    private val mDeviceName: String
        get() {
            return "Circulo2"
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val baseView = super.onCreateView(inflater, container, savedInstanceState)

        context?.let {
            tvActionDone.setTextColor(ContextCompat.getColor(it, R.color.holo_grey_dark))
        }
        tvActionDone.setOnClickListener {
            tvActionDone.text = getString(R.string.updating)
            mDisplayName = editTextName.text.toString() //need to turn this into a circulo-ABC123 type id
            startRegistration()
        }

        editTextName.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){

                mDisplayName = v.text.toString() //need to turn this into a circulo-ABC123 type id
                startRegistration()
                true
            } else {
                false
            }
        }
        return baseView
    }

    private fun startRegistration() {
        val passwd = generatePassword()
        //store generated password here in case a user wants to change it to something they know later
        val sharedPref = activity?.getSharedPreferences("tmpprefs",Context.MODE_PRIVATE)
        if (sharedPref != null) {
            var editor = sharedPref.edit()
            editor.putString("tmppass", passwd).apply()
            editor.putString("displayname",mDisplayName).apply()
            editor.commit()
        }

        register(generateUsername(mDisplayName), passwd, mDisplayName, Config.HOME_SERVER, Config.HOME_SERVER)
        showNavigationViewIfNeeded()
    }

    private val chars = ('a'..'Z') + ('A'..'Z') + ('0'..'9')

    private fun generatePassword(): String = List(16) { chars.random() }.joinToString("")

    private fun generateUsername(displayname: String) : String {

        return "circulo" + UUID.randomUUID().toString().split("-")[0]
    }

    private fun register(username: String, password: String, display: String, homeServer: String, idServer: String?)
    {
        coroutineScope.launch {
            app?.signOut()

            try {
                val loginFlow = getLoginFlow(homeServer, idServer)

                val registration = mRegistrationWizard?.getRegistrationFlow()
                    ?: throw RuntimeException(getString(info.guardianproject.keanuapp.R.string.general_error, -667))

                mOnboardingAccount = OnboardingAccount(username, loginFlow.homeServerUrl, password, true)

                lifecycleScope.launch { onSuccess(registration) }
            }
            catch (failure: Throwable) {
                lifecycleScope.launch { showError(failure) }
            }
        }
    }

    private suspend fun getLoginFlow(homeServer: String, idServer: String?): LoginFlowResult {
        var hServer = Uri.parse(homeServer)

        if (hServer.scheme?.toLowerCase(Locale.ROOT)?.startsWith("http") != true) {
            hServer = hServer.buildUpon().scheme("https").build()
        }

        val builder = HomeServerConnectionConfig.Builder()

        builder.withHomeServerUri(hServer)

        if (idServer != null) {
            var iServer = Uri.parse(idServer)

            if (iServer.scheme?.toLowerCase(Locale.ROOT)?.startsWith("http") != true) {
                iServer = iServer.buildUpon().scheme("https").build()
            }

            builder.withIdentityServerUri(iServer)
        }

        val loginFlow = try {
            mAuthService?.getLoginFlow(builder.build())
        }
        catch (failure: Throwable) {
            null
        }

        if (loginFlow is LoginFlowResult) {
            if (!loginFlow.isLoginAndRegistrationSupported) {
                throw RuntimeException(getString(info.guardianproject.keanuapp.R.string.not_implemented))
            }

            return loginFlow
        }
        else {
            throw RuntimeException(getString(info.guardianproject.keanuapp.R.string.general_error, -666))
        }
    }

    /**
     * Registration requests callback.
     */
    fun onSuccess(data: RegistrationResult) {

        if (data is RegistrationResult.Success) {
            app?.matrixSession = data.session

            val password = mOnboardingAccount?.password

            if (password != null) {
                // Opportunistically try to enable cross signing, if not done, yet.
                // Fail silently, if not possible here.
                // User will be asked to enable cross-signing on device verification, otherwise.
                VerificationManager.instance.enableCrossSigning(password)
            }

            coroutineScope.launch {
                app?.matrixSession?.profileService()?.setDisplayName(app?.matrixSession?.myUserId.toString(),mDisplayName)
                val isDefaultAvatarUploaded = uploadDefaultAvatarIfNeeded()
                if (!isDefaultAvatarUploaded) {
                    updateAvatar(customAvatarBitmap)
                }

                val intent = Intent()
                intent.putExtra(AuthenticationActivity.EXTRA_ONBOARDING_ACCOUNT, mOnboardingAccount)
                activity?.setResult(RESULT_OK, intent)

                activity?.startActivity(app?.router?.main(requireActivity()))

                activity?.finish()
            }


            return
        }

        if (data is RegistrationResult.FlowResponse) {
            val mandatory = data.flowResult.missingStages.filter { it.mandatory }
            val stage = mandatory.firstOrNull()

            // Not success, but no mandatory stages left to fulfill?
            // Time to finally create the account!
            if (stage == null
                // Workaround: Servers with only a mandatory dummy stage want the username/password set
                // on that dummy call. The SDK can't do that, so we create the account first,
                // and let the dummy stage get fulfilled in the next round. That works.
                || (stage is Stage.Dummy && mandatory.size == 1 && mRegistrationWizard?.isRegistrationStarted() == false)
            ) {
                val username = mOnboardingAccount?.username ?: return
                val password = mOnboardingAccount?.password ?: return

                coroutineScope.launch {
                    try {

                        val registrationAccount = mRegistrationWizard?.createAccount(username, password, mDeviceName)
                            ?: throw RuntimeException(getString(info.guardianproject.keanuapp.R.string.general_error, -667))

                        val registration = mRegistrationWizard?.dummy()
                            ?: throw RuntimeException(getString(info.guardianproject.keanuapp.R.string.general_error, -667))


                        lifecycleScope.launch { onSuccess(registration) }
                    }
                    catch (failure: Throwable) {
                        lifecycleScope.launch { showError(failure) }
                    }
                }

                return
            }

            when (stage) {
                is Stage.ReCaptcha -> {
                    mResolveCaptcha.launch(app?.router?.recaptcha(requireActivity(), stage.publicKey, mOnboardingAccount?.server))
                }

                is Stage.Email -> {
                    // We're not currently supporting this, yet.
                    showError(Throwable(getString(info.guardianproject.keanuapp.R.string.internal_server_error)))
                }
                is Stage.Msisdn -> {
                    // We're not currently supporting this, yet.
                    showError(Throwable(getString(info.guardianproject.keanuapp.R.string.internal_server_error)))
                }

                is Stage.Dummy -> {
                    coroutineScope.launch {
                        try {
                            val registration = mRegistrationWizard?.dummy()
                                ?: throw RuntimeException(getString(info.guardianproject.keanuapp.R.string.general_error, -667))

                            lifecycleScope.launch { onSuccess(registration) }
                        }
                        catch (failure: Throwable) {
                            lifecycleScope.launch { showError(failure) }
                        }
                    }
                }

                is Stage.Terms -> {
                    mAcceptTerms.launch(app?.router?.terms(requireActivity(),
                        TermsActivityArgument(stage.policies.toLocalizedLoginTerms(Locale.getDefault().language))
                    ))
                }

                is Stage.Other -> {
                    // No idea, what we have to do here. Must be a future server with
                    // additional requirements this SDK version cannot fulfill.
                    showError(Throwable(getString(info.guardianproject.keanuapp.R.string.internal_server_error)))
                }
            }
        }
    }

    private val mResolveCaptcha = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        val response = it.data?.getStringExtra(RecaptchaActivity.EXTRA_RESPONSE)

        if (it.resultCode == RESULT_OK && response != null) {
            coroutineScope.launch {
                try {
                    val registration = mRegistrationWizard?.performReCaptcha(response)
                        ?: throw RuntimeException(getString(info.guardianproject.keanuapp.R.string.general_error, -667))

                    lifecycleScope.launch { onSuccess(registration) }
                }
                catch (failure: Throwable) {
                    lifecycleScope.launch { showError(failure) }
                }
            }
        }
        else {
            // User abort.
            mOnboardingAccount = null

          //  mBinding.btSignIn.isEnabled = true
           // mBinding.progress.visibility = View.GONE
        }
    }

    private val mAcceptTerms = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == RESULT_OK) {
            coroutineScope.launch {
                try {
                    val registration = mRegistrationWizard?.acceptTerms()
                        ?: throw RuntimeException(getString(info.guardianproject.keanuapp.R.string.general_error, -667))

                    lifecycleScope.launch { onSuccess(registration) }
                }
                catch (failure: Throwable) {
                    lifecycleScope.launch { showError(failure) }
                }
            }
        }
        else {
            // User abort.
            mOnboardingAccount = null

           // mBinding.btSignIn.isEnabled = true
           // mBinding.progress.visibility = View.GONE
        }
    }

    private fun showError(throwable: Throwable) {
//        Timber.e(throwable)

        Log.e("onboarding","error in onboarding", throwable)

        if (activity != null) {
            Toast.makeText(activity,throwable.betterMessage,Toast.LENGTH_LONG).show()
        }

        /**
        mBinding.tvError.text = throwable.betterMessage
        mBinding.tvError.visibility = View.VISIBLE

        mBinding.btSignIn.isEnabled = true
        mBinding.progress.visibility = View.GONE
        **/
    }

    private fun showNavigationViewIfNeeded() {
        if (activity is MainActivity) {
            (activity as MainActivity).showNavigationView()
        }
    }
}