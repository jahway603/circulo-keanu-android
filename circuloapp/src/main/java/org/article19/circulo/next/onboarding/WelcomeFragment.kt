package org.article19.circulo.next.onboarding

import android.app.Activity.RESULT_OK
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import info.guardianproject.keanu.core.ui.auth.AuthenticationActivity
import info.guardianproject.keanu.core.ui.onboarding.OnboardingAccount
import info.guardianproject.keanu.core.util.extensions.getParcelableExtraCompat
import org.article19.circulo.next.CirculoRouter
import org.article19.circulo.next.databinding.FragmentWelcomeBinding

class WelcomeFragment: Fragment() {

    private lateinit var mBinding: FragmentWelcomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        mBinding = FragmentWelcomeBinding.inflate(layoutInflater, container, false)

        mBinding.btnGetStarted.setOnClickListener {
            (activity as LauncherActivity).openCreateProfileView()
        }

        mBinding.btnLogin.setOnClickListener {
            mRequestSignInUp.launch(activity?.let { it1 -> CirculoRouter.instance.authentication(it1) })

        }

        return mBinding.root
    }

    private var mRequestSignInUp = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode != RESULT_OK) return@registerForActivityResult

        result.data?.getParcelableExtraCompat(AuthenticationActivity.EXTRA_ONBOARDING_ACCOUNT, OnboardingAccount::class.java)?.let {
            showMainScreen(it.isNew)
        }
    }

    private fun showMainScreen(isNewAccount: Boolean) {
        startActivity(CirculoRouter.instance.main(requireActivity(), isFirstTime = isNewAccount))

        requireActivity().finish()
    }
}