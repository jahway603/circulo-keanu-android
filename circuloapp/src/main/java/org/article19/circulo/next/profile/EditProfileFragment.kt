package org.article19.circulo.next.profile

import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import info.guardianproject.keanu.core.util.GlideUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.R
import org.article19.circulo.next.common.utils.KeyboardUtils
import org.article19.circulo.next.main.MainActivity
import org.article19.circulo.next.main.ProfileFragment
import org.article19.circulo.next.main.ProfileFragment.Companion.BUNDLE_KEY_IS_AVATAR_CHANGE
import org.article19.circulo.next.main.ProfileFragment.Companion.REQUEST_KEY_PROFILE_CHANGE
import org.matrix.android.sdk.api.session.content.ContentUrlResolver

class EditProfileFragment: BaseProfileFragment() {

    private var mFragmentView: View? = null
    private lateinit var mAvatarUrl: String
    private var mIsDisplayNameChanged = false
    private var mCurrentDisplayName: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mFragmentView = super.onCreateView(inflater, container, savedInstanceState)
        binding.toolbar.tvBackText.setOnClickListener {
            activity?.let {
                if (activity is MainActivity) {
                    (activity as MainActivity).showProfileFragment(false)
                }
            }
            showNavigationViewIfNeeded()
        }

        binding.toolbar.tvAction.visibility = View.VISIBLE
        binding.toolbar.tvAction.setOnClickListener {

            //Updating new user display name and avatar
            coroutineScope.launch {
                val newDisplayName = editTextName.text.toString()
                withContext(Dispatchers.Main) {
                    binding.toolbar.tvAction.text = getString(R.string.updating)
                }

                if (newDisplayName != mCurrentDisplayName) {
                    mIsDisplayNameChanged = true
                    app?.matrixSession?.profileService()?.setDisplayName(app?.matrixSession?.myUserId.toString(), newDisplayName)
                }

                if (TextUtils.isEmpty(mAvatarUrl)) {
                    uploadDefaultAvatarIfNeeded()
                }

                withContext(Dispatchers.Main) {
                    setFragmentResult(REQUEST_KEY_PROFILE_CHANGE, bundleOf(BUNDLE_KEY_IS_AVATAR_CHANGE to (isAvatarChanged || mIsDisplayNameChanged)))
                    activity?.let {
                        if (activity is MainActivity) {
                            (activity as MainActivity).showProfileFragment(true)
                        }
                    }
                    showNavigationViewIfNeeded()
                }
            }
        }

        mCurrentDisplayName = arguments?.getString(ProfileFragment.BUNDLE_EXTRA_DISPLAY_NAME) ?: ""
        editTextName.setText(mCurrentDisplayName)
        mAvatarUrl = arguments?.getString(ProfileFragment.BUNDLE_EXTRA_AVATAR_URL) ?: ""

        app?.matrixSession?.contentUrlResolver()?.resolveThumbnail(mAvatarUrl, 512, 512, ContentUrlResolver.ThumbnailMethod.SCALE)
            ?.let {
                GlideUtils.loadImageFromUri(activity, Uri.parse(it), ivAvatar, true)
            }

        ivSelectDefaultAvatar.setOnClickListener {
            if (!TextUtils.isEmpty(mAvatarUrl)) {
                showClearAvatarWarningDialog()
            } else {
                selectDefaultAvatar()
            }
        }

        return mFragmentView
    }

    private fun showClearAvatarWarningDialog() {
        val alertDialogBuilder = AlertDialog.Builder(context)

        alertDialogBuilder
            .setMessage(getString(R.string.msg_clear_current_avatar_warning))
            .setPositiveButton(android.R.string.ok) { dialog, which ->
                mAvatarUrl = ""
                selectDefaultAvatar()
            }
            .setNegativeButton(
                android.R.string.cancel) { dialog, which -> }

        alertDialogBuilder.create().show()
    }

    private fun showNavigationViewIfNeeded() {
        if (activity is MainActivity) {
            (activity as MainActivity).showNavigationView()
            KeyboardUtils.hideKeyboard(activity as MainActivity)
        }
    }
}