package org.article19.circulo.next.repository.push.remote

import android.content.pm.PackageManager
import android.os.Build
import androidx.core.os.ConfigurationCompat
import info.guardianproject.keanuapp.R
import org.article19.circulo.next.CirculoApp
import org.matrix.android.sdk.api.session.pushers.HttpPusher
import javax.inject.Inject

class PushRemoteRepositoryImpl @Inject constructor() : PushRemoteRepository {
    override suspend fun registerPushToken(token: String) {
        val appContext = CirculoApp.getInstance()
        val session = appContext.matrixSession ?: return
        val deviceId = session.sessionParams.deviceId ?: return
        val locale = ConfigurationCompat.getLocales(appContext.resources.configuration)[0] ?: return

        val pm = appContext.packageManager

        val appInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            pm.getApplicationInfo(appContext.packageName, PackageManager.ApplicationInfoFlags.of(0))
        } else {
            @Suppress("DEPRECATION")
            pm.getApplicationInfo(appContext.packageName, 0)
        }

        val pusher = HttpPusher(
            pushkey = token,
            appId = appContext.packageName,
            profileTag = "mobile_${session.myUserId.hashCode()}",
            lang = locale.language,
            appDisplayName = pm.getApplicationLabel(appInfo).toString(),
            deviceDisplayName = deviceId,
            url = "https://${appContext.getString(R.string.default_server)}/_matrix/push/v1/notify",
            enabled = true,
            deviceId = deviceId,
            append = false,
            withEventIdOnly = true
        )

        session.pushersService().enqueueAddHttpPusher(pusher)
    }
}