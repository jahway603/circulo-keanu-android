package org.article19.circulo.next.settings

object NotificationType {

    const val NOTIFICATION_NEW_STATUS_ANYTIME = 0
    const val NOTIFICATION_NEW_STATUS_URGENT = 1

    const val NOTIFICATION_RESPONSE_ANYTIME = 0
    const val NOTIFICATION_RESPONSE_MENTIONED = 1
    const val NOTIFICATION_RESPONSE_NEVER = 2
}