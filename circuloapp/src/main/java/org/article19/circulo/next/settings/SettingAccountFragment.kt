package org.article19.circulo.next.settings

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maxkeppeler.sheets.core.SheetStyle
import com.maxkeppeler.sheets.info.InfoSheet
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.FragmentSettingAccountBinding
import org.article19.circulo.next.main.MainActivity
import org.matrix.android.sdk.api.session.Session

open class SettingAccountFragment : SettingBaseFragment() {

    private lateinit var mViewBinding: FragmentSettingAccountBinding

    private val mApp: CirculoApp?
        get() = requireActivity().application as? CirculoApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mViewBinding = FragmentSettingAccountBinding.inflate(inflater, container, false)
        setupNavigation(mViewBinding.toolbar.tvBackText, mViewBinding.toolbar.tvTitle,
            getString(R.string.account),
            savedInstanceState
        )

        mViewBinding.tvStateUsername.text = mSession?.myUserId

        mViewBinding.layoutChangePassword.setOnClickListener {

            showChangePassword()
        }

        mViewBinding.layoutAccountLogout.setOnClickListener {
            showLogoutPrompt ()
        }


        return mViewBinding.root
    }

    private fun showChangePassword () {

        val sharedPref = activity?.getSharedPreferences("tmpprefs",Context.MODE_PRIVATE)
        val tmpPass = sharedPref?.getString("tmppass","")

        PasswordDialogFragment(
            PasswordDialogFragment.Style.NEW_PASSWORD,
            title = getString(R.string.change_password),
            okLabel = getString(info.guardianproject.keanuapp.R.string.ok),
            cachedOldPassword = tmpPass
        ) { pdf ->
            val oldPassword = pdf.oldPassword ?: return@PasswordDialogFragment
            val newPassword = pdf.password ?: return@PasswordDialogFragment

            mCoroutineScope.launch {
                mSession?.accountService()?.changePassword(oldPassword, newPassword)
                sharedPref?.edit()?.remove("tmppass")?.apply()
            }
        }.show(requireActivity().supportFragmentManager, "PASSWORD")
    }

    private fun showLogoutPrompt () {

        InfoSheet().show(requireActivity()) {
            title(R.string.are_you_sure)
            content(R.string.logout_warning)
            style(SheetStyle.BOTTOM_SHEET)
            displayCloseButton()
            onNegative (R.string.cancel){ }
            onPositive (R.string.yes) {
                doLogout ()
            }
            onClose { }

        }

    }

    private fun doLogout () {

        mCoroutineScope.launch {
            mApp?.signOut()
            mApp?.matrix?.rawService()?.clearCache()
            (activity as MainActivity).stopApp()
            activity?.finish()
        }

    }
}