package org.article19.circulo.next.settings

import android.app.Activity
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanu.core.util.extensions.getParcelableExtraCompat
import org.article19.circulo.next.R
import org.article19.circulo.next.common.providers.PreferenceProvider
import org.article19.circulo.next.common.ui.SwitchButton
import org.article19.circulo.next.databinding.FragmentSettingNotificationsBinding
import org.article19.circulo.next.main.ProfileFragment
import org.article19.circulo.next.main.listeners.OnNotificationChanged


open class SettingNotificationsFragment constructor(): SettingBaseFragment() {

    private var mNotificationChangeCallback: OnNotificationChanged? = null
    private var mNotificationsEnabled: Boolean = false
    private var mSelectingUrgentRingtone = false

    private lateinit var mBinding: FragmentSettingNotificationsBinding

    private lateinit var mLaunchRingtonePickerIntent: ActivityResultLauncher<Intent>
    constructor(onNotificationChangeCallback: OnNotificationChanged) : this() {
        mNotificationChangeCallback = onNotificationChangeCallback
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mLaunchRingtonePickerIntent = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val uri = it.data?.getParcelableExtraCompat(RingtoneManager.EXTRA_RINGTONE_PICKED_URI, Uri::class.java)
                uri?.let {
                    val ringtoneName = getRingtoneNameFromUri(uri)

                    if (mSelectingUrgentRingtone) {
                        mBinding.tvStateUrgentRingtone.text = ringtoneName
                        PreferenceProvider.notificationRingtoneUrgent = uri
                    } else {
                        mBinding.tvStateRingtone.text = ringtoneName
                        Preferences.setNotificationRingtoneUri(uri.toString())
                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding = FragmentSettingNotificationsBinding.inflate(inflater, container, false)

        setupNavigation(mBinding.toolbar.tvBackText, mBinding.toolbar.tvTitle,
            getString(R.string.notifications), savedInstanceState)

        mNotificationsEnabled = arguments?.getBoolean(ProfileFragment.BUNDLE_EXTRA_NOTIFICATION_ENABLED) ?: false

        mBinding.sbNotification.isChecked = mNotificationsEnabled
        mBinding.sbNotification.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                mNotificationChangeCallback?.onChanged(isChecked)
                info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider(requireActivity().applicationContext)
                    .setNotificationEnable(isChecked)

                if (isChecked) {
                    mBinding.layoutDetailSettings.visibility = View.VISIBLE
                } else {
                    mBinding.layoutDetailSettings.visibility = View.GONE
                    resetToDefaultSettings()
                }
            }
        })

        initNotificationTypeSettings()
        initNotificationStyleSettings()

        return mBinding.root
    }

    private fun resetToDefaultSettings () {
        PreferenceProvider.notificationNewStatus = NotificationType.NOTIFICATION_NEW_STATUS_ANYTIME
        PreferenceProvider.notificationResponse = NotificationType.NOTIFICATION_RESPONSE_ANYTIME
    }

    private fun initNotificationStyleSettings() {
        mBinding.sbCustomVibrate.isChecked = Preferences.getNotificationVibrate()
        mBinding.sbCustomVibrate.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                Preferences.setNotificationVibrate(isChecked)
            }
        })
        mBinding.sbCustomSound.isChecked = Preferences.getNotificationSound()
        mBinding.sbCustomSound.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                Preferences.setNotificationSound(isChecked)
            }
        })

        mBinding.tvStateRingtone.text = getRingtoneNameFromUri(Preferences.getNotificationRingtoneUri())
        mBinding.tvStateRingtone.setOnClickListener {
            mSelectingUrgentRingtone = false
            pickRingtone()
        }

        mBinding.sbUrgentVibrate.isChecked = PreferenceProvider.notificationVibrateUrgent
        mBinding.sbUrgentVibrate.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                PreferenceProvider.notificationVibrateUrgent = isChecked
            }
        })
        mBinding.sbUrgentSound.isChecked = PreferenceProvider.notificationSoundUrgent
        mBinding.sbUrgentSound.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                PreferenceProvider.notificationSoundUrgent = isChecked
            }
        })

        mBinding.tvStateUrgentRingtone.text = getRingtoneNameFromUri(PreferenceProvider.notificationRingtoneUrgent)
        mBinding.tvStateUrgentRingtone.setOnClickListener {
            mSelectingUrgentRingtone = true
            pickRingtone()
        }
    }

    private fun pickRingtone() {
        val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, getString(R.string.notification_select_ringtone))
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM)
        mLaunchRingtonePickerIntent.launch(intent)
    }


    private fun initNotificationTypeSettings() {
        //New status setting
        val isNewStatusNotifiedAnytime = PreferenceProvider.notificationNewStatus == NotificationType.NOTIFICATION_NEW_STATUS_ANYTIME
        mBinding.rbNotiNewStatusAnytime.isChecked = isNewStatusNotifiedAnytime
        mBinding.rbNotiNewStatusUrgent.isChecked = !isNewStatusNotifiedAnytime

        mBinding.rbNotiNewStatusAnytime.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                PreferenceProvider.notificationNewStatus = NotificationType.NOTIFICATION_NEW_STATUS_ANYTIME
            }
        }

        mBinding.rbNotiNewStatusUrgent.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                PreferenceProvider.notificationNewStatus = NotificationType.NOTIFICATION_NEW_STATUS_URGENT
            }
        }

        //Updates & Responses setting
        when (PreferenceProvider.notificationResponse) {
            NotificationType.NOTIFICATION_RESPONSE_ANYTIME -> {
                mBinding.rbNotiResponseAnytime.isChecked = true
                mBinding.rbNotiResponseMentioned.isChecked = false
                mBinding.rbNotiResponseNever.isChecked = false
            }
            NotificationType.NOTIFICATION_RESPONSE_MENTIONED -> {
                mBinding.rbNotiResponseAnytime.isChecked = false
                mBinding.rbNotiResponseMentioned.isChecked = true
                mBinding.rbNotiResponseNever.isChecked = false
            }
            else -> {
                mBinding.rbNotiResponseAnytime.isChecked = false
                mBinding.rbNotiResponseMentioned.isChecked = false
                mBinding.rbNotiResponseNever.isChecked = true
            }
        }

        mBinding.rbNotiResponseAnytime.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                PreferenceProvider.notificationResponse = NotificationType.NOTIFICATION_RESPONSE_ANYTIME
            }
        }

        mBinding.rbNotiResponseMentioned.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                PreferenceProvider.notificationResponse = NotificationType.NOTIFICATION_RESPONSE_MENTIONED
            }
        }

        mBinding.rbNotiResponseNever.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                PreferenceProvider.notificationResponse = NotificationType.NOTIFICATION_RESPONSE_NEVER
            }
        }
    }

    private fun getRingtoneNameFromUri(ringtoneUri: Uri): String {
        val ringtone = RingtoneManager.getRingtone(context, ringtoneUri)
        val title = ringtone?.getTitle(context)

        if (title != null) return title

        val path = ringtoneUri.path ?: return ""

        val idx = path.lastIndexOf("/")

        if (idx < 0) return  ""

        return path.substring(idx + 1)
    }
}