package org.article19.circulo.next.settings

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.FragmentActivity
import com.beautycoder.pflockscreen.security.PFSecurityManager
import com.beautycoder.pflockscreen.viewmodels.PFPinCodeViewModel
import `in`.myinnos.library.AppIconNameChanger
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.KeanuConstants
import org.article19.circulo.next.BuildConfig
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.common.providers.PreferenceProvider
import org.article19.circulo.next.common.ui.SwitchButton
import org.article19.circulo.next.databinding.FragmentSettingPhysicalSafetyBinding
import org.article19.circulo.next.main.MainActivity
import org.article19.circulo.next.settings.camouflage.AppIconChooserActivity

open class SettingPhysicalSafetyFragment : SettingBaseFragment() {

    companion object {
        val disguisePackage = listOf(
            "info.guardianproject.keanu.core.RouterActivity",
            "org.article19.circulo.next.main.MainActivitySettings",
            "org.article19.circulo.next.main.MainActivityCamera",
            "org.article19.circulo.next.main.Firefox",
            "org.article19.circulo.next.main.Messenger",
            "org.article19.circulo.next.main.Navigator",
            "org.article19.circulo.next.main.Skype",
            "org.article19.circulo.next.main.Telegram",
            "org.article19.circulo.next.main.WhatsApp",
            "org.article19.circulo.next.main.Youtube"
        )
    }

    private lateinit var mBinding: FragmentSettingPhysicalSafetyBinding

    private val mCreateLockScreenListener =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == FragmentActivity.RESULT_OK) {
                mBinding.layoutChangePin.isEnabled = true
                mBinding.layoutLockApp.isEnabled = true
            }
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        mBinding = FragmentSettingPhysicalSafetyBinding.inflate(inflater, container, false)

        setupNavigation(mBinding.toolbar.tvBackText, mBinding.toolbar.tvTitle,
            getString(R.string.physical_safety),
            savedInstanceState
        )

        PFPinCodeViewModel().isPinCodeEncryptionKeyExist.observe(
            activity as MainActivity,
            androidx.lifecycle.Observer { result ->
                if (result == null) {
                    return@Observer
                }
                if (result.error != null) {
                    Toast.makeText(
                        activity,
                        info.guardianproject.keanuapp.R.string.lock_screen_passphrases_not_matching,
                        Toast.LENGTH_SHORT
                    ).show()
                    return@Observer
                }

                mBinding.layoutChangePin.isEnabled = result.result
                mBinding.layoutLockApp.isEnabled = result.result
                mBinding.swPinLock.isChecked = result.result
            }
        )

        mBinding.layoutChangePin.setOnClickListener {
            (activity?.application as? ImApp)?.router?.lockScreen(requireActivity())
                ?.let { intent -> startActivity(intent) }
        }

        mBinding.layoutLockApp.setOnClickListener {
            (activity as MainActivity).showLockScreenFragment(mBinding.swPinLock.isChecked)
        }

        mBinding.swPinLock.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                if (isChecked) {
                    showCreateLockScreen()
                }
                else {
                    disableLock()
                }
            }
        })

        val enableDisguise = PreferenceProvider.enableDisguise
        mBinding.swCustomAppIcon.isChecked = enableDisguise
        enableLayoutChangeIcon(enableDisguise)

        mBinding.swCustomAppIcon.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                mBinding.layoutChangeIcon.isEnabled = isChecked
                enableLayoutChangeIcon(isChecked)
                PreferenceProvider.enableDisguise = isChecked

                //If Use custom app icon switch is disabled, then reset name & icon to default.
                if (!isChecked) {
                    val activePackage = disguisePackage[0]
                    val disablePackages: MutableList<String> = ArrayList()

                    for (i in 1 until disguisePackage.size) {
                        disablePackages.add(disguisePackage[i])
                    }

                    setAppIconName(activePackage, disablePackages)

                    PreferenceProvider.disguiseId = 0
                }
            }
        })

        mBinding.layoutChangeIcon.setOnClickListener {
            startActivity(Intent(context, AppIconChooserActivity::class.java))
        }

        return mBinding.root
    }

    private fun enableLayoutChangeIcon(enable: Boolean) {
        mBinding.layoutChangeIcon.isEnabled = enable
        if (!enable) {
            mBinding.layoutChangeIcon.alpha = 0.5f
        } else {
            mBinding.layoutChangeIcon.alpha = 1f
        }
    }

    private fun setAppIconName(activePackage: String, disablePackages: MutableList<String>) {
        AppIconNameChanger.Builder(activity)
            .activeName(activePackage) // String
            .disableNames(disablePackages) // List<String>
            .packageName(BuildConfig.APPLICATION_ID)
            .build()
            .setNow()
    }

    private fun showCreateLockScreen() {
        val context = context ?: return

        mCreateLockScreenListener.launch(CirculoApp.getInstance().router.lockScreen(context, changePassphrase = false))
    }

    private fun disableLock() {
        PFSecurityManager.getInstance().pinCodeHelper.delete { result ->
            if (result == null || result.error != null) {
                Toast.makeText(activity, R.string.lock_screen_disable_error, Toast.LENGTH_LONG).show()
            }
            else {
                PreferenceProvider.appPreferences.edit()
                    .remove(KeanuConstants.PREFERENCE_KEY_ENCODED_PASS)
                    .apply()

                mBinding.layoutChangePin.isEnabled = false
                mBinding.layoutLockApp.isEnabled = false
            }
        }
    }
}
