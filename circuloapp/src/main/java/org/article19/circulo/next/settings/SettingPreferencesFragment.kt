package org.article19.circulo.next.settings

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.Preferences
import org.article19.circulo.next.R
import org.article19.circulo.next.common.ui.SwitchButton
import org.article19.circulo.next.databinding.FragmentSettingPreferencesBinding
import java.util.*

/**
 * https://medium.com/@hectorricardomendez/how-to-get-the-current-locale-in-android-fc12d8be6242
 */
open class SettingPreferencesFragment : SettingBaseFragment() {

    private val supportedLocales = arrayOf("en", "de", "es", "fa", "fi", "fr", "it", "nb_NO", "pt_BR")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentSettingPreferencesBinding.inflate(inflater, container, false)

        setupNavigation(binding.toolbar.tvBackText, binding.toolbar.tvTitle,
            getString(R.string.preferences), savedInstanceState)

        val locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            resources.configuration.locales.get(0)
        } else {
            @Suppress("DEPRECATION")
            resources.configuration.locale
        }

        binding.tvStateLanguage.text = locale.getDisplayLanguage(locale)
        binding.layoutLanguage.setOnClickListener {
            val activity = activity ?: return@setOnClickListener

            val locales = supportedLocales.map {
                val code = it.split("_")

                if (code.size > 1) Locale(code[0], code[1]) else  Locale(it)
            }.sortedBy { it.getDisplayLanguage(it) }

            val languagesAdapter = ArrayAdapter(activity,
                android.R.layout.simple_list_item_single_choice,
                locales.map { "${it.getDisplayLanguage(it)} (${it.getDisplayLanguage(Locale.US)})" })

            val checked = locales.indexOf(locale)

            AlertDialog.Builder(activity)
                .setIcon(info.guardianproject.keanuapp.R.drawable.ic_settings_language)
                .setTitle(info.guardianproject.keanuapp.R.string.KEY_PREF_LANGUAGE_TITLE)
                .setSingleChoiceItems(languagesAdapter, checked) { dialog: DialogInterface, position: Int ->

                    dialog.dismiss()

                    ImApp.resetLanguage(activity, locales[position].language)
                }
                .show()
        }

        binding.sbStartAutomatically.isChecked = Preferences.startOnBoot()
        binding.sbStartAutomatically.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                Preferences.setStartOnBoot(isChecked)
            }
        })

        return binding.root
    }
}