package org.article19.circulo.next.settings.camouflage

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import `in`.myinnos.library.AppIconNameChanger
import org.article19.circulo.next.BuildConfig
import org.article19.circulo.next.R
import org.article19.circulo.next.common.providers.PreferenceProvider
import org.article19.circulo.next.databinding.RowItemAppIconListBinding

class AppIconChooserAdapter(private var activity: Activity, private var appIconList: List<AppIconChooserModel>) :
    RecyclerView.Adapter<AppIconChooserAdapter.ViewHolder>() {

    private lateinit var mContext: Context

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val itemBinding = RowItemAppIconListBinding.inflate(LayoutInflater.from(mContext), parent, false)

        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentAppIcon = appIconList[position]
        holder.tvName.text = mContext.getString(currentAppIcon.appIconNameRes)
        holder.ivIcon.setImageResource(currentAppIcon.appIconRes)

        holder.rootLayout.setOnClickListener {
            changeAppIconName(position)
        }

        if (position == PreferenceProvider.disguiseId) {
            holder.rootLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.background_mention))
        } else {
            holder.rootLayout.setBackgroundColor(0)
        }
    }

    private fun changeAppIconName(selectedPos: Int) {
        PreferenceProvider.disguiseId = selectedPos
        val activeName: String = appIconList[selectedPos].packageName
        val disableNames: MutableList<String> = ArrayList()

        for (i in appIconList.indices) {
            if (i != selectedPos) {
                disableNames.add(appIconList[i].packageName)
            }
        }

        setAppIconName(activeName, disableNames)
    }

    private fun setAppIconName(activePackage: String, disablePackages: MutableList<String>) {
        AppIconNameChanger.Builder(activity)
            .activeName(activePackage) // String
            .disableNames(disablePackages) // List<String>
            .packageName(BuildConfig.APPLICATION_ID)
            .build()
            .setNow()
    }

    override fun getItemCount(): Int {
        return appIconList.size
    }

    class ViewHolder(itemBinding: RowItemAppIconListBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        val rootLayout = itemBinding.root
        val tvName = itemBinding.tvAppName
        val ivIcon = itemBinding.ivAppIcon
    }
}