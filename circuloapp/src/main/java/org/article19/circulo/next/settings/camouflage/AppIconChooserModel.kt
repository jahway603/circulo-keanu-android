package org.article19.circulo.next.settings.camouflage

data class AppIconChooserModel(val packageName: String, val appIconRes: Int, val appIconNameRes: Int)
